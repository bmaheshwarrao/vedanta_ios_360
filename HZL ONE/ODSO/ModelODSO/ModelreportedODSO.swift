//
//  ModelreportedODSO.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class ReportedDataODSOModel: NSObject {
    
    
    var ID = Int()
    var Observation_ID = String()
    var Location_ID = String()
    var Location_Name = String()
    var Unit_ID = String()
    var Unit_Name = String()
    var Area_ID = String()
    var Area_Name = String()
    var From_Time = String()
    
    
    var To_Time = String()
    var Observation_detail_ID = String()
    var Observation = String()
    var Suggestion = String()
    var Before_Pic = String()
    var Observation_Status = String()
    var Responsible_Dept_ID = String()
    var Responsible_Dept_Name = String()
    var Observation_DateTime = String()
    
    var Status = String()
    
    

    
    
    func setDataInModel(str:[String:AnyObject])
    {
        
        if str["Status"] is NSNull || str["Status"] == nil{
            self.Status = ""
        }else{
            
            let ros = str["Status"]
            
            self.Status = (ros?.description)!
            
            
        }
        
        if str["Observation_ID"] is NSNull || str["Observation_ID"] == nil{
            self.Observation_ID = ""
        }else{
            
            let ros = str["Observation_ID"]
            
            self.Observation_ID = (ros?.description)!
            
            
        }
        if str["Location_ID"] is NSNull || str["Location_ID"] == nil{
            self.Location_ID =  ""
        }else{
            
            let fross = str["Location_ID"]
            
            self.Location_ID = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Location_Name"] is NSNull || str["Location_Name"] == nil{
            self.Location_Name = "0"
        }else{
            let emp1 = str["Location_Name"]
            
            self.Location_Name = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Unit_ID"] is NSNull || str["Unit_ID"] == nil{
            self.Unit_ID = ""
        }else{
            
            let empname = str["Unit_ID"]
            
            self.Unit_ID = (empname?.description)!
            
        }
        
        if str["Unit_Name"] is NSNull || str["Unit_Name"] == nil{
            self.Unit_Name = ""
        }else{
            let emp2 = str["Unit_Name"]
            print((emp2?.description)!)
            self.Unit_Name = (emp2?.description)!
            
            
            
        }
        
        
        if str["Area_ID"] is NSNull || str["Area_ID"] == nil{
            self.Area_ID = ""
        }else{
            let empname2 = str["Area_ID"]
            
            self.Area_ID = (empname2?.description)!
            
        }
        
        
        if str["Area_Name"] is NSNull || str["Area_Name"] == nil{
            self.Area_Name = ""
        }else{
            let locid = str["Area_Name"]
            
            self.Area_Name = (locid?.description)!
            
        }
        if str["From_Time"] is NSNull || str["From_Time"] == nil{
            self.From_Time = ""
        }else{
            let locname = str["From_Time"]
            
            self.From_Time = (locname?.description)!
            
        }
        
        
        
        
        
        if str["To_Time"] is NSNull || str["To_Time"] == nil{
            self.To_Time = ""
        }else{
            let locname = str["To_Time"]
            
            self.To_Time = (locname?.description)!
            
        }
        if str["Observation_detail_ID"] is NSNull || str["Observation_detail_ID"] == nil{
            self.Observation_detail_ID = ""
        }else{
            let locname = str["Observation_detail_ID"]
            
            self.Observation_detail_ID = (locname?.description)!
            
        }
        if str["Observation"] is NSNull || str["Observation"] == nil{
            self.Observation = ""
        }else{
            let locname = str["Observation"]
            
            self.Observation = (locname?.description)!
            
        }
        if str["Suggestion"] is NSNull || str["Suggestion"] == nil{
            self.Suggestion = ""
        }else{
            let locname = str["Suggestion"]
            
            self.Suggestion = (locname?.description)!
            
        }
        if str["Before_Pic"] is NSNull || str["Before_Pic"] == nil{
            self.Before_Pic = ""
        }else{
            let locname = str["Before_Pic"]
            
            self.Before_Pic = (locname?.description)!
            
        }
        if str["Observation_Status"] is NSNull || str["Observation_Status"] == nil{
            self.Observation_Status = ""
        }else{
            let locname = str["Observation_Status"]
            
            self.Observation_Status = (locname?.description)!
            
        }
        
        if str["Responsible_Dept_ID"] is NSNull || str["Responsible_Dept_ID"] == nil{
            self.Responsible_Dept_ID = ""
        }else{
            let locname = str["Responsible_Dept_ID"]
            
            self.Responsible_Dept_ID = (locname?.description)!
            
        }
        if str["Responsible_Dept_Name"] is NSNull || str["Responsible_Dept_Name"] == nil{
            self.Responsible_Dept_Name = ""
        }else{
            let locname = str["Responsible_Dept_Name"]
            
            self.Responsible_Dept_Name = (locname?.description)!
            
        }
        if str["Observation_DateTime"] is NSNull || str["Observation_DateTime"] == nil{
            self.Observation_DateTime = ""
        }else{
            let locname = str["Observation_DateTime"]
            
            self.Observation_DateTime = (locname?.description)!
            
        }
        
    }
    
}





class ReportedDataODSOAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:ODSOReportedDataViewController, param : [String: String] , status: String, statusData: String , url: String, success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: url, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    print(response)
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ReportedDataODSOModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ReportedDataODSOModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class ReportedDataODSOAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:ODSOReportedDataViewController, param : [String: String], url : String , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: url , parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ReportedDataODSOModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ReportedDataODSOModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}




