//
//  WorkPermitTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class WorkPermitTableViewCell: UITableViewCell {
    

    
    
     @IBOutlet weak var heightViewofClose: NSLayoutConstraint!
    @IBOutlet weak var heightImageselected: NSLayoutConstraint!
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var lblUnit: UILabel!
    
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var viewArea: UIView!
    
    @IBOutlet weak var textViewJob: UITextView!
    
    @IBOutlet weak var viewPTWClassno: UIView!
    @IBOutlet weak var textPTWClassno: UITextField!
    
    @IBOutlet weak var viewMeasureTaken: UIView!
    @IBOutlet weak var textMeasureTaken: UITextField!
    
    
    @IBOutlet weak var btnPTWIssuerEmpType: UIButton!
    @IBOutlet weak var viewPTWIssuerEmpType: UIView!
    @IBOutlet weak var lblPTWIssuerEmpType: UILabel!
    @IBOutlet weak var viewPTWIssuerEmpId: UIView!
    @IBOutlet weak var textPTWIssuerEmpId: UITextField!
    
    @IBOutlet weak var btnPTWRecEmpType: UIButton!
    @IBOutlet weak var viewPTWRecEmpType: UIView!
    @IBOutlet weak var lblPTWRecEmpType: UILabel!
    @IBOutlet weak var viewPTWRecEmpId: UIView!
    @IBOutlet weak var textPTWRecEmpId: UITextField!
    @IBOutlet weak var viewCorrectiveMeasureTaken: UIView!
    @IBOutlet weak var textCorrectiveMeasureTaken: UITextField!
    @IBOutlet weak var btnCamera: UIButton!
     @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var lblOptional: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        
        textViewJob.layer.borderWidth = 1.0
        textViewJob.layer.borderColor = UIColor.black.cgColor
        textViewJob.layer.cornerRadius = 10.0
        
        viewPTWClassno.layer.borderWidth = 1.0
        viewPTWClassno.layer.borderColor = UIColor.black.cgColor
        viewPTWClassno.layer.cornerRadius = 10.0
        viewMeasureTaken.layer.borderWidth = 1.0
        viewMeasureTaken.layer.borderColor = UIColor.black.cgColor
        viewMeasureTaken.layer.cornerRadius = 10.0
        
        viewPTWIssuerEmpType.layer.borderWidth = 1.0
        viewPTWIssuerEmpType.layer.borderColor = UIColor.black.cgColor
        viewPTWIssuerEmpType.layer.cornerRadius = 10.0
        viewPTWIssuerEmpId.layer.borderWidth = 1.0
        viewPTWIssuerEmpId.layer.borderColor = UIColor.black.cgColor
        viewPTWIssuerEmpId.layer.cornerRadius = 10.0
        
        viewPTWRecEmpType.layer.borderWidth = 1.0
        viewPTWRecEmpType.layer.borderColor = UIColor.black.cgColor
        viewPTWRecEmpType.layer.cornerRadius = 10.0
        
        viewPTWRecEmpId.layer.borderWidth = 1.0
        viewPTWRecEmpId.layer.borderColor = UIColor.black.cgColor
        viewPTWRecEmpId.layer.cornerRadius = 10.0
        
        viewCorrectiveMeasureTaken.layer.borderWidth = 1.0
        viewCorrectiveMeasureTaken.layer.borderColor = UIColor.black.cgColor
        viewCorrectiveMeasureTaken.layer.cornerRadius = 10.0
        textPTWIssuerEmpId.keyboardType = UIKeyboardType.numberPad
        textPTWRecEmpId.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
