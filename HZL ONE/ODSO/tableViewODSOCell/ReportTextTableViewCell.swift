//
//  ReportTextTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ReportTextTableViewCell: UITableViewCell {
    @IBOutlet weak var textViewRemark : UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
