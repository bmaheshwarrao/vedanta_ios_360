//
//  ODSODashboardTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 15/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ODSODashboardTableViewCell: UITableViewCell {

    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var countLeading: NSLayoutConstraint!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblMyInbox: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
