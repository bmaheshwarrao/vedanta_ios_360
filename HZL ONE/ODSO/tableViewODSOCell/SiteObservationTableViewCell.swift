//
//  SiteObservationTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SiteObservationTableViewCell: UITableViewCell {
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightRDept: NSLayoutConstraint!
    
    @IBOutlet weak var heightLblRDept: NSLayoutConstraint!
    
    @IBOutlet weak var heightImageRDept: NSLayoutConstraint!
    
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var viewArea: UIView!
    
    @IBOutlet weak var imgDept: UIImageView!
    @IBOutlet weak var lblOptional: UILabel!
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    @IBOutlet weak var heightImageselected: NSLayoutConstraint!
    @IBOutlet weak var heightViewofClose: NSLayoutConstraint!
  
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
      @IBOutlet weak var lblRDept: UILabel!
    @IBOutlet weak var lblDept: UILabel!
    @IBOutlet weak var viewDept: UIView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var textViewUnsafeObserved: UITextView!
    @IBOutlet weak var textViewCorrective: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        viewDept.layer.borderWidth = 1.0
        viewDept.layer.borderColor = UIColor.black.cgColor
        viewDept.layer.cornerRadius = 10.0
        viewStatus.layer.borderWidth = 1.0
        viewStatus.layer.borderColor = UIColor.black.cgColor
        viewStatus.layer.cornerRadius = 10.0
        
        textViewUnsafeObserved.layer.borderWidth = 1.0
        textViewUnsafeObserved.layer.borderColor = UIColor.black.cgColor
        textViewUnsafeObserved.layer.cornerRadius = 10.0
        
        textViewCorrective.layer.borderWidth = 1.0
        textViewCorrective.layer.borderColor = UIColor.black.cgColor
        textViewCorrective.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
