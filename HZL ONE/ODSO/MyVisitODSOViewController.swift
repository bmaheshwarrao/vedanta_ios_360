//
//  MyVisitODSOViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyVisitODSOViewController:CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var MyVisitDB:[MyVisitDataODSOModel] = []
    var MyVisitAPI = MyVisitDataODSOAPI()
    
    var MyVisitLoadMoreDB : [MyVisitDataODSOModel] = []
    
    
    
    
    var MyVisitLoadMoreAPI = MyVisitDataODSOAPILoadMore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl.addTarget(self, action: #selector(getMyVisitData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
         getMyVisitData()
        self.tabBarController?.tabBar.isHidden = true
        self.title = "My Visit"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMyVisitData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "ODSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "SubmitReportODSOViewController") as! SubmitReportODSOViewController
        ZIVC.MyVisitReportDB = self.MyVisitDB[(indexPath?.section)!]
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.MyVisitDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMyVisit", for: indexPath) as! MyVisitTableViewCell
        
        cell.visitIdLbl.text = "Visit Id #" + String(self.MyVisitDB[indexPath.section].ID)
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        if(self.MyVisitDB[indexPath.section].Rost_Date != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MMM dd yyyy"
            let date : Date = dateFormatter.date(from: self.MyVisitDB[indexPath.section].Rost_Date)!
            dateFormatter.string(from: date)
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            let finalDate = dateFormatter2.string(from: date)
            cell.dateLbl.text = finalDate
        }else{
            cell.dateLbl.text = ""
        }
        cell.nameEmpLbl.text = self.MyVisitDB[indexPath.section].Employee_Name_1
        cell.nameEmp2Lbl.text = self.MyVisitDB[indexPath.section].Employee_Name_2
        cell.zoneLbl.text = self.MyVisitDB[indexPath.section].Location_Name
        
        self.data = String(self.MyVisitDB[indexPath.section].ID)
        self.lastObject = String(self.MyVisitDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.MyVisitDB.count - 1)
        {
            
            // self.getMyVisitDataLoadMore( ID: String(Int(self.MyVisitDB[indexPath.section].ID)))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getMyVisitData(){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]

        MyVisitAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.MyVisitDB = dict as! [MyVisitDataODSOModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getMyVisitDataLoadMore(ID : String){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,"ID":ID]
        MyVisitLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.MyVisitLoadMoreDB =  [MyVisitDataODSOModel]()
            self.MyVisitLoadMoreDB = dict as! [MyVisitDataODSOModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.MyVisitDB.append(contentsOf: self.MyVisitLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
