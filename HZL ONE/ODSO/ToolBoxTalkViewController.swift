//
//  ToolBoxTalkViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Alamofire
class ToolBoxTalkViewController: CommonVSClass, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        // Do any additional setup after loading the view.
    }
      var MyVisitReportDB = MyVisitDataODSOModel()
    let reachablty = Reachability()!
    @IBAction func btnAddObClicked(_ sender: UIButton) {
        
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if ( FilterDataFromServer.unit_Name == String()) {
            
            self.view.makeToast("Please Select Unit")
        }
        else if ( FilterDataFromServer.area_Name == String()) {
            
            self.view.makeToast("Please Select Area")
        }
        else if (cellData.textGiveno.text == "") {
            
            self.view.makeToast("Enter Tool Box talk give no")
        }
        else if (cellData.textJob.text == "") {
            
            self.view.makeToast("Enter Job / Topic discussed")
        }else if (cellData.textWorkers.text == "") {
            
            self.view.makeToast("Enter no of workers")
        }else{
            
            
            
            self.startLoadingPK(view: self.view)
            
           
            
            
        let parameter = [ "Team_ID":String(MyVisitReportDB.ID),
                          "Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
                          "Location_ID":MyVisitReportDB.Location_ID,
                          "Unit_ID": String(FilterDataFromServer.unit_Id),
                          "Area_ID":String(FilterDataFromServer.area_Id) ,
                          "Talk_Given_By":cellData.textGiveno.text!,
                          "Topic":cellData.textJob.text! ,
                          "No_Of_Workers":cellData.textWorkers.text!
                ] as [String:String]
            
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    var res : UIImage = UIImage()
                    
                    let size = CGSize(width: 150, height: 150)
                    res = self.imageResize(image: self.cellData.imageViewPhoto.image!,sizeChange: size)
                    
                    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Add_Add_Tool_Box)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("HZl Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                    self.stopLoadingPK(view: self.view)
                                    MoveStruct.isMove = true
                                    
                                    MoveStruct.message = msg
                                    
                                    
                                    self.navigationController?.popViewController(animated: false)
                                    
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }
        
        
        
    }
    
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            
            
            let storyboard = UIStoryboard(name: "NDSO", bundle: nil)
            let moveVC = storyboard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
            moveVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(moveVC, animated: true)
            
        }
    }
    @IBAction func btnUnitClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Unit"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        updateData()
        let storyboard = UIStoryboard(name: "NDSO", bundle: nil)
        let moveVC = storyboard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
        moveVC.titleStr = "Select Unit"
        self.navigationController?.pushViewController(moveVC, animated: true)
    }
    
    
    @objc func updateData(){
       
        if(FilterDataFromServer.unit_Name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.area_Name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
        
        // tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Tool Box Talk Observed"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if( FilterDataFromServer.unit_Name != "" || FilterDataFromServer.area_Name != ""){
            updateData()
        }
        //updateData()
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func btnImageButtonClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
        
    }
    let imagePicker = UIImagePickerController()
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cellData.imageViewPhoto.image = image
            imagedata = UIImageJPEGRepresentation(image, 1.0)!
            cellData.btnCamera.isHidden = true
            cellData.lblOptional.isHidden = true
            cellData.btnClose.isHidden = false
            cellData.imageViewPhoto.isHidden = false
            updateConst()
            
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // var reachability = Reachability()!
    var imagedata: Data? = nil
    @IBAction func btnCloseButtonClicked(_ sender: UIButton) {
        
        self.imagedata = nil
        self.cellData.btnClose.isHidden = true
        cellData.btnCamera.isHidden = false
        cellData.lblOptional.isHidden = false
        updateConst()
    }
    func updateConst(){
     
            
            if(self.cellData.btnClose.isHidden == true){
                self.cellData.heightViewofClose.constant = 0
                self.cellData.heightImageselected.constant = 0
                self.cellData.heightViewImage.constant = 40
            }else{
                self.cellData.heightViewofClose.constant = 20
                self.cellData.heightImageselected.constant = 100
                self.cellData.heightViewImage.constant = 120
            }
            
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Fusuma
    
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
    var indexing : Int = 1
    var cellData : ToolBoxTalkTableViewCell!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ToolBoxTalkViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 619
        
    }
}
extension ToolBoxTalkViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ToolBoxTalkTableViewCell
        
        cellData = cell
        
        if(indexing == 1){
            cellData.btnClose.isHidden = true
            indexing = indexing + 1
        }
        updateData()
        updateConst()
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
