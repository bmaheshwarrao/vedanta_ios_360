//
//  ODSODashboardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Charts


import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation

class ODSODashboardViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    
    var StrNav = String()
    @IBOutlet weak var homeTableView: UITableView!
    
    
    var applicationName = String()
    
    
    
    
    
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
        
        
        
        
        
    }
    
    
    var odsoDisplay : [ODSODisplay] = []
    let reachability = Reachability()!
    @objc func Reportdata() {
        if(self.reachability.connection != .none) {
            let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
            
            let parameters = ["Employee_ID": pno ]
            
            self.startLoading()
            print(parameters)
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Observations_Details_CountODSO,parameters: parameters, successHandler: { (dict) in
                
                //self.stopLoading()
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        self.stopLoading()
                        
                        self.odsoDisplay = []
                        if let data = dict["data"]
                        {
                            
                            self.refresh.endRefreshing()
                            
                            
                            
                            
                            
                            
                            let obData = ODSODisplay()
                            obData.Pending_to_Assign = String(Int(data["Pending_to_Assign"] as! NSNumber))
                            obData.Assign_to_Me_Open = String(Int(data["Assign_to_Me_Open"] as! NSNumber))
                            
                            obData.Assign_to_Me_Close = String(Int(data["Assign_to_Me_Close"] as! NSNumber))
                            self.odsoDisplay.append(obData)
                            
                            self.homeTableView.reloadData();
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {self.stopLoading()
                        self.refresh.endRefreshing()
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                self.refresh.endRefreshing()
                self.stopLoading()
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
            // self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            self.refresh.endRefreshing()
            self.stopLoading()
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = StrNav
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        Reportdata()
        
    }
    @IBAction func btnMyVisitClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ODSO", bundle: nil)
        let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "MyVisitODSOViewController") as! MyVisitODSOViewController
        
        self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
    }
    
    @IBAction func btnMyInboxClicked(_ sender: UIButton) {
        if(sender.tag == 2){
            UserDefaults.standard.set("Pending to assign", forKey: "Status")
            let storyBoard = UIStoryboard(name: "ODSO", bundle: nil)
            let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "ODSOReportedDataViewController") as! ODSOReportedDataViewController
            
            self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        }else{
            UserDefaults.standard.set("Assigned to me", forKey: "Status")
            let storyBoard = UIStoryboard(name: "ODSO", bundle: nil)
            let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "ODSOReportedMenuViewController") as! ODSOReportedMenuViewController
            
            self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageODSOTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: applicationName)
            tableView.isScrollEnabled = false
            return cell
            
        } else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellVisit") as! UITableViewCell
            
            
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMyInbox") as! ODSODashboardTableViewCell
            cell.lblMyInbox.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
            cell.viewStatus.backgroundColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
            cell.lblStatus.text = "Pending to assign"
            if(odsoDisplay.count > 0){
                cell.lblCount.text = odsoDisplay[0].Pending_to_Assign
            }else{
                cell.lblCount.text = "0"
            }
             cell.btnStatus.tag = indexPath.section
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMyInbox") as! ODSODashboardTableViewCell
            
            cell.lblStatus.text = "Assigned to me"
            cell.countLeading.constant = 50
            if(odsoDisplay.count > 0){
                cell.lblCount.text = odsoDisplay[0].Assign_to_Me_Open
            }else{
                cell.lblCount.text = "0"
            }
        cell.btnStatus.tag = indexPath.section
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
       
        let screenSize = UIScreen.main.bounds
      //  let screenHeight = screenSize.height-navheight
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else{
            return 112
        }
        
    }
    
    
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
