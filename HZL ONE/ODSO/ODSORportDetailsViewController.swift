//
//  ODSORportDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class ODSORportDetailsViewController: CommonVSClass,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    var ReportedDataDB = ReportedDataODSOModel()
    

    @IBOutlet weak var stackView: UIStackView!
   
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        if(status == "Pending to assign"){
            stackView.isHidden = false
            btnClose.isHidden = true
            
        }else{
            stackView.isHidden = true
            btnClose.isHidden = false
            
        }
  
        // Do any additional setup after loading the view.
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = true
            self.navigationController?.popViewController(animated: false)
        }
        
        self.title = "Observation Detail"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        FilterDataFromServer.unit_Id = Int()
        
        
    }
    @IBAction func btnAssignClicked(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ODSO",bundle : nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AssignODSOViewController") as! AssignODSOViewController
        ZIVC.ReportedDataDB = ReportedDataDB
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    @IBAction func btnForwardClicked(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ODSO",bundle : nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ForwardODSOViewController") as! ForwardODSOViewController
        ZIVC.ReportedDataDB = ReportedDataDB
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ODSO",bundle : nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "CloseObservationODSOViewController") as! CloseObservationODSOViewController
        ZIVC.ReportedDataDB = ReportedDataDB
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.0
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "details", for: indexPath) as! NDSOReportDetailsTableViewCell
        
        
        cell.lblDepartment.text = self.ReportedDataDB.Responsible_Dept_Name
        cell.lblArea.text = self.ReportedDataDB.Area_Name
        cell.lblUnit.text = self.ReportedDataDB.Unit_Name
        cell.lblLocation.text = self.ReportedDataDB.Location_Name
        cell.textViewSuggestion.text = self.ReportedDataDB.Suggestion
        cell.textViewObservation.text = self.ReportedDataDB.Observation
        
        cell.lblTime.text = self.ReportedDataDB.From_Time + " to " + self.ReportedDataDB.To_Time
        if(self.ReportedDataDB.Observation_DateTime != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MMM dd yyyy"
            let date : Date = dateFormatter.date(from: self.ReportedDataDB.Observation_DateTime)!
            dateFormatter.string(from: date)
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            let finalDate = dateFormatter2.string(from: date)
            cell.lblDate.text = finalDate
        }else{
            cell.lblDate.text = ""
        }
        
        let urlString = self.ReportedDataDB.Before_Pic
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.ReportedDataDB.Before_Pic) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageViewDetails.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageViewDetails.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 120, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageViewDetails.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        cell.btnStatus.setTitle(self.ReportedDataDB.Status, for: .normal)
  
        let stattusColor = self.ReportedDataDB.Status
        switch stattusColor {
        case "Close":
            cell.btnStatus.backgroundColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
            break;
        case "Open":
            cell.btnStatus.backgroundColor = UIColor.yellow
            break;
            
        default:
            break;
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
