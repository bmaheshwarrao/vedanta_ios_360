//
//  PendingDoneTaskProcessViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PendingDoneTaskProcessViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
 
    var status = String()
    var task_Id = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
     
      
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Process Task"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
  
    var statusSwitch = String()
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if(sender.isOn == true){
            cellData.switchProcess.onTintColor = UIColor.green
            statusSwitch = "Open"
        }else{
            cellData.switchProcess.onTintColor = UIColor.darkGray
            statusSwitch = "Close"
        }
         cellData.lblStatus.text = statusSwitch
    }
    @IBAction func btnProcessClicked(_ sender: UIButton) {
        if(cellData.textViewRemark.text == ""){
            self.view.makeToast("Enter Remark")
        }else{
            self.startLoadingPK(view: self.view)
            
            
            
            
            let parameter = [
                "Meeting_ID":PendingDoneDB.ID,
                "Task_ID":PendingDoneDB.Task_ID,
                "Status":statusSwitch,
                "Task_Desc":cellData.textViewRemark.text! ,
                
                "Created_By": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Update_Task_MOM, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                
                let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var PendingDoneDB = ManageMeetingModel()
   
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var cellData : ProcessTaskMOMTableViewCell!
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 ) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return 1
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyTaskMOMTableViewCell
            
            
            cell.lblOwner.text = "Owner : " + self.PendingDoneDB.Meeting_Owner
            cell.textViewTask.text = "Task : " + self.PendingDoneDB.Task
            cell.textViewDesc.text = self.PendingDoneDB.Meeting_Title
            cell.containerView.layer.cornerRadius = 10
            cell.lblDate.text = "Date : " + self.PendingDoneDB.Meeting_Date
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        }else{
             let cell = tableView.dequeueReusableCell(withIdentifier: "cellProcess", for: indexPath) as! ProcessTaskMOMTableViewCell
            cellData = cell
            cell.switchProcess.isOn = false
             cell.switchProcess.onTintColor = UIColor.darkGray
            statusSwitch = "Close"
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
}
