//
//  ManageMeetingViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import ViewPager_Swift
class ManageMeetingViewController:CommonVSClass {
    
   
    @IBOutlet weak var viewSplitter: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
        let myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: viewSplitter.frame.width, height: viewSplitter.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = true
        // myOptions.is
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.yellow
        myOptions.tabViewBackgroundHighlightColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewTextDefaultColor = UIColor.yellow
        myOptions.tabViewTextHighlightColor = UIColor.yellow
        
        let viewPager = ViewPagerController()
        
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        
        
        viewSplitter.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
        // Do any additional setup after loading the view.
    }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = "Meeting Manager"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
        
    }
    extension ManageMeetingViewController : ViewPagerControllerDataSource {
        func numberOfPages() -> Int {
            return 3
        }
        
        func viewControllerAtPosition(position: Int) -> UIViewController {
            let storyBoard : UIStoryboard = UIStoryboard(name: "MOM",bundle : nil)
                    if(position == 0) {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "ManageMeetingUpCompCancelViewController") as! ManageMeetingUpCompCancelViewController
                        EmpSearchDB = []
                        EmpDB = []
                        empVC.flagMeeting = "Upcoming"
            return  empVC as CommonVSClass
                    }else if(position == 1) {
                        
                        let empVC = storyBoard.instantiateViewController(withIdentifier: "ManageMeetingUpCompCancelViewController") as! ManageMeetingUpCompCancelViewController
                        empVC.flagMeeting = "Completed"
                        return  empVC as CommonVSClass
                    }
                    else {
                        let empVC = storyBoard.instantiateViewController(withIdentifier: "ManageMeetingUpCompCancelViewController") as! ManageMeetingUpCompCancelViewController
                        empVC.flagMeeting = "Missed"
                        return  empVC as CommonVSClass
                    }
        }
        
        func tabsForPages() -> [ViewPagerTab] {
            return [ViewPagerTab(title: "Upcoming" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "Completed" , image: #imageLiteral(resourceName: "BackBlack")), ViewPagerTab(title: "Missed" , image: #imageLiteral(resourceName: "BackBlack"))]
        }
        
        
    }
    extension ManageMeetingViewController : ViewPagerControllerDelegate {
        
}


