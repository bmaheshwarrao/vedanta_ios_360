//
//  ManageMeetingMOMTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ManageMeetingMOMTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblVenue: UITextView!
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var lblDesc: UITextView!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonYear: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var containerView: UIViewX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
