//
//  ProcessTaskMOMTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ProcessTaskMOMTableViewCell: UITableViewCell {

    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var switchProcess: UISwitch!
    @IBOutlet weak var textViewRemark: UITextView!
    @IBOutlet weak var viewRemark: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewRemark.layer.borderWidth = 1.0
        viewRemark.layer.borderColor = UIColor.black.cgColor
        viewRemark.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
