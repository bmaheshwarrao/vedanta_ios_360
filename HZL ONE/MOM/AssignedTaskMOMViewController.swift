//
//  AssignedTaskMOMViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AssignedTaskMOMViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var ManageMeetingDB:[AssignedTaskListModel] = []
    var ManageMeetingAPI = AssignedTaskListModelDataAPI()
    
     var meetingDetailsDB = ViewMeetingManagerModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getMeetingData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = ""
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getMeetingData()
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMeetingData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var flagMeeting = String()
    @objc func getMeetingData(){
        var param = [String:String]()
       
        
        param = ["Meeting_ID": self.meetingDetailsDB.viewMeet[0].ID]
        print(param)
        
        
        ManageMeetingAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.ManageMeetingDB = dict as! [AssignedTaskListModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ManageMeetingDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AssignedListTableViewCell
        
     
       
        
        cell.lblEmp.text = String(indexPath.row + 1) + ") " + ManageMeetingDB[indexPath.row].Assigned_Name
        cell.textViewTask.text = ManageMeetingDB[indexPath.row].TASK
        
        
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
