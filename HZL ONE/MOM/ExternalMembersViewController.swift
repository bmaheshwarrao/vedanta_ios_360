//
//  ExternalMembersViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
   var EmailDB:[String] = []
class ExternalMembersViewController:  CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
  
    @IBOutlet weak var txtEmailId: UITextField!
 
    
    
    
    
    @IBOutlet weak var viewEmail: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        txtEmailId.keyboardType = UIKeyboardType.emailAddress
        viewEmail.layer.borderWidth = 1.0
        viewEmail.layer.borderColor = UIColor.black.cgColor
        viewEmail.layer.cornerRadius = 10.0
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    
    
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        EmailDB.remove(at: sender.tag)
        self.tableView.reloadData()
         self.txtEmailId.text = ""
        
    }
    @IBAction func btnAddClicked(_ sender: UIButton) {
        if(txtEmailId.text == ""){
            self.view.makeToast("Enter Employee Id")
        }else{
            EmailDB.append(txtEmailId.text!)
            tableView.reloadData()
            txtEmailId.text = ""
        }
        
    }
    
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(EmailDB.count == 0){
            return 1
        }else{
            return EmailDB.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! PartnerTableViewCell
        
        
        if(EmailDB.count > 0){
            cell.lblEmpName.text = EmailDB[indexPath.row]
            cell.lblEmpName.textAlignment = NSTextAlignment.left
            cell.clearBtn.tag = indexPath.row
            cell.clearBtn.isHidden = false
        }else{
            cell.lblEmpName.text = "No Email Added"
            cell.lblEmpName.textAlignment = NSTextAlignment.center
            cell.clearBtn.isHidden = true
        }
        
        
        
        
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
}


