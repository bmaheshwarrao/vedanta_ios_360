//
//  InternalMembersViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var EmpDB:[EmpListMOMModel] = []
var EmpSearchDB:[EmpListMOMModel] = []
class InternalMembersViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    
    @IBOutlet weak var viewEmpID: UIView!
    @IBOutlet weak var txtEmployeIds: UITextField!
    

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
  txtEmployeIds.keyboardType = UIKeyboardType.numbersAndPunctuation
        viewEmpID.layer.borderWidth = 1.0
        viewEmpID.layer.borderColor = UIColor.black.cgColor
        viewEmpID.layer.cornerRadius = 10.0
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
      
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        tableView.reloadData()
    }
 
    

    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
       
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        EmpDB.remove(at: sender.tag)
        self.tableView.reloadData()
        
    }
    @IBAction func btnAddClicked(_ sender: UIButton) {
EmpSearchDB = []
        
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "SearchNameListMOMViewController") as! SearchNameListMOMViewController
        
        self.navigationController?.pushViewController(auditVC, animated: true)
        
    }
 
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 0.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(EmpDB.count == 0){
            return 1
        }else{
            return EmpDB.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! PartnerTableViewCell
        

        if(EmpDB.count > 0){
            cell.lblEmpName.text = EmpDB[indexPath.row].Employee_Name + " - " + EmpDB[indexPath.row].Employee_ID
            cell.lblEmpName.textAlignment = NSTextAlignment.left
            cell.clearBtn.tag = indexPath.row
            cell.clearBtn.isHidden = false
        }else{
            cell.lblEmpName.text = "No Member Selected"
            cell.lblEmpName.textAlignment = NSTextAlignment.center
            cell.clearBtn.isHidden = true
        }
      
        
        
        
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
   
    
    
}

class EmpMOMModel: NSObject {
    
    
    
    var EmpId = String()
    var EmpName = String()
    var Email = String()
    var Mobile_Number = String()
    var is_Selected = Bool()
}
