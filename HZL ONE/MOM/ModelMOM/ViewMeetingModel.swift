//
//  ViewMeetingModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class ViewMeetingModel: NSObject {
    
    
    
    var ID = String()
    var Meeting_Title = String()
    var Meeting_Agenda = String()
    var Meeting_Venue = String()
    
    var Meeting_DateTime = String()
    var Meeting_Date = String()
    var Meeting_Time = String()
    var Meeting_Duration = String()
    
    
    var Meeting_Status = String()
    var Meeting_ActualStartedOn = String()
    var Meeting_ActualDuration = String()
    var Meeting_Discussion = String()
    var Owner_Name = String()
    var Meeting_Owner = String()
    
  
    var Created_BY = String()
    var Created_Date = String()
    

    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Meeting_Title"] is NSNull || cell["Meeting_Title"] == nil {
            self.Meeting_Title = ""
        }else{
            let app = cell["Meeting_Title"]
            self.Meeting_Title = (app?.description)!
        }
        
        if cell["Meeting_Agenda"] is NSNull || cell["Meeting_Agenda"] == nil {
            self.Meeting_Agenda = ""
        }else{
            let app = cell["Meeting_Agenda"]
            self.Meeting_Agenda = (app?.description)!
        }
        if cell["Meeting_Venue"] is NSNull || cell["Meeting_Venue"] == nil {
            self.Meeting_Venue = ""
        }else{
            let app = cell["Meeting_Venue"]
            self.Meeting_Venue = (app?.description)!
        }
        
        //
        
        
        if cell["Meeting_DateTime"] is NSNull || cell["Meeting_DateTime"] == nil {
            self.Meeting_DateTime = ""
        }else{
            let app = cell["Meeting_DateTime"]
            self.Meeting_DateTime = (app?.description)!
        }
        
        if cell["Meeting_Date"] is NSNull || cell["Meeting_Date"] == nil {
            self.Meeting_Date = ""
        }else{
            let app = cell["Meeting_Date"]
            self.Meeting_Date = (app?.description)!
        }
        
        if cell["Meeting_Time"] is NSNull || cell["Meeting_Time"] == nil {
            self.Meeting_Time = ""
        }else{
            let app = cell["Meeting_Time"]
            self.Meeting_Time = (app?.description)!
        }
        if cell["Meeting_Duration"] is NSNull || cell["Meeting_Duration"] == nil {
            self.Meeting_Duration = ""
        }else{
            let app = cell["Meeting_Duration"]
            self.Meeting_Duration = (app?.description)!
        }
        //
        
        
        if cell["Meeting_Status"] is NSNull || cell["Meeting_Status"] == nil {
            self.Meeting_Status = ""
        }else{
            let app = cell["Meeting_Status"]
            self.Meeting_Status = (app?.description)!
        }
        
        if cell["Meeting_ActualStartedOn"] is NSNull || cell["Meeting_ActualStartedOn"] == nil {
            self.Meeting_ActualStartedOn = ""
        }else{
            let app = cell["Meeting_ActualStartedOn"]
            self.Meeting_ActualStartedOn = (app?.description)!
        }
        
        if cell["Meeting_ActualDuration"] is NSNull || cell["Meeting_ActualDuration"] == nil {
            self.Meeting_ActualDuration = ""
        }else{
            let app = cell["Meeting_ActualDuration"]
            self.Meeting_ActualDuration = (app?.description)!
        }
        if cell["Meeting_Discussion"] is NSNull || cell["Meeting_Discussion"] == nil {
            self.Meeting_Discussion = ""
        }else{
            let app = cell["Meeting_Discussion"]
            self.Meeting_Discussion = (app?.description)!
        }
        
        if cell["Owner_Name"] is NSNull || cell["Owner_Name"] == nil {
            self.Owner_Name = ""
        }else{
            let app = cell["Owner_Name"]
            self.Owner_Name = (app?.description)!
        }
        if cell["Meeting_Owner"] is NSNull || cell["Meeting_Owner"] == nil {
            self.Meeting_Owner = ""
        }else{
            let app = cell["Meeting_Owner"]
            self.Meeting_Owner = (app?.description)!
        }
        
        
       
        if cell["Created_BY"] is NSNull || cell["Created_BY"] == nil {
            self.Created_BY = ""
        }else{
            let app = cell["Created_BY"]
            self.Created_BY = (app?.description)!
        }
        if cell["Created_Date"] is NSNull || cell["Created_Date"] == nil {
            self.Created_Date = ""
        }else{
            let app = cell["Created_Date"]
            self.Created_Date = (app?.description)!
        }
        
    }
}




class ViewInternalModel: NSObject {
    
    
    
    var ID = String()
    var Meeting_ID = String()
    var Employee_ID = String()
    var Confirmation = String()
    
    var Confirmation_On = String()
    var Attend_Status = String()
    var Attend_On = String()
    var Employee_Name = String()
    

    
    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Meeting_ID"] is NSNull || cell["Meeting_ID"] == nil {
            self.Meeting_ID = ""
        }else{
            let app = cell["Meeting_ID"]
            self.Meeting_ID = (app?.description)!
        }
        
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil {
            self.Employee_ID = ""
        }else{
            let app = cell["Employee_ID"]
            self.Employee_ID = (app?.description)!
        }
        if cell["Confirmation"] is NSNull || cell["Confirmation"] == nil {
            self.Confirmation = ""
        }else{
            let app = cell["Confirmation"]
            self.Confirmation = (app?.description)!
        }
        
        //
        
        
        if cell["Confirmation_On"] is NSNull || cell["Confirmation_On"] == nil {
            self.Confirmation_On = ""
        }else{
            let app = cell["Confirmation_On"]
            self.Confirmation_On = (app?.description)!
        }
        
        if cell["Attend_Status"] is NSNull || cell["Attend_Status"] == nil {
            self.Attend_Status = ""
        }else{
            let app = cell["Attend_Status"]
            self.Attend_Status = (app?.description)!
        }
        
        if cell["Attend_On"] is NSNull || cell["Attend_On"] == nil {
            self.Attend_On = ""
        }else{
            let app = cell["Attend_On"]
            self.Attend_On = (app?.description)!
        }
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil {
            self.Employee_Name = ""
        }else{
            let app = cell["Employee_Name"]
            self.Employee_Name = (app?.description)!
        }
        //
        
       
        
    }
}


class ViewExternalModel: NSObject {
    
    
    
    var ID = String()
    var Meeting_ID = String()
    var Email_ID = String()
    var Confirmation = String()
    
    var Confirmation_On = String()
    var Attend_Status = String()
    var Attend_On = String()
    var Created_By = String()
    
    var Created_Date = String()
    var Updated_By = String()
    var Updated_Date = String()
    
    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Meeting_ID"] is NSNull || cell["Meeting_ID"] == nil {
            self.Meeting_ID = ""
        }else{
            let app = cell["Meeting_ID"]
            self.Meeting_ID = (app?.description)!
        }
        
        if cell["Email_ID"] is NSNull || cell["Email_ID"] == nil {
            self.Email_ID = ""
        }else{
            let app = cell["Email_ID"]
            self.Email_ID = (app?.description)!
        }
        if cell["Confirmation"] is NSNull || cell["Confirmation"] == nil {
            self.Confirmation = ""
        }else{
            let app = cell["Confirmation"]
            self.Confirmation = (app?.description)!
        }
        
        //
        
        
        if cell["Confirmation_On"] is NSNull || cell["Confirmation_On"] == nil {
            self.Confirmation_On = ""
        }else{
            let app = cell["Confirmation_On"]
            self.Confirmation_On = (app?.description)!
        }
        
        if cell["Attend_Status"] is NSNull || cell["Attend_Status"] == nil {
            self.Attend_Status = ""
        }else{
            let app = cell["Attend_Status"]
            self.Attend_Status = (app?.description)!
        }
        
        if cell["Attend_On"] is NSNull || cell["Attend_On"] == nil {
            self.Attend_On = ""
        }else{
            let app = cell["Attend_On"]
            self.Attend_On = (app?.description)!
        }
        if cell["Created_By"] is NSNull || cell["Created_By"] == nil {
            self.Created_By = ""
        }else{
            let app = cell["Created_By"]
            self.Created_By = (app?.description)!
        }
        
        if cell["Created_Date"] is NSNull || cell["Created_Date"] == nil {
            self.Created_Date = ""
        }else{
            let app = cell["Created_Date"]
            self.Created_Date = (app?.description)!
        }
        if cell["Updated_By"] is NSNull || cell["Updated_By"] == nil {
            self.Updated_By = ""
        }else{
            let app = cell["Updated_By"]
            self.Updated_By = (app?.description)!
        }
        if cell["Updated_Date"] is NSNull || cell["Updated_Date"] == nil {
            self.Updated_Date = ""
        }else{
            let app = cell["Updated_Date"]
            self.Updated_Date = (app?.description)!
        }
       
        
        
    }
}

class ViewTaskModel: NSObject {
    
    
   
    var ID = String()
    var Meeting_ID = String()
    var Task = String()
    var Assigned_To = String()
    
    var Status = String()
    var Assigned_Name = String()
    var Status1 = String()
    
    
    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Meeting_ID"] is NSNull || cell["Meeting_ID"] == nil {
            self.Meeting_ID = ""
        }else{
            let app = cell["Meeting_ID"]
            self.Meeting_ID = (app?.description)!
        }
        
        if cell["Task"] is NSNull || cell["Task"] == nil {
            self.Task = ""
        }else{
            let app = cell["Task"]
            self.Task = (app?.description)!
        }
        if cell["Assigned_To"] is NSNull || cell["Assigned_To"] == nil {
            self.Assigned_To = ""
        }else{
            let app = cell["Assigned_To"]
            self.Assigned_To = (app?.description)!
        }
        
        //
        
        
        if cell["Status"] is NSNull || cell["Status"] == nil {
            self.Status = ""
        }else{
            let app = cell["Status"]
            self.Status = (app?.description)!
        }
        
        if cell["Assigned_Name"] is NSNull || cell["Assigned_Name"] == nil {
            self.Assigned_Name = ""
        }else{
            let app = cell["Assigned_Name"]
            self.Assigned_Name = (app?.description)!
        }
        
        if cell["Status1"] is NSNull || cell["Status1"] == nil {
            self.Status1 = ""
        }else{
            let app = cell["Status1"]
            self.Status1 = (app?.description)!
        }
       
        
        
    }
}

class ViewMeetingManagerModel: NSObject {
    var viewMeet = [ViewMeetingModel]()
    var viewInternal = [ViewInternalModel]()
    var viewExternal = [ViewExternalModel]()
    var viewTask = [ViewTaskModel]()
    func setDataInModel(str:[String:AnyObject])
    {
        if let dataMeet = str["Meeting_Data"] as? [AnyObject]
        {
            
            var dataArrayViewMeet : [ViewMeetingModel] = []
            for i in 0..<dataMeet.count
            {
                
                if let cell = dataMeet[i] as? [String:AnyObject]
                {
                    let object = ViewMeetingModel()
                    object.setDataInModel(cell: cell)
                    dataArrayViewMeet.append(object)
                }
                
            }
            
            
          viewMeet = dataArrayViewMeet
            
            
        }
        
        if let dataInternal = str["Internal_Member"] as? [AnyObject]
        {
            
            var dataArrayViewInternal : [ViewInternalModel] = []
            for i in 0..<dataInternal.count
            {
                
                if let cell = dataInternal[i] as? [String:AnyObject]
                {
                    let object = ViewInternalModel()
                    object.setDataInModel(cell: cell)
                    dataArrayViewInternal.append(object)
                }
                
            }
            
            
            viewInternal = dataArrayViewInternal
            
            
        }
        
        if let dataExternal = str["External_Member"] as? [AnyObject]
        {
            
            var dataArrayViewExternal : [ViewExternalModel] = []
            for i in 0..<dataExternal.count
            {
                
                if let cell = dataExternal[i] as? [String:AnyObject]
                {
                    let object = ViewExternalModel()
                    object.setDataInModel(cell: cell)
                    dataArrayViewExternal.append(object)
                }
                
            }
            
            
            viewExternal = dataArrayViewExternal
            
            
        }
        if let dataTask = str["Task_List"] as? [AnyObject]
        {
            
            var dataArrayViewTask : [ViewTaskModel] = []
            for i in 0..<dataTask.count
            {
                
                if let cell = dataTask[i] as? [String:AnyObject]
                {
                    let object = ViewTaskModel()
                    object.setDataInModel(cell: cell)
                    dataArrayViewTask.append(object)
                }
                
            }
            
            
            viewTask = dataArrayViewTask
            
            
        }
        
        
    }
    
    
    
}
class ViewManageMeetingModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:MyMeetingDetailsViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.View_Meeting_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [ViewMeetingManagerModel] = []
                           
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = ViewMeetingManagerModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
             obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
           // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class ViewManageMeetingCompletedModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:CompletedMeetingManagerViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Completed_Meeting_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [ViewMeetingManagerModel] = []
                            
                            
                            if let cell = data as? [String:AnyObject]
                            {
                                let object = ViewMeetingManagerModel()
                                object.setDataInModel(str: cell)
                                dataArray.append(object)
                            }
                            
                            
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}




class ViewManageMeetingAssignModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ManageMeetingUpCompCancelViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.View_Meeting_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [ViewMeetingManagerModel] = []
                            
                            
                            if let cell = data as? [String:AnyObject]
                            {
                                let object = ViewMeetingManagerModel()
                                object.setDataInModel(str: cell)
                                dataArray.append(object)
                            }
                            
                            
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
             obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
           // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class TaskListModel: NSObject {
    
    
    
    var ID = String()
    var TaskUpdate = String()
    var Status = String()
    var Created_ON = String()

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["TaskUpdate"] is NSNull || cell["TaskUpdate"] == nil {
            self.TaskUpdate = ""
        }else{
            let app = cell["TaskUpdate"]
            self.TaskUpdate = (app?.description)!
        }
        
        if cell["Status"] is NSNull || cell["Status"] == nil {
            self.Status = ""
        }else{
            let app = cell["Status"]
            self.Status = (app?.description)!
        }
        if cell["Created_ON"] is NSNull || cell["Created_ON"] == nil {
            self.Created_ON = ""
        }else{
            let app = cell["Created_ON"]
            self.Created_ON = (app?.description)!
        }
        

       
        
    }
}


class TaskListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:PendingDoneTaskDetailsViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Task_List_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.label.isHidden = true
                     
                        obj.noDataLabel(text: "" )
                       
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [TaskListModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TaskListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        
                       obj.label.isHidden = false
//                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
             obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
//
            obj.label.isHidden = false
//            obj.tableView.isHidden = true
            //obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}




class AssignedTaskListModel: NSObject {
    
  
    
    var ID = String()
    var Meeting_ID = String()
    var TASK = String()
    var Assigned_To = String()
    var Assigned_Name = String()
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Meeting_ID"] is NSNull || cell["Meeting_ID"] == nil {
            self.Meeting_ID = ""
        }else{
            let app = cell["Meeting_ID"]
            self.Meeting_ID = (app?.description)!
        }
        
        if cell["TASK"] is NSNull || cell["TASK"] == nil {
            self.TASK = ""
        }else{
            let app = cell["TASK"]
            self.TASK = (app?.description)!
        }
        if cell["Assigned_To"] is NSNull || cell["Assigned_To"] == nil {
            self.Assigned_To = ""
        }else{
            let app = cell["Assigned_To"]
            self.Assigned_To = (app?.description)!
        }
        if cell["Assigned_Name"] is NSNull || cell["Assigned_Name"] == nil {
            self.Assigned_Name = ""
        }else{
            let app = cell["Assigned_Name"]
            self.Assigned_Name = (app?.description)!
        }
        
        
        
    }
}


class AssignedTaskListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AssignedTaskMOMViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Temp_Task_List_MOM, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [AssignedTaskListModel] = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = AssignedTaskListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
             obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
           // obj.noDataLabel(text: "No Data Found!" )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

