//
//  ForwardNDSOViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ForwardNDSOViewController: CommonVSClass {
var ReportedDataDB = ReportedDataModel()
    @IBOutlet weak var viewDept: UIView!
    @IBOutlet weak var lblDept: UILabel!
    @IBAction func btnDeptClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Department"
        
        
        updateData()
        
        FilterDataFromServer.unit_Id = Int(ReportedDataDB.Unit_ID)!
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
        ZIVC.titleStr = "Select Department"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDept.layer.borderWidth = 1.0
        viewDept.layer.borderColor = UIColor.black.cgColor
        viewDept.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
    }

    @IBAction func btnForwardClicked(_ sender: UIButton) {
        submitData()
    }
    
    var reachablty = Reachability()!
    @objc func submitData(){
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if lblDept.text == "Select Department" || lblDept.text == "" {
            
            self.view.makeToast("Please Select Department")
        }
        else{
            
            self.startLoadingPK(view: self.view)
            
            
            
     
            
            
            let parameter = [
                             "Observation_Details_ID":ReportedDataDB.Observation_detail_ID,
                             
                             
                             "Responsible_Dept_ID":String(FilterDataFromServer.dept_Id) ,
                             
                             "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Forward_Observation, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                  let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func updateData(){
        if(FilterDataFromServer.dept_Name != "") {
            lblDept.text = FilterDataFromServer.dept_Name
            lblDept.textColor = UIColor.black
        } else {
            lblDept.text = "Select Department"
            lblDept.textColor = UIColor.lightGray
        }
        
        
        // tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Forward Observation"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if(FilterDataFromServer.dept_Name != ""){
            updateData()
        }
        //updateData()
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
