//
//  AddRewardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 05/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AddRewardViewController: CommonVSClass {
    
    @IBOutlet weak var tableView: UITableView!
    var Question = String()
    var visit_Id = String()
    var Question_Id = String()
    var type_Id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    var empType = ["Employee", "Contractor"]
    @IBAction func btnEmployeeClicked(_ sender: UIButton) {
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.empType
            
            popup.sourceView = self.cellData.lblEmployee
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblEmployee.text = self.empType[row]
                self.cellData.lblEmployee.textColor = UIColor.black
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
        
        
    }
    @IBAction func btnAddClicked(_ sender: UIButton) {
        submitData()
    }
    @objc func updateData(){
      
        
        if(FilterDataFromServer.employyetype != "" ) {
            cellData.lblEmployee.text = FilterDataFromServer.employyetype
            cellData.lblEmployee.textColor = UIColor.black
        } else {
            cellData.lblEmployee.text = "Select Employee"
            cellData.lblEmployee.textColor = UIColor.lightGray
        }
      
        // tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var reachablty = Reachability()!
    @objc func submitData(){
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if cellData.lblEmployee.text == "Select Employee" || cellData.lblEmployee.text == "" {
            
            self.view.makeToast("Please Select Employee Type")
        }
        else if cellData.textGatepass.text == "" {
            
            self.view.makeToast("Please Enter Employee Id/Gate pass Id")
        }
        else if cellData.textViewRemark.text == "" {
            
            self.view.makeToast("Please Enter Remark")
        }else{
            
            self.startLoadingPK(view: self.view)
            
            
            
            
            
            
            
           
            
            let parameter = ["Team_ID":visit_Id,
                             
                             "Type_ID":cellData.textGatepass.text!,
                             "Employee_Type":cellData.lblEmployee.text!,
                             
                             
                             "Remarks":cellData.textViewRemark.text! ,
                             
                             "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Submit_Reward, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                 let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
    }
    
    
    
    
    
    
    
    var cellData : RewardSetObTableViewCell!
  
    var QuestionStrNo = String()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Add Reward"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        tableView.reloadData()
        //updateData()
        
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension AddRewardViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
extension AddRewardViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RewardSetObTableViewCell
        cellData = cell
        updateData()
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
