//
//  AddObservationTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AddObservationTableViewCell: UITableViewCell {

   
    
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightRDept: NSLayoutConstraint!
    
    @IBOutlet weak var heightLblRDept: NSLayoutConstraint!
    
    @IBOutlet weak var heightImageRDept: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
  
    
    
    @IBOutlet weak var imgDept: UIImageView!
    @IBOutlet weak var heightImageselected: NSLayoutConstraint!
    @IBOutlet weak var heightViewofClose: NSLayoutConstraint!
    @IBOutlet weak var lblRDept: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblOptional: UILabel!
    @IBOutlet weak var lblDept: UILabel!
    @IBOutlet weak var viewDept: UIView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var textViewSuggestion: UITextView!
    @IBOutlet weak var textViewObservation: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewDept.layer.borderWidth = 1.0
        viewDept.layer.borderColor = UIColor.black.cgColor
        viewDept.layer.cornerRadius = 10.0
        viewStatus.layer.borderWidth = 1.0
        viewStatus.layer.borderColor = UIColor.black.cgColor
        viewStatus.layer.cornerRadius = 10.0
        
        textViewObservation.layer.borderWidth = 1.0
        textViewObservation.layer.borderColor = UIColor.black.cgColor
        textViewObservation.layer.cornerRadius = 10.0
        
        textViewSuggestion.layer.borderWidth = 1.0
        textViewSuggestion.layer.borderColor = UIColor.black.cgColor
        textViewSuggestion.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
