//
//  NDSOASUZTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NDSOASUZTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIViewX!
    @IBOutlet weak var lblASUZ: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    required init(coder aDecoder: (NSCoder?)) {
        super.init(coder: aDecoder!)!
    }
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
    }
}
