//
//  NDSOReportedDataTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NDSOReportedDataTableViewCell: UITableViewCell {

    @IBOutlet weak var lblList: UILabel!
    @IBOutlet weak var textViewSuggestion: UITextView!
    @IBOutlet weak var textViewObservation: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var imgData: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
