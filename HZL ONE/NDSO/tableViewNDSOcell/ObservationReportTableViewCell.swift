//
//  ObservationReportTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 05/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ObservationReportTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewQ1: UITextView!
    @IBOutlet weak var textViewQ2: UITextView!
    @IBOutlet weak var textViewQ3: UITextView!
     @IBOutlet weak var textViewQ4: UITextView!
    @IBOutlet weak var textViewRemark: UITextView!
    @IBOutlet weak var observationQuestion1Lbl: UILabel!
    @IBOutlet weak var observationQuestion2Lbl: UILabel!
    @IBOutlet weak var observationQuestion3Lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
