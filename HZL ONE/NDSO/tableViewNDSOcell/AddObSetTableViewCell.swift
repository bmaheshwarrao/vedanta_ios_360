//
//  AddObSetTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AddObSetTableViewCell: UITableViewCell {

    @IBOutlet weak var imgOb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var textViewOb1: UITextView!
    @IBOutlet weak var textViewOb2: UITextView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
