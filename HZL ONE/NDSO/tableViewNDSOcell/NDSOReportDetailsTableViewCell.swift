//
//  NDSOReportDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NDSOReportDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var textViewSuggestion: UITextView!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var textViewObservation: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imageViewDetails: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
