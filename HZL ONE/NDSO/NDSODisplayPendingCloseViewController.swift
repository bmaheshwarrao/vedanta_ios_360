//
//  NDSODisplayPendingCloseViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 07/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NDSODisplayPendingCloseViewController:CommonVSClass {
    
    @IBOutlet weak var tableDisplayHazAction: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //barSetup()
        tableDisplayHazAction.rowHeight = 70
        tableDisplayHazAction.delegate = self;
        tableDisplayHazAction.dataSource = self;
        refresh.addTarget(self, action: #selector(Reportdata), for: .valueChanged)
        
        self.tableDisplayHazAction.addSubview(refresh)
        
        tableDisplayHazAction.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    func setUp() {
       
            self.title = "My Inbox"
        
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        Reportdata()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUp()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    var ndsoDisplay : [NDSODisplay] = []
    
    
   
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    let reachability = Reachability()!
    @objc func Reportdata() {
        if(self.reachability.connection != .none) {
            let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
            
            let parameters = ["Employee_ID": pno ]
            
            self.startLoading()
            print(parameters)
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Observations_Details_Count,parameters: parameters, successHandler: { (dict) in
                
                //self.stopLoading()
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        self.stopLoading()
                       
                        self.ndsoDisplay = []
                        if let data = dict["data"]
                        {
                            
                            self.refresh.endRefreshing()
                           
                            
                            
                            
                      
                            
                            let obData = NDSODisplay()
                            obData.Pending_to_Assign = String(Int(data["Pending_to_Assign"] as! NSNumber))
                             obData.Assign_to_Me_Open = String(Int(data["Assign_to_Me_Open"] as! NSNumber))
                            
                            obData.Assign_to_Me_Close = String(Int(data["Assign_to_Me_Close"] as! NSNumber))
                            self.ndsoDisplay.append(obData)
                            
                            self.tableDisplayHazAction.reloadData();
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {self.stopLoading()
                        self.refresh.endRefreshing()
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                self.refresh.endRefreshing()
                self.stopLoading()
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
            // self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            self.refresh.endRefreshing()
            self.stopLoading()
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension NDSODisplayPendingCloseViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
       
            
            
            
            if(indexPath.row == 0){
                UserDefaults.standard.set("Pending to assign", forKey: "Status")
                let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
                let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "NDSOReportedDataViewController") as! NDSOReportedDataViewController
                
                self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
            } else  if(indexPath.row == 1){
                UserDefaults.standard.set("Assigned to me", forKey: "Status")
                let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
                let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "NDSOReportedMenuViewController") as! NDSOReportedMenuViewController
                
                self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
            }
       
        
        
    }
}
extension NDSODisplayPendingCloseViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return 2
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NDSODisplayPendingCloseTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if(self.ndsoDisplay.count > 0){
        if(indexPath.row == 0){
            cell.lblShowHazAction.textColor = UIColor.darkGray
            cell.lblShowHazAction.text = "Pending to assign"
            cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
            cell.lblAddData.text = String(self.ndsoDisplay[0].Pending_to_Assign)
            
        }else if(indexPath.row == 1){
            cell.lblShowHazAction.textColor = UIColor.darkGray
            cell.lblShowHazAction.text = "Assigned to me"
            cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
            cell.lblAddData.text = String(self.ndsoDisplay[0].Assign_to_Me_Open)
            
        }
        }
            return cell
        }
        
    }

class NDSODisplay :  NSObject {

   
    var Pending_to_Assign = String()
    var Assign_to_Me_Open = String()
    var Assign_to_Me_Close = String()
    
    
}
