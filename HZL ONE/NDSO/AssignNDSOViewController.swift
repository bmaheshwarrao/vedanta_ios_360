//
//  AssignNDSOViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AssignNDSOViewController: CommonVSClass {
var ReportedDataDB = ReportedDataModel()
    @IBOutlet weak var viewEmp: UIView!
    @IBOutlet weak var txtEmp: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewEmp.layer.borderWidth = 1.0
        viewEmp.layer.borderColor = UIColor.black.cgColor
        viewEmp.layer.cornerRadius = 10.0
        txtEmp.keyboardType = UIKeyboardType.phonePad
        // Do any additional setup after loading the view.
    }

    @IBAction func btnAssignClicked(_ sender: UIButton) {
        submitData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var reachablty = Reachability()!
    @objc func submitData(){
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if  txtEmp.text == "" {
            
            self.view.makeToast("Enter Employee Id")
        }
        else{
            
            self.startLoadingPK(view: self.view)
            
            
            
            
        
            let parameter = [
                "Observation_Details_ID":ReportedDataDB.Observation_detail_ID,
                
                
                "Assigned_Emp_ID":txtEmp.text! ,
                
                "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Assign_Observation, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                  let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Assign Observation"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
