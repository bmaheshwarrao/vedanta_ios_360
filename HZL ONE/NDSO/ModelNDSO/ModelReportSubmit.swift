//
//  ModelReportSubmit.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class MyQuestionDataModel: NSObject {
    
    
    var ID = Int()
    var Q1 = String()
    var Q2 = String()
    var Q3 = String()
    var Q4 = String()
    
    
    
    
    
    

    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Q1"] is NSNull || str["Q1"] == nil{
            self.Q1 = ""
        }else{
            
            let qq1 = str["Q1"]
            
            self.Q1 = (qq1?.description)!
            
            
        }
        if str["Q2"] is NSNull || str["Q2"] == nil{
            self.Q2 = ""
        }else{
            
            let qq2 = str["Q2"]
            
            self.Q2 = (qq2?.description)!
            
            
        }
        if str["Q3"] is NSNull || str["Q3"] == nil{
            self.Q3 = ""
        }else{
            
            let qq3 = str["Q3"]
            
            self.Q3 = (qq3?.description)!
            
            
        }
        if str["Q4"] is NSNull || str["Q4"] == nil{
            self.Q4 = ""
        }else{
            
            let qq4 = str["Q4"]
            
            self.Q4 = (qq4?.description)!
            
            
        }
        
    }
    
}





class MyQuestionDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:SubmitReportViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Question_ListNSDO, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    print(response)
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        print(dict["data"])
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray: [MyQuestionDataModel] = []
                            
                            
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = MyQuestionDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}


class MyCountDataModel: NSObject {
    
    
    var Reward = String()
    var Q1 = String()
    var Q2 = String()
    var Q3 = String()
    var Q4 = String()
     var Q5 = String()
    var Observation = String()
    
    
    
    
    
    
    func setDataInModel(reward : String , Question1 : String, Question2 : String, Question3 : String, Question4 : String, Question5 : String, Observation : String)
    {
        if reward == "" {
            self.Reward = ""
        }else{
            
            let re = reward
            
            self.Reward = (re.description)
            
            
        }
        
        
        if Observation == ""{
            self.Observation = ""
        }else{
            
            let ob = Observation
            
            self.Observation = (ob.description)
            
            
        }
        
        
        if Question1 == ""{
            self.Q1 = ""
        }else{
            
            let qq1 = Question1
            
            self.Q1 = (qq1.description)
            
            
        }
        if  Question2 == ""{
            self.Q2 = ""
        }else{
            
            let qq2 = Question2
            
            self.Q2 = (qq2.description)
            
            
        }
        if  Question3 == ""{
            self.Q3 = ""
        }else{
            
            let qq3 = Question3
            
            self.Q3 = (qq3.description)
            
            
        }
        if  Question4 == ""{
            self.Q4 = ""
        }else{
            
            let qq4 = Question4
            
            self.Q4 = (qq4.description)
            
            
        }
        
    }
    
}

class MyObCountDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:SubmitReportViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Observations_DetailsNSDO, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                      
                        var reward = String()
                        var Q1 = String()
                        var Q2 = String()
                        var Q3 = String()
                        var Q4 = String()
                        var Q5 = String()
                        var Observation = String()
                        if let reward1 = dict["Reward"] as? [AnyObject]
                             {
                        reward = String(reward1.count)
                        }
                        if let Q11 = dict["Q1"] as? [AnyObject]
                             {
                               Q1 = String(Q11.count)
                        }
                        if let Q21 = dict["Q2"] as? [AnyObject]
                             {
                                 Q2 = String(Q21.count)
                        }
                        if let Q31 = dict["Q3"] as? [AnyObject]
                             {
                                  Q3 = String(Q31.count)
                        }
                        if let Q41 = dict["Q4"] as? [AnyObject]
                             {
                                  Q4 = String(Q41.count)
                        }
                        if let Q51 = dict["Q5"] as? [AnyObject]
                             {
                                  Q5 = String(Q51.count)
                        }
                        if let Observation1 = dict["Observation"] as? [AnyObject]
                             {
                                  Observation = String(Observation1.count)
                        }
                         var dataArray: [MyCountDataModel] = []
                        let object = MyCountDataModel()
                        object.setDataInModel(reward: reward, Question1: Q1, Question2: Q2, Question3: Q3, Question4: Q4, Question5: Q5, Observation: Observation)
                        dataArray.append(object)
                        
                        success(dataArray as AnyObject)
                     
                    }
                    else
                    {
                     
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}




