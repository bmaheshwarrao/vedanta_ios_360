//
//  LsrFirstTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class LsrFirstTableViewCell: UITableViewCell {

    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var viewCompany: UIView!
    
    @IBOutlet weak var stackViewHeightCon: NSLayoutConstraint!
    @IBOutlet weak var textViewHeightCon: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var textViewLSRComment: UITextView!
    @IBOutlet weak var viewEmpId: UIView!
    @IBOutlet weak var textEmpId: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textEmpId.keyboardType = UIKeyboardType.numberPad
        
        viewCategory.layer.borderWidth = 1.0
        viewCategory.layer.borderColor = UIColor.black.cgColor
        viewCategory.layer.cornerRadius = 10.0
        
        
        viewCompany.layer.borderWidth = 1.0
        viewCompany.layer.borderColor = UIColor.black.cgColor
        viewCompany.layer.cornerRadius = 10.0
        
        
        viewName.layer.borderWidth = 1.0
        viewName.layer.borderColor = UIColor.black.cgColor
        viewName.layer.cornerRadius = 10.0
        
        
      
        
        viewEmpId.layer.borderWidth = 1.0
        viewEmpId.layer.borderColor = UIColor.black.cgColor
        viewEmpId.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
