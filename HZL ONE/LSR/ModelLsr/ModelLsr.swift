//
//  ModelLsr.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class EmployeeListLsrModel: NSObject {
    
    
    
    var Employee_ID: String?
    var Employee_Name : String?
    var Company : String?
    var Company_ID : String?
    
    
    var Category : String?
    var Category_Name : String?
    var User_Type : String?
   var Location : String?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil {
            self.Employee_ID = ""
        }else{
            let app = cell["Employee_ID"]
            self.Employee_ID = (app?.description)!
        }
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil {
            self.Employee_Name = ""
        }else{
            let app = cell["Employee_Name"]
            self.Employee_Name = (app?.description)!
        }
        if cell["Company"] is NSNull || cell["Company"] == nil {
            self.Company = ""
        }else{
            let app = cell["Company"]
            self.Company = (app?.description)!
        }
        
        
        if cell["Company_ID"] is NSNull || cell["Company_ID"] == nil {
            self.Company_ID = ""
        }else{
            let app = cell["Company_ID"]
            self.Company_ID = (app?.description)!
        }
        if cell["Category"] is NSNull || cell["Category"] == nil {
            self.Category = ""
        }else{
            let app = cell["Category"]
            self.Category = (app?.description)!
        }
        if cell["Category_Name"] is NSNull || cell["Category_Name"] == nil {
            self.Category_Name = ""
        }else{
            let app = cell["Category_Name"]
            self.Category_Name = (app?.description)!
        }
        
        if cell["User_Type"] is NSNull || cell["User_Type"] == nil {
            self.User_Type = ""
        }else{
            let app = cell["User_Type"]
            self.User_Type = (app?.description)!
        }
        
        if cell["Location"] is NSNull || cell["Location"] == nil {
            self.Location = ""
        }else{
            let app = cell["Location"]
            self.Location = (app?.description)!
        }
    }
}


class EmployeelListLsrDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:LsrFirstViewController,param : [String:String] , empid : String , success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
           
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Employee_List_By_ID, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                 
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        
                        
                        
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [EmployeeListLsrModel] = []
                            if let celll = data as? [String:AnyObject]
                            {
                                let object = EmployeeListLsrModel()
                                object.setDataInModel(cell: celll)
                                dataArray.append(object)
                            }
                            
                            //                            for i in 0..<data.count
                            //                            {
                            //
                            
                            //
                            //                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.cellData.textViewLSRComment.text = ""
                        obj.cellData.textViewLSRComment.isHidden = true
                        obj.onceDone = false
                        obj.cellData.textViewHeightCon.constant = 0
                        obj.cellData.stackViewHeightCon.constant = 80
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                       obj.view.makeToast("No employee found with id " + empid)
                        obj.cellData.textViewLSRComment.isHidden = false
                        obj.cellData.lblName.text = ""
                        obj.cellData.lblCompany.text = ""
                        obj.cellData.lblCategory.text = ""
                        obj.cellData.stackView.isHidden = true
                        obj.cellData.btnNext.isHidden = true
                        obj.cellData.textViewLSRComment.text = "No employee found with id " + empid
                        obj.onceDone = true
                        obj.cellData.textViewHeightCon.constant = 40
                        obj.cellData.stackViewHeightCon.constant = 120
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                 obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.cellData.textViewLSRComment.text = "No internet connection found. Check your internet connection and try again."
            
            obj.cellData.textViewHeightCon.constant = 40
            obj.cellData.stackViewHeightCon.constant = 120
            obj.refresh.endRefreshing()
             obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}




class CheckLocationModel: NSObject {
    
    
    
    var Is_Location: String?
    var MSG: String?
     var Location_ID: String?
     var Location_Name: String?
    
  
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Is_Location"] is NSNull || cell["Is_Location"] == nil {
            self.Is_Location = ""
        }else{
            let app = cell["Is_Location"]
            self.Is_Location = (app?.description)!
        }
        
        if cell["MSG"] is NSNull || cell["MSG"] == nil {
            self.MSG = ""
        }else{
            let app = cell["MSG"]
            self.MSG = (app?.description)!
        }
        
        if cell["Location_ID"] is NSNull || cell["Location_ID"] == nil {
            self.Location_ID = ""
        }else{
            let app = cell["Location_ID"]
            self.Location_ID = (app?.description)!
        }
        if cell["Location_Name"] is NSNull || cell["Location_Name"] == nil {
            self.Location_Name = ""
        }else{
            let app = cell["Location_Name"]
            self.Location_Name = (app?.description)!
        }
        
        
        
    }
}


class CheckLocationDataAPI
{
    
    var reachablty = Reachability()!
    

    func serviceCalling(obj:LsrFirstViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Check_Location_Admin, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
               
                        
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [CheckLocationModel] = []
                            if let celll = data as? [String:AnyObject]
                            {
                                let object = CheckLocationModel()
                                object.setDataInModel(cell: celll)
                                dataArray.append(object)
                            }
                            
                            //                            for i in 0..<data.count
                            //                            {
                            //
                            
                            //
                            //                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                       obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        
                        
                   
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
         
            obj.refresh.endRefreshing()
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}







class BreakModel: NSObject {
    
    
    
    var ID: String?
    var LSRViolation: String?

    

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["LSRViolation"] is NSNull || cell["LSRViolation"] == nil {
            self.LSRViolation = ""
        }else{
            let app = cell["LSRViolation"]
            self.LSRViolation = (app?.description)!
        }
        
      
        
        
    }
}


class BreakDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:LsrSubmitViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Rules_List, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [BreakModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = BreakModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
}




