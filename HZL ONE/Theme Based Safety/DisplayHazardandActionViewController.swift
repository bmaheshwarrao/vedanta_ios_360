//
//  DisplayHazardandActionViewController.swift
//  VallSafety
//
//  Created by Bunga Maheshwar Rao on 03/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class DisplayHazardandActionViewController: CommonVSClass {

    @IBOutlet weak var tableDisplayHazAction: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        hazarddisplay = UserDefaults.standard.integer(forKey: "hazard")
        
        //barSetup()
        tableDisplayHazAction.rowHeight = 70
        tableDisplayHazAction.delegate = self;
        tableDisplayHazAction.dataSource = self;
        refresh.addTarget(self, action: #selector(Reportdata), for: .valueChanged)
        
        self.tableDisplayHazAction.addSubview(refresh)
        
           tableDisplayHazAction.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    func setUp() {
        if(hazarddisplay == 1) {
            self.title = "Hazards Reported"
        } else if(hazarddisplay == 5) {
            
            self.title = "Action Required"
        } else {
            
            self.title = "Action Required"
        }
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
         Reportdata()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUp()
    }
    override func viewWillDisappear(_ animated: Bool) {
     
    }
    var hazarddisplay = Int()
    var arrayHazAction = ["Reported","Pending" ,"Closed","Rejected"]
    var arrayHazActionCount : [Int] = [0,0,0,0]
    
    
    var arrayActionReq = ["Actions I need to close","Actions I need to assign","Assigned but not yet closed","Hazards I have closed"]
    var arrayActionReqCount : [Int] = [0,0,0,0]
    func barSetup() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width , height: 44))
        view.backgroundColor = UIColor.clear
//        let imgView = UIImageView(frame: CGRect(x: 0, y: 5, width: 25, height: 25))
//        imgView.image = UIImage(named: "leftArrowWhite")
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBack(_:)))
//        imgView.isUserInteractionEnabled = true;
//        imgView.addGestureRecognizer(tapGesture)
        let txtBarRight = UITextField(frame: CGRect(x: 100, y: 5, width: self.view.bounds.width , height: 30))
        if(hazarddisplay == 1){
        txtBarRight.text = "Hazards Reported"
        } else {
           txtBarRight.text = "Actions Required"
        }
        txtBarRight.textColor = UIColor.white
        
        txtBarRight.font = UIFont(name: "Georgia", size: 17.0)
        view.addSubview(txtBarRight)
       // view.addSubview(imgView)
        let barButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = barButtonItem
        
//
    }
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    let reachability = Reachability()!
    @objc func Reportdata() {
        if(self.reachability.connection != .none) {
            let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
           
            let parameters = ["Employee_ID": pno ]
           
            self.startLoading()
            print(parameters)
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Count,parameters: parameters, successHandler: { (dict) in
                
                //self.stopLoading()
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        self.stopLoading()
                        self.arrayHazActionCount = []
                        self.arrayActionReqCount = []
                        
                        if let data = dict["data"]
                        {

                            self.refresh.endRefreshing()
                            self.arrayHazActionCount.append(Int(data["Total_Reported_Count"] as! NSNumber))
                            self.arrayHazActionCount.append(Int(data["Reported_Pending"] as! NSNumber))
                            self.arrayHazActionCount.append(Int(data["Reported_Close"] as! NSNumber))
                            self.arrayHazActionCount.append(Int(data["Reported_Reject"] as! NSNumber))
                            
                            
                            
                            self.arrayActionReqCount.append(Int(data["Assign_Pending"] as! NSNumber))
                            
                            self.arrayActionReqCount.append(Int(data["Waiting_For_Assign"] as! NSNumber))
                            self.arrayActionReqCount.append(Int(data["Total_MY_Assign_Pending"] as! NSNumber))
                            
                            self.arrayActionReqCount.append(Int(data["Assign_Close"] as! NSNumber))
                            
                            
                            
                            self.tableDisplayHazAction.reloadData();
                            
                            
                            
                            
                            
                
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {self.stopLoading()
                        self.refresh.endRefreshing()
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                self.refresh.endRefreshing()
                self.stopLoading()
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
           // self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            self.refresh.endRefreshing()
            self.stopLoading()
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension DisplayHazardandActionViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
 if(hazarddisplay == 1) {
    
    
    
    if(indexPath.row == 3){
        UserDefaults.standard.set("Reject", forKey: "Status")
    } else  if(indexPath.row == 1){
        UserDefaults.standard.set("Pending", forKey: "Status")
    } else  if(indexPath.row == 2){
        UserDefaults.standard.set("Close", forKey: "Status")
    }else {
          UserDefaults.standard.set("All", forKey: "Status")
    }
    let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
    
        let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
        
        self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        } else if(hazarddisplay == 5) {
            
            
            
            if(indexPath.row == 3){
                UserDefaults.standard.set("Reject", forKey: "Status")
            } else  if(indexPath.row == 1){
                UserDefaults.standard.set("Pending", forKey: "Status")
            } else  if(indexPath.row == 2){
                UserDefaults.standard.set("Close", forKey: "Status")
            }else {
                UserDefaults.standard.set("All", forKey: "Status")
            }
    let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
    
    let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
            
            self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        } else {
    
    if(indexPath.row == 3){
        UserDefaults.standard.set("Close", forKey: "Status")
        
        // UserDefaults.standard.set("Assign_Pending_To_Me", forKey: "Status")
    } else  if(indexPath.row == 0){
        UserDefaults.standard.set("Pending", forKey: "Status")
    } else  if(indexPath.row == 2){
        UserDefaults.standard.set("Assign_Pending_To_Me", forKey: "Status")
    }else {
        UserDefaults.standard.set("Change", forKey: "Status")
    }
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DisplayHazardTableViewCell
    let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
    
    let ReportedHazardVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController

    self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        }
    
    
}
}
extension DisplayHazardandActionViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(hazarddisplay == 1) {
        return arrayHazAction.count
        } else if(hazarddisplay == 5) {
            return arrayHazAction.count
        }
        else {
            return arrayActionReq.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DisplayHazardTableViewCell
              cell.selectionStyle = UITableViewCellSelectionStyle.none
        if(hazarddisplay == 1) {
            if(arrayHazAction[indexPath.row] == "Open") {
               cell.lblShowHazAction.textColor = UIColor.darkText
            } else {
                cell.lblShowHazAction.textColor = UIColor.darkGray
            }
            print(arrayHazActionCount[indexPath.row])
            cell.lblShowHazAction.text = arrayHazAction[indexPath.row]
         cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
            cell.lblAddData.text = String(arrayHazActionCount[indexPath.row])
            return cell
        
        } else if(hazarddisplay == 5) {
            if(arrayHazAction[indexPath.row] == "Open") {
                cell.lblShowHazAction.textColor = UIColor.darkText
            } else {
                cell.lblShowHazAction.textColor = UIColor.darkGray
            }
            print(arrayHazActionCount[indexPath.row])
            cell.lblShowHazAction.text = arrayHazAction[indexPath.row]
             cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
            cell.lblAddData.text = String(arrayHazActionCount[indexPath.row])
            return cell
            
        } else {
            cell.lblShowHazAction.textColor = UIColor.darkGray
            cell.lblShowHazAction.text = arrayActionReq[indexPath.row]
             cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
            cell.lblAddData.text = String(arrayActionReqCount[indexPath.row])
            return cell
        }
        
    }
}

