//
//  ReportHazardThemeViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 12/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Alamofire
class ReportHazardThemeViewController: CommonVSClass,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

  
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Report Hazard"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
           tableView.reloadData()
    }
    func reset(){
//        FilterDataFromServer.theme_Id = Int()
//        FilterDataFromServer.theme = String()
//
//        FilterDataFromServer.dept_Id = Int()
//        FilterDataFromServer.dept_Name = String()
//
//        FilterDataFromServer.filterType = String()
//        updateData()
//        textViewIdea.text = PLACEHOLDER_TEXT
//        textViewBenefits.text = PLACEHOLDER_TEXT
//        self.imageData.isHidden = true;
//        self.buttonCloseImage.isHidden = true;
//
//        self.imageData.image = nil
//        imagedataValue = nil
//
//
//        self.lblClick.isHidden = false;
//        self.imgCmaeraicon.isHidden = false;
//        btnSubmit.frame = CGRect(x: 5, y: lblClick.frame.origin.y + lblClick.frame.height + 30, width: self.view.frame.width - 10, height: 40)
//        scrollView.updateContentView()
//        scrollView.frame.size.height = self.view.frame.size.height
        
    }
    var reachablty = Reachability()!
    @IBAction func SubmitHazardClicked(_ sender: UIButton) {
//        "{
//        ""Employee_ID"":""1001""
//        ,""Hazard_Type_ID"":""1""
//        ,""Unit_ID"":""1""
//        ,""Zone_ID"":""1""
//        ,""Area_ID"":""1""
//        ,""Sub_Area_ID"":""1""
//        ,""Description"":""! St Test""
//        ,""Platform"":""Android""
//        ,""Mac_Device_ID"":""252515582585""
//        ,""EMP_Type"":""E""
//        ,""Hazard_device_ID"":""62381585858""
//        ,""Caption"":""Hi ybtg""
//        ,""Theme_ID"":""1""
//
//    } file, file1"
    
    
    
    
    
    
    if reachablty.connection == .none{
    
    
//    if FilterDataFromServer.location_name == ""  {
//
//    self.view.makeToast("Please Select Zone")
//    }
//    else  if FilterDataFromServer.unit_Name == ""  {
//
//        self.view.makeToast("Please Select Unit")
//    }
//    else  if FilterDataFromServer.area_Name == ""  {
//
//        self.view.makeToast("Please Select Area")
//    }
//    else  if FilterDataFromServer.sub_Area_Name == ""  {
//
//        self.view.makeToast("Please Select Sub Area")
//    }
//    else  if FilterDataFromServer.hazard_Name == ""  {
//
//        self.view.makeToast("Please Select Hazard Type")
//    }
//    else if FilterDataFromServer.theme == ""  {
//
//    self.view.makeToast("Please Select Theme")
//    } else if cellData.textViewCaption.text == ""  {
//
//        self.view.makeToast("Please Enter Caption")
//    }else if cellData.textViewDesc.text == ""  {
//
//        self.view.makeToast("Please Enter Description")
//    }
//
//    else{
//    let dateFormatter2 = DateFormatter()
//    dateFormatter2.timeZone = NSTimeZone.system
//    dateFormatter2.dateFormat = "ddMMyyyyHHmmssSSS"
//    let date_TimeStr = dateFormatter2.string(from: Date())
//    //  let hazardUniqueId = date_TimeStr + UserDefaults.standard.string(forKey: "EmployeeID")! + String(FilterDataFromServer.hazard_Id) + String(FilterDataFromServer.area_Id) + String(FilterDataFromServer.location_id) + String(FilterDataFromServer.sub_Area_Id) + String(FilterDataFromServer.department_Id)
//    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
//
//    let actionSheetController: UIAlertController = UIAlertController(title: "No Network Connection", message: "Do you want to save hazard report locally? When network connection will be available, it will be Sync.", preferredStyle: .actionSheet)
//    self.startLoadingPK(view: self.view)
//    let report = UIAlertAction(title: "YES", style: .default, handler: { (alert) in
//    if(self.cellData.imageViewHazard.image != nil && self.cellData.imageViewHazard.image != UIImage(named: "placed")) {
//    var res : UIImage = UIImage()
//    //  res =  (self.imageData.image?.resizedTo1MB())!
//    let size = CGSize(width: 200, height: 200)
//    res = self.imageResize(image: self.self.cellData.imageViewHazard.image!,sizeChange: size)
//    self.imageCameraHazard = UIImageJPEGRepresentation(res, 1.0)!
//
//    }
//        if(self.cellData.imageViewSelfie.image != nil && self.cellData.imageViewSelfie.image != UIImage(named: "placed")) {
//            var res : UIImage = UIImage()
//            //  res =  (self.imageData.image?.resizedTo1MB())!
//            let size = CGSize(width: 200, height: 200)
//            res = self.imageResize(image: self.self.cellData.imageViewSelfie.image!,sizeChange: size)
//            self.imageCameraSelfie = UIImageJPEGRepresentation(res, 1.0)!
//
//        }
//
//      self.saveOfflineHazardReportData(unique : hazardUniqueId)
//    self.stopLoading(view: self.view)
//    self.alert(title: "Report Hazard Saved successfully")
//    self.reset()
//
//    })
//
//
//    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
//    // self.reset()
//    self.stopLoadingPK(view: self.view)
//    })
//
//    actionSheetController.addAction(report)
//    actionSheetController.addAction(cancel)
//    self.present(actionSheetController, animated: true, completion: nil)
//    //self.stopLoading(view: self.view)
//
//    }
//
//
//    }
//
        
        
    self.view.makeToast("Please Check your Internet Connection")
    }
    else  if FilterDataFromServer.location_name == ""  {
        
        self.view.makeToast("Please Select Zone")
    }
    else  if FilterDataFromServer.unit_Name == ""  {
        
        self.view.makeToast("Please Select Unit")
    }
    else  if FilterDataFromServer.area_Name == ""  {
        
        self.view.makeToast("Please Select Area")
    }
    else  if FilterDataFromServer.sub_Area_Name == ""  {
        
        self.view.makeToast("Please Select Sub Area")
    }
    else  if FilterDataFromServer.hazard_Name == ""  {
        
        self.view.makeToast("Please Select Hazard Type")
    }
    else if FilterDataFromServer.theme == ""  {
        
        self.view.makeToast("Please Select Theme")
    } else if cellData.textViewCaption.text == ""  {
        
        self.view.makeToast("Please Enter Caption")
    }else if cellData.textViewDesc.text == ""  {
        
        self.view.makeToast("Please Enter Description")
    }
    
    
    else{
    
   
    
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "ddMMyyyyHHmmssSSS"
            let date_TimeStr = dateFormatter2.string(from: Date())
              let hazardUniqueId = date_TimeStr + UserDefaults.standard.string(forKey: "EmployeeID")! + String(FilterDataFromServer.hazard_Id) + String(FilterDataFromServer.area_Id) + String(FilterDataFromServer.location_id) + String(FilterDataFromServer.sub_Area_Id) + String(FilterDataFromServer.department_Id)
    
     
    
    self.startLoadingPK(view: self.view)
    
    DispatchQueue.global(qos: .background).async {
    print(UserDefaults.standard.string(forKey: "LoginType")!)
    let parameter = [
    "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!,
    "Hazard_Type_ID": String(FilterDataFromServer.hazard_Id),
    "Unit_ID": String(FilterDataFromServer.unit_Id) ,
    "Zone_ID": String(FilterDataFromServer.location_id),
    "Area_ID": String(FilterDataFromServer.area_Id) ,
    "Sub_Area_ID": String(FilterDataFromServer.sub_Area_Id),
    "Description": self.cellData.textViewDesc.text,
    "Platform": URLConstants.platform ,
    "Mac_Device_ID": hazardUniqueId,
    "EMP_Type": "E" ,
    "Hazard_device_ID": hazardUniqueId,
    "Theme_ID": String(FilterDataFromServer.theme_Id),
    "Caption": self.cellData.textViewCaption.text
    ] as [String : String]
    print(parameter)
    
    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
    let para: [String: Data] = ["data": jsonData!]
        if(self.cellData.imageViewSelfie.image != nil && self.cellData.imageViewSelfie.image != UIImage(named: "placed")) {
            var res : UIImage = UIImage()
            //  res =  (self.imageData.image?.resizedTo1MB())!
            let size = CGSize(width: 200, height: 200)
            res = self.imageResize(image: self.self.cellData.imageViewSelfie.image!,sizeChange: size)
            self.imageCameraSelfie = UIImageJPEGRepresentation(res, 1.0)!
            
        }
    Alamofire.upload(multipartFormData: { multipartFormData in
    
    if !(self.imageCameraSelfie == nil) && !(self.imageCameraHazard == nil) {
    
    multipartFormData.append(self.imageCameraHazard!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
         multipartFormData.append(self.imageCameraSelfie!, withName: "file1", fileName: "file.jpg", mimeType: "image/jpeg")
    }
    
    for (key, value) in para {
    multipartFormData.append(value, withName: key)
    
    
    }
    
    },
    to:URLConstants.themeSafetyNewHazard)
    { (result) in
    switch result {
    case .success(let upload, _, _):
    
    upload.uploadProgress(closure: { (progress) in
    
    })
    
    
    upload.responseJSON { response in
    
    
    if let json = response.result.value
    {
    var dict = json as! [String: AnyObject]
    
    if let response = dict["response"]{
    print(response)
    let statusString = response["status"] as! String
    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
    self.stopLoadingPK(view: self.view)
    if(statusString == "success")
    {
    
    self.stopLoadingPK(view: self.view)
    self.reset()
    let actionSheetController: UIAlertController = UIAlertController(title:"Thank You", message: msg, preferredStyle: .actionSheet)
    
    let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
    
    
  
    self.navigationController?.popViewController(animated: true)
   
    
    
    }
    actionSheetController.addAction(cancelAction)
    
    let image = UIImage(named: "rassafetysuccess")
    let vc = UIViewController()
    vc.preferredContentSize = CGSize(width:150,height: 150)
    
    let actionSheetControllerWidth = actionSheetController.view.frame.size.width
    
    let width = (actionSheetControllerWidth-155)/2
    
    let imageView = UIImageView(frame:CGRect(x: width , y:0, width:150, height:150))
    
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
    
    imageView.image = image
    
    vc.view.addSubview(imageView)
    
    actionSheetController.setValue(vc, forKey: "contentViewController")
    
    actionSheetController.popoverPresentationController?.sourceView = sender as? UIView;
    
    self.present(actionSheetController, animated: true, completion: nil)
    
    
    
    }
    else{
    
    self.stopLoadingPK(view: self.view)
    self.view.makeToast( msg)
    
    
    
    }
    
    //  self.stopLoadingPK()
    
    }
    
    //self.stopLoadingPK()
    }
    
    // self.stopLoadingPK()
    }
    
    case .failure(let encodingError):
    
    self.errorChecking(error: encodingError)
    self.stopLoadingPK(view: self.view)
    print(encodingError.localizedDescription)
    }
    //self.stopLoadingPK()
    }
    // self.stopLoadingPK()
    
    }
    }
    
    
    
        
        
    }
    var imageCameraSelfie: Data? = nil
    var imageCameraHazard: Data? = nil
    var camHaz : Int = 0
     var camSelfie : Int = 0
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async {
                if(self.cameraInt == 1){
                    self.cellData.imageViewHazard.image = image
                    self.cellData.btnCloseHazard.isHidden = false
                    self.cellData.viewHazardClose.isHidden = false
                    self.camHaz = 1
                }
                if(self.cameraInt == 2){
                     self.cellData.imageViewSelfie.image = image
                    self.cellData.btnCloseSelfie.isHidden = false
                     self.cellData.viewSelfieClose.isHidden = false
                   self.camSelfie = 1
                }
            }
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    var cameraInt = Int()
    @IBAction func btnCloseCameraHazardClicked(_ sender: UIButton) {
      cellData.imageViewHazard.image = UIImage(named: "placed")
        cellData.btnCloseHazard.isHidden = true
        self.cellData.viewHazardClose.isHidden = true
        self.camHaz = 0
    }
    @IBAction func btnCloseCameraSelfieClicked(_ sender: UIButton) {
        cellData.imageViewSelfie.image = UIImage(named: "placed")
           cellData.btnCloseSelfie.isHidden = true
        cellData.viewSelfieClose.isHidden = true
        self.camSelfie = 0
    }
     @IBAction func btnCameraHazardClicked(_ sender: UIButton) {
        cameraInt = 1
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnCameraSelfieClicked(_ sender: UIButton) {
        cameraInt = 2
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnZoneClicked(_ sender: UIButton) {
        
            FilterDataFromServer.filterType = "Location"
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.location_name = String()
            FilterDataFromServer.location_id = Int()
            updateData()
            
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Zone"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    @IBAction func btnUnitClicked(_ sender: UIButton) {
         if(FilterDataFromServer.location_name != "") {
        FilterDataFromServer.filterType = "Unit"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        updateData()
        
        
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Unit"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        }
    }
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
            
        }
    }
    @IBAction func btnSubAreaClicked(_ sender: UIButton) {
        
          if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "" ) {
            FilterDataFromServer.filterType = "Sub Area"
           
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area Location"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
    }
    @IBAction func btnHazardClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Hazard"
        FilterDataFromServer.hazard_Id = Int()
        FilterDataFromServer.hazard_Name = String()
        
       
        updateData()
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Hazard Type"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func btnThemeClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Theme"
        FilterDataFromServer.theme_Id = Int()
        FilterDataFromServer.theme = String()
        
        
        updateData()
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Theme"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    let imagePicker = UIImagePickerController()
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func updateData(){
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "" && FilterDataFromServer.sub_Area_Name != "" ) {
            cellData.lblSubArea.text = FilterDataFromServer.sub_Area_Name
            cellData.lblSubArea.textColor = UIColor.black
        } else {
            cellData.lblSubArea.text = "Select Area Location"
            cellData.lblSubArea.textColor = UIColor.lightGray
        }
        
        if(FilterDataFromServer.location_name != "") {
            cellData.lblZone.text = FilterDataFromServer.location_name
            cellData.lblZone.textColor = UIColor.black
        } else {
            cellData.lblZone.text = "Select Zone"
            cellData.lblZone.textColor = UIColor.lightGray
        }
        
        if(FilterDataFromServer.theme != "") {
            cellData.lblTheme.text = FilterDataFromServer.theme
            cellData.lblTheme.textColor = UIColor.black
        } else {
            cellData.lblTheme.text = "Select Theme"
            cellData.lblTheme.textColor = UIColor.lightGray
        }
        
        if(FilterDataFromServer.hazard_Name != "") {
            cellData.lblhazard.text = FilterDataFromServer.hazard_Name
            cellData.lblhazard.textColor = UIColor.black
        } else {
            cellData.lblhazard.text = "Select Hazard"
            cellData.lblhazard.textColor = UIColor.lightGray
        }
        if(cellData.imageViewHazard.image == nil){
            cellData.imageViewHazard.image = UIImage(named: "placed")
        }
        if(cellData.imageViewSelfie.image == nil){
            cellData.imageViewSelfie.image = UIImage(named: "placed")
        }
    }
    var cellData : ReportHazardThemeTableViewCell!
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.0
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
            
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportHazardThemeTableViewCell
        cellData = cell
        updateData()
        if(self.camHaz == 0){
            cell.btnCloseHazard.isHidden = true
            
            cell.viewHazardClose.isHidden = true
        }
        if(self.camSelfie == 0){
           cell.btnCloseSelfie.isHidden = true
            cell.viewSelfieClose.isHidden = true
        }
        
        
            return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
