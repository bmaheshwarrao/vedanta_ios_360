//
//  ReportedHazardTableViewCell.swift
//  VallSafety
//
//  Created by Bunga Maheshwar Rao on 04/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class ReportedHazardTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewHeightDesc: NSLayoutConstraint!
    @IBOutlet weak var viewPlaced: UIViewX!
    @IBOutlet weak var lblSatatus2: UILabel!
    @IBOutlet weak var lblDesc: UITextView!
    @IBOutlet weak var lblTransFormData: UITextView!
    @IBOutlet weak var imgData: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
