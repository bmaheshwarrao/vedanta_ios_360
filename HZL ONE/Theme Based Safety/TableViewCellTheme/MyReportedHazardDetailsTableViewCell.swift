//
//  MyReportedHazardDetailsTableViewCell.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 13/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class MyReportedHazardDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewInDetails: UIImageView!
    
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var assignedLabel: UITextView!
    @IBOutlet weak var assignManagerBtn: UIButton!
    
    @IBOutlet weak var areaManagersLbl: UILabel!
    @IBOutlet weak var noManagerlbl: UILabel!
    @IBOutlet weak var dateAssignedLbl: UILabel!
    @IBOutlet weak var assignedReminderBtn: UIButton!
    @IBOutlet weak var assignedLabelPlaced: UILabel!
    @IBOutlet weak var assignHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusColorView: UIViewX!
    
    @IBOutlet weak var statusOfHazard: UIButton!
    
//    @IBOutlet weak var viewRiskLevel: UIView!
//    @IBOutlet weak var riskLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
