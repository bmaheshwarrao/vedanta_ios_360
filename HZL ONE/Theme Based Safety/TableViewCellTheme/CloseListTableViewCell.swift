//
//  CloseListTableViewCell.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 19/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class CloseListTableViewCell: UITableViewCell {

    @IBOutlet weak var viewImageHold: UIView!
    @IBOutlet weak var viewImageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageFile: UIImageView!
    
     @IBOutlet weak var employeeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    // @IBOutlet weak var statusColorView: UIViewX!
    
    @IBOutlet weak var titleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var statusViewHeight: NSLayoutConstraint!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var viewData: UIViewX!
    @IBOutlet weak var statusOfHazard: UILabel!
    
    @IBOutlet weak var heightRiskLevel: NSLayoutConstraint!
    @IBOutlet weak var viewRiskLevel: UIView!
    @IBOutlet weak var image1left: NSLayoutConstraint!
    @IBOutlet weak var statusColorView: UIImageView!
    @IBOutlet weak var idOfHazard: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var image2left: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleTextView.font = UIFont.systemFont(ofSize: 15.0)
        employeeLabel.font = UIFont.systemFont(ofSize: 15.0)
        dateLabel.font = UIFont.systemFont(ofSize: 15.0)
        idOfHazard.font = UIFont.systemFont(ofSize: 15.0)
        statusOfHazard.font = UIFont.systemFont(ofSize: 15.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
