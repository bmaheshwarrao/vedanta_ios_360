//
//  MyReportedHazardDetailsAssignManagerTableViewCell.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 13/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class MyReportedHazardDetailsAssignManagerTableViewCell: UITableViewCell {
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var employeeMobileNoLabel: UILabel!
    @IBOutlet weak var employeeEmaillabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
