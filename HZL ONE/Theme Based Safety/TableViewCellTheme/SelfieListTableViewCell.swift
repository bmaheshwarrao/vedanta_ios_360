//
//  SelfieListTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 13/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SelfieListTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewData: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblemp: UILabel!
    @IBOutlet weak var imageViewSelfie: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
