//
//  LeadershipModel.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 15/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import Foundation


class Leadership: NSObject {

 

    var PointsCr = String()
    var PointsDr = String()
    var Employee_ID = String()
    var Total_balance = String()
    var Employee_Name = String()


    func setDataInModel(str:[String:AnyObject])
    {
      
        if str["Employee_ID"] is NSNull || str["Employee_ID"] == nil {
            self.Employee_ID = ""
        }else{
            let emp1 = str["Employee_ID"]
            
            self.Employee_ID = (emp1?.description)!
        }
        if str["PointsCr"] is NSNull || str["PointsCr"] == nil{
            self.PointsCr = ""
        }else{
            let emp1 = str["PointsCr"]
            
            self.PointsCr = (emp1?.description)!
        }
        if str["PointsDr"] is NSNull || str["PointsDr"] == nil{
            self.PointsDr = ""
        }else{
            let emp1 = str["PointsDr"]
            
            self.PointsDr = (emp1?.description)!
        }
        if str["Employee_Name"] is NSNull || str["Employee_Name"] == nil{
            self.Employee_Name = ""
        }else{
            let emp1 = str["Employee_Name"]
            
            self.Employee_Name = (emp1?.description)!
        }
       
        if str["Total_balance"] is NSNull || str["Total_balance"] == nil{
            self.Total_balance = ""
        }else{
            let emp1 = str["Total_balance"]
            
            self.Total_balance = (emp1?.description)!
        }
        
    }
    
    
    
}



class LeadershipData
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:LeaderShipViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
                  WebServices.sharedInstances.sendGetRequest(Url: URLConstants.Leader_Board, successHandler: { (dict) in
                
                if let response = dict["response"]{
              
             
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                     
                        obj.stopLoading(view: obj.tableView)
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [Leadership] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = Leadership()
                                    object.setDataInModel(str: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableView)
                    }
                    else
                    {
                        
                        
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        obj.label.isHidden = false
                        //  obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                //  obj.stopLoading()
            })
            
            
        }
        else{
           
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
           
            obj.tableView.isHidden = true
           // obj.noDataLabel(text: "No Data Found")
            obj.refresh.endRefreshing()
            obj.stopLoading()
            obj.label.isHidden = false
            // obj.stopLoading()
        }
        
        
    }
    
    
}





class ThemeClubPointsListDataModel: NSObject {
    
    var PointsCr = String()
    var PointsDr = String()
    var DateTime = String()
    var Total_PointsCr = String()
    var Total_PointsDr = String()
    var Total_Balance = String()
    var Description = String()
    var ID = String()
    func setDataInModel(str:[String:AnyObject])
    {
        if str["ID"] is NSNull || str["ID"] == nil {
            self.ID = ""
        }else{
            let emp1 = str["ID"]
            
            self.ID = (emp1?.description)!
        }
        
        if str["Description"] is NSNull || str["Description"] == nil {
            self.Description = ""
        }else{
            let emp1 = str["Description"]
            
            self.Description = (emp1?.description)!
        }
        if str["PointsCr"] is NSNull || str["PointsCr"] == nil{
            self.PointsCr = ""
        }else{
            let emp1 = str["PointsCr"]
            
            self.PointsCr = (emp1?.description)!
        }
        if str["PointsDr"] is NSNull || str["PointsDr"] == nil{
            self.PointsDr = ""
        }else{
            let emp1 = str["PointsDr"]
            
            self.PointsDr = (emp1?.description)!
        }
        if str["DateTime"] is NSNull || str["DateTime"] == nil{
            self.DateTime = ""
        }else{
            let emp1 = str["DateTime"]
            
            self.DateTime = (emp1?.description)!
        }
        if str["Total_PointsCr"] is NSNull || str["Total_PointsCr"] == nil{
            self.Total_PointsCr = ""
        }else{
            let emp1 = str["Total_PointsCr"]
            
            self.Total_PointsCr = (emp1?.description)!
        }
        if str["Total_PointsDr"] is NSNull || str["Total_PointsDr"] == nil{
            self.Total_PointsDr = ""
        }else{
            let emp1 = str["Total_PointsDr"]
            
            self.Total_PointsDr = (emp1?.description)!
        }
        if str["Total_Balance"] is NSNull || str["Total_Balance"] == nil{
            self.Total_Balance = ""
        }else{
            let emp1 = str["Total_Balance"]
            
            self.Total_Balance = (emp1?.description)!
        }
        
    }
}

class ThemeClubPointsListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ThemeScoreBoardViewController, parameter : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Point_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ThemeClubPointsListDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ThemeClubPointsListDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        obj.label.isHidden = false
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
            obj.label.isHidden = false
        }
        
        
    }
    
    
}
class ThemeClubPointsListDataAPILoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ThemeScoreBoardViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Point_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ThemeClubPointsListDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ThemeClubPointsListDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No data found" )
                        obj.refresh.endRefreshing()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            
        }
        
        
    }
    
    
}
