//
//  HazardDetailsViewController.swift
//  VallSafety
//
//  Created by Bunga Maheshwar Rao on 04/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class HazardDetailsViewController: CommonVSClass,UITableViewDelegate, UITableViewDataSource {
    
    
    //var description : String = ""
    
    @IBOutlet weak var assignedlblPlaced: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var heightForward: NSLayoutConstraint!
    @IBOutlet weak var heightAssign: NSLayoutConstraint!
    @IBOutlet weak var heightReject: NSLayoutConstraint!
    var MyReportedHazardDetailsAPI = MyReportedHazardDetailsDataAPI()
    var MyReportedHazardDB:[MyReportedHazardDetailsDataModel] = []
    
    var AreaManagersListAPI = AreaManagersDataListDataInDetailsAPI()
    var AreaManagersListDB:[AreaManagersDataListDataInDetailsModel] = []
    
    @IBOutlet weak var btnReject: UIButton!
    var hazardID = String()
    var empId = String()
    let statusReported : String = UserDefaults.standard.value(forKey: "Status") as! String
    let hazarddisplay = UserDefaults.standard.integer(forKey: "hazard")
    var sta = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        refresh.addTarget(self, action: #selector(callMyReportedHazardData), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        print(statusReported)
        
        
        widthAssign.constant = self.view.frame.width/3
        widthForward.constant = self.view.frame.width/3
        widthReject.constant = self.view.frame.width/3
        
        if(hazarddisplay != 4) {
            if(statusReported == "Change") {
                btnAssign.isHidden = false
                btnForward.isHidden = false
                heightAssign.constant = 50
                heightForward.constant = 50
            }else{
                btnAssign.isHidden = true
                btnForward.isHidden = true
                heightAssign.constant = 0
                heightForward.constant = 0
            }
        }else{
            btnAssign.isHidden = true
            btnForward.isHidden = true
            heightAssign.constant = 0
            heightForward.constant = 0
        }
    }
    var reachability = Reachability()!
    
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callMyReportedHazardData()
            }
            else
            {
                self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
            }
            
        }
    }
    
    func createDesign() {
        tableView.isScrollEnabled = false
        let  btnAssign = UIButton()
        btnAssign.frame = CGRect(x: 0, y: self.view.frame.height - 100, width: self.view.frame.width/2, height: 40)
        
        btnAssign.addTarget(self, action: #selector(pressAssign(_:)), for: UIControlEvents.touchUpInside)
        
        btnAssign.backgroundColor = UIColor.colorwithHexString("75BF44", alpha: 1.0)
        let lblAss = UILabel(frame: CGRect(x: 0, y: 5, width: btnAssign.frame.width, height: 30))
        lblAss.text = "Assign"
        lblAss.textColor = UIColor.white
        lblAss.textAlignment = NSTextAlignment.center
        // lblAss.font  = UIFont(name: "Georgia", size: 13.0)
        btnAssign.addSubview(lblAss)
        
        btnAssign.clipsToBounds = true;
        
        
        
        tableView.addSubview(btnAssign)
        
        
        let  btnForward = UIButton()
        btnForward.frame = CGRect(x: self.view.frame.width/2, y: self.view.frame.height - 100, width: self.view.frame.width/2, height: 40)
        
        btnForward.addTarget(self, action: #selector(pressForward(_:)), for: UIControlEvents.touchUpInside)
        
        btnForward.backgroundColor = UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
        let lblFor = UILabel(frame: CGRect(x: 0, y: 5, width: btnForward.frame.width, height: 30))
        lblFor.text = "Forward"
        lblFor.textColor = UIColor.white
        lblFor.textAlignment = NSTextAlignment.center
        //lblFor.font  = UIFont(name: "Georgia", size: 13.0)
        btnForward.addSubview(lblFor)
        
        btnForward.clipsToBounds = true;
        
        
        
        tableView.addSubview(btnForward)
        
        
        //        let  btnReject = UIButton()
        //        btnReject.frame = CGRect(x: 2 * (self.view.frame.width/3), y: self.view.frame.height - 100, width: self.view.frame.width/2, height: 40)
        //
        //       btnReject.addTarget(self, action: #selector(pressReject(_:)), for: UIControlEvents.touchUpInside)
        //
        //        btnReject.backgroundColor = UIColor.red
        //        let lblRej = UILabel(frame: CGRect(x: 0, y: 5, width: btnReject.frame.width, height: 30))
        //        lblRej.text = "Reject"
        //        lblRej.textColor = UIColor.white
        //        lblRej.textAlignment = NSTextAlignment.center
        //        //lblRej.font  = UIFont(name: "Georgia", size: 13.0)
        //        btnReject.addSubview(lblRej)
        //
        //        btnReject.clipsToBounds = true;
        //        tableView.addSubview(btnReject)
    }
    @IBAction func pressReject(_ sender : UIButton) {
        

//
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "RejectViewController") as! RejectViewController
        ZIVC.Hazardname = self.MyReportedHazardDB[0].Hazard_Name!
        ZIVC.AreaName = self.MyReportedHazardDB[0].area_Name!
        ZIVC.SubAreaName = self.MyReportedHazardDB[0].subarea_Name!
        ZIVC.ZoneName = self.MyReportedHazardDB[0].Zone_Name!
        ZIVC.UnitName = self.MyReportedHazardDB[0].Unit_Name!
        ZIVC.employeeId = self.MyReportedHazardDB[0].Employee_ID!
        ZIVC.employeeName = self.MyReportedHazardDB[0].Employee_Name!
      //  ZIVC.riskLevel = String(self.MyReportedHazardDB[0].Risk_Level!)
        ZIVC.dateVal = self.MyReportedHazardDB[0].Date_Time!
        ZIVC.desc = self.MyReportedHazardDB[0].Description!
        ZIVC.status = self.MyReportedHazardDB[0].Status!
        
        ZIVC.hazard_ID = String(self.MyReportedHazardDB[0].ID)
             ZIVC.riskLevel = String(describing: MyReportedHazardDB[0].Risk_Level!)
        ZIVC.imgdata = self.MyReportedHazardDB[0].Image_path!
        
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @IBAction func pressForward(_ sender : UIButton) {
        
        
        
        FilterDataFromServer.department_Id = Int()
        FilterDataFromServer.department_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.hazard_Name = String()
        FilterDataFromServer.hazard_Id = Int()
        
        
        
        
        FilterGraphStruct.area_Name = "Select Area"
        FilterGraphStruct.sub_area_Name = "Select Sub-Area"
        FilterGraphStruct.hazard_Name = "Select Hazard Type"
        FilterGraphStruct.department_Name = "Select Department"
        FilterGraphStruct.location_Name = "Select Location"
        FilterGraphStruct.RiskLevel = "Risk Level"
        FilterGraphStruct.minRiskLevel = String()
        FilterGraphStruct.mixRiskLevel = String()
        FilterGraphStruct.department_NameID = String()
        FilterGraphStruct.location_ID = String()
        FilterGraphStruct.area_ID = String()
        FilterGraphStruct.sub_area_ID = String()
        FilterGraphStruct.hazard_Name_ID = String()
        FilterGraphStruct.locationType = String()
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        
        // FilterGraphStruct.filterBylocation_Name = String()
        //  FilterGraphStruct.filterBylocation_ID = String()
        
        
        
        //cross
        //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        // locationViewBtn.backgroundColor = UIColor.clear
        FilterGraphStruct.isHazardView = String()
        FilterGraphStruct.hazard_NameByFilter = String()
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ForwardViewController") as! ForwardViewController
        ZIVC.Hazardname = self.MyReportedHazardDB[0].Hazard_Name!
        ZIVC.AreaName = self.MyReportedHazardDB[0].area_Name!
        ZIVC.SubAreaName = self.MyReportedHazardDB[0].subarea_Name!
        ZIVC.ZoneName = self.MyReportedHazardDB[0].Zone_Name!
        ZIVC.UnitName = self.MyReportedHazardDB[0].Unit_Name!
        ZIVC.employeeId = self.MyReportedHazardDB[0].Employee_ID!
        ZIVC.employeeName = self.MyReportedHazardDB[0].Employee_Name!
        
        ZIVC.dateVal = self.MyReportedHazardDB[0].Date_Time!
        ZIVC.desc = self.MyReportedHazardDB[0].Description!
        ZIVC.status = self.MyReportedHazardDB[0].Status!
        ZIVC.hazardID = String(self.MyReportedHazardDB[0].ID)
        print(hazardID)
        let urlString = self.MyReportedHazardDB[0].Image_path!
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        ZIVC.riskLevel = String(describing: MyReportedHazardDB[0].Risk_Level!)
        ZIVC.imgdata = urlShow!
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
        
        
        
        
    }
    var cellData : MyReportedHazardDetailsTableViewCell!
    @IBOutlet weak var btnAssign: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var widthForward: NSLayoutConstraint!
    @IBOutlet weak var widthAssign: NSLayoutConstraint!
    @IBOutlet weak var widthReject: NSLayoutConstraint!
    @IBAction func pressAssign(_ sender : UIButton) {
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AssignSavedViewController") as! AssignSavedViewController
        ZIVC.hazard_ID = String(self.MyReportedHazardDB[0].ID)
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true) {
            MoveStruct.isMove = true
            self.navigationController?.popViewController(animated: false)
        }
        self.title = "Hazard Detail"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        callMyReportedHazardData()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return MyReportedHazardDB.count
        }else{
            return self.AreaManagersListDB.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "details", for: indexPath) as! MyReportedHazardDetailsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.areaManagersLbl.isHidden = true;
            
            cell.imageViewInDetails.tag = indexPath.row
            cell.assignManagerBtn.tag = indexPath.row
            cell.assignManagerBtn.layer.cornerRadius = 8
            
            cell.detailsLabel.text = self.MyReportedHazardDB[0].Description!
            
            cell.statusOfHazard.setTitle(self.MyReportedHazardDB[indexPath.row].Status, for: .normal)
            var stattusColor = self.MyReportedHazardDB[indexPath.row].Status
            switch stattusColor {
            case "Close":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
                break;
            case "Pending":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
                break;
            case "Reject":
                cell.statusOfHazard.backgroundColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
                break;
            default:
                break;
            }
            
            
            
            switch self.MyReportedHazardDB[0].Assign_Employee_Name {
            case ""?:
                cell.assignHeight.constant = 40
                cell.bottomHeight.constant = 10
                cell.assignManagerBtn.isHidden = false
                cell.assignedLabel.text = "Not assigned to anyone"
                break;
            case "null"?:
                cell.assignHeight.constant = 40
                cell.bottomHeight.constant = 10
                cell.assignManagerBtn.isHidden = false
                cell.assignedLabel.text = "Not assigned to anyone"
                break;
            default:
                
                cell.assignHeight.constant = 0
                cell.bottomHeight.constant = 0
                cell.assignManagerBtn.isHidden = true
                cell.assignedLabel.text = self.MyReportedHazardDB[0].Assign_Employee_Name! + "- " + String(self.MyReportedHazardDB[0].Assign_Employee_ID!)
                break;
            }
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            dateFormatter.dateFormat = "MMM dd yyyy hh:mm:ss:SSSa"
            print(self.MyReportedHazardDB[indexPath.section].Date_Time!)
            let date = dateFormatter.date(from: self.MyReportedHazardDB[indexPath.section].Date_Time!)
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            
            
            let dateFormatterTime = DateFormatter()
            dateFormatterTime.timeZone = NSTimeZone.system
            dateFormatterTime.dateFormat = "hh:mm a"
            
            switch date {
            case nil:
                let date_TimeStr = dateFormatter2.string(from: Date())
                let time_TimeStr = dateFormatterTime.string(from: Date())
                cell.dateLabel.text = date_TimeStr + " " + time_TimeStr
                break;
            default:
                let date_TimeStr = dateFormatter2.string(from: date!)
                
                let time_TimeStr = dateFormatterTime.string(from: date!)
                cell.dateLabel.text = date_TimeStr + " " + time_TimeStr
                break;
            }
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .left
            paragraph.lineSpacing = 0
            
            var mBuilder = String()
            
            
            let empIdVal : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            
            var startString : String = "You "
            if(empIdVal != MyReportedHazardDB[0].Employee_ID){
                startString =  "\(self.MyReportedHazardDB[0].Employee_Name!)-\(self.MyReportedHazardDB[0].Employee_ID!)" + " "
            }
            
            mBuilder = startString  + "submitted hazard of type " + self.MyReportedHazardDB[0].Hazard_Name! + " for location " +  self.MyReportedHazardDB[0].Zone_Name! + " area " + self.MyReportedHazardDB[0].Unit_Name! +
                " subarea " + self.MyReportedHazardDB[0].area_Name! + " department " +  self.MyReportedHazardDB[0].subarea_Name!
            
            
            
            //let mBuilder = startString  + "Submitted hazard of type "  +  self.MyReportedHazardDB[0].Hazard_Name  +  " for Location "  + self.MyReportedHazardDB[0].Zone_Name!  + " area "  + self.MyReportedHazardDB[0].Unit_Name!  + " subarea "  + self.MyReportedHazardDB[0].area_Name!  + " department " + self.MyReportedHazardDB[0].subarea_Name!
            
            let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            
            
            //Submitted
            let SubmittedAttributedString = NSAttributedString(string:"submitted hazard of type ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let range: NSRange = (agreeAttributedString.string as NSString).range(of: "submitted hazard of type ")
            if range.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
            }
            
            //for
            let forAttributedString = NSAttributedString(string:"for location ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for location ")
            if forRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
            }
            
            let areaAttributedString = NSAttributedString(string:"area", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let areaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "area")
            if areaRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: areaRange, with: areaAttributedString)
            }
            
            
            
            
            let subAreaAttributedString = NSAttributedString(string:"subarea", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "subarea")
            if subAreaRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
            }
            
            let deptAttributedString = NSAttributedString(string:"department", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let deptRange: NSRange = (agreeAttributedString.string as NSString).range(of: "department")
            if deptRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: deptRange, with: deptAttributedString)
            }
            
            agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
            
            
            cell.statusTextView.attributedText = agreeAttributedString
            cell.textViewHeight.constant =  cell.statusTextView.contentSize.height
            
            let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
            cell.imageViewInDetails.addGestureRecognizer(postImageTapGesture)
            cell.imageViewInDetails.isUserInteractionEnabled = true
            postImageTapGesture.numberOfTapsRequired = 1
            
            let urlString = self.MyReportedHazardDB[indexPath.section].Image_path!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let url = NSURL(string: urlShow!) {
                cell.imageViewInDetails.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
            }
            
            cell.statusTextView.isEditable = false
            cell.statusTextView.isSelectable = false
            cell.statusTextView.isScrollEnabled = false
            if(hazarddisplay != 4) {
                if(statusReported == "Change") {
                    cell.assignHeight.constant = 0
                    cell.bottomHeight.constant = 0
                    cell.assignManagerBtn.isHidden = true
                    cell.assignedLabel.isHidden = true
                    cell.assignedLabelPlaced.isHidden = true
                }
            }
            
            if(MyReportedHazardDB[0].Assign_Employee_ID != "") {
                
                
                
                if(statusReported == "Assign_Pending_To_Me"){
                    cell.assignedReminderBtn.isHidden = false;
                    cell.assignedReminderBtn.isEnabled = true
                }else if(self.MyReportedHazardDB[0].Status == "Pending"){
                    cell.assignedReminderBtn.isHidden = false;
                    cell.assignedReminderBtn.isEnabled = true
                }
                else {
                    cell.assignedReminderBtn.isHidden = true;
                    cell.assignedReminderBtn.isEnabled = false
                }
            } else {
                cell.assignedReminderBtn.isHidden = true;
                cell.assignedReminderBtn.isEnabled = false
            }
            if(self.AreaManagersListDB.count == 0 && lblInt == 1) {
                cell.areaManagersLbl.isHidden = true;
                cell.noManagerlbl.isHidden = false
                
            } else if(self.AreaManagersListDB.count > 0 ) {
                cell.assignHeight.constant = 0
                cell.bottomHeight.constant = 0
                cell.assignManagerBtn.isHidden = true
                cell.areaManagersLbl.isHidden = false;
                cell.noManagerlbl.isHidden = true
            }
            else {
                
                cell.areaManagersLbl.isHidden = true;
                cell.noManagerlbl.isHidden = true
            }
            
            cellData = cell
//            let cal : Int = Int(MyReportedHazardDB[0].Risk_Level!)
//            cell.riskLabel.textAlignment = NSTextAlignment.center
//            cell.riskLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
//            if(cal == 0){
//                cell.riskLabel.text =  String(cal) + "-" + "Low"
//                cell.viewRiskLevel.backgroundColor = UIColor.clear
//            }
//            else if(cal <= 4 && cal > 0) {
//                cell.riskLabel.text =  String(cal) + "-" + "Low"
//
//                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("87ceeb", alpha: 1.0)
//            }  else if(cal <= 9 && cal > 4) {
//                cell.riskLabel.text =  String(cal) + "-" + "Medium"
//                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
//            } else if(cal <= 16 && cal > 9 ) {
//                cell.riskLabel.text =   String(cal) + "-" + "High"
//                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("ffa500", alpha: 1.0)
//            } else if(cal <= 25 && cal > 16 ) {
//                cell.riskLabel.text =  String(cal) + "-" + "Very High"
//                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("FF0000", alpha: 1.0)
//            }
       
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! MyReportedHazardDetailsAssignManagerTableViewCell
            
            switch AreaManagersListDB[indexPath.row].name {
            case "":
                cell.employeeNameLabel.text = "**********"
                break;
            default:
                cell.employeeNameLabel.text = "\(AreaManagersListDB[indexPath.row].name)"
                break;
            }
            switch AreaManagersListDB[indexPath.row].mobileNo {
            case "":
                cell.employeeMobileNoLabel.text = "**********"
                break;
            default:
                cell.employeeMobileNoLabel.text = AreaManagersListDB[indexPath.row].mobileNo
                break;
            }
            switch AreaManagersListDB[indexPath.row].email {
            case "":
                cell.employeeEmaillabel.text = "**********"
                break;
            default:
                cell.employeeEmaillabel.text = AreaManagersListDB[indexPath.row].email
                break;
            }
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(areaManagerViewedTapped(_:)))
            cell.isUserInteractionEnabled = true;
            cell.addGestureRecognizer(tapGesture)
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    var reachablty = Reachability()!
    @IBAction func btnReminderClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = tableView.dequeueReusableCell(withIdentifier: "details", for: indexPath!) as! MyReportedHazardDetailsTableViewCell
        
        
        cellData.assignedReminderBtn.rotate360Degrees()
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        } else{
            
            self.startLoading()
          
            let parameter = ["Hazard_ID":hazardID,
                             "MY_Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
                             
                             "Employee_ID": self.MyReportedHazardDB[0].Assign_Employee_ID!
                
                ] as [String:String]
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Remainder_Assign_But_User_Side_Pending, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                        
                        self.stopLoading()
                       
                        self.view.makeToast(msg)
                        self.tableView.reloadData()
                        
                    }
                    
                    
                }else{
                    self.stopLoading()
                    self.view.makeToast(msg)
                    self.tableView.reloadData()
                }
                
            }) { (err) in
                self.stopLoading()
                print(err.description)
                self.tableView.reloadData()
            }
            
        }
        
        
        
        
        
    }
    @IBOutlet weak var assignManagerButtonWidthConstraint: NSLayoutConstraint!
    @objc func callMyReportedHazardData() {
        if(self.reachability.connection != .none)
        {
            
            self.MyReportedHazardDetailsAPI.serviceCalling(obj: self, hazardID: self.hazardID, pNo: self.empId) { (dict) in
                
                self.MyReportedHazardDB = [MyReportedHazardDetailsDataModel]()
                self.MyReportedHazardDB = dict as! [MyReportedHazardDetailsDataModel]
                self.tableView.reloadData()
                
            }
        } else {
            self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
        }
        
    }
    @IBAction func areaManagerViewedTapped(_ sender: UITapGestureRecognizer) {
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        
        let phoneNo = AreaManagersListDB[(indexPath?.row)!].mobileNo
        
        if(phoneNo != "NULL"  && phoneNo != "") {
        if let url = URL(string: "tel://\(phoneNo)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        }
    }
    
    
    @IBAction func ImageViewTapped(_ sender: UITapGestureRecognizer) {
        if MyReportedHazardDB[0].Image_path! != "" {
            print(MyReportedHazardDB[0].Image_path!)
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoomViewController
            let urlString = self.MyReportedHazardDB[0].Image_path!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            
            ZIVC.zoomImageUrl = urlShow!
            
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }
    }
    
    var lblInt = 0
    @IBAction func assigneManagerAction(_ sender: Any) {
        if(lblInt == 0) {
            switch self.AreaManagersListDB.count {
            case 0:
                
                AreaManagersListAPI.serviceCalling(obj: self, hazard_ID: String(describing: MyReportedHazardDB[0].ID), pNo: MyReportedHazardDB[0].Employee_ID!) { (dict) in
                    
                    self.AreaManagersListDB = [AreaManagersDataListDataInDetailsModel]()
                    self.AreaManagersListDB = dict as! [AreaManagersDataListDataInDetailsModel]
                    self.tableView.reloadData()
                    let indexPath1 = IndexPath(row: 0, section: 0)
                    self.tableView.scrollToRow(at: indexPath1, at: .top, animated: true)
                    //self.lblInt = 1
                }
                print(lblInt)
                if(lblInt == 1) {
                    self.tableView.reloadData()
                }
                break;
            default:
                lblInt = 1;
                self.tableView.reloadData()
                break;
                
                
            }
        }
        
    }
    
    
}
