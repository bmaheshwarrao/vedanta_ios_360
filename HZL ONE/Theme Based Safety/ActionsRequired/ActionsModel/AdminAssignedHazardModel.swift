//
//  AdminAssignedHazardModel.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import Foundation
import Reachability


class AdminAssignedHazardModel:NSObject {
    
    
    
    var Area_ID : Int?
    var Area_Name : String?
    var Assign_Employee_ID : String?
    var Assign_Employee_Name : String?
    var ID = Int()
    var Consequence  : Int?
    var Date_Time : String?
    var Description : String?
    var Employee_ID : String?
    var Employee_Name : String?
    var Hazard_Name : String?
    var Hazard_Type_ID : Int?
    var Image_path : String?
    
    var Likelihood : Int?
    var Risk_Level : Int?
    var Status : String?
    
    
    
    var Status_Employee_ID : String?
    var Status_Employee_Name : String?
    var SubArea_Name : String?
    var Sub_Area_ID : Int?
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["Area_ID"] is NSNull{
            
        }else{
            self.Area_ID = (cell["Area_ID"] as? Int)!
        }
        if cell["Area_Name"] is NSNull{
            self.Area_Name = ""
        }else{
            self.Area_Name = (cell["Area_Name"] as? String)!
        }
        if cell["Assign_Employee_ID"] is NSNull{
            self.Assign_Employee_ID = ""
        }else{
            self.Assign_Employee_ID = (cell["Assign_Employee_ID"] as? String)!
        }
        if cell["Assign_Employee_Name"] is NSNull{
            self.Assign_Employee_Name = ""
        }else{
            self.Assign_Employee_Name = (cell["Assign_Employee_Name"] as? String)!
        }
        if cell["ID"] is NSNull{
            
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        if cell["Consequence"] is NSNull{
            
        }else{
            self.Consequence = (cell["Consequence"] as? Int)!
        }
        
        if cell["Date_Time"] is NSNull{
            self.Date_Time = ""
        }else{
            self.Date_Time = (cell["Date_Time"] as? String)!
        }
        
        if cell["Description"] is NSNull{
            self.Description = ""
        }else{
            self.Description = (cell["Description"] as? String)!
        }
        if cell["Employee_ID"] is NSNull{
            self.Employee_ID = ""
        }else{
            self.Employee_ID = (cell["Employee_ID"] as? String)!
        }
        if cell["Employee_Name"] is NSNull{
            self.Employee_Name = ""
        }else{
            self.Employee_Name = (cell["Employee_Name"] as? String)!
        }
        
        if cell["Hazard_Name"] is NSNull{
            self.Hazard_Name = ""
        }else{
            self.Hazard_Name = (cell["Hazard_Name"] as? String)!
        }
        if cell["Hazard_Type_ID"] is NSNull{
            
        }else{
            self.Hazard_Type_ID = (cell["Hazard_Type_ID"] as? Int)!
        }
        if cell["Image_path"] is NSNull{
            self.Image_path = ""
        }else{
            self.Image_path = (cell["Image_path"] as? String)!
        }
        
        if cell["Likelihood"] is NSNull{
            
        }else{
            self.Likelihood = (cell["Likelihood"] as? Int)!
        }
        if cell["Risk_Level"] is NSNull{
            
        }else{
            self.Risk_Level = (cell["Risk_Level"] as? Int)!
        }
        
        
        if cell["Status"] is NSNull{
            self.Status = ""
        }else{
            self.Status = (cell["Status"] as? String)!
        }
        if cell["Status_Employee_ID"] is NSNull{
            self.Status_Employee_ID = ""
        }else{
            self.Status_Employee_ID = (cell["Status_Employee_ID"] as? String)!
        }
        if cell["Status_Employee_Name"] is NSNull{
            self.Status_Employee_Name = ""
        }else{
            self.Status_Employee_Name = (cell["Status_Employee_Name"] as? String)!
        }
        if cell["SubArea_Name"] is NSNull{
            self.SubArea_Name = ""
        }else{
            self.SubArea_Name = (cell["SubArea_Name"] as? String)!
        }
        if cell["Sub_Area_ID"] is NSNull{
            
        }else{
            self.Sub_Area_ID = (cell["Sub_Area_ID"] as? Int)!
        }
        
        
        
    }
    
    
    
}

class AdminAssignedHazardAPI
{

    var reachablty = Reachability()!



    func serviceCalling(obj:AssignSavedViewController,pNo:String, success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()

        if(reachablty.connection != .none)
        {
            


            WebServices.sharedInstances.sendPostRequest(url: URLConstants.List_New_Hazard, parameters: ["pNo" : pNo, "status":"Pending"], successHandler: { (dict) in

                if let response = dict["response"]{

                    let statusString : String = response["status"] as! String
                  let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )

                        if let data = dict["data"] as? [AnyObject]
                        {

                            var dataArray = [AdminAssignedHazardModel]()

                            for i in 0..<data.count
                            {

                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = AdminAssignedHazardModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)

                                }

                            }

                            success(dataArray as AnyObject)
                            obj.refresh.endRefreshing()
                            obj.stopLoading()
                            //obj.tableView.reloadData()

                        }


                    }
                    else
                    {

                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()

                    }


                }




            })
            { (error) in

                let errorStr : String = error.description

                print("DATA:error",errorStr)

               // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            

        }
        else{

            obj.refresh.endRefreshing()
            obj.stopLoading()
        }


    }


}

