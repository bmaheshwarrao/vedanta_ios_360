//
//  ForwardViewController.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//
import UIKit
import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import SDWebImage

class ForwardViewController: CommonVSClass {
 
    
    @IBOutlet weak var tableView: UITableView!
    //    var ZoneDB:[Zone_DB] = []
//    var UnitDB:[Zone_DB] = []
  
//    var HazardTypeDB:[HazardTypeList] = []
    
//    var zone_NameArray = [String]()
//    var unit_NameArray = [String]()
    
    var Area_NameArray = [String]()
    var Sub_Area_NameArray = [String]()
    //var hazard_NameArray = [String]()
  
    var AreaIDVal = String()
//    var UnitIDVal = String()
//    var ZoneIDVal = String()
    var SubAreaIDVal = String()
//    var HazardIDVal = String()
    @IBOutlet weak var viewForward: UIView!
    var riskLevel = String()
    
    @IBOutlet weak var textViewDesc: UITextView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    var employeeId = String()
    var AreaName = String()
    var SubAreaName = String()
    
    var ZoneName = String()
    var UnitName = String()
    var Hazardname = String()
    var desc = String()
    var dateVal = String()
    var status = String()
    var imgdata = String()
    var employeeName = String()
    var hazardID = String()
    var MyReportedHazardDetailsAPI = HazardDetailsDataAPIForward()
    var MyReportedHazardDB:[MyReportedHazardDetailsDataModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        print(hazardID)
       
        

        if(imgdata == "") {
            self.tableView.reloadData()
        } else {
            showOnlineData()
        }
        // Do any additional setup after loading the view.
        
        
    }
    @objc func showOnlineData() {
        
        self.MyReportedHazardDetailsAPI.serviceCalling(obj: self, hazardID: self.hazardID, pNo: UserDefaults.standard.string(forKey: "EmployeeID")!) { (dict) in
            
            self.MyReportedHazardDB = [MyReportedHazardDetailsDataModel]()
            self.MyReportedHazardDB = dict as! [MyReportedHazardDetailsDataModel]
            
            
            self.employeeId = self.MyReportedHazardDB[0].Employee_ID!
            self.employeeName = self.MyReportedHazardDB[0].Employee_Name!
            self.AreaName = self.MyReportedHazardDB[0].area_Name!
            self.SubAreaName = self.MyReportedHazardDB[0].subarea_Name!
            self.UnitName = self.MyReportedHazardDB[0].Unit_Name!
            self.ZoneName = self.MyReportedHazardDB[0].Zone_Name!
            self.dateVal = self.MyReportedHazardDB[0].Date_Time!
            
            
            
            
           
            let urlString = self.MyReportedHazardDB[0].Image_path!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            self.imgdata = urlShow!
            
            
            self.desc = self.MyReportedHazardDB[0].Description!
            self.hazardID = String(self.MyReportedHazardDB[0].ID)
            self.Hazardname = self.MyReportedHazardDB[0].Hazard_Name!
            self.status = self.MyReportedHazardDB[0].Status!
            self.tableView.reloadData()
            
        }
    }
//    func ShowDataOffline(){
//
//
//
//
//        let empIdVal : String = UserDefaults.standard.string(forKey: "EmployeeID")!
//
//
//
//
//
//        let paragraph = NSMutableParagraphStyle()
//        paragraph.alignment = .left
//        paragraph.lineSpacing = 0
//        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
//        var startString : String = "You "
//        if(empId != employeeId){
//            startString =  employeeName + "-" +   String(employeeId) + " "
//        }
//
//        let  mBuilder = startString  + "Submitted hazard of type " + Hazardname + " for Location " +  ZoneName + " area " + UnitName +
//            " subarea " + AreaName + " department " +  SubAreaName
//
//        let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
//
//
//        //Submitted
//        let SubmittedAttributedString = NSAttributedString(string:"Submitted hazard of type ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
//
//        let range: NSRange = (agreeAttributedString.string as NSString).range(of: "Submitted hazard of type ")
//        if range.location != NSNotFound {
//            agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
//        }
//
//        //for
//        let forAttributedString = NSAttributedString(string:"for Location ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
//
//        let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for Location ")
//        if forRange.location != NSNotFound {
//            agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
//        }
//
//        let areaAttributedString = NSAttributedString(string:"area", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
//
//        let areaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "area")
//        if areaRange.location != NSNotFound {
//            agreeAttributedString.replaceCharacters(in: areaRange, with: areaAttributedString)
//        }
//
//
//
//
//        let subAreaAttributedString = NSAttributedString(string:"subarea", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
//        let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "subarea")
//        if subAreaRange.location != NSNotFound {
//            agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
//        }
//
//        let deptAttributedString = NSAttributedString(string:"department", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
//
//        let deptRange: NSRange = (agreeAttributedString.string as NSString).range(of: "department")
//        if deptRange.location != NSNotFound {
//            agreeAttributedString.replaceCharacters(in: deptRange, with: deptAttributedString)
//        }
//
//        agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
//
//        print(agreeAttributedString)
//       textView.attributedText = agreeAttributedString
//
//
//
//
//        lblId.text = "ID #" + hazardID
//
//
//
//        print(textView.contentSize.height)
//        //  textViewHeight.constant =  textView.contentSize.height + 150
//        let paragraph1 = NSMutableParagraphStyle()
//        paragraph1.alignment = .left
//        paragraph1.lineSpacing = 0
//        let titleStr = NSMutableAttributedString(string: desc, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
//        titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
//        textViewDesc.attributedText = titleStr
//        textViewDesc.textContainer.maximumNumberOfLines = 3
//        textViewDesc.textContainer.lineBreakMode = .byTruncatingTail
//        textViewDesc.textColor = UIColor.colorwithHexString("295890", alpha: 1.0)
//        lblStatus.text = status
//        switch lblStatus.text! {
//        case "Close":
//            lblStatus.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
//            break;
//        case "Pending":
//            lblStatus.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
//            break;
//        case "Reject":
//            lblStatus.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
//            break;
//        default:
//            break;
//        }
//        if let url = NSURL(string: imgdata) {
//
//            imgView.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "placed"))
//        }
//        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = NSTimeZone.system
//        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
//        let date = dateFormatter.date(from: dateVal)
//
//        let dateFormatter2 = DateFormatter()
//        dateFormatter2.timeZone = NSTimeZone.system
//        dateFormatter2.dateFormat = "dd MMM yyyy"
//
//        switch date {
//        case nil:
//            let date_TimeStr = dateFormatter2.string(from: Date())
//            lbldate.text = date_TimeStr
//            break;
//        default:
//            let date_TimeStr = dateFormatter2.string(from: date!)
//
//            lbldate.text = date_TimeStr
//            break;
//        }
//        let layoutManager:NSLayoutManager = textViewDesc.layoutManager
//        let numberOfGlyphs = layoutManager.numberOfGlyphs
//        var numberOfLines = 0
//        var index = 0
//        var lineRange:NSRange = NSRange()
//
//        while (index < numberOfGlyphs) {
//            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
//            index = NSMaxRange(lineRange);
//            numberOfLines = numberOfLines + 1
//        }
//
//        let layoutManager1:NSLayoutManager = textView.layoutManager
//        let numberOfGlyphs1 = layoutManager1.numberOfGlyphs
//        var numberOfLines1 = 0
//        var index1 = 0
//        var lineRange1:NSRange = NSRange()
//
//        while (index1 < numberOfGlyphs1) {
//            layoutManager1.lineFragmentRect(forGlyphAt: index1, effectiveRange: &lineRange1)
//            index1 = NSMaxRange(lineRange1);
//            numberOfLines1 = numberOfLines1 + 1
//        }
//
//        print(numberOfLines)
//        var v1 = 0
//        var v2 = 0
//        let urlString = self.imgdata
//        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//        if let url = NSURL(string: urlShow!) {
//            imgView.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
//            if(numberOfLines == 1 || numberOfLines == 0) {
//                v1 = 40
//            }  else if(numberOfLines == 2) {
//                v1 = 50
//            } else {
//                v1 = 60
//            }
//            if(numberOfLines1 == 1 || numberOfLines1 == 0) {
//                v2 = 60
//            }  else if(numberOfLines1 == 2) {
//                v2 = 70
//            }else if(numberOfLines1 == 3) {
//                v2 = 85
//            }
//            else {
//                v2 = 95
//            }
//
//            heightImage.constant = CGFloat(v1 + v2)
//            print(heightImage.constant)
//            print(url)
//        } else {
//            imgView.image =  UIImage(named: "placed")
//
//            if(numberOfLines == 1 || numberOfLines == 0) {
//                v1 = 40
//            }  else if(numberOfLines == 2) {
//                v1 = 50
//            } else {
//                v1 = 60
//            }
//            if(numberOfLines1 == 1 || numberOfLines1 == 0) {
//                v2 = 60
//            }  else if(numberOfLines1 == 2) {
//                v2 = 70
//            }else if(numberOfLines1 == 3) {
//                v2 = 85
//            }
//            else {
//                v2 = 95
//            }
//            heightImage.constant = CGFloat(v1 + v2)
//            print(heightImage.constant)
//        }
//        print(textViewDesc.attributedText.size().height)
//        // heightImage.constant = 120
//        image1Left.constant = self.view.frame.width/3
//        image2Left.constant = self.view.frame.width/3
//        //   cell.heightImage.constant = cell.statusTextView.contentSize.height + cell.titleTextView.contentSize.height
//
//        let cal : Int = Int(self.riskLevel)!
//
//        if(cal <= 4 ) {
//
//            viewRiskLevel.backgroundColor = UIColor.colorwithHexString("87ceeb", alpha: 1.0)
//        } else if(cal <= 9 && cal > 4) {
//
//            viewRiskLevel.backgroundColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
//        } else if(cal <= 16 && cal > 9 ) {
//
//            viewRiskLevel.backgroundColor = UIColor.colorwithHexString("ffb6c1", alpha: 1.0)
//        } else if(cal <= 25 && cal > 16 ) {
//
//            viewRiskLevel.backgroundColor = UIColor.colorwithHexString("FF0000", alpha: 1.0)
//        }
//        self.view.updateConstraints()
//
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Forward Hazard"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        updateData()
    }
    var lblArea = UILabel()
    var lblSubArea = UILabel()
     var lblLocation = UILabel()
    var lblDepartment = UILabel()
    func createDesign() {
        for view in cellData.viewData.subviews{
            view.removeFromSuperview()
        }
        
        let btnLocation = UIButton()
        
        btnLocation.frame = CGRect(x: 5, y: 20 , width: cellData.viewData.frame.width - 10, height: 40)
        btnLocation.layer.borderWidth = 1.0
        btnLocation.addTarget(self, action: #selector(pressLocation(_:)), for: UIControlEvents.touchUpInside)
        let imgViewlo = UIImageView(frame: CGRect(x: btnLocation.frame.width - 30, y: 15, width: 15    , height: 15))
        imgViewlo.image = UIImage(named: "downArrow")
        btnLocation.addSubview(imgViewlo)
        
        lblLocation = UILabel(frame: CGRect(x: 10, y: 5, width: btnLocation.frame.width - 40, height: 30))
        lblLocation.text = "Select Zone"
        lblLocation.font  = UIFont.systemFont(ofSize: 17.0)
        lblLocation.textColor = UIColor.lightGray
        btnLocation.addSubview(lblLocation)
        btnLocation.layer.borderColor = UIColor.black.cgColor
        btnLocation.layer.cornerRadius = 10.0
        btnLocation.clipsToBounds = true;
        
        cellData.viewData.addSubview(btnLocation)
        
        let btnArea = UIButton()
        btnArea.frame = CGRect(x: 5, y: btnLocation.frame.origin.y + btnLocation.frame.height + 10, width: cellData.viewData.frame.width - 10, height: 40)
        btnArea.layer.borderWidth = 1.0
        
        btnArea.addTarget(self, action: #selector(pressArea(_:)), for: UIControlEvents.touchUpInside)
        let imgView = UIImageView(frame: CGRect(x: btnArea.frame.width - 30, y: 10, width: 15    , height: 15))
        imgView.image = UIImage(named: "downArrow")
        btnArea.addSubview(imgView)
        
        lblArea = UILabel(frame: CGRect(x: 10, y: 5, width: btnArea.frame.width - 40, height: 30))
        lblArea.text = "Select Unit"
        lblArea.font  = UIFont.systemFont(ofSize: 17.0)
        lblArea.textColor = UIColor.lightGray
        btnArea.addSubview(lblArea)
        btnArea.layer.borderColor = UIColor.black.cgColor
        btnArea.layer.cornerRadius = 10.0
        btnArea.clipsToBounds = true;
        
        cellData.viewData.addSubview(btnArea)
        
        let btnSubArea = UIButton()
        btnSubArea.frame = CGRect(x: 5, y: btnArea.frame.origin.y + btnArea.frame.height + 10, width: cellData.viewData.frame.width - 10, height: 40)
        btnSubArea.layer.borderWidth = 1.0
        btnSubArea.addTarget(self, action: #selector(pressSubArea(_:)), for: UIControlEvents.touchUpInside)
        let imgViewSA = UIImageView(frame: CGRect(x: btnArea.frame.width - 30, y: 10, width: 15    , height: 15))
        imgViewSA.image = UIImage(named: "downArrow")
        btnSubArea.addSubview(imgViewSA)
        
        lblSubArea = UILabel(frame: CGRect(x: 10, y: 5, width: btnSubArea.frame.width - 40, height: 30))
        lblSubArea.text = "Select Area"
        lblSubArea.font  = UIFont.systemFont(ofSize: 17.0)
        lblSubArea.textColor = UIColor.lightGray
        btnSubArea.addSubview(lblSubArea)
        btnSubArea.layer.borderColor = UIColor.black.cgColor
        btnSubArea.layer.cornerRadius = 10.0
        btnSubArea.clipsToBounds = true;
        
        cellData.viewData.addSubview(btnSubArea)
        let btnDepartment = UIButton()
        btnDepartment.frame = CGRect(x: 5, y: btnSubArea.frame.origin.y + btnSubArea.frame.height + 10, width: cellData.viewData.frame.width - 10, height: 40)
        btnDepartment.layer.borderWidth = 1.0
        btnDepartment.addTarget(self, action: #selector(pressDepartment(_:)), for: UIControlEvents.touchUpInside)
        let imgViewde = UIImageView(frame: CGRect(x: btnArea.frame.width - 30, y: 15, width: 15    , height: 15))
        imgViewde.image = UIImage(named: "downArrow")
        btnDepartment.addSubview(imgViewde)
        
        lblDepartment = UILabel(frame: CGRect(x: 10, y: 5, width: btnDepartment.frame.width - 40, height: 30))
        lblDepartment.text = "Select Area Location"
        lblDepartment.font  = UIFont.systemFont(ofSize: 17.0)
        lblDepartment.textColor = UIColor.lightGray
        btnDepartment.addSubview(lblDepartment)
        btnDepartment.layer.borderColor = UIColor.black.cgColor
        btnDepartment.layer.cornerRadius = 10.0
        btnDepartment.clipsToBounds = true;
        
        cellData.viewData.addSubview(btnDepartment)
        
       let btnSubmit = UIButton()
        btnSubmit.frame = CGRect(x: 5, y: btnDepartment.frame.origin.y + btnDepartment.frame.height + 30, width: cellData.viewData.frame.width - 10, height: 40)
 
        btnSubmit.addTarget(self, action: #selector(pressSubmit(_:)), for: UIControlEvents.touchUpInside)
        
        btnSubmit.backgroundColor = UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
        let lblSubmit = UILabel(frame: CGRect(x: 0, y: 5, width: btnSubmit.frame.width, height: 30))
        lblSubmit.text = "Forward"
        lblSubmit.textAlignment = NSTextAlignment.center
        lblSubmit.textColor = UIColor.white
        lblSubmit.font  = UIFont.systemFont(ofSize: 17.0)
        btnSubmit.addSubview(lblSubmit)
        
        btnSubmit.clipsToBounds = true;
        
        cellData.viewData.addSubview(btnSubmit)
    }
    var cellData : ForwardTableViewCell!
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    let reachablty = Reachability()!
     @objc func pressSubmit(_ sender : UIButton) {
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
       else if FilterDataFromServer.location_name == "" {
            
            self.view.makeToast("Please Select Zone")
        }
        else if FilterDataFromServer.unit_Name == "" {
            
            self.view.makeToast("Please Select Unit")
        }
        else if FilterDataFromServer.area_Name == "" {
            
            self.view.makeToast("Please Select Area")
        }
        else if FilterDataFromServer.sub_Area_Name == "" {
            
            self.view.makeToast("Please Select Sub-Area")
        }
        else{
            
           self.startLoadingPK(view: self.view)
          
            let parameter = ["Hazard_ID":hazardID,
                             "Area_ID":String(FilterDataFromServer.area_Id) ,
                             "Sub_Area_ID":String(FilterDataFromServer.sub_Area_Id),
                             "Unit_ID":String(FilterDataFromServer.unit_Id) ,
                             "Zone_ID":String(FilterDataFromServer.location_id),
                             "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                            
                ] as [String:String]
           
          
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Forward_Hazard, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                 self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                   self.stopLoadingPK(view: self.view)
                  
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                     self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
               self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
    
        
        
        
        
    }
    
    
    @objc func pressDepartment(_ sender : UIButton) {
        
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "" ) {
            FilterDataFromServer.filterType = "Sub Area"
            
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area Location"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
        
        
    }
    @objc func pressLocation(_ sender : UIButton) {
        
        
        
        FilterDataFromServer.filterType = "Location"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        updateData()
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Zone"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @objc func pressSubArea(_ sender : UIButton) {
     
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
            
        }
        
    }
    
    @objc func pressArea(_ sender : UIButton) {
        
        if(FilterDataFromServer.location_name != "") {
            FilterDataFromServer.filterType = "Unit"
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Unit"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
    }
    @objc func updateData(){
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            lblArea.text = FilterDataFromServer.unit_Name
            lblArea.textColor = UIColor.black
        } else {
            lblArea.text = "Select Unit"
            lblArea.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "") {
            lblSubArea.text = FilterDataFromServer.area_Name
            lblSubArea.textColor = UIColor.black
        } else {
            lblSubArea.text = "Select Area"
            lblSubArea.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "" && FilterDataFromServer.sub_Area_Name != "") {
            lblDepartment.text = FilterDataFromServer.sub_Area_Name
            lblDepartment.textColor = UIColor.black
        } else {
            lblDepartment.text = "Select Area Location"
            lblDepartment.textColor = UIColor.lightGray
        }
        
        if(FilterDataFromServer.location_name != "") {
            lblLocation.text = FilterDataFromServer.location_name
            lblLocation.textColor = UIColor.black
        } else {
           lblLocation.text = "Select Zone"
           lblLocation.textColor = UIColor.lightGray
        }
        
     
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ForwardViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return UITableViewAutomaticDimension
        }else{
            return 384
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}
extension ForwardViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportedDataTableViewCell
            
            
            cell.statusTextView.tag = indexPath.section
            
            
            cell.statusOfHazard.text = status
            cell.idOfHazard.text =  "ID #" + hazardID
            
            let paragraph1 = NSMutableParagraphStyle()
            paragraph1.alignment = .left
            paragraph1.lineSpacing = 0
            let titleStr = NSMutableAttributedString(string: desc, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
            cell.titleTextView.attributedText = titleStr
            
            
            switch cell.statusOfHazard.text! {
            case "Close":
                cell.statusOfHazard.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
                break;
            case "Pending":
                cell.statusOfHazard.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
                break;
            case "Reject":
                cell.statusOfHazard.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
                break;
            default:
                break;
            }
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
            let date = dateFormatter.date(from: dateVal)
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            
            switch date {
            case nil:
                let date_TimeStr = dateFormatter2.string(from: Date())
                cell.dateLabel.text = date_TimeStr
                break;
            default:
                let date_TimeStr = dateFormatter2.string(from: date!)
                
                cell.dateLabel.text = date_TimeStr
                break;
            }
            
            
            
            //  cell.statusTextView.delegate = self
            
            
            // cell.titleTextView.text = self.hazardDB[indexPath.section].descriptionn
            cell.titleTextView.font = UIFont.systemFont(ofSize: 15.0)
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .left
            paragraph.lineSpacing = 0
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            var startString : String = "You "
            
            if(empId != employeeId){
                startString =  employeeName + "-" +   String(employeeId) + " "
            }
            let  mBuilder = startString  + "Submitted hazard of type " + Hazardname + " for Location " +  ZoneName + " area " + UnitName +
                " subarea " + AreaName + " department " +  SubAreaName
            
            let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            
            
            //Submitted
            let SubmittedAttributedString = NSAttributedString(string:"Submitted hazard of type ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let range: NSRange = (agreeAttributedString.string as NSString).range(of: "Submitted hazard of type ")
            if range.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
            }
            
            //for
            let forAttributedString = NSAttributedString(string:"for Location ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for Location ")
            if forRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
            }
            
            let areaAttributedString = NSAttributedString(string:"area", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let areaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "area")
            if areaRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: areaRange, with: areaAttributedString)
            }
            
            
            
            
            let subAreaAttributedString = NSAttributedString(string:"subarea", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "subarea")
            if subAreaRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
            }
            
            let deptAttributedString = NSAttributedString(string:"department", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let deptRange: NSRange = (agreeAttributedString.string as NSString).range(of: "department")
            if deptRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: deptRange, with: deptAttributedString)
            }
            
            agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
            
            print(agreeAttributedString)
            
            print(cell.statusTextView.contentSize.height)
            cell.statusTextView.attributedText = agreeAttributedString
            cell.statusTextView.font = UIFont.systemFont(ofSize: 15.0)
            
            
            
            let imageViewDataCell = UIImageView()
            imageViewDataCell.image = UIImage(named : "placed")
            //        let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
            //        if(cell.imageFile != nil) {
            //            cell.imageFile.addGestureRecognizer(postImageTapGesture)
            //            cell.imageFile.isUserInteractionEnabled = true
            //            postImageTapGesture.numberOfTapsRequired = 1
            //        }
            //
            
            
            
            
            
            
            
            cell.statusTextView.isEditable = false
            cell.statusTextView.isSelectable = false
            cell.statusTextView.isScrollEnabled = false
            cell.titleTextView.isEditable = false
            cell.titleTextView.isSelectable = false
            cell.titleTextView.isScrollEnabled = false
            cell.titleTextView.textContainer.maximumNumberOfLines = 3
            cell.titleTextView.textContainer.lineBreakMode = .byTruncatingTail
            cell.titleTextView.textColor = UIColor.colorwithHexString("295890", alpha: 1.0)
            cell.selectionStyle = .none
            cell.image1left.constant = self.view.frame.width/3
            cell.image2left.constant = self.view.frame.width/3
            
            let layoutManager:NSLayoutManager = cell.titleTextView.layoutManager
            let numberOfGlyphs = layoutManager.numberOfGlyphs
            var numberOfLines = 0
            var index = 0
            var lineRange:NSRange = NSRange()
            
            while (index < numberOfGlyphs) {
                layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
                index = NSMaxRange(lineRange);
                numberOfLines = numberOfLines + 1
            }
            
            let layoutManager1:NSLayoutManager = cell.statusTextView.layoutManager
            let numberOfGlyphs1 = layoutManager1.numberOfGlyphs
            var numberOfLines1 = 0
            var index1 = 0
            var lineRange1:NSRange = NSRange()
            
            while (index1 < numberOfGlyphs1) {
                layoutManager1.lineFragmentRect(forGlyphAt: index1, effectiveRange: &lineRange1)
                index1 = NSMaxRange(lineRange1);
                numberOfLines1 = numberOfLines1 + 1
            }
            //  cell.heightImage.constant = cell.viewImageHeight.constant
            
            var v1 = 0
            var v2 = 0
            
            let imageView = UIImageView()
            let urlString = self.imgdata
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let url = NSURL(string: self.imgdata) {
                
                SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil) {
                        let size = CGSize(width: 120, height: 100)
                        let imageCell = self.imageResize(image: image!, sizeChange: size)
                        cell.imageFile.image = imageCell
                        //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                        
                    }else{
                        imageView.image =  UIImage(named: "placed")
                        let size = CGSize(width: 120, height: 100)
                        let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                        cell.imageFile.image = imageCell
                        // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    }
                })
            }else{
                imageView.image =  UIImage(named: "placed")
                let size = CGSize(width: 120, height: 100)
                let imageCell = imageResize(image: imageView.image!, sizeChange: size)
                cell.imageFile.image = imageCell
                //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
            }
            
            //   cell.heightImage.constant = cell.statusTextView.contentSize.height + cell.titleTextView.contentSize.height
            if(riskLevel == ""){
                riskLevel = "0"
            }
            let cal : Int = Int(riskLevel)!
            
            if(cal == 0 ) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.clear
            }else if(cal <= 4 && cal > 0) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("87ceeb", alpha: 1.0)
            }
            else if(cal <= 9 && cal > 4) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
            } else if(cal <= 16 && cal > 9 ) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("ffa500", alpha: 1.0)
            } else if(cal <= 25 && cal > 16 ) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("FF0000", alpha: 1.0)
            }
            
            //   cell.heightImage.constant = cell.heightRiskLevel.constant
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "forward", for: indexPath) as! ForwardTableViewCell
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cellData = cell
             self.createDesign()
            return cell
        }
        
        
        
        
        
        
        
        
    }
}




