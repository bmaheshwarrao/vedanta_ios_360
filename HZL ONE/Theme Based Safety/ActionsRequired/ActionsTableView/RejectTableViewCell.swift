//
//  RejectTableViewCell.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 04/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class RejectTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
@IBOutlet weak var textViewData: UITextView!
    @IBOutlet weak var btnReject: UIButton!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
