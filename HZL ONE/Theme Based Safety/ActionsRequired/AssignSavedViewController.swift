//
//  AssignSavedViewController.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import Reachability
import CoreData

class AssignSavedViewController: CommonVSClass, UISearchBarDelegate,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var employeeSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var assignDetailsDB:[AdminAssignedHazardModel] = []
    
    let reachablty = Reachability()!
    //    var indPath = IndexPath()
    //    var spec = Bool()
    var searchBarActiveOrNot : Bool = false
    var hazard_ID = String()
    
    var managerPNo = String()
    
    var areaManagersModel: [AreaManagersAssignListDataModel] = []
    var areaManagersAPI = AreaManagersAssignListDataAPI()
    
    //Save locally
    
    var AreaManagerDB:[AreaManager] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        getAreaManagerListData()
        // spec = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
     self.employeeSearchBar.delegate = self
       self.employeeSearchBar.returnKeyType = UIReturnKeyType.done
       // self.employeeSearchBar.becomeFirstResponder()
        
        tableView.estimatedRowHeight = 124
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Assign Manager"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.employeeSearchBar.endEditing(true)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchBarActiveOrNot == false{
            return self.AreaManagerDB.count
        }else{
            return self.areaManagersModel.count
        }
        
    }
    
    var LastSelected : IndexPath?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! AssignSavedTableViewCell
        
        cell.clearBtn.tag = indexPath.row
        
        switch searchBarActiveOrNot {
        case false:
            
            cell.clearBtn.isEnabled = true
            cell.clearBtn.isHidden = false
            switch AreaManagerDB[indexPath.row].name {
            case "":
                cell.employeeNameLabel.text = "**********"
                break;
            default:
                cell.employeeNameLabel.text = AreaManagerDB[indexPath.row].name!
                break;
            }
            switch AreaManagerDB[indexPath.row].mobileNo {
            case "":
                cell.employeeMobileNoLabel.text = "**********"
                break;
            case "NULL":
                cell.employeeMobileNoLabel.text = "**********"
                break;
            default:
                cell.employeeMobileNoLabel.text = AreaManagerDB[indexPath.row].mobileNo
                break;
            }
            switch AreaManagerDB[indexPath.row].email {
            case "":
                cell.employeeEmaillabel.text = "**********"
                break;
            default:
                cell.employeeEmaillabel.text = AreaManagerDB[indexPath.row].email
                break;
            }
            
            break;
        default:
            
            cell.clearBtn.isEnabled = false
            cell.clearBtn.isHidden = true
            
            
            switch areaManagersModel[indexPath.row].name {
            case "":
                cell.employeeNameLabel.text = "**********"
                break;
            default:
                cell.employeeNameLabel.text = areaManagersModel[indexPath.row].name
                break;
            }
            switch areaManagersModel[indexPath.row].mobileNo {
            case "":
                cell.employeeMobileNoLabel.text = "**********"
                break;
            case "NULL":
                cell.employeeMobileNoLabel.text = "**********"
                break;
            default:
                cell.employeeMobileNoLabel.text = areaManagersModel[indexPath.row].mobileNo
                break;
            }
            switch areaManagersModel[indexPath.row].email {
            case "":
                cell.employeeEmaillabel.text = "**********"
                break;
            default:
                cell.employeeEmaillabel.text = areaManagersModel[indexPath.row].email
                break;
            }
            
            break;
        }
        
        
        
        //        if self.LastSelected == indexPath && spec == true {
        //           cell.areaView.backgroundColor = UIColor.lightGray
        //
        //        }else{
        //           cell.areaView.backgroundColor = UIColor.white
        //        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch searchBarActiveOrNot {
        case false:
            managerPNo = AreaManagerDB[indexPath.row].empId!
            break;
        default:
            managerPNo = String(Int(areaManagersModel[indexPath.row].empId))
            saveAreaManagerData(email: areaManagersModel[indexPath.row].email, mobileNo: areaManagersModel[indexPath.row].mobileNo, name: areaManagersModel[indexPath.row].name, empId: String(Int(areaManagersModel[indexPath.row].empId)))
            break;
        }
        
        
        //
        
        //     let cell = tableView.cellForRow(at: indexPath) as! AssignSavedTableViewCell
        
        //
        //        if cell.areaView.backgroundColor == UIColor.lightGray {
        //
        //            cell.areaView.backgroundColor = UIColor.white
        //            spec = false
        //
        //        }else{
        //
        //            self.LastSelected = indexPath
        //            spec = true
        //            tableView.reloadData()
        //
        //
        //        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /// SEARCH BAR DELEGATES
    
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        // return NO to not become first responder
        
        return true
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        
    }
    
    public func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool
    {
        // return NO to not resign first responder
        
        return true
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        
        self.employeeSearchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
         self.employeeSearchBar.endEditing(true)
      // self.employeeSearchBar.resignFirstResponder()
     // IQKeyboardManager.shared.enable = true;
    }
    
    
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchBarActiveOrNot = true
        managerPNo = String()
        
        if searchText != ""{
            searchAreaManagerFromServer(searchText: searchText)
        }else{
            self.tableView.isHidden = true
            self.noDataLabel(text: "")
        }
        
    }
    
    func searchAreaManagerFromServer(searchText:String)
    {
        
        
        self.areaManagersAPI.serviceCalling(obj: self, name: searchText) { (dict) in
            
            self.areaManagersModel = dict as! [AreaManagersAssignListDataModel]
            
            if self.areaManagersModel.count>0{
                
                self.tableView.isHidden = false
                self.noDataLabel(text: "")
                
            }else{
                self.tableView.isHidden = true
                self.noDataLabel(text: "Sorry! No area manager Found")
            }
            
            self.tableView.reloadData()
            
        }
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.employeeSearchBar.endEditing(true)
        
    }
    
    
    @IBAction func assignActionBtn(_ sender: Any) {
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }else if managerPNo == ""{
            self.view.makeToast("Please select Area manager")
        }else{
            
            if searchBarActiveOrNot == false && self.AreaManagerDB.count == 0{
                //
            }else{
              
                self.startLoadingPK(view: self.view)
               // hazard_ID = String(Int(assignDetailsDB[0].ID))
                let parameter = ["Hazard_ID":hazard_ID,
                                 "Employee_ID":managerPNo,
                                 "My_Employee_ID":  UserDefaults.standard.string(forKey: "EmployeeID")!
                    ] as [String:String]
               
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.Assign_Hazard,parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                    
                    let respon = response["response"] as! [String:AnyObject]
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                  
                    if respon["status"] as! String == "success" {
                         self.stopLoadingPK(view: self.view)
                         
                        MoveStruct.isMove = true
                        MoveStruct.message = msg
                        self.navigationController?.popViewController(animated: false)
                        
                        
                    }else{
                       self.stopLoadingPK(view: self.view)
                        self.view.makeToast(msg)
                    }
                    
                }) { (err) in
                     self.stopLoadingPK(view: self.view)
                    print(err.description)
                }
            }
        }
    }
    
    @IBAction func clearActionManager(_ sender: Any) {
        
        managerPNo = String()
        
        switch  searchBarActiveOrNot {
        case false:
            self.getContext().delete(self.AreaManagerDB[(sender as AnyObject).tag])
            getAreaManagerListData()
        default:
            break;
        }
        
    }
    
    func saveAreaManagerData (email:String,mobileNo:String,name:String,empId:String){
        
        let entries : NSArray = self.AreaManagerDB as NSArray
        let predicate = NSPredicate(format: "empId = %@ ", empId)
        let FavouriteData = entries.filtered(using: predicate) as! [AreaManager]
        switch FavouriteData.count {
        case 0:
            
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let tasks = AreaManager(context: context)
            
            tasks.email = email
            tasks.mobileNo = mobileNo
            tasks.name = name
            tasks.empId = empId
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            CheckingAreaManagerListData()
            
            break;
        default:
            break;
        }
        
    }
    
    
    @objc func getAreaManagerListData() {
        
        switch searchBarActiveOrNot {
        case false:
            
            self.AreaManagerDB = [AreaManager]()
            let fetchRequest: NSFetchRequest<AreaManager> = AreaManager.fetchRequest()
            fetchRequest.returnsObjectsAsFaults = false
            
            do {
                self.AreaManagerDB = try context.fetch(fetchRequest)
                self.tableView.isHidden = false
                self.noDataLabel(text: "")
                self.label.isHidden = true
                self.tableView.reloadData()
                
            } catch {
                self.tableView.isHidden = true
                self.label.isHidden = false
                print("Cannot fetch Expenses")
            }
            
            break;
        default:
            break;
        }
        
        
    }
    
    @objc func CheckingAreaManagerListData() {
        
        self.AreaManagerDB = [AreaManager]()
        let fetchRequest: NSFetchRequest<AreaManager> = AreaManager.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            self.AreaManagerDB = try context.fetch(fetchRequest)
        } catch {
            print("Cannot fetch Expenses")
        }
        
    }
    
    func getContext () -> NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
}
