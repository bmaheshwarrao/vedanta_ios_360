//
//  SearchHazardResultTableViewCell.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class SearchHazardResultTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageFile: UIImageView!
    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // @IBOutlet weak var statusColorView: UIViewX!
    
    @IBOutlet weak var statusOfHazard: UILabel!
    
    @IBOutlet weak var idOfHazard: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
