//
//  TopTenCloserViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 07/02/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class TopTenCloserViewController: CommonVSClass,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var totalHazard: UILabel!
    
    var TopCloserApi = TopCloserAPI()
    var TopCloserDB : [TopCloserModel] = []
    var TopCloserLoadMoreDB : [TopCloserModel] = []
    var TopCloserLoadMoreApi = TopCloserLoadMoreAPI()
    
    var data: String?
    var lastObject: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if FilterAchieverStruct.topAchieverFlag != "Hazard" {
            totalHazard.text = "Total Observation"
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        
        tableView.estimatedRowHeight = 113
        tableView.rowHeight = UITableViewAutomaticDimension
        
        refresh.addTarget(self, action: #selector(self.callTopCloserData), for: .valueChanged)
        self.tableView.addSubview(refresh)
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.callTopCloserData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "fromTopTenTypeFilter")), object: nil)
        
         callTopCloserData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callTopCloserData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
                
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TopCloserDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Closer", for: indexPath) as! TopTenCloserTableViewCell
        
        cell.totalHazard.text = String(describing: self.TopCloserDB[indexPath.row].Total_Count)
        cell.employeeName.text = self.TopCloserDB[indexPath.row].Employee_Name
       
        
        self.data = String(self.TopCloserDB[indexPath.row].Employee_ID)
        self.lastObject = String (self.TopCloserDB[indexPath.row].Employee_ID)
        
        if ( self.data ==  self.lastObject && indexPath.row == self.TopCloserDB.count - 1)
        {
            
            self.callTopCloserDataLoadMore(SrNo: String(self.TopCloserDB[indexPath.row].SrNo))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func callTopCloserData(){
        
        
        switch FilterAchieverStruct.topAchieverFlag {
        case "Hazard":
            
            var para = [String:String]()
            
            let parameter = [
                "FromDate":FilterAchieverStruct.fromDate,
                "ToDate":FilterAchieverStruct.toDate
                ,"Hazard_Type_ID":FilterAchieverStruct.hazard_Name_ID
                ,"Unit_ID":FilterAchieverStruct.unit_Id
                ,"Zone_ID":FilterAchieverStruct.location_id
                ,"Area_ID":FilterAchieverStruct.area_Id
                ,"Sub_Area_ID":FilterAchieverStruct.sub_Area_Id]
            
            para = parameter.filter { $0.value != ""}
            
            
            
            print("para",para)
         
            self.TopCloserApi.serviceCalling(obj: self, Url: URLConstants.Dashbord_List_HAZARD_CLOSER, parameter: para) { (dict) in
                
                self.TopCloserDB = [TopCloserModel]()
                self.TopCloserDB = dict as! [TopCloserModel]
                
                self.tableView.reloadData()
                
            }
            
            break;
        default:
            
            var para = [String:String]()
            
            let parameter = [
                "SrNo":""]
            
            para = parameter.filter { $0.value != ""}
            
            
            
            print("para",para)
            
            self.TopCloserApi.serviceCalling(obj: self, Url: URLConstants.hazard_by_status, parameter: para) { (dict) in
                
                self.TopCloserDB = [TopCloserModel]()
                self.TopCloserDB = dict as! [TopCloserModel]
                
                self.tableView.reloadData()
                
            }
            
            break;
        }
        
        
      
    }
    
    @objc func callTopCloserDataLoadMore(SrNo:String){
        
        
        switch FilterAchieverStruct.topAchieverFlag {
        case "Hazard":
            
            var para = [String:String]()
            
            let parameter = [
                "FromDate":FilterAchieverStruct.fromDate,
                "ToDate":FilterAchieverStruct.toDate
                ,"Hazard_Type_ID":FilterAchieverStruct.hazard_Name_ID
                ,"Unit_ID":FilterAchieverStruct.unit_Id
                ,"Zone_ID":FilterAchieverStruct.location_id
                ,"Area_ID":FilterAchieverStruct.area_Id
                ,"Sub_Area_ID":FilterAchieverStruct.sub_Area_Id,
                 "SrNo":SrNo]
            
            para = parameter.filter { $0.value != ""}
            
            
            
            print("para",para)
            
            self.TopCloserLoadMoreApi.serviceCalling(obj: self, Url: URLConstants.hazard_by_status, parameter: para) { (dict) in
                
                self.TopCloserLoadMoreDB = [TopCloserModel]()
                self.TopCloserLoadMoreDB = dict as! [TopCloserModel]
                switch self.TopCloserLoadMoreDB.count {
                case 0:
                    break;
                default:
                    self.TopCloserDB.append(contentsOf: self.TopCloserLoadMoreDB)
                    self.tableView.reloadData()
                    break;
                }
                
                
            }
            
            break;
        default:
            
            var para = [String:String]()
            
            let parameter = [
                 "SrNo":SrNo]
            
            para = parameter.filter { $0.value != ""}
            
            
            
            print("para",para)
            
            self.TopCloserLoadMoreApi.serviceCalling(obj: self, Url: URLConstants.hazard_by_status, parameter: para) { (dict) in
                
                self.TopCloserLoadMoreDB = [TopCloserModel]()
                self.TopCloserLoadMoreDB = dict as! [TopCloserModel]
                switch self.TopCloserLoadMoreDB.count {
                case 0:
                    break;
                default:
                    self.TopCloserDB.append(contentsOf: self.TopCloserLoadMoreDB)
                    self.tableView.reloadData()
                    break;
                }
                
                
            }
            
            break;
        }
        
        
        
    }
    
    
    
}

