//
//  TopTenReportedViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 07/02/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class TopTenReportedViewController: CommonVSClass,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalHazard: UILabel!
    
    var TopReportedApi = TopReportedAPI()
    var TopReportedDB : [TopReportedModel] = []
    var TopReportedLoadMoreDB : [TopReportedModel] = []
    var TopReportedLoadMoreApi = TopReportedLoadMoreAPI()
    
    var data: String?
    var lastObject: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if FilterAchieverStruct.topAchieverFlag != "Hazard" {
            totalHazard.text = "Total Observation"
        }
            
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 113
        tableView.rowHeight = UITableViewAutomaticDimension
        
        refresh.addTarget(self, action: #selector(self.callTopReportedData), for: .valueChanged)
        self.tableView.addSubview(refresh)
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callTopReportedData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "fromTopTenTypeFilter")), object: nil)
        
        callTopReportedData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callTopReportedData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
                
                
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TopReportedDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reported", for: indexPath) as! TopTenReportedTableViewCell
        
        cell.totalHazard.text = String(describing: self.TopReportedDB[indexPath.row].Total_Count)
        cell.employeeName.text = self.TopReportedDB[indexPath.row].Employee_Name
      
        
    
        self.data = self.TopReportedDB[indexPath.row].Employee_Name
        self.lastObject = self.TopReportedDB[indexPath.row].Employee_Name
        
        if ( self.data ==  self.lastObject && indexPath.row == self.TopReportedDB.count - 1)
        {
            
            self.callTopReportedDataLoadMore(SrNo: String(self.TopReportedDB[indexPath.row].SrNo))
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    @objc func callTopReportedData(){
        
        
        switch FilterAchieverStruct.topAchieverFlag {
        case "Hazard":
            
            var para = [String:String]()
           
            
            let parameter = [
                
                "FromDate":FilterAchieverStruct.fromDate,
                "ToDate":FilterAchieverStruct.toDate
                ,"Hazard_Type_ID":FilterAchieverStruct.hazard_Name_ID
                ,"Unit_ID":FilterAchieverStruct.unit_Id
                ,"Zone_ID":FilterAchieverStruct.location_id
                ,"Area_ID":FilterAchieverStruct.area_Id
                ,"Sub_Area_ID":FilterAchieverStruct.sub_Area_Id]
            
            para = parameter.filter { $0.value != ""}
            
            
            print("para",para)
            
            self.TopReportedApi.serviceCalling(obj: self, Url: URLConstants.Dashbord_List_HAZARD_REPORTER, parameter:para) { (dict) in
                
                self.TopReportedDB = [TopReportedModel]()
                self.TopReportedDB = dict as! [TopReportedModel]
                
                self.tableView.reloadData()
                
            }
            
            break;
        default:
            
//            var para = [String:String]()
//
//
//            let parameter = [
//                "SrNo":""]
//
//            para = parameter.filter { $0.value != ""}
//
//
//            print("para",para)
//
//            self.TopReportedApi.serviceCalling(obj: self, Url: URLConstants.hazard_by_status, parameter:para) { (dict) in
//
//                self.TopReportedDB = [TopReportedModel]()
//                self.TopReportedDB = dict as! [TopReportedModel]
//
//                self.tableView.reloadData()
//
//            }
            
            break;
        }
        
       
    }
    
    @objc func callTopReportedDataLoadMore(SrNo:String){
        
        
        switch FilterAchieverStruct.topAchieverFlag {
        case "Hazard":
            
            var para = [String:String]()
            
            
            let parameter = [
                "FromDate":FilterAchieverStruct.fromDate,
                "ToDate":FilterAchieverStruct.toDate
                ,"Hazard_Type_ID":FilterAchieverStruct.hazard_Name_ID
                ,"Unit_ID":FilterAchieverStruct.unit_Id
                ,"Zone_ID":FilterAchieverStruct.location_id
                ,"Area_ID":FilterAchieverStruct.area_Id
                ,"Sub_Area_ID":FilterAchieverStruct.sub_Area_Id,
                 "ID":SrNo]
            
            para = parameter.filter { $0.value != ""}
            
            
            print("para",para)
         
            self.TopReportedLoadMoreApi.serviceCalling(obj: self, Url: URLConstants.hazard_by_status, parameter:para) { (dict) in
                
                self.TopReportedLoadMoreDB = [TopReportedModel]()
                self.TopReportedLoadMoreDB = dict as! [TopReportedModel]
                switch self.TopReportedLoadMoreDB.count {
                case 0:
                    break;
                default:
                    self.TopReportedDB.append(contentsOf: self.TopReportedLoadMoreDB)
                    self.tableView.reloadData()
                    break;
                }
                
            }
            
            break;
        default:
            
            var para = [String:String]()
            
            
//            let parameter = [
//                 "SrNo":SrNo]
//
//            para = parameter.filter { $0.value != ""}
//
//
//            print("para",para)
//
//            self.TopReportedLoadMoreApi.serviceCalling(obj: self, Url: URLConstants.hazard_by_status, parameter:para) { (dict) in
//
//                self.TopReportedLoadMoreDB = [TopReportedModel]()
//                self.TopReportedLoadMoreDB = dict as! [TopReportedModel]
//                switch self.TopReportedLoadMoreDB.count {
//                case 0:
//                    break;
//                default:
//                    self.TopReportedDB.append(contentsOf: self.TopReportedLoadMoreDB)
//                    self.tableView.reloadData()
//                    break;
//                }
//
//            }
            
            break;
        }
        
        
    }
    
    
    
}

