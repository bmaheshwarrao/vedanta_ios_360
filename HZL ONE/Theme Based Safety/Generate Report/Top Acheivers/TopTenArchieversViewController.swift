//
//  TopTenArchieversViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 07/02/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit
import Parchment
import ViewPager_Swift




class TopTenArchieversViewController: CommonVSClass {
    @IBOutlet weak var viewHandler: UIView!
    var views = [UIViewController]()
    
    @IBOutlet weak var filterMenuItem: UIBarButtonItem!
    //Filter
    
    @IBOutlet weak var firstthCon: NSLayoutConstraint!
    
    @IBOutlet weak var secondThCon: NSLayoutConstraint!
    @IBOutlet weak var thirdthCon: NSLayoutConstraint!
    
    @IBOutlet weak var forthcon: NSLayoutConstraint!
    @IBOutlet weak var filterAppcon: NSLayoutConstraint!
    @IBOutlet weak var hazardTypeLabel: UILabel!
    @IBOutlet weak var locationTypeLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var toDateLabel: UILabel!
    @IBOutlet weak var filterAppLabel: UILabel!
    @IBOutlet weak var viewOf: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        
         createDesign()
       
        
    }
    
    
    func createDesign() {
        

        
       
       let  myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = false
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.white
        myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        
        myOptions.tabViewTextDefaultColor = UIColor.white
       // myOptions.tabViewTextHighlightColor = UIColor.yellow
        let viewPager = ViewPagerController()
        // viewPager.view.backgroundColor = UIColor.red
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        self.viewHandler.addSubview(viewPager.view)
      //  self.viewHandler.constrainToEdges(viewPager.view)
        
       // self.view.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Top Achievers"
        switch FilterAchieverStruct.topAchieverFlag {
        case "Hazard":
            filterMenuItem.tintColor = UIColor.white
            break;
        default:
            filterMenuItem.tintColor = UIColor.clear
            break;
        }
        
        self.viewContentShape()
        
//        let nav = self.navigationController?.navigationBar
//        nav?.tintColor = UIColor.white
//       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
//        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(true)
//        
//        FilterAchieverStruct.fromDate = String()
//        FilterAchieverStruct.toDate = String()
//        FilterAchieverStruct.hazard_Name_ID = String()
//        FilterAchieverStruct.business_NameID = String()
//        FilterAchieverStruct.location_ID = String()
//        FilterAchieverStruct.area_ID = String()
//        FilterAchieverStruct.sub_area_ID = String()
//    }

    func viewContentShape(){
        
        
        switch FilterAchieverStruct.department_NameID {
        case "":
            FilterAchieverStruct.locationType = String()
            break;
        default:
            if FilterAchieverStruct.location_name != "" && FilterAchieverStruct.area_Name != "" && FilterAchieverStruct.sub_Area_Name != "" {
                FilterAchieverStruct.locationType = "Department : "+FilterAchieverStruct.sub_Area_Name
            }else{
                //
            }
            
            break;
        }
        switch FilterAchieverStruct.sub_Area_Name {
        case "":
            FilterAchieverStruct.locationType = String()
            break;
        default:
            if FilterAchieverStruct.sub_Area_Name != "" {
                FilterAchieverStruct.locationType = "Area Location : "+FilterAchieverStruct.sub_Area_Name
            }else{
                //
            }
            
            break;
        }
        switch FilterAchieverStruct.area_Name {
        case "":
            FilterAchieverStruct.locationType = String()
            break;
        default:
            if FilterAchieverStruct.sub_Area_Name == "" {
                FilterAchieverStruct.locationType = "Area : "+FilterAchieverStruct.area_Name
            }else{
                //
            }
            
            break;
        }
        
        switch FilterAchieverStruct.unit_Name {
        case "":
            FilterAchieverStruct.locationType = String()
            break;
        default:
            if FilterAchieverStruct.area_Name == "" {
                FilterAchieverStruct.locationType = "Unit : "+FilterAchieverStruct.unit_Name
            }else{
                //
            }
            
            break;
        }
        switch FilterAchieverStruct.location_name {
        case "":
            FilterAchieverStruct.locationType = String()
            break;
        default:
            if FilterAchieverStruct.location_name != "" && FilterAchieverStruct.unit_Name == "" && FilterAchieverStruct.area_Name == "" && FilterAchieverStruct.sub_Area_Name == "" {
                FilterAchieverStruct.locationType = "Zone : "+FilterAchieverStruct.location_name
            }else{
                //
            }
            
            break;
        }
        
        switch FilterAchieverStruct.hazard_Name_ID {
        case "":
            FilterAchieverStruct.hazard_NameByFilter = String()
            break;
        default:
            FilterAchieverStruct.hazard_NameByFilter = FilterAchieverStruct.hazard_Name
            break;
        }
        
        
        if FilterAchieverStruct.hazard_NameByFilter.isEmpty == true{
            self.firstthCon.constant = 0
            self.hazardTypeLabel.text = ""
        }else{
            self.firstthCon.constant = 5
            self.hazardTypeLabel.text = "Hazard Type : "+FilterAchieverStruct.hazard_NameByFilter
        }
        
        if FilterAchieverStruct.locationType.isEmpty == true{
            self.secondThCon.constant = 0
            self.locationTypeLabel.text = ""
        }else{
            self.secondThCon.constant = 5
            self.locationTypeLabel.text = FilterAchieverStruct.locationType
        }
        
        if FilterAchieverStruct.isHazardView.isEmpty == true {
            self.thirdthCon.constant = 0
            self.fromDateLabel.text = ""
        }else{
            
            self.thirdthCon.constant = 5
            self.fromDateLabel.text = "View : "+FilterAchieverStruct.isHazardView
        }
        
        if FilterAchieverStruct.toDate.isEmpty == true && FilterAchieverStruct.fromDate.isEmpty == true{
            self.forthcon.constant = 0
            self.toDateLabel.text = ""
        }else{
            self.forthcon.constant = 5
            self.toDateLabel.text = "From Date : "+FilterAchieverStruct.fromDate+"   To Date : "+FilterAchieverStruct.toDate
        }
        
        if FilterAchieverStruct.hazard_NameByFilter.isEmpty == true && FilterAchieverStruct.locationType.isEmpty == true && FilterAchieverStruct.isHazardView.isEmpty == true && FilterAchieverStruct.toDate.isEmpty == true  {
            
            self.filterAppcon.constant = 0
            self.viewOf.isHidden = true
            self.filterAppLabel.text = ""
            self.viewOf.frame.size.height = 0
            
        }else{
            
            self.viewOf.isHidden = false
            self.filterAppcon.constant = 10
            self.filterAppLabel.text = "Filter Applied"
            self.filterAppLabel.font = UIFont.boldSystemFont(ofSize: 14)
        }
       
        if(self.viewOf.isHidden == true) {
            self.view.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.view.layer.borderWidth = 0.5
            self.view.layer.borderColor = UIColor.white.cgColor
        } else {
            self.viewHandler.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.view.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
            self.viewHandler.layer.borderWidth = 0.5
            self.viewHandler.layer.borderColor = UIColor.white.cgColor
            self.view.layer.borderWidth = 0.5
            self.view.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    
    @IBAction func filterOfDashboardTypeHazardAction(_ sender: Any) {
        
        switch FilterAchieverStruct.topAchieverFlag {
        case "Hazard":
            let TopTenFilter = self.storyboard?.instantiateViewController(withIdentifier: "TopTenFilter") as! TopTenFilterViewController
            TopTenFilter.identifierStr = "TopTenFilter"
            self.navigationController?.pushViewController(TopTenFilter, animated: true)
            break;
        default:
            
            break;
        }
        
        
    }

}

extension TopTenArchieversViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 2
    }
 
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ThemeSafety",bundle : nil)
        if(position == 0) {
           
            let closerVC = storyBoard.instantiateViewController(withIdentifier: "TopTenCloser") as! TopTenCloserViewController
            return  closerVC as CommonVSClass
        }else {
            let ReportedVC = storyBoard.instantiateViewController(withIdentifier: "TopTenReported") as! TopTenReportedViewController
            return  ReportedVC as CommonVSClass
        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "MOST HAZARDS CLOSED  " , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "MOST HAZARDS REPORTED" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension TopTenArchieversViewController : ViewPagerControllerDelegate {
    
}
