//
//  SearchViewController.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class SearchViewController: CommonVSClass {
    
    @IBOutlet weak var searchTF: DesignableUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.title = "Search Hazard"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        FilterGraphStruct.hazard_NameByFilter = String()
        FilterGraphStruct.fromDate  = String()
        FilterGraphStruct.toDate  = String()
        FilterGraphStruct.locationType  = String()
        FilterGraphStruct.flagOfReportFilte = String()
        
        
        FilterGraphStruct.hazard_Name_ID = String()
        FilterGraphStruct.hazard_Name = String()
//        FilterGraphStruct.business_NameID = String()
//        FilterGraphStruct.business_Name = String()
//        FilterGraphStruct.location_ID = String()
//        FilterGraphStruct.location_Name = String()
        FilterGraphStruct.area_ID = String()
        FilterGraphStruct.area_Name = String()
        FilterGraphStruct.sub_area_ID = String()
        FilterGraphStruct.sub_area_Name = String()
        FilterGraphStruct.isHazardView = String()
        
//        FilterGraphStruct.filterByAreaID = String()
//        FilterGraphStruct.filterBySubAreaID = String()
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        if searchTF.text != ""{
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.location_name = String()
            FilterDataFromServer.location_id = Int()
            FilterDataFromServer.hazard_Name = String()
            FilterDataFromServer.hazard_Id = Int()
            
            
            
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            FilterGraphStruct.hazard_Name = "Select Hazard Type"
            FilterGraphStruct.department_Name = "Select Department"
            FilterGraphStruct.location_Name = "Select Location"
            FilterGraphStruct.RiskLevel = "Risk Level"
            FilterGraphStruct.minRiskLevel = String()
            FilterGraphStruct.mixRiskLevel = String()
            FilterGraphStruct.department_NameID = String()
            FilterGraphStruct.location_ID = String()
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.hazard_Name_ID = String()
            FilterGraphStruct.locationType = String()
            
            FilterGraphStruct.fromDate = String()
            FilterGraphStruct.toDate = String()
            
            // FilterGraphStruct.filterBylocation_Name = String()
            //  FilterGraphStruct.filterBylocation_ID = String()
            
            
            
            //cross
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            // locationViewBtn.backgroundColor = UIColor.clear
            FilterGraphStruct.isHazardView = String()
            FilterGraphStruct.hazard_NameByFilter = String()
             let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let NSearchHazardResult = storyBoard.instantiateViewController(withIdentifier: "NSearchHazardResult") as! NSearchHazardResultViewController
            
            FilterGraphStruct.serachStr = searchTF.text!
            self.navigationController?.pushViewController(NSearchHazardResult, animated: true)
            
        }else{
            self.view.makeToast("Enter hazard Id")
        }
    }
    
    
}

struct BannerStruct {
    static var bannerPath = String()
}
struct FilterGraphStruct {
    
    static var department_Name = String()
    static var department_NameID = String()
//
    static var location_Name = String()
    static var location_ID = String()
    static var unit_Name = String()
    static var unit_Id = String()
    static var area_Name = String()
    static var area_ID = String()
    
    static var sub_area_Name = String()
    static var sub_area_ID = String()
    
    static var hazard_Name = String()
    static var hazard_Name_ID = String()
    static var hazard_NameByFilter = String()
    
    static var filterIdentifier = Bool()
    
    static var Location = String()
    static var Hazard = String()
    
//    static var filterBySubAreaID = String()
//    static var filterByAreaID = String()
    
    static var locationType = String()
    
    static var isHazardView = String()
    
    static var fromDate = String()
    static var toDate = String()
    
    static var serachStr = String()
    
    static var typeIDStr = String()
    static var flagOfReportFilte = String()
    
    static var toppendencyFlag = String()
    static var topAchieverFlag = String()
    
    
    static var AssignedPNo = String()
    
    static var RiskLevel = String()
    static var minRiskLevel = String()
    static var mixRiskLevel = String()
    static var visitDate = String()
    
}
