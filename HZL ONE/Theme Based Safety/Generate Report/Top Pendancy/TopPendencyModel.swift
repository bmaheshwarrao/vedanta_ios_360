import Foundation
import Reachability

struct FilterPendencyStruct {
    
    static var department_Name = String()
    static var department_NameID = String()
    //
    static var location_id = String()
    static var location_name = String()
    static var unit_Id = String()
    static var unit_Name = String()
    static var area_Id = String()
    static var area_Name = String()
    
    static var sub_Area_Id = String()
    static var sub_Area_Name = String()
    
    static var hazard_Name = String()
    static var hazard_Id = Int()
    static var hazard_Name_ID = String()
    static var hazard_NameByFilter = String()
    static var filterType = String()
    static var filterIdentifier = Bool()
    
    static var Location = String()
    static var Hazard = String()
    
    //    static var filterBySubAreaID = String()
    //    static var filterByAreaID = String()
    
    static var locationType = String()
    
    static var isHazardView = String()
    
    static var fromDate = String()
    static var toDate = String()
    
    static var serachStr = String()
    
    static var typeIDStr = String()
    static var flagOfReportFilte = String()
    
    static var toppendencyFlag = String()
    static var topAchieverFlag = String()
     static var filterApply = String()
    
    static var AssignedPNo = String()
    
    static var RiskLevel = String()
    static var minRiskLevel = String()
    static var mixRiskLevel = String()
    
    
}

class TopPendencyForAssigningModel: NSObject {
    
    
    
    var Employee_ID = Double()
    var TotalPending = Int()
    var Name = ""
    var Sbu_Department = ""
    var SrNo = Int()
    var Pending30 = Int()
    var Pending90 = Int()
    var Sub_Area_ID = Int()
    var Sub_Area = ""
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Employee_ID"] is NSNull{
            self.Employee_ID = 0
        }else{
            self.Employee_ID = ((str["Employee_ID"] as? NSString)?.doubleValue)!
        }
        if str["name"] is NSNull{
            self.Name = ""
        }else{
            self.Name = (str["name"] as? String)!
        }
        if str["Sub_Area"] is NSNull{
            self.Sub_Area = ""
        }else{
            self.Sub_Area = (str["Sub_Area"] as? String)!
        }
        if str["Sub_Area_ID"] is NSNull{
            self.Sub_Area_ID = 0
        }else{
            self.Sub_Area_ID = (str["Sub_Area_ID"] as? Int)!
        }
        
        
        
        if str["Total_Count"] is NSNull{
            
            self.TotalPending = 0
        }else{
            
            self.TotalPending = (str["TotalPending"] as? Int)!
        }
        if str["Total_Count_30"] is NSNull{
            
            self.Pending30 = 0
        }else{
            
            self.Pending30 = (str["Pending30"] as? Int)!
        }
        if str["Total_Count_90"] is NSNull{
            
            self.Pending90 = 0
        }else{
            
            self.Pending90 = (str["Pending90"] as? Int)!
        }
        if str["R"] is NSNull{
            
            self.SrNo = 0
        }else{
            
            self.SrNo = (str["R"] as? Int)!
        }
       
        
        
    }
}


class TopPendencyForAssigningAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:TopPendencyForAssigningViewController,Url:String, parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
       
         var statusString  =  String()
        if(reachablty.connection != .none)
        {
            obj.startLoading(view: obj.tableView)
            WebServices.sharedInstances.sendPostRequest(url: Url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                     statusString  = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                         obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [TopPendencyForAssigningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TopPendencyForAssigningModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        obj.stopLoading(view: obj.tableView)
                        obj.refresh.endRefreshing()
                      
                    }
                    else
                    {
                        
                        statusString = "success"
                        print("DATA:fail")
                        obj.stopLoading(view: obj.tableView)
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                         obj.label.isHidden = false
                        obj.refresh.endRefreshing()
                    
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
              //  obj.errorChecking(error: error)
               
            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                if(statusString != "success") {
                    obj.noDataLabel(text:  "Check your internet connection and try again.")
                    obj.label.isHidden = false
                    obj.tableView.isHidden = true
                    obj.refresh.endRefreshing()
                    obj.stopLoading(view: obj.tableView)
                }
            }
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.label.isHidden = false
        }
        
        
    }
    
    
}

class TopPendencyForAssigningLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:TopPendencyForAssigningViewController,Url:String,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
     
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: Url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [TopPendencyForAssigningModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TopPendencyForAssigningModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                     
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No data found" )
                        obj.refresh.endRefreshing()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
              //  obj.errorChecking(error: error)
               
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            
        }
        
        
    }
    
    
}


//Top pendency closure


class TopPendencyForClosureModel: NSObject {
    
    
    
    var Employee_ID = Double()
    var TotalPending = Int()
    var Name = ""
    var Sbu_Department = ""
    var SrNo = Int()
    var Pending30 = Int()
    var Pending90 = Int()
   // var Sub_Area_ID = Int()
   // var Sub_Area = ""
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Employee_ID"] is NSNull{
            self.Employee_ID = 0
        }else{
            self.Employee_ID = ((str["Employee_ID"] as? NSString)?.doubleValue)!
        }
        if str["name"] is NSNull{
            self.Name = ""
        }else{
            self.Name = (str["name"] as? String)!
        }
//        if str["Sub_Area"] is NSNull{
//            self.Sub_Area = ""
//        }else{
//            self.Sub_Area = (str["Sub_Area"] as? String)!
//        }
//        if str["Sub_Area_ID"] is NSNull{
//            self.Sub_Area_ID = 0
//        }else{
//            self.Sub_Area_ID = (str["Sub_Area_ID"] as? Int)!
//        }
        
        
        
        if str["Total_Count"] is NSNull{
            
            self.TotalPending = 0
        }else{
            
            self.TotalPending = (str["TotalPending"] as? Int)!
        }
        if str["Total_Count_30"] is NSNull{
            
            self.Pending30 = 0
        }else{
            
            self.Pending30 = (str["Pending30"] as? Int)!
        }
        if str["Total_Count_90"] is NSNull{
            
            self.Pending90 = 0
        }else{
            
            self.Pending90 = (str["Pending90"] as? Int)!
        }
        if str["R"] is NSNull{
            
            self.SrNo = 0
        }else{
            
            self.SrNo = (str["R"] as? Int)!
        }
        
        
        
    }
}


class TopPendencyForClosureAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:TopPendencyForClosureViewController,Url:String,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
var statusString  =  String()
        print(reachablty.connection)
        if(reachablty.connection != .none)
        {
            obj.startLoading(view: obj.tableView)
            WebServices.sharedInstances.sendPostRequest(url: Url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                     statusString  = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [TopPendencyForClosureModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TopPendencyForClosureModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        obj.stopLoading(view: obj.tableView)
                        obj.refresh.endRefreshing()
                        
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        statusString = "success"
                        obj.noDataLabel(text: msg)
                        obj.label.isHidden = false
                        obj.refresh.endRefreshing()
                       obj.stopLoading(view: obj.tableView)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "success"
                print("DATA:error",errorStr)
                 obj.stopLoading(view: obj.tableView)
                //obj.errorChecking(error: error)
                
            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                if(statusString != "success") {
                    obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
                    obj.label.isHidden = false
                    obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
             obj.stopLoading(view: obj.tableView)
            }
            }
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
           obj.stopLoading(view: obj.tableView)
        }
        
        
    }
    
    
}

class TopPendencyForClosureLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:TopPendencyForClosureViewController,Url:String,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
       
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: Url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [TopPendencyForClosureModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = TopPendencyForClosureModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No data found" )
                        obj.refresh.endRefreshing()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
               // obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            
        }
        
        
    }
    
    
}


