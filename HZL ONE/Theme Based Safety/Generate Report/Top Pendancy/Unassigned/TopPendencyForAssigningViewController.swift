//
//  TopPendencyForAssigningViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 17/03/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class TopPendencyForAssigningViewController: CommonVSClass,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalHazard: UILabel!
    
    var TopReportedApi = TopPendencyForAssigningAPI()
    var TopReportedDB : [TopPendencyForAssigningModel] = []
    var TopReportedLoadMoreDB : [TopPendencyForAssigningModel] = []
    var TopReportedLoadMoreApi = TopPendencyForAssigningLoadMoreAPI()
    
    var data: String?
    var lastObject: String?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        
        tableView.estimatedRowHeight = 113
        tableView.rowHeight = UITableViewAutomaticDimension
        
        refresh.addTarget(self, action: #selector(self.callTopCloserData), for: .valueChanged)
        self.tableView.addSubview(refresh)
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callTopCloserData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "fromTopTenPendancyTypeFilter")), object: nil)
        
        callTopCloserData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callTopCloserData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
                
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TopReportedDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "assigning", for: indexPath) as! TopPendencyForAssigningTableViewCell

        cell.totalHazard.text = String(describing: self.TopReportedDB[indexPath.row].TotalPending)
        cell.employeeName.text = self.TopReportedDB[indexPath.row].Name
       
        cell.greaterThanThirtyHazard.text = String(describing: self.TopReportedDB[indexPath.row].Pending30)
        cell.greaterThanNintyHazard.text = String(describing: self.TopReportedDB[indexPath.row].Pending90)
        
        self.data = String(self.TopReportedDB[indexPath.row].SrNo)
        self.lastObject = String(self.TopReportedDB[indexPath.row].SrNo)
        
        if ( self.data ==  self.lastObject && indexPath.row == self.TopReportedDB.count - 1)
        {
            
            self.callTopCloserDataLoadMore(SrNo: String(self.TopReportedDB[indexPath.row].SrNo))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func callTopCloserData(){
        
        
        switch FilterPendencyStruct.toppendencyFlag {
        case "PendancyHazard":
         
            var para = [String:String]()
            
            let parameter = [
             
                "FromDate":FilterPendencyStruct.fromDate,
                "ToDate":FilterPendencyStruct.toDate
                ,"Hazard_Type_ID":FilterPendencyStruct.hazard_Name_ID
                ,"Unit_ID":FilterPendencyStruct.unit_Id
                ,"Zone_ID":FilterPendencyStruct.location_id
                ,"Area_ID":FilterPendencyStruct.area_Id
                ,"Sub_Area_ID":FilterPendencyStruct.sub_Area_Id]
        
            para = parameter.filter { $0.value != ""}
            
          
            
            print("para",para)
            
            self.TopReportedApi.serviceCalling(obj: self, Url: URLConstants.Dashbord_List_Assign_For_Pending, parameter: para) { (dict) in
                
                self.TopReportedDB = [TopPendencyForAssigningModel]()
                self.TopReportedDB = dict as! [TopPendencyForAssigningModel]
                
                self.tableView.reloadData()
                
            }
            
            break;
        default:
//
//            var para = [String:String]()
//
//            let parameter = [
//                "SrNo": ""]
//
//            para = parameter.filter { $0.value != ""}
//
//            print("para",para)
//
//            self.TopReportedApi.serviceCalling(obj: self, Url: URLConstants.Dashbord_Type_Hazard, parameter: para) { (dict) in
//
//                self.TopReportedDB = [TopPendencyForAssigningModel]()
//                self.TopReportedDB = dict as! [TopPendencyForAssigningModel]
//
//                self.tableView.reloadData()
//
//            }
            
            break;
        }
        
    }
    
    @objc func callTopCloserDataLoadMore(SrNo:String){
        
        
        switch FilterPendencyStruct.toppendencyFlag {
        case "PendancyHazard":
            
            var para = [String:String]()
          
            let parameter = [
               
                "FromDate":FilterPendencyStruct.fromDate,
                "ToDate":FilterPendencyStruct.toDate
                ,"Hazard_Type_ID":FilterPendencyStruct.hazard_Name_ID
                ,"Unit_ID":FilterPendencyStruct.unit_Id
                ,"Zone_ID":FilterPendencyStruct.location_id
                ,"Area_ID":FilterPendencyStruct.area_Id
                ,"Sub_Area_ID":FilterPendencyStruct.sub_Area_Id
                 , "ID":SrNo]
            
            para = parameter.filter { $0.value != ""}
            
            
            
            print("para",para)
            
            self.TopReportedLoadMoreApi.serviceCalling(obj: self, Url: URLConstants.Dashbord_List_Assign_For_Pending, parameter: para) { (dict) in
                
                self.TopReportedLoadMoreDB = [TopPendencyForAssigningModel]()
                self.TopReportedLoadMoreDB = dict as! [TopPendencyForAssigningModel]
                switch self.TopReportedLoadMoreDB.count {
                case 0:
                    break;
                default:
                    self.TopReportedDB.append(contentsOf: self.TopReportedLoadMoreDB)
                    self.tableView.reloadData()
                    break;
                }
                
                
            }
            
            break;
        default:
            
//            var para = [String:String]()
//
//            let parameter = [
//                "SrNo": SrNo]
//
//            para = parameter.filter { $0.value != ""}
//
//
//
//            print("para",para)
//
//            self.TopReportedLoadMoreApi.serviceCalling(obj: self, Url: URLConstants.Dashbord_Type_Hazard, parameter: para) { (dict) in
//
//                self.TopReportedLoadMoreDB = [TopPendencyForAssigningModel]()
//                self.TopReportedLoadMoreDB = dict as! [TopPendencyForAssigningModel]
//                switch self.TopReportedLoadMoreDB.count {
//                case 0:
//                    break;
//                default:
//                    self.TopReportedDB.append(contentsOf: self.TopReportedLoadMoreDB)
//                    self.tableView.reloadData()
//                    break;
//                }
//
//
//            }
            
            break;
        }
        
       
    }
    
    
    
}


