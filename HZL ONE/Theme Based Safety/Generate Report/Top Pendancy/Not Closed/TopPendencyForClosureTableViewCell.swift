//
//  TopPendencyForClosureTableViewCell.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 17/03/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class TopPendencyForClosureTableViewCell: UITableViewCell {
    
    @IBOutlet weak var employeeName: UILabel!
    
    @IBOutlet weak var SBUDepartment: UILabel!
    
    @IBOutlet weak var totalHazard: UILabel!
    @IBOutlet weak var greaterThanThirtyHazard: UILabel!
    @IBOutlet weak var greaterThanNintyHazard: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
