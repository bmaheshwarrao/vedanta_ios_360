//
//  DashboardTypeHazardFilterViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 22/01/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit
import Reachability
import CoreData

class DashboardTypeHazardFilterViewController: CommonVSClass ,UITextFieldDelegate {
    
    var hazard_ID = String()
    var locationStr = String()
    var subLocationStr = String()
    
    let reachablty = Reachability()!
    
    @IBOutlet weak var departmentBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var areaBtn: UIButton!
    @IBOutlet weak var subAreaBtn: UIButton!
    @IBOutlet weak var hazardTypeBtn: UIButton!

    
    @IBOutlet weak var departmentCrosBtn: UIButton!
    @IBOutlet weak var locationCrosBtn: UIButton!
    @IBOutlet weak var areaCrosBtn: UIButton!
    @IBOutlet weak var subAreaCrosBtn: UIButton!
    @IBOutlet weak var hazardCrosBtn: UIButton!
    
    @IBOutlet weak var departmentView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var subAreaView: UIView!
    @IBOutlet weak var areaView: UIView!
    @IBOutlet weak var hazardView: UIView!
    @IBOutlet weak var riskLevelView: UIView!
    
    
    @IBOutlet weak var hazardViewBtn: UIButton!
    @IBOutlet weak var locationViewBtn: UIButton!
    @IBOutlet weak var noneViewBtn: UIButton!
    
    
    
  
    var AreaIDVal = String()
    
    var SubAreaIDVal = String()
    
    
   
    
   // var business_NameArray = [String]()
  //  var location_NameArray = [String]()
    
    var Area_NameArray = [String]()
    var Sub_Area_NameArray = [String]()
    
  
    var hazard_NameArray = [String]()
    
    
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var toDateTF: UITextField!
    var datePicker : UIDatePicker!
    
    @IBOutlet weak var riskLevelBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
        self.fromDateTF.delegate = self
        self.toDateTF.delegate = self
        //new
        FilterGraphStruct.flagOfReportFilte = "reportFilter"
        
        
        
//        switch FilterGraphStruct.business_Name {
//        case "":
//            FilterGraphStruct.business_Name = "Select Business"
//            break;
//        default:
//            break;
//        }
//
//        switch FilterGraphStruct.location_Name {
//        case "":
//            FilterGraphStruct.location_Name = "Select Location"
//            break;
//        default:
//
//            break;
//        }
        
        switch FilterGraphStruct.area_Name {
        case "":
            FilterGraphStruct.area_Name = "Select Area"
            break;
        default:
            
            break;
        }
        
        switch FilterGraphStruct.sub_area_Name {
        case "":
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            break;
        default:
            
            break;
        }
        
        switch FilterGraphStruct.hazard_Name {
        case "":
            FilterGraphStruct.hazard_Name = "Select Hazard Type"
            break;
        default:
            
            break;
        }
        
        
        
        
        
//        FilterGraphStruct.business_NameID = String()
//        FilterGraphStruct.location_ID = String()
//        FilterGraphStruct.area_ID = String()
//        FilterGraphStruct.sub_area_ID = String()
//        FilterGraphStruct.hazard_Name_ID = String()
//        FilterGraphStruct.fromDate = String()
//        FilterGraphStruct.toDate = String()
//        FilterGraphStruct.hazard_NameByFilter = String()
        
        hazardViewBtn.backgroundColor = UIColor.clear
        noneViewBtn.backgroundColor = UIColor.red
       // locationViewBtn.backgroundColor = UIColor.clear
        
       departmentView.layer.cornerRadius = 10
        departmentView.layer.borderWidth = 0.5
        departmentView.layer.borderColor = UIColor.black.cgColor
        
        locationView.layer.cornerRadius = 10
        locationView.layer.borderWidth = 0.5
        locationView.layer.borderColor = UIColor.black.cgColor
//
        areaView.layer.cornerRadius = 10
        areaView.layer.borderWidth = 0.5
        areaView.layer.borderColor = UIColor.black.cgColor
        
        subAreaView.layer.cornerRadius = 10
        subAreaView.layer.borderWidth = 0.5
        subAreaView.layer.borderColor = UIColor.black.cgColor
        
        
        hazardView.layer.cornerRadius = 10
        hazardView.layer.borderWidth = 0.5
        hazardView.layer.borderColor = UIColor.black.cgColor
        
        riskLevelView.layer.cornerRadius = 10
        riskLevelView.layer.borderWidth = 0.5
        riskLevelView.layer.borderColor = UIColor.black.cgColor
        
        buttonShapeModify(button: noneViewBtn)
        buttonShapeModify(button: hazardViewBtn)
       // buttonShapeModify(button: locationViewBtn)
        
        switch FilterGraphStruct.isHazardView {
//        case "Location":
//            hazardViewBtn.backgroundColor = UIColor.clear
//            noneViewBtn.backgroundColor = UIColor.clear
//            locationViewBtn.backgroundColor = UIColor.red
        case "Hazard":
            hazardViewBtn.backgroundColor = UIColor.red
            noneViewBtn.backgroundColor = UIColor.clear
            //locationViewBtn.backgroundColor = UIColor.clear
            FilterGraphStruct.isHazardView = "Hazard"
        default:
            hazardViewBtn.backgroundColor = UIColor.clear
            noneViewBtn.backgroundColor = UIColor.red
            //locationViewBtn.backgroundColor = UIColor.clear
           FilterGraphStruct.isHazardView = "Location"
            break;
        }
        
        switch FilterGraphStruct.fromDate {
        case "":
            fromDateTF.placeholder = "From Date"
            break;
        default:
            fromDateTF.text = FilterGraphStruct.fromDate
            break;
        }
        
        switch FilterGraphStruct.toDate {
        case "":
            toDateTF.placeholder = "To Date"
            break;
        default:
            toDateTF.text = FilterGraphStruct.toDate
            break;
        }
        
        FilterGraphStruct.RiskLevel = "Risk Level"
        FilterGraphStruct.minRiskLevel = String()
        FilterGraphStruct.mixRiskLevel = String()
        
        locationUpdate()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//        self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//        self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//        self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//        self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
//        FilterGraphStruct.filterBylocation_Name = String()
//        FilterGraphStruct.filterBylocation_ID = String()
//        FilterGraphStruct.hazard_Name_ID = String()
//
//        FilterGraphStruct.fromDate = String()
//        FilterGraphStruct.toDate = String()
//        FilterGraphStruct.locationType = String()
//        FilterGraphStruct.hazard_NameByFilter = String()
        
        
//        buttonShapeModify(button: noneViewBtn)
//        buttonShapeModify(button: hazardViewBtn)
//        buttonShapeModify(button: locationViewBtn)
//        hazardViewBtn.backgroundColor = UIColor.clear
//        noneViewBtn.backgroundColor = UIColor.red
//        locationViewBtn.backgroundColor = UIColor.clear
        
        
        //FilterGraphStruct.isHazardView = String()
        
        
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Filter"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        locationUpdate()
        
    }
    
    
    @IBAction func departmentAction(_ sender: Any) {
        departmentData()
    }
    
    @IBAction func locationAction(_ sender: Any) {
       locationData()
    }
    
    
    @IBAction func areaAction(_ sender: Any) {
        areaData()
    }
    
    @IBAction func subAreaAction(_ sender: Any) {
        subAreaData()
    }
    
    @IBAction func hazardTypeAction(_ sender: Any){
        hazardData()
    }
    
    @IBAction func businessTap(_ sender: Any) {
        
        
//        if (businessCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
//            businessData()
//
//        }else{
//
//           // FilterGraphStruct.business_Name = "Select Business"
//          //  FilterGraphStruct.location_Name = "Select Location"
//            FilterGraphStruct.area_Name = "Select Area"
//            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
//
//          //  self.businessBtn.setTitle( FilterGraphStruct.business_Name, for: .normal)
//          //  self.locationBtn.setTitle( FilterGraphStruct.location_Name, for: .normal)
//            self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
//            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
//
//
//          //  FilterGraphStruct.business_NameID = String()
//          //  FilterGraphStruct.location_ID = String()
//            FilterGraphStruct.area_ID = String()
//            FilterGraphStruct.sub_area_ID = String()
//
//
//
//            self.businessBtn.setTitleColor(UIColor.darkGray, for: .normal)
//            self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//
//
//            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
//            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//
//
//            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
//            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//
//
//            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
//            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
//
//
//        }
        
    }
    
    @objc func locationUpdate(){
        
        
        if  FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
            //            FilterGraphStruct.filterBylocation_Name = String()
            //            FilterGraphStruct.filterBylocation_ID = String()
        }else{
            //
        }
        switch FilterDataFromServer.hazard_Name == "" {
        case true:
            self.hazardTypeBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.hazardTypeBtn.setTitle("Select Hazard Type", for: .normal)
            break;
        default:
            self.hazardTypeBtn.setTitleColor(UIColor.black, for: .normal)
            self.hazardCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.hazardTypeBtn.setTitle(FilterDataFromServer.hazard_Name, for: .normal)
            break;
        }
        
        
        switch FilterDataFromServer.unit_Name == "" {
        case true:
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.areaBtn.setTitle("Select Unit", for: .normal)
            break;
        default:
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaBtn.setTitle(FilterDataFromServer.unit_Name, for: .normal)
            break;
        }
        
        switch FilterDataFromServer.area_Name == "" {
        case true:
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaBtn.setTitle("Select Area", for: .normal)
            break;
        default:
            self.subAreaBtn.setTitleColor(UIColor.black, for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaBtn.setTitle(FilterDataFromServer.area_Name, for: .normal)
            break;
        }
        
        
        
        switch FilterDataFromServer.location_name == "" {
        case true:
            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.locationBtn.setTitle("Select Zone", for: .normal)
            break;
        default:
            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.locationBtn.setTitle(FilterDataFromServer.location_name, for: .normal)
            break;
        }
        
        
        switch FilterDataFromServer.sub_Area_Name == "" {
        case true:
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.departmentBtn.setTitle("Select Area Location", for: .normal)
            break;
        default:
            self.departmentBtn.setTitleColor(UIColor.black, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.departmentBtn.setTitle(FilterDataFromServer.sub_Area_Name, for: .normal)
            break;
        }
        
        
        
    }
    @IBAction func areaTap(_ sender: Any) {
        
        
        if (self.areaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            areaData()
            
            
        }else{
            
            
            //   FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_Name
            FilterGraphStruct.unit_Name = "Select Unit"
            FilterGraphStruct.area_Name = "Select Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.unit_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            
            
            
            
            
        }
        
    }
    
    @IBAction func subAreaTap(_ sender: Any) {
        
        
        if (self.subAreaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            subAreaData()
            
            
        }else{
            
            
            FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
            FilterGraphStruct.area_Name = "Select Area"
            
            self.subAreaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            
            //            self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //            self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            //
            //            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            
            
            
            FilterGraphStruct.sub_area_ID = String()
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            locationUpdate()
            
            
            
        }
        
    }
    @IBAction func hazardCrosAction(_ sender: Any) {
        if (hazardCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
           hazardData()
            
        }else{
            FilterGraphStruct.hazard_Name = "Select Hazard Type"
            FilterGraphStruct.hazard_Name_ID = String()
            
            
            
            
            
            
            
            
            
            self.hazardTypeBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
          
            locationUpdate()
        }
        
    }
    
    @IBAction func departmentCrossAction(_ sender: Any) {
        
        
        if (departmentCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            departmentData()
            
        }else{
            
            
            
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            FilterGraphStruct.department_NameID = String()
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            
            //            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //
            //            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //
            //            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //            FilterGraphStruct.locationType = String()
            
            
        }
        //
    }
    
    @IBAction func locationCrossAction(_ sender: Any) {
        
        
        if (self.locationCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            locationData()
            
            
        }else{
            
            
            
            FilterGraphStruct.location_Name = "Select Location"
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            
            self.locationBtn.setTitle( FilterGraphStruct.location_Name, for: .normal)
            
            
            
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            
            
            
            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.location_ID = String()
            
            FilterGraphStruct.location_Name = String()
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.location_name = String()
            FilterDataFromServer.location_id = Int()
            locationUpdate()
            
        }
        
    }
    @IBAction func areaCrossAction(_ sender: Any) {
        
        
        if (self.areaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            areaData()
            
            
        }else{
            
            
            //   FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_Name
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            FilterGraphStruct.department_NameID = String()
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            
        }
        
    }
    @IBAction func subAreaCrossAction(_ sender: Any) {
        
        
        if (self.subAreaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            subAreaData()
            
            
        }else{
            
            FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //            self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //            self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            //
            //            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            
            
            
            FilterGraphStruct.sub_area_ID = String()
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            
        }
        
    }
    
    
    
    
    func departmentData(){
        
        
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "" ) {
            FilterDataFromServer.filterType = "Sub Area"
            
            
//            FilterDataFromServer.sub_Area_Id = Int()
//            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area Location"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
        
        
        
        
        
        
    }
    //
    func locationData(){
        
        
        FilterDataFromServer.filterType = "Location"
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
//        FilterDataFromServer.location_name = String()
//        FilterDataFromServer.location_id = Int()
        updateData()
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Zone"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    func areaData(){
        
        
        
        if(FilterDataFromServer.location_name != "") {
            FilterDataFromServer.filterType = "Unit"
//            FilterDataFromServer.unit_Id = Int()
//            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Unit"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
    }
    
    func subAreaData(){
        
        
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
//            FilterDataFromServer.area_Id = Int()
//            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
            
        }
        
    }
    @objc func updateData(){
        if(FilterDataFromServer.location_name != "") {
            FilterGraphStruct.location_Name = FilterDataFromServer.location_name
            FilterGraphStruct.location_ID = String(FilterDataFromServer.location_id)
        } else {
            FilterGraphStruct.location_Name = "Select Location"
            FilterGraphStruct.location_ID = String()
        }
        if(FilterDataFromServer.location_name != "" && FilterDataFromServer.unit_Name != "") {
            FilterGraphStruct.unit_Name = FilterDataFromServer.unit_Name
            FilterGraphStruct.unit_Id = String(FilterDataFromServer.unit_Id)
        } else {
            
            FilterGraphStruct.unit_Name = "Select Unit"
            FilterGraphStruct.unit_Id = String()
        }
        if(FilterDataFromServer.location_name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.sub_Area_Name != "") {
            FilterGraphStruct.area_Name = FilterDataFromServer.area_Name
            FilterGraphStruct.area_ID = String(FilterDataFromServer.area_Id)
        } else {
            
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.area_ID = String()
        }
        if(FilterDataFromServer.location_name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.sub_Area_Name != "" && FilterDataFromServer.sub_Area_Name != "") {
            FilterGraphStruct.sub_area_Name = FilterDataFromServer.sub_Area_Name
            FilterGraphStruct.sub_area_ID = String(FilterDataFromServer.sub_Area_Id)
        } else {
            
            FilterGraphStruct.department_Name = "Select Area Location"
            FilterGraphStruct.department_NameID = String()
        }
        if(FilterDataFromServer.hazard_Name != "") {
            
            FilterGraphStruct.hazard_Name = FilterDataFromServer.hazard_Name
            FilterGraphStruct.hazard_Name_ID = String(FilterDataFromServer.hazard_Id)
        } else {
            
            
            FilterGraphStruct.hazard_Name = "Select Hazard Type"
            FilterGraphStruct.hazard_Name_ID = String()
        }
    }
    
    
    func hazardData(){
        
        
        
        FilterDataFromServer.filterType = "Hazard"
//        FilterDataFromServer.hazard_Id = Int()
//        FilterDataFromServer.hazard_Name = String()
        
        
        updateData()
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Hazard Type"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    
    
   var whereAmI = String()
    @IBAction func applyFilterAction(_ sender: Any) {
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }else{
           
        UserDefaults.standard.set(true, forKey: "fromDashboardTypeFilter")
            
            switch whereAmI {
            case "Business":
                updateData()
                NotificationCenter.default.post(name:NSNotification.Name.init("BusinessdashboardTypeHazardFilter"), object: nil)
                
                break;
            case "Location":
                updateData()
                NotificationCenter.default.post(name:NSNotification.Name.init("LocationdashboardTypeHazardFilter"), object: nil)
                
                break;
            case "Area":
                updateData()
                NotificationCenter.default.post(name:NSNotification.Name.init("AreadashboardTypeHazardFilter"), object: nil)
                
                break;
            default:
                updateData()
                NotificationCenter.default.post(name:NSNotification.Name.init("dashboardTypeHazardFilter"), object: nil)
                
                break;
            }
            
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    @IBAction func resetFilterAction(_ sender: Any) {

       // FilterGraphStruct.business_Name = "Select Business"
       // FilterGraphStruct.location_Name = "Select Location"
        FilterGraphStruct.area_Name = "Select Area"
        FilterGraphStruct.sub_area_Name = "Select Sub-Area"
        FilterGraphStruct.hazard_Name = "Select Hazard Type"

        FilterGraphStruct.RiskLevel = "Risk Level"
        FilterGraphStruct.minRiskLevel = String()
        FilterGraphStruct.mixRiskLevel = String()
     //   FilterGraphStruct.business_NameID = String()
     //   FilterGraphStruct.location_ID = String()
        FilterGraphStruct.area_ID = String()
        FilterGraphStruct.sub_area_ID = String()
        FilterGraphStruct.hazard_Name_ID = String()
        FilterGraphStruct.locationType = "Location"
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        
       // FilterGraphStruct.filterBylocation_Name = String()
      //  FilterGraphStruct.filterBylocation_ID = String()

        //title
        self.hazardTypeBtn.setTitle(FilterGraphStruct.hazard_Name, for: .normal)
     //   self.businessBtn.setTitle(FilterGraphStruct.business_Name, for: .normal)
      //  self.locationBtn.setTitle(FilterGraphStruct.location_Name, for: .normal)
        self.areaBtn.setTitle(FilterGraphStruct.area_Name, for: .normal)
        self.subAreaBtn.setTitle(FilterGraphStruct.sub_area_Name, for: .normal)
        self.fromDateTF.text = nil
        self.toDateTF.text = nil
        self.fromDateTF.placeholder = "From Date"
        self.toDateTF.placeholder = "To Date"
        
        //cross
    //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
     //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        buttonShapeModify(button: noneViewBtn)
        hazardViewBtn.backgroundColor = UIColor.clear
        noneViewBtn.backgroundColor = UIColor.red
       // locationViewBtn.backgroundColor = UIColor.clear
        FilterGraphStruct.isHazardView = String()
        FilterGraphStruct.hazard_NameByFilter = String()
        
        FilterGraphStruct.department_Name = "Select Department"
        FilterGraphStruct.location_Name = "Select Location"
        FilterGraphStruct.area_Name = "Select Area"
        FilterGraphStruct.sub_area_Name = "Select Sub-Area"
        FilterGraphStruct.hazard_Name = "Select Hazard Type"
        
        FilterGraphStruct.department_NameID = String()
        FilterGraphStruct.location_ID = String()
        FilterGraphStruct.area_ID = String()
        FilterGraphStruct.sub_area_ID = String()
        FilterGraphStruct.hazard_Name_ID = String()
        FilterGraphStruct.locationType = String()
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        FilterDataFromServer.hazard_Name = String()
         FilterDataFromServer.hazard_Id = Int()
        // FilterGraphStruct.filterBylocation_Name = String()
        // FilterGraphStruct.filterBylocation_ID = String()
        
        //title
        self.hazardTypeBtn.setTitle(FilterGraphStruct.hazard_Name, for: .normal)
        self.departmentBtn.setTitle(FilterGraphStruct.department_Name, for: .normal)
        self.locationBtn.setTitle(FilterGraphStruct.location_Name, for: .normal)
        self.areaBtn.setTitle(FilterGraphStruct.area_Name, for: .normal)
        self.subAreaBtn.setTitle(FilterGraphStruct.sub_area_Name, for: .normal)
        self.fromDateTF.text = nil
        self.toDateTF.text = nil
        self.fromDateTF.placeholder = "From Date"
        self.toDateTF.placeholder = "To Date"
        
        //cross
        self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        FilterGraphStruct.isHazardView = String()
        FilterGraphStruct.hazard_NameByFilter = String()
        FilterDataFromServer.department_Id = Int()
        FilterDataFromServer.department_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        
        self.locationUpdate()
        
       

    }
    
    @IBAction func noneViewBtnAction(_ sender: Any) {
        
        buttonShapeModify(button: noneViewBtn)
        hazardViewBtn.backgroundColor = UIColor.clear
        noneViewBtn.backgroundColor = UIColor.red
       // locationViewBtn.backgroundColor = UIColor.clear
        FilterGraphStruct.isHazardView = "Location"
    }
    
    @IBAction func hazardViewBtnAction(_ sender: Any) {
        
        buttonShapeModify(button: hazardViewBtn)
        hazardViewBtn.backgroundColor = UIColor.red
        noneViewBtn.backgroundColor = UIColor.clear
       // locationViewBtn.backgroundColor = UIColor.clear
        FilterGraphStruct.isHazardView = "Hazard"
        
    }
    
    func buttonShapeModify(button:UIButton){
        
        button.layer.masksToBounds = false
        button.layer.borderWidth = 5
        button.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        button.layer.cornerRadius = button.frame.height/2
        button.clipsToBounds = true
    }
    
    @IBAction func locationViewBtnAction(_ sender: Any) {
        
//        buttonShapeModify(button: locationViewBtn)
//        hazardViewBtn.backgroundColor = UIColor.clear
//        noneViewBtn.backgroundColor = UIColor.clear
//        locationViewBtn.backgroundColor = UIColor.red
//        FilterGraphStruct.isHazardView = "Location"
        
        
    }
    
    func pickUpFromDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        if FilterGraphStruct.toDate.isEmpty == false{
            dateValidation(datePicker: self.datePicker, date: FilterGraphStruct.toDate, type: "from")
        }
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar

        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickFromTF))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func pickUpToDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        if FilterGraphStruct.fromDate.isEmpty == false{
            dateValidation(datePicker: self.datePicker, date: FilterGraphStruct.fromDate, type: "to")
        }
        textField.inputView = self.datePicker
        
       
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickToTF))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func dateValidation(datePicker:UIDatePicker, date:String, type:String) {
        
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        let minDate: Date =  dateFormatterFinal.date(from: date)!
        switch type {
        case "to":
            datePicker.minimumDate = minDate
            break;
        default:
            datePicker.maximumDate = minDate
            break;
        }
        
    }
    
    @objc func doneClickFromTF() {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        self.fromDateTF.text = dateFormatter1.string(from: datePicker.date)
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.timeZone = NSTimeZone.system
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        FilterGraphStruct.fromDate = dateFormatterFinal.string(from: datePicker.date)
        self.fromDateTF.resignFirstResponder()
        
    }
    
    @objc func doneClickToTF() {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none

        self.toDateTF.text = dateFormatter1.string(from: datePicker.date)
        
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.timeZone = NSTimeZone.system
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        FilterGraphStruct.toDate = dateFormatterFinal.string(from: datePicker.date)
        
        self.toDateTF.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        
        self.fromDateTF.resignFirstResponder()
        //
        self.toDateTF.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.fromDateTF{
          self.pickUpFromDate(self.fromDateTF)
        }else if textField == self.toDateTF {
            self.pickUpToDate(self.toDateTF)
        }else
        {
            //self.toDateTF.resignFirstResponder()
        }
    }
    
    let riskLevelArray = ["Risk Level","Low(1-4)","Medium(5-9)","High(10-16)","Very High(20-25)"]
    let minRiskLevelArray = ["0","1","5","10","20"]
    let mixRiskLevelArray = ["0","4","9","16","25"]
    
    @IBAction func riskLevelAction(_ sender: Any) {
        
        
       
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.riskLevelArray
            
            popup.sourceView = self.riskLevelBtn
            popup.isScroll = true
            popup.firstRow = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                self.riskLevelBtn.setTitle(self.riskLevelArray[row], for: .normal)
                FilterGraphStruct.RiskLevel = self.riskLevelArray[row]
                FilterGraphStruct.minRiskLevel = self.minRiskLevelArray[row]
                FilterGraphStruct.mixRiskLevel = self.mixRiskLevelArray[row]
                
                self.locationUpdate()
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
        })
        
    }
    
    
}

