//
//  DashboardTypeHazradTotalTableViewCell.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 25/01/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class DashboardTypeHazradTotalTableViewCell: UITableViewCell {

    @IBOutlet weak var TypeNameLabel: UILabel!
    @IBOutlet weak var YesterdayLabel: UILabel!
    @IBOutlet weak var MTDLabel: UILabel!
    @IBOutlet weak var YTDLabel: UILabel!
    @IBOutlet weak var ClosedLabel: UILabel!
    @IBOutlet weak var openLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
