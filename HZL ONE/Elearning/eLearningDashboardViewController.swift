//
//  eLearningDashboardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 07/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Charts


import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation
import AVKit

class eLearningDashboardViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    
    var StrNav = String()
    @IBOutlet weak var homeTableView: UITableView!
    
    
    var applicationName = String()
    
    
    
    
    
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
        
        
        
        
        
    }
       var videoPlayer = AVPlayer()
    func playerDidFinishPlaying(note: NSNotification) {
        
        self.videoPlayer.pause()
        
    }
    
    func videoPlay(str: String) {
        
        let videoURL = URL(string: str)
        self.videoPlayer = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = videoPlayer
        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer)
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
            
        }
    }
    @IBAction func btnShowAnotherClicked(_ sender: UIButton) {
        print(dataArrayCell2Elearning[sender.tag].FilePath)
        if(dataArrayCell2Elearning[sender.tag].Filetype == "PDF" ){
            let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
            let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HzlPdfReaderViewController") as! HzlPdfReaderViewController
            HZlPolicyTempData.ELCat_ID = String(dataArrayCell2Elearning[sender.tag].ID)
            
            HZlPolicyTempData.file_Url = dataArrayCell2Elearning[sender.tag].FilePath
            HZlPolicyTempData.file_Name = dataArrayCell2Elearning[sender.tag].Filename
            self.navigationController?.pushViewController(ELPVC, animated: true)
        }else if(dataArrayCell2Elearning[sender.tag].Filetype == "PPT" || dataArrayCell2Elearning[sender.tag].Filetype == "Document" ||  dataArrayCell2Elearning[sender.tag].Filetype == "Excel"){
           let urlValue = "https://view.officeapps.live.com/op/view.aspx?src=" + dataArrayCell2Elearning[sender.tag].FilePath
           linkHzl(urlData: urlValue, title: dataArrayCell2Elearning[sender.tag].Filename)
        }else if(dataArrayCell2Elearning[sender.tag].Filetype == "RAR" || dataArrayCell2Elearning[sender.tag].Filetype == "ZIP" ){
          self.view.makeToast("Cannot Open Zip/RAR file in this Application")
        }
        else{
            videoPlay(str: dataArrayCell2Elearning[sender.tag].FilePath)
        }
        
        
       
        
    }
    func linkHzl(urlData : String , title : String){
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
        
        let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
        guard   let url = URL(string: urlData) else {
            return
        }
        progressWebViewController.disableZoom = true
        progressWebViewController.url = url
        progressWebViewController.bypassedSSLHosts = [url.host!]
        progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
        progressWebViewController.websiteTitleInNavigationBar = false
        progressWebViewController.navigationItem.title = title
        progressWebViewController.leftNavigaionBarItemTypes = [.reload]
        progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
        self.present(proNav, animated: true, completion: nil)
    }
    @objc func pressFileHistory(Id : String ,fileType : String){
        
        let parameter = [
            
            "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")! ,
            "File_ID": Id ,
            "Action_Type" : fileType ,
            "Activity_Type" : "" ,
            "MSG" : "View Library File"
            
            ] as! [String:String]
        
        
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.ELibrary_History_Write, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            print(respon)
            if respon["status"] as! String == "success" {
                
                
            }else{
                
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            
            print(err.description)
        }
    }
    @IBAction func btnShowClicked(_ sender: UIButton) {
      
        
        let storyBoard = UIStoryboard(name: "eLearning", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "StartQuizListViewController") as! StartQuizListViewController
        ELPVC.CatId = dataArrayCell1Elearning[sender.tag].Category_ID
        ELPVC.StrNav = dataArrayCell1Elearning[sender.tag].Category_Title
        ELPVC.CourseID = dataArrayCell1Elearning[sender.tag].ID
        self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    @IBAction func btnmLibraryClicked(_ sender: UIButton) {
        
         let storyBoard = UIStoryboard(name: "mLibrary", bundle: nil)
                let ELPVC = storyBoard.instantiateViewController(withIdentifier: "mLibraryCategoryViewController") as! mLibraryCategoryViewController
           
                self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    @IBAction func btnmLearningClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "eLearning", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "mLearningCategoryViewController") as! mLearningCategoryViewController
        
        self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    var reachability = Reachability()!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var dataInt1 = Int()
    var dataInt2 = Int()
    
    var dataIntCount1 = 1
    var dataIntCount2 = 1
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = StrNav
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 {
//            if(dataArrayCell1Elearning.count > 0 ){
            print(dataIntCount1)
            return dataIntCount1
//            }else{
//                return 0
//            }
        }else if section == 2 {
//            if(dataArrayCell2Elearning.count > 0 ){
                return dataIntCount2
//            }else{
//                return 0
//            }
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageElearningTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: applicationName)
           
            return cell
            
        } else if indexPath.section == 1 {
            
            let page = "buttons"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! ButtonsElearningTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            if(dataInt1 == 0){
       
                cell.pageViewCell1(obj : self)
            }
            
            
            
            return cell
        }else  {
            
            let page = "buttonsData"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! ButtonsDataElearningTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            if(dataInt2 == 0){
  
                cell.pageViewCell1(obj : self)
            }
            
            return cell
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
     
        
        let screenSize = UIScreen.main.bounds
       // let screenHeight = screenSize.height-navheight
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
            
        }else{
            return 200
        }
        
    }
    
    
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
