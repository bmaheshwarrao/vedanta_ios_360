//
//  CustomAlertElearningViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CustomAlertElearningViewController: UIViewController {
    
    
    
    
    
    static func instantiate() -> CustomAlertElearningViewController? {
        return UIStoryboard(name: "eLearning", bundle: nil).instantiateViewController(withIdentifier: "\(CustomAlertElearningViewController.self)") as? CustomAlertElearningViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //        titleLabel.text = titleString
        //        messageLabel.text = messageString
    }
    
    // MARK: Actions
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name:NSNotification.Name.init("submitElearningQuiz"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
