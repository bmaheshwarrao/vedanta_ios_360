//
//  DownloadedViewController.swift
//  mLearning
//
//  Created by SARVANG INFOTCH on 08/12/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
import AVKit
import AVFoundation
import MZDownloadManager
class DownloadedViewController: CommonVSClass {
  @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        getofflineFileDetailsData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshDataDownload), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "refreshDataDownload")), object: nil)
        refresh.addTarget(self, action: #selector(refreshDataDownload), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        // Do any additional setup after loading the view.
    }
  
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.refreshDataDownload()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.title = "Downloaded"
        
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    @objc func refreshDataDownload(){
        getofflineFileDetailsData()
    }
    @IBAction func btnDeleteClicked(_ sender : UIButton){
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete?", preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Delete", style: .default, handler: { (alert) in
            
            let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
            let id = self.offlineFileDB[sender.tag].id
            let destinationURL =    URL(fileURLWithPath: self.offlineFileDB[sender.tag].filePath!)
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineFilesSaved")
            let predicate = NSPredicate(format: "id == %@", id!)
            fetchRequest.predicate = predicate
            let moc = self.getContext()
            let result = try? moc.fetch(fetchRequest)
            let resultData = result as! [OfflineFilesSaved]
            
            for object in resultData {
                
                moc.delete(object)
            }
            
            do {
                
                
                let fileManager = FileManager.default
                try? fileManager.removeItem(at: destinationURL)
                try moc.save()
                self.getofflineFileDetailsData()
                print("saved!")
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
            
            NotificationCenter.default.post(name:NSNotification.Name.init("refreshData"), object: nil)
            
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            
            
            
        })
        
        actionSheetController.addAction(camera)
        
        actionSheetController.addAction(cancel)
        self.present(actionSheetController, animated: true, completion: nil)
        
        
    }
        
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var offlineFileDB =  [OfflineFilesSaved]()
    @objc func getofflineFileDetailsData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        self.offlineFileDB = [OfflineFilesSaved]()
        
        do {
            
            self.offlineFileDB = try context.fetch(OfflineFilesSaved.fetchRequest())
            self.tableView.reloadData()
            self.refresh.endRefreshing()
            if(self.offlineFileDB.count == 0){
                tableView.isHidden = true
                label.isHidden = false
                noDataLabel(text: "Files have not been downloaded yet , If you want to save your files offline in Elibrary , Click on 'Download' button." )
                refresh.endRefreshing()
                stopLoading()
            }
            
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
      
            
            
            
    
            
            var fileSavePath = String()
            
            if(fileSavePath == String()){
                fileSavePath = self.offlineFileDB[(indexPath?.row)!].fileSavedPath!
            }
        if(self.offlineFileDB[(indexPath?.row)!].filetype! == "PDF"){
                let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
                let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HzlPdfReaderViewController") as! HzlPdfReaderViewController
                HZlPolicyTempData.ELCat_ID = String(self.offlineFileDB[(indexPath?.row)!].id!)
                
                HZlPolicyTempData.file_Url = fileSavePath
                HZlPolicyTempData.file_Name = self.offlineFileDB[(indexPath?.row)!].filename!
                self.navigationController?.pushViewController(ELPVC, animated: true)
        }else if(self.offlineFileDB[(indexPath?.row)!].filetype! == "PPT" || self.offlineFileDB[(indexPath?.row)!].filetype! == "Document" ||  self.offlineFileDB[(indexPath?.row)!].filetype! == "Excel"){
            let urlValue = "https://view.officeapps.live.com/op/view.aspx?src=" + self.offlineFileDB[(indexPath?.row)!].filePath!
            linkHzl(urlData: urlValue, title: self.offlineFileDB[(indexPath?.row)!].filename!)
        }else if(self.offlineFileDB[(indexPath?.row)!].filetype! == "RAR" || self.offlineFileDB[(indexPath?.row)!].filetype! == "ZIP" ){
           self.view.makeToast("Cannot Open Zip/RAR file in this Application")
        }else{
            
            
            
                let urlString = fileSavePath
                let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                videoPlay(str: urlShow!)
            }
        
        
    }
    func linkHzl(urlData : String , title : String){
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
        
        let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
        guard   let url = URL(string: urlData) else {
            return
        }
        progressWebViewController.disableZoom = true
        progressWebViewController.url = url
        progressWebViewController.bypassedSSLHosts = [url.host!]
        progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
        progressWebViewController.websiteTitleInNavigationBar = false
        progressWebViewController.navigationItem.title = title
        progressWebViewController.leftNavigaionBarItemTypes = [.reload]
        progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
        self.present(proNav, animated: true, completion: nil)
    }
    func playerDidFinishPlaying(note: NSNotification) {
        
        self.videoPlayer.pause()
        
    }
    
    
    var videoPlayer = AVPlayer()
    func videoPlay(str: String) {
        
        let downloadURLHD = str
        //        let fileName = downloadURL.characters.split("/").map(String.init).last as String!
        let fileNameHD = downloadURLHD.characters.split(separator: "/").map(String.init).last as String?
        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as! String
        
        let myDownPath = MZUtility.baseFilePath + "/DownloadData/"
        
        let downloadFilePath = myDownPath + "\(fileNameHD!)"
        
        let checkValidation = FileManager.default
        
        if checkValidation.fileExists(atPath: downloadFilePath){
            print("video found")
            let mediaURL = URL(fileURLWithPath: downloadFilePath)
            let asset = AVURLAsset(url: mediaURL)
            let playerItem = AVPlayerItem(asset: asset)
            let playerViewController = AVPlayerViewController()
            let player = AVPlayer(playerItem: playerItem)
            playerViewController.player = player
            NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player)
            self.present(playerViewController, animated: true)
            {
                playerViewController.player!.play()
            }
            do {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
            } catch _ {
                
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DownloadedViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.offlineFileDB.count
    }
    
    
}

extension DownloadedViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DownloadedTableViewCell
        
        let tapGestureCell = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGestureCell)
        var type = String()
        cell.btnDelete.tag = indexPath.row
        cell.CategoryTitle.text =  self.offlineFileDB[indexPath.row].filename
        cell.textViewDesc.text = self.offlineFileDB[indexPath.row].descriptionValue
        cell.lblDate.text =  self.offlineFileDB[indexPath.row].downloadDate
        
        if(offlineFileDB[indexPath.row].thumbPath == ""){
            
            if(  offlineFileDB[indexPath.row].filetype == "PDF"){
                cell.imageFile.image = UIImage.init(named: "pdfFile")
                type = ".pdf"
            }else if(offlineFileDB[indexPath.row].filetype == "PPT" ){
                cell.imageFile.image = UIImage(named: "ppt")
                type = ".ppt"
            }else if(offlineFileDB[indexPath.row].filetype == "Document" ){
                cell.imageFile.image = UIImage(named: "word")
                type = ".docx"
            }else if(offlineFileDB[indexPath.row].filetype == "Excel" ){
                cell.imageFile.image = UIImage(named: "excel")
                type = ".xls"
            }else if(offlineFileDB[indexPath.row].filetype == "Audio" ){
                cell.imageFile.image = UIImage(named: "music")
                type = ".mp3"
            }else if(offlineFileDB[indexPath.row].filetype == "RAR" ){
                cell.imageFile.image = UIImage(named: "rar")
                type = ".rar"
            }else if(offlineFileDB[indexPath.row].filetype == "ZIP" ){
                cell.imageFile.image = UIImage(named: "zip")
                type = ".zip"
            } else {
                cell.imageFile.image = UIImage.init(named: "video")
                type = ".mp4"
            }
        }else{
            if let url = NSURL(string: offlineFileDB[indexPath.row].thumbPath!) {
                cell.imageFile.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
            }
        }

        
        return cell;
    }
}
