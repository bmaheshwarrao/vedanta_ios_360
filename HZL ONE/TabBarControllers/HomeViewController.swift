//
//  HomeViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//
import UIKit

import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation
import EzPopup
var UpadteVal = Int()
var dataHomePage : [HomePageDataModel] = []
var dataHomePageDB : [HzlApplicationMarket] = []
class HomeViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource{
    
    
    
 
    @IBOutlet weak var homeTableView: UITableView!
    
    
    
    
    
    
    
    
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //searchBarButton.isHidden = true
       // self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
        let leftToRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(leftTorightDidfire))
        
        leftToRight.direction = .right
        
        let rightToLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(rightToLeftDidfire))
        rightToLeft.direction = .left
        
        self.homeTableView.isScrollEnabled = true
        self.homeTableView.addGestureRecognizer(leftToRight)
        self.homeTableView.addGestureRecognizer(rightToLeft)
        self.view.addGestureRecognizer(leftToRight)
        self.view.addGestureRecognizer(rightToLeft)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(graphDataInsert), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "graphDataUpdate")), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(GetBusinessData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "mainLocationDataUpdate")), object: nil)
        //
        //
        //        graphDataInsert()
        
        
        getBannerImageData()
        profileDetailsData()
        let gcmRegister : Bool =  UserDefaults.standard.bool(forKey: "GCMRegister")
        if(gcmRegister == false){
            GCMRegister()
        }
        
        if reachability.connection == .none{
//            self.view.makeToast("Internet is not available, please check your internet connection try again." )
            self.showSingleButtonAlertWithoutAction(title: "Internet is not available, please check your internet connection try again.")
        }
     
        
    }
    
    func alertOptional(){
        
        
        
        
            guard let updateAlertVC = updateAlertVC else { return }
            
            updateAlertVC.message = "Optional"
            let popupVC = PopupViewController(contentController: updateAlertVC, popupWidth: 300)
            popupVC.cornerRadius = 5
            popupVC.canTapOutsideToDismiss = false
            present(popupVC, animated: true, completion: nil)
            UpadteVal = 0
        
        
    }
    let updateAlertVC = UpdateVersionPageViewController.instantiate()
    func alertManadatory(){
        
        
        
            guard let updateAlertVC = updateAlertVC else { return }
            
            updateAlertVC.message = "Mandatory"
            let popupVC = PopupViewController(contentController: updateAlertVC, popupWidth: 300)
            popupVC.cornerRadius = 5
            popupVC.canTapOutsideToDismiss = false
            present(popupVC, animated: true, completion: nil)
            UpadteVal = 1
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        updateApp()
     
    }
    var UpdateDB = [UpdateAppData]()
    var UrlDB = [UrlData]()
    var UpDataAPI = UpdateDataAPI()
    func updateApp() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        self.UpdateDB = [UpdateAppData]()
        self.UrlDB = [UrlData]()
        self.UpDataAPI.serviceCalling()
            { (dict) in
                do {
                    
                    if( UpadteVal != 2){
                        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                        print(version!)
                        let updateVersion = UserDefaults.standard.string(forKey: "version")
                        // let updateVersion = "Optional"
                        // print(updateVersion!)
                        let updateType = UserDefaults.standard.string(forKey: "updateType")
                        // let updateType = "Optional"
                        if(updateVersion != nil){
                            if( version != updateVersion!) {
                                let type = updateType!.lowercased()
                                if(type == "optional") {
                                    
                                    //                alertManadatory()
                                    //                UpadteVal = 1
                                    self.alertOptional()
                                    UpadteVal = 0
                                    
                                } else {
                                    self.alertManadatory()
                                    UpadteVal = 1
                                }
                                
                            }
                        }
                    }
                } catch {
                    
                }
        }
        
        
        
        
        
    }
    @objc func reloadHome(){
        if(dataHomePage.count == 0){
        getBannerImageData()
        profileDetailsData()
        indexing = 0
        self.homeTableView.reloadData()
        }
    }
    @IBAction func btnPromotionImageClicked(_ sender: UIButton) {
        if(dataPromotion[sender.tag].Type_Data != ""){
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HzlPdfReaderViewController") as! HzlPdfReaderViewController
        HZlPolicyTempData.ELCat_ID = String(dataPromotion[sender.tag].ID)
        
        HZlPolicyTempData.file_Url = dataPromotion[sender.tag].Type_Data
        HZlPolicyTempData.file_Name = "PDF"
        self.navigationController?.pushViewController(ELPVC, animated: true)
        }
    }
    
    @IBAction func btnSocialImageClicked(_ sender: UIButton) {
        if(dataSocial[sender.tag].Account_URL != ""){
          linkHzl(urlData: dataSocial[sender.tag].Account_URL, title: dataSocial[sender.tag].Account_Name)
        }
    }
    func linkHzl(urlData : String , title : String){
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
        
        let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
        guard   let url = URL(string: urlData) else {
            return
        }
        progressWebViewController.disableZoom = true
        progressWebViewController.url = url
        progressWebViewController.bypassedSSLHosts = [url.host!]
        progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
        progressWebViewController.websiteTitleInNavigationBar = false
        progressWebViewController.navigationItem.title = title
        progressWebViewController.leftNavigaionBarItemTypes = [.reload]
        progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
        self.present(proNav, animated: true, completion: nil)
    }
    var reachability = Reachability()!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var Profile_AuthKey = String()
    @objc func profileDetailsData(){
        self.Profile_AuthKey = UserDefaults.standard.value(forKey: "Profile_AuthKey") as! String
     
        let parameters = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
                          "Profile_AuthKey":self.Profile_AuthKey,
                          "AppName":URLConstants.appName]
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetProfile, parameters: parameters, successHandler: { (response:[String : AnyObject]) in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                
                let respon = response["response"] as! [String:AnyObject]
                
                if self.reachablty.connection != .none{
                    
                    print(respon)
                    if respon["status"] as! String == "success" {
                        
                        self.deleteProfileData()
                        
                        let dict = response["data"] as! [String:AnyObject]
                        var Employee_Name = String()
                        var Employee_ID = String()
                        var Gender = String()
                        var Email_ID_Official = String()
                        var Profile_Pic_Path = String()
                        var Mobile_Number = String()
                        
                        var Zone_Name = String()
                        var Unit_Name = String()
                        var Area_Name = String()
                        var Designation = String()
                         var Designation_Name = String()
                        
                        
                        if dict["Employee_ID"] is NSNull || dict["Employee_ID"] == nil{
                            Employee_ID = ""
                        }else{
                            Employee_ID = (dict["Employee_ID"] as? String)!
                            
                        }
                        
                
                        if dict["Employee_Name"] is NSNull || dict["Employee_Name"] == nil{
                            Employee_Name = ""
                        }else{
                            Employee_Name = (dict["Employee_Name"] as? String)!
                              UserDefaults.standard.set(Employee_Name, forKey: "empName")
                        }
                        
                        if dict["Gender"] is NSNull || dict["Gender"] == nil{
                            Gender = ""
                        }else{
                            Gender = (dict["Gender"] as? String)!
                        }
                        if dict["Email_ID"] is NSNull || dict["Email_ID"] == nil{
                            Email_ID_Official = ""
                        }else{
                            Email_ID_Official = (dict["Email_ID"] as? String)!
                        }
                        if dict["Profile_Pic_Path"] is NSNull || dict["Profile_Pic_Path"] == nil{
                            Profile_Pic_Path = ""
                        }else{
                            Profile_Pic_Path = (dict["Profile_Pic_Path"] as? String)!
                        }
                        if dict["Mobile_Number"] is NSNull || dict["Mobile_Number"] == nil{
                            Mobile_Number = ""
                        }else{
                            Mobile_Number = (dict["Mobile_Number"] as? String)!
                          UserDefaults.standard.set(Mobile_Number, forKey: "mobileData")
                        }
                        if dict["Designation_Name"] is NSNull || dict["Designation_Name"] == nil{
                            Designation_Name = ""
                        }else{
                            Designation_Name = dict["Designation_Name"] as! String
                        }
                        
                        if dict["Designation"] is NSNull || dict["Designation"] == nil{
                            Designation = ""
                        }else{
                            Designation = dict["Designation"] as! String
                        }
                        
                        if dict["Area_Name"] is NSNull || dict["Area_Name"] == nil{
                            Area_Name = ""
                        }else{
                            Area_Name = dict["Area_Name"] as! String
                        }
                        if dict["Zone_Name"] is NSNull || dict["Zone_Name"] == nil{
                            Zone_Name = ""
                        }else{
                            Zone_Name = dict["Zone_Name"] as! String
                        }
                        
                        if dict["Unit_Name"] is NSNull || dict["Unit_Name"] == nil{
                            Unit_Name = ""
                        }else{
                            Unit_Name = dict["Unit_Name"] as! String
                        }
                        
                        print(Mobile_Number)
                        
                        self.saveProfileData(Employee_Name: Employee_Name as! String, Gender: Gender as! String, Email_ID_Official: Email_ID_Official as! String, Profile_Pic_Path: Profile_Pic_Path as! String, Mobile_Number: String(Mobile_Number), Zone_Name: Zone_Name, Unit_Name: Unit_Name, Area_Name: Area_Name, Designation: Designation,Employee_Id :Employee_ID,Designation_Name : Designation_Name)
                        
                        //self.saveProfileData(Employee_Name: Employee_Name as! String, SBU_ID: String(SBU_ID), SBU_Name: SBU_Name as! String, Department: String(Department), Department_Name: Department_Name as! String, Designation:  String(Designation), Gender: Gender as! String, Email_ID_Official: Email_ID_Official as! String, Profile_Pic_Path: Profile_Pic_Path as! String, Mobile_Number: String(Mobile_Number), Designation_Name: Designation_Name as! String)
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    }
                }
            }
            
        }) { (err) in
            print(err.description)
        }
        
    }
    
    
   
    
    //Core Data
    func saveProfileData (Employee_Name:String,Gender:String,Email_ID_Official:String,Profile_Pic_Path:String,Mobile_Number:String,Zone_Name:String,Unit_Name:String,Area_Name:String,Designation:String,Employee_Id:String,Designation_Name : String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = Profile(context: context)
        
        tasks.employee_Name = Employee_Name
        //         tasks.sBU_ID = SBU_ID
        //         tasks.sBU_Name = SBU_Name
        //         tasks.department = Department
        //         tasks.department_Name = Department_Name
        //         tasks.designation = Designation
        tasks.gender = Gender
        tasks.mobile_Number = Mobile_Number
        tasks.email_ID_Official = Email_ID_Official
        tasks.profile_Pic_Path = Profile_Pic_Path
        //tasks.designation_Name = Designation_Name
         tasks.employeeId = Employee_Id
        tasks.zone_Name = Zone_Name
        tasks.unit_Name = Unit_Name
        tasks.area_Name = Area_Name
        tasks.designation = Designation
        tasks.designation_Name = Designation_Name
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
        
        
    }
    
    func deleteProfileData()
    {
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Profile")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    @IBAction func openSettingVC(_ sender: UIBarButtonItem) {
        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
        self.navigationController?.pushViewController(messageVC, animated: true)
    }
    @IBAction func openMessageVC(_ sender: Any) {
        
        //        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "Message") as! MessageViewController
        //        self.navigationController?.pushViewController(messageVC, animated: true)
        
    }
    
    //////////////// Button Clicked Events  //////////////////////////////
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = ProjectName
        self.tabBarController?.tabBar.isHidden = false
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let logoImage = UIImage.init(named: "hzlLogo1")
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x:0.0,y:0.0, width:40,height:40.0)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 40)
        let heightConstraint = logoImageView.heightAnchor.constraint(equalToConstant: 40)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        self.navigationItem.leftBarButtonItem =  imageItem
        reloadHome();
        
    }
    @objc func getBannerImageData() {
        
        var imageDB = [BannerImage]()
        do {
            
            imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(imageDB.count > 0) {
                //BannerStruct.bannerPath = imageDB[0].text_Message!
            }
            self.homeTableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 || section == 1 || section == 2 ){
            
            return 0.0
        }else{
            return 50.0
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
        
        
        
        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HzlMainCellHeaderTableViewCell
        if(section == 3){
            header.lblHeader.text = "Share and Commodity Price"
        }else if(section == 4){
            header.lblHeader.text = "Social Media"
        }else{
            header.lblHeader.text = ""
        }
        header.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return header
    }
    var indexing = 0
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageTableViewCell
            if(indexing == 0){
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
                cell.pageView(ApplicationName: "HZL_ONE")
            tableView.isScrollEnabled = true
            }
            return cell
            
        }else  if indexPath.section == 1 {
            let page = "buttons"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! MainGraphTableViewCell
            if(indexing == 0){
            //            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            //            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.HomePageCalling(obj: self)
            tableView.isScrollEnabled = true
            }
            return cell
            
        }
        
        else  if indexPath.section == 2 {
            let page = "promotion"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PromotionTableViewCell
            if(indexing == 0){
//            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
//            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.HomePromotionCalling()
            tableView.isScrollEnabled = true
            }
            return cell
            
        }else  if indexPath.section == 3 {
            let page = "price"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PriceTableViewCell
            if(indexing == 0){
            //            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            //            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView()
            tableView.isScrollEnabled = true
            }
            return cell
            
        }else  {
            let page = "social"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! SocialTableViewCell
           
            //            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            //            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
         
            cell.HomeSocialCalling()
            tableView.isScrollEnabled = true
            
            
            indexing = indexing + 1;
            return cell
            
        }
        
        
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        

        let screenSize = UIScreen.main.bounds
        //let screenHeight = screenSize.height-navheight-tabHeight!
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else if indexPath.section == 2{
            if(dataHomePage.count == 0){
                return 0
            }else{
            return 200
            }
        }else if indexPath.section == 4{
            if(dataHomePage.count == 0){
                return 0
            }else{
                return 200
            }
        }
        else if indexPath.section == 3{
            if(dataHomePage.count == 0){
                return 0
            }else{
                return 100
            }
        }
        else{
            print(dataHomePageDB.count)
            var height : CGFloat = 0
            let dd = dataHomePageDB.count / 4
            let ff = dataHomePageDB.count % 4
            if(ff == 0){
                height = CGFloat(dd * 100)
            }else{
                height = CGFloat((dd+1) * 100)
            }
            
            
            return height
        }
        
    }
    
  
    @IBAction func MQuizClicked(_ sender: UIButton) {
        
        
        
        
        
        
        
        
    }
    
    @IBAction func HistoryClicked(_ sender: UIButton) {
        
    }
    @IBAction func NewsandUpdateClicked(_ sender: UIButton) {
       
        
    }
    @IBAction func ButtonDashboardClicked(_ sender: UIButton) {
        print(sender.tag)
        print(dataHomePageDB[sender.tag].actionData)
         if(dataHomePageDB[sender.tag].actionData == "Hazard_Reporting"){
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "ThemeDashboardViewController") as! ThemeDashboardViewController
            submitVC.themeStrNav = dataHomePageDB[sender.tag].appName!
            submitVC.applicationName = dataHomePageDB[sender.tag].actionData!
            self.navigationController?.pushViewController(submitVC, animated: true)
         }else if(dataHomePageDB[sender.tag].actionData == "5s_Audit"){
            
            let storyBoard = UIStoryboard(name: "Audit", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "DashboardAuditViewController") as! DashboardAuditViewController
            submitVC.applicationName = dataHomePageDB[sender.tag].actionData!
            submitVC.statusStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(submitVC, animated: true)
            
            
            
         }else if(dataHomePageDB[sender.tag].actionData == "SI"){
            
            let storyBoard = UIStoryboard(name: "SI", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "SIDashboardViewController") as! SIDashboardViewController
            submitVC.applicationName = dataHomePageDB[sender.tag].actionData!
            submitVC.statusStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(submitVC, animated: true)
            
            
            
         }else if(dataHomePageDB[sender.tag].actionData == "HZL_KPI"){
            
            let storyBoard = UIStoryboard(name: "SafetyKpi", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "SafetyKpiDashboardViewController") as! SafetyKpiDashboardViewController
            submitVC.applicationName = dataHomePageDB[sender.tag].actionData!
            submitVC.statusStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(submitVC, animated: true)
            
            
            
         }else if(dataHomePageDB[sender.tag].actionData == "Idea_Generation"){
            
            let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "DashboardIdeaViewController") as! DashboardIdeaViewController
            submitVC.applicationName = dataHomePageDB[sender.tag].actionData!
            submitVC.statusStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(submitVC, animated: true)
            
            
          
        }else if(dataHomePageDB[sender.tag].actionData == "E_Learning"){
            
            let storyBoard = UIStoryboard(name: "eLearning", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "eLearningDashboardViewController") as! eLearningDashboardViewController
            submitVC.applicationName = dataHomePageDB[sender.tag].actionData!
            submitVC.StrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(submitVC, animated: true)
            
            
            
        }else if(dataHomePageDB[sender.tag].actionData == "M_Quiz"){
            
            let storyBoard = UIStoryboard(name: "mQuiz", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "mQuizFirstViewController") as! mQuizFirstViewController
           submitVC.QuizStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(submitVC, animated: true)
            
            
            
        }else if(dataHomePageDB[sender.tag].actionData == "Safety_Whistleblower"){
            
            FilterDataFromServer.dept_Name = String()
            FilterDataFromServer.dept_Id = Int()
            FilterDataFromServer.unit_Name = String()
             FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.area_Name = String()
             FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.location_name = String()
             FilterDataFromServer.location_id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            
            let storyBoard = UIStoryboard(name: "SafetyWhistle", bundle: nil)
            let messageVC = storyBoard.instantiateViewController(withIdentifier: "SafetyWhisleBlowerViewController") as! SafetyWhisleBlowerViewController
            messageVC.SafetyStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(messageVC, animated: true)
        }else if(dataHomePageDB[sender.tag].actionData == "HZL_Travel"){
            
            let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
            let messageVC = storyBoard.instantiateViewController(withIdentifier: "VehicleDashboardViewController") as! VehicleDashboardViewController
            messageVC.VehicleStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(messageVC, animated: true)
            
        }else if(dataHomePageDB[sender.tag].actionData == "Movie_Update"){
            let storyBoard = UIStoryboard(name: "MovieUpdate", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
            ZIVC.movieStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }else if(dataHomePageDB[sender.tag].actionData == "News_Updates"){
            let storyBoard = UIStoryboard(name: "NewsUpdate", bundle: nil)
            let newsVC = storyBoard.instantiateViewController(withIdentifier: "NewsandupdateViewController") as! NewsandupdateViewController
            newsVC.newsStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(newsVC, animated: true)
            
        }else if(dataHomePageDB[sender.tag].actionData == "Organization_Advice"){
            
            let storyBoard = UIStoryboard(name: "OrganizationAdvice", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "OrganizationAdviceViewController") as! OrganizationAdviceViewController
            ZIVC.adviceStrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }else if(dataHomePageDB[sender.tag].actionData == "IT_Security_Tips"){
            let storyBoard = UIStoryboard(name: "Security", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "SecurityTipsViewController") as! SecurityTipsViewController
             ZIVC.StrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(ZIVC, animated: true)
          
        }else if(dataHomePageDB[sender.tag].actionData == "Health_Tips"){
            let storyBoard = UIStoryboard(name: "Health", bundle: nil)
            let newsVC = storyBoard.instantiateViewController(withIdentifier: "HealthTipsViewController") as! HealthTipsViewController
             newsVC.StrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(newsVC, animated: true)
            
        }else if(dataHomePageDB[sender.tag].actionData == "CSC"){
                        let storyBoard : UIStoryboard = UIStoryboard(name: "CSC",bundle : nil)
                        let questionVC = storyBoard.instantiateViewController(withIdentifier: "CSCFirstViewController") as! CSCFirstViewController
            moveLight = 0
             questionVC.StrNav = dataHomePageDB[sender.tag].appName!
                        self.navigationController?.pushViewController(questionVC, animated: true)
        }else if(dataHomePageDB[sender.tag].actionData == "Guest_House"){
            let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "GuestHouseDashboardViewController") as! GuestHouseDashboardViewController
              ZIVC.StrNav = dataHomePageDB[sender.tag].appName!
            ZIVC.applicationName = dataHomePageDB[sender.tag].actionData!
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }else if(dataHomePageDB[sender.tag].actionData == "NDSO"){
            let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "NDSODashboardViewController") as! NDSODashboardViewController
            ZIVC.applicationName = dataHomePageDB[sender.tag].actionData!
            ZIVC.StrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }else if(dataHomePageDB[sender.tag].actionData == "ODSO"){
            let storyBoard = UIStoryboard(name: "ODSO", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ODSODashboardViewController") as! ODSODashboardViewController
            ZIVC.applicationName = dataHomePageDB[sender.tag].actionData!
            ZIVC.StrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }else if(dataHomePageDB[sender.tag].actionData == "LSR"){
            let storyBoard = UIStoryboard(name: "LSR", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "LsrFirstViewController") as! LsrFirstViewController
           ZIVC.StrNav = dataHomePageDB[sender.tag].appName!
            ZIVC.applicationName = dataHomePageDB[sender.tag].actionData!
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        else if(dataHomePageDB[sender.tag].actionData == "Welldone"){
            let storyBoard = UIStoryboard(name: "WellDone", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "WellDoneDashboardViewController") as! WellDoneDashboardViewController
            ZIVC.StrNav = dataHomePageDB[sender.tag].appName!
              ZIVC.applicationName = dataHomePageDB[sender.tag].actionData!
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
         else if(dataHomePageDB[sender.tag].actionData == "Township"){
            let storyboard = UIStoryboard(name: "Township", bundle: nil)
            let FBVC = storyboard.instantiateViewController(withIdentifier: "DashboardTownshipViewController") as! DashboardTownshipViewController
            FBVC.statusStrNav = dataHomePageDB[sender.tag].appName!
             FBVC.applicationName = dataHomePageDB[sender.tag].actionData!
            self.navigationController?.pushViewController(FBVC, animated: true)
         }
         else if(dataHomePageDB[sender.tag].actionData == "MOM"){
            let storyboard = UIStoryboard(name: "MOM", bundle: nil)
            let FBVC = storyboard.instantiateViewController(withIdentifier: "MOMDashboardViewController") as! MOMDashboardViewController
            FBVC.statusStrNav = dataHomePageDB[sender.tag].appName!
            FBVC.applicationName = dataHomePageDB[sender.tag].actionData!
            self.navigationController?.pushViewController(FBVC, animated: true)
         }
        
        else if(dataHomePageDB[sender.tag].actionData == "Feedback"){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FBVC = storyboard.instantiateViewController(withIdentifier: "Feedback") as! FeedbackViewController
            FBVC.StrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(FBVC, animated: true)
        }
         else if(dataHomePageDB[sender.tag].actionData == "GHOSNA"){
            let storyboard = UIStoryboard(name: "Ghosna", bundle: nil)
            let FBVC = storyboard.instantiateViewController(withIdentifier: "GhosnaFileListViewController") as! GhosnaFileListViewController
            FBVC.StrNav = dataHomePageDB[sender.tag].appName!
            self.navigationController?.pushViewController(FBVC, animated: true)
         }else{
            self.view.makeToast("Application will launch soon.")
        }
        
   
       
    }
    @IBAction func btnSecurityClicked(_ sender: UIButton) {
      
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = false
//    }
    
    @IBAction func btnVehicleRequisitionClicked(_ sender: UIButton) {
        
        let vehicleRequistionVC = self.storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
        self.navigationController?.pushViewController(vehicleRequistionVC, animated: true)
        
    }
    
    @IBAction func btnNotificationClicked(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let messageVC = storyBoard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        
        self.navigationController?.pushViewController(messageVC, animated: true)
    }
    @IBAction func btnSearchClicked(_ sender: UIBarButtonItem) {
        
    }
    
    
    @IBAction func btnODSOClicked(_ sender: UIButton) {
   
    }
    @IBAction func healthTipsClicked(_ sender: UIButton) {
       
        
    }
    @IBAction func NotificationClicked(_ sender: UIButton) {
        
        //        let InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxDisplayViewController") as! InboxDisplayViewController
        //
        //        self.navigationController?.pushViewController(InboxVC, animated: true)
    }
    func addLeftBarIcon() {
        
       
    }
    @objc func leftTorightDidfire()
    {
        switch UserDefaults.standard.bool(forKey: "isContractorLogin") || UserDefaults.standard.bool(forKey: "isWorkmanLogin"){
        case true:
            
            break;
        default:
            let tabBar1: UITabBar = (self.tabBarController?.tabBar)!
            
            let index: Int = (tabBar1.items)!.index(of: tabBar1.selectedItem!)!
            if(index > 0)
            {
                
                self.tabBarController?.selectedIndex = index - 1
                
            }
            else{
                return
            }
            break;
        }
        
    }
    
    @objc func rightToLeftDidfire()
    {
        
        switch UserDefaults.standard.bool(forKey: "isContractorLogin") || UserDefaults.standard.bool(forKey: "isWorkmanLogin"){
        case true:
            
            break;
        default:
            
            let tabBar1: UITabBar = (self.tabBarController?.tabBar)!
            
            let index: Int = (tabBar1.items)!.index(of: tabBar1.selectedItem!)!
            if(index < (tabBar1.items?.count)! - 1)
            {
                
                self.tabBarController?.selectedIndex = index + 1
                
            }
            else{
                return
            }
            
            
            break;
        }
        
        
        
    }
    
    var GCMToken = String()
    func GCMRegister(){
        
        if let object = UserDefaults.standard.value(forKey: "Token") {
            
            GCMToken = object as! String
        }
        print(GCMToken)
        let para = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
                    "GCM_Token":GCMToken,
                    "Platform":"iOS"]
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.Post_Gcm, parameters: para, successHandler: { (dict) in
            
            print("GCMRegister",dict)
            if let response = dict["response"]{
                
                let statusString : String = response["status"] as! String
                
                if(statusString == "success")
                {
                    
                    
                    //                        let tokenIDString  = data["Token_ID"]
                    UserDefaults.standard.set(true, forKey: "GCMRegister")
                    //                        UserDefaults.standard.set(tokenIDString, forKey: "myTokenId")
                    
                    
                }
            }
            
        }) { (err) in
            print(err)
        }
        
    }
    
    
    
    //Core Data
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
