//
//  MyInboxTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyInboxTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblShowHazAction: UILabel!
    
    @IBOutlet weak var viewShow: UIView!
    @IBOutlet weak var lblAddData: UILabel!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        img.layer.cornerRadius = 20;
        img.backgroundColor = UIColor.colorwithHexString("2c3e50", alpha: 1.0)
        viewShow.layer.cornerRadius = 22.5;
        viewShow.backgroundColor = UIColor.colorwithHexString("2c3e50", alpha: 0.3)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        img.layer.cornerRadius = 20;
        img.backgroundColor = UIColor.colorwithHexString("2c3e50", alpha: 1.0)
        viewShow.layer.cornerRadius = 22.5;
        viewShow.backgroundColor = UIColor.colorwithHexString("2c3e50", alpha: 0.3)
        
        // Configure the view for the selected state
    }
    required init(coder aDecoder: (NSCoder?)) {
        super.init(coder: aDecoder!)!
    }
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        img.layer.cornerRadius = 20;
        
    }
    
}
