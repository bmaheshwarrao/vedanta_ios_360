//
//  ModelHomePageCall.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability
import CoreData

class HomePageDataModel: NSObject {
    
     let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var ID = Int()
    var AppName = String()
    var ActionType = String()
    var ActionData = String()
    var Platform = String()
    var DateTime = String()
    var CategoryName = String()
    var Featured_App = String()
    var New_App = String()
    var App_Icon = String()
    
   
    func saveBannerPathData (appName:String, id:String,actionType:String,actionData:String,platform:String,dateTime:String,categoryName:String,featured_App:String,new_App:String,app_Icon:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = HzlApplicationMarket(context: context)
        
        tasks.appName = appName
        tasks.id = id
        tasks.actionType = actionType
        tasks.actionData = actionData
        tasks.platform = platform
        tasks.dateTime = dateTime
        tasks.categoryName = categoryName
        tasks.featured_App = featured_App
        tasks.new_App = new_App
        tasks.app_Icon = app_Icon
        
        
        
        
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
       
        
    }
    

    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["AppName"] is NSNull || str["AppName"] == nil{
            self.AppName = ""
        }else{
            
            let tit = str["AppName"]
            
            self.AppName = (tit?.description)!
            
            
        }
        if str["ActionType"] is NSNull || str["ActionType"] == nil{
            self.ActionType =  ""
        }else{
            
            let desc = str["ActionType"]
            
            self.ActionType = (desc?.description)!
            
          
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Platform"] is NSNull || str["Platform"] == nil{
            self.Platform = ""
        }else{
            let catId = str["Platform"]
            
            self.Platform = (catId?.description)!
        
        }
        if str["DateTime"] is NSNull || str["DateTime"] == nil{
            self.DateTime = ""
        }else{
            
            let rem = str["DateTime"]
            
            self.DateTime = (rem?.description)!
            
        }
        
        if str["CategoryName"] is NSNull || str["CategoryName"] == nil{
            self.CategoryName = ""
        }else{
            let cat = str["CategoryName"]
            
            self.CategoryName = (cat?.description)!
            
            
            
        }
        if str["ActionData"] is NSNull || str["ActionData"] == nil{
            self.ActionData = ""
        }else{
            let createDate = str["ActionData"]
            
            self.ActionData = (createDate?.description)!
        
            
        }
        
        
        
        if str["Featured_App"] is NSNull || str["Featured_App"] == nil{
            self.Featured_App = ""
        }else{
            let createDate = str["Featured_App"]
            
            self.Featured_App = (createDate?.description)!
            
            
        }
        if str["New_App"] is NSNull || str["New_App"] == nil{
            self.New_App = ""
        }else{
            let createDate = str["New_App"]
            
            self.New_App = (createDate?.description)!
            
            
        }
        if str["App_Icon"] is NSNull || str["App_Icon"] == nil{
            self.App_Icon = ""
        }else{
            let createDate = str["App_Icon"]
            
            self.App_Icon = (createDate?.description)!
            
            
        }
        
        saveBannerPathData(appName: AppName, id: String(ID), actionType: ActionType, actionData: ActionData, platform: Platform, dateTime: DateTime, categoryName: CategoryName, featured_App: Featured_App, new_App: New_App, app_Icon: App_Icon)
        
        
        
    }
    
}


class HomeSharePageDataModel: NSObject {
    
    
    var ID = Int()
    var Name = String()
    var Price = String()
    var Unit = String()
    var Date_Time = String()
  
    
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
    
        if str["Name"] is NSNull || str["Name"] == nil{
            self.Name = ""
        }else{
            
            let tit = str["Name"]
            
            self.Name = (tit?.description)!
            
            
        }
        if str["Price"] is NSNull || str["Price"] == nil{
            self.Price =  ""
        }else{
            
            let desc = str["Price"]
            
            self.Price = (desc?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Unit"] is NSNull || str["Unit"] == nil{
            self.Unit = ""
        }else{
            let catId = str["Unit"]
            
            self.Unit = (catId?.description)!
            
        }
        if str["Date_Time"] is NSNull || str["Date_Time"] == nil{
            self.Date_Time = ""
        }else{
            
            let rem = str["Date_Time"]
            
            self.Date_Time = (rem?.description)!
            
        }
        
        
        
    }
    
}

//class HomePageAPI
//{
//
//    var reachablty = Reachability()!
//
//    func serviceCalling(obj:SecurityTipsViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
//    {
//        obj.startLoading()
//
//        if(reachablty.connection != .none)
//        {
//
//
//            WebServices.sharedInstances.sendPostRequest(url: URLConstants.AppMarket, parameters: param , successHandler: { (dict) in
//
//                if let response = dict["response"]{
//
//                    let statusString : String = response["status"] as! String
//
//                    if(statusString == "success")
//                    {
//
//                        if let data = dict["data"] as? [AnyObject]
//                        {
//
//                            var dataArray: [HomePageDataModel] = []
//
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = HomePageDataModel()
//                                    object.setDataInModel(str: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//
//                            success(dataArray as AnyObject)
//
//
//                        }
//
//                        obj.refreshControl.endRefreshing()
//                        obj.stopLoading()
//                    }
//                    else
//                    {
//                        obj.label.isHidden = false
//                        obj.tableView.isHidden = true
//                        obj.noDataLabel(text: "No Data Found!" )
//                        obj.refreshControl.endRefreshing()
//                        obj.stopLoading()
//
//                    }
//
//
//                }
//
//
//
//
//            })
//            { (error) in
//
//                let errorStr : String = error.description
//
//                print("DATA:error",errorStr)
//
//                //                obj.errorChecking(error: error)
//                obj.stopLoading()
//            }
//
//
//        }
//        else{
//            obj.label.isHidden = false
//            obj.tableView.isHidden = true
//            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
//            obj.stopLoading()
//        }
//
//
//    }
//
//
//}

