//
//  HZlPolicyViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 28/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
struct HZlPolicyTempData {
    static var Cat_ID = String()
    static var ELCat_ID = String()
    static var file_Url = String()
    static var file_Name = String()
    static var policy = String()
}
class HZlPolicyViewController: CommonVSClass,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var ELPolicyAPI = HzlCategoryPolicyDataAPI()
    var ELPolicyDB:[HzlCategoryPolicyModel] = []
    
    var videoPlayer = AVPlayer()
    var titleVal = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callELPolicyData()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 62
        tableView.rowHeight = UITableViewAutomaticDimension
        
        refresh.addTarget(self, action: #selector(callELPolicyData), for: .valueChanged)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        self.tableView.addSubview(refresh)
        titleVal = HZlPolicyTempData.policy
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callELPolicyData()
            }
            else
            {
                self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        
        if(self.titleVal == self.title){
            
        }else {
            self.title = HZlPolicyTempData.policy
        }
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ELPolicyDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HZlCategoryPolicyTableViewCell
        
        cell.nameLabel.text = self.ELPolicyDB[indexPath.row].Title
      
        switch self.ELPolicyDB[indexPath.row].File_Type {
        case "pdf":
            cell.typeImage.image = UIImage.init(named: "elpdf")
            break;
        case "Folder":
            cell.typeImage.image = UIImage.init(named: "folderSet")
            break;
        default:
            cell.typeImage.image = UIImage.init(named: "elyt")
            break;
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if  self.ELPolicyDB[indexPath.row].File_Type == "pdf"{
               let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
            let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HzlPdfReaderViewController") as! HzlPdfReaderViewController
            HZlPolicyTempData.ELCat_ID = String(self.ELPolicyDB[indexPath.row].ID)
         
            HZlPolicyTempData.file_Url = self.ELPolicyDB[indexPath.row].Link
            HZlPolicyTempData.file_Name = self.ELPolicyDB[indexPath.row].Title
            self.navigationController?.pushViewController(ELPVC, animated: true)
            
        }else{
            if self.ELPolicyDB[indexPath.row].Link != ""{
                videoPlay(str: self.ELPolicyDB[indexPath.row].Link)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func callELPolicyData(){
        
        self.ELPolicyAPI.serviceCalling(obj: self, Cat_ID: HZlPolicyTempData.Cat_ID)
        { (dict) in
            self.ELPolicyDB = dict as! [HzlCategoryPolicyModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    
    func playerDidFinishPlaying(note: NSNotification) {
        
        self.videoPlayer.pause()
        
    }
    
    func videoPlay(str: String) {
        
        let videoURL = URL(string: str)
        self.videoPlayer = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = videoPlayer
        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer)
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
            
        }
    }
    
}
