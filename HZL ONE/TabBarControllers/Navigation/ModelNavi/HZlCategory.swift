//
//  HZlCategory.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 28/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//


import Foundation
import Reachability
import CoreData

class HzlCategoryDataModel: NSObject {
    
    
    var ID = Int()
    var Category_Name = String()
   
    
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Category_Name"] is NSNull || str["Category_Name"] == nil{
            self.Category_Name = ""
        }else{
            
            let tit = str["Category_Name"]
            
            self.Category_Name = (tit?.description)!
            
            
        }
      
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
       
        
    }
    
}


class HzlcategoryDataAPI
{
    
    var reachablty = Reachability()!
    var categories_Name = String()
    var ID = Int()
    
    func serviceCalling(obj:HZLCategoryViewController , param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
           
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.Company_Policies_Category, parameters: param , successHandler: { (dict) in
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                      
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true;
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [HzlCategoryDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = HzlCategoryDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                            
                        }
                        
                        obj.tableView.reloadData()
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                      
                        print("DATA:fail")
                        obj.label.isHidden = false;
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                // obj.label.isHidden = false;
                //  obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
         
            obj.label.isHidden = false;
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, Please check internet connection and try again." )
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}




