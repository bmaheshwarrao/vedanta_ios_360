//
//  SubmittedQuizTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 18/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class SubmittedQuizTableViewCell:  UITableViewCell {
    
  
    
    @IBOutlet weak var lblGivenAns: UILabel!
    @IBOutlet weak var lblCorrectAns: UILabel!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var textViewQuestion: UITextView!
    @IBOutlet weak var questionNoLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        closeButton.isHidden = true
        // Initialization code
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
