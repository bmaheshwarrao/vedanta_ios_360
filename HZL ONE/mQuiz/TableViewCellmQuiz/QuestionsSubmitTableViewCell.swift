//
//  QuestionsSubmitTableViewCell.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 13/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
 var dataArrayLoad : [DataObject] = []
class QuestionsSubmitTableViewCell: UITableViewCell {

   @IBOutlet var btns: [DLRadioButton]!
    
    @IBOutlet weak var radioOptionD: DLRadioButton!
    @IBOutlet weak var radioOptionA: DLRadioButton!
    
    @IBOutlet weak var widthQuestionCon: NSLayoutConstraint!
    
    @IBOutlet weak var radioOptionC: DLRadioButton!
    @IBOutlet weak var radioOptionB: DLRadioButton!
    var arrayOption = ["A","B","C","D"]
  
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var textViewQuestion: UITextView!
    @IBOutlet weak var questionNoLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
         closeButton.isHidden = true
        // Initialization code
    }
    @IBAction func radioButtonClicked(_ sender: DLRadioButton){
        var strAction = String()
        for button in sender.selectedButtons() {
            print(String(format: "%@ is selected.\n", (button.titleLabel!.text!.characters.first?.description)!));
            strAction = (button.titleLabel!.text!.characters.first?.description)!
        }
        let object = DataObject()
       object.Id = String(sender.tag)
        object.val = strAction
        object.isSelected = true
        dataArrayLoad[sender.tag] = object
        print(sender.tag)
        closeButton.isHidden = false
        closeButton.tag = IntVal
       
    }
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        radioOptionA.deselectOtherButtons()
        radioOptionB.deselectOtherButtons()
        radioOptionC.deselectOtherButtons()
        radioOptionD.deselectOtherButtons()
        let object = DataObject()
        object.Id = String(sender.tag)
        object.val = ""
        object.isSelected = false
        dataArrayLoad[sender.tag] = object
        
        sender.isHidden = true
        //}
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class DataObject : NSObject{
    var Id : String?
    var val : String?
    var isSelected : Bool?
}
