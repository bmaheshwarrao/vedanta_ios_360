//
//  ELPolicyTableViewCell.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 09/11/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit

class ELPolicyTableViewCell: UITableViewCell {

   
    
    @IBOutlet weak var imageType: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
   
    @IBOutlet weak var containerView: UIViewX!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
