//
//  CustomViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 16/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CustomViewController: UIViewController {

    @IBOutlet weak var lblPending: UILabel!
    @IBOutlet weak var lblAttempt: UILabel!
    @IBOutlet weak var lblTotalQuestion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    static func instantiate() -> CustomViewController? {
        return UIStoryboard(name: "mQuiz", bundle: nil).instantiateViewController(withIdentifier: "CustomViewController") as? CustomViewController
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
