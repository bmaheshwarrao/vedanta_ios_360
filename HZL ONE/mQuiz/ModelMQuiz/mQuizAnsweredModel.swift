//
//  mQuizAnsweredModel.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 14/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

//
//  mQuizHistoryModel.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 14/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


//
//  ModelQuizQuestion.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 12/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


import Foundation
import Reachability



class QuizAnswerdModel: NSObject {
    
    
    
   
    var Que_ID : Int?
    var Question : String?
    var Given_Ans : String?
    var Correct_Ans : String?
    var IS_Correct : String?
    var Mark : String?
    var Correct_Answer : String?
    var Given_Answer : String?
   
    
    //    var Sbu_Department : String?
    //  var location_ID  : Int?
    // var sub_location_ID : Int?
    //   var Sub_location_Name : String?
    //  var main_location : String?
    // var business : String?
    //var main_location_ID : Int?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Question"] is NSNull || cell["Question"] == nil{
            self.Question = ""
        }else{
            let app = cell["Question"]
            self.Question = (app?.description)!
           
        }
        if cell["Given_Ans"] is NSNull || cell["Given_Ans"] == nil{
            self.Given_Ans = ""
        }else{
            let app = cell["Given_Ans"]
            self.Given_Ans = (app?.description)!
            
        }
        if cell["Correct_Ans"] is NSNull || cell["Correct_Ans"] == nil{
            self.Correct_Ans = ""
        }else{
            let app = cell["Correct_Ans"]
            self.Correct_Ans = (app?.description)!
          
        }
        if cell["IS_Correct"] is NSNull || cell["IS_Correct"] == nil{
            self.IS_Correct = ""
        }else{
            let app = cell["IS_Correct"]
            self.IS_Correct = (app?.description)!
           
        }
        if cell["Mark"] is NSNull || cell["Mark"] == nil{
            self.Mark = ""
        }else{
            let app = cell["Mark"]
            self.Mark = (app?.description)!
            
        }
        if cell["Correct_Answer"] is NSNull || cell["Correct_Answer"] == nil{
            self.Correct_Answer = ""
        }else{
            let app = cell["Correct_Answer"]
            self.Correct_Answer = (app?.description)!
            
        }
        if cell["Given_Answer"] is NSNull || cell["Given_Answer"] == nil{
            self.Given_Answer = ""
        }else{
            let app = cell["Given_Answer"]
            self.Given_Answer = (app?.description)!
            
        }
        
      
        if cell["Que_ID"] is NSNull{
            
        }else{
            self.Que_ID = (cell["Que_ID"] as? Int)!
        }
        
        
        
    }
}


class QuizAnswredAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:mQuizAnsweredListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Answered_Questions_History, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizAnswerdModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizAnswerdModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuizAnswredLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:mQuizAnsweredListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Answered_Questions_History, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizAnswerdModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizAnswerdModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}



