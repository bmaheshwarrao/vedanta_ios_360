
//
//  ModelQuizQuestion.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 12/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


import Foundation
import Reachability



class QuizSubmitModel: NSObject {
    
    
    
    var Question: String?
    
    var Option_A : String?
    var Option_B : String?
    var Option_C : String?
    var Option_D : String?
    
    var ID  = Int()
    
   
    var Subject_Title : String?
    var Test_ID : String?
    var Subject_ID : String?
    var Description : String?
    
    var Que_Count : String?
   var SR_No : String?
    var QuizTiming : String?
  

    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Question"] is NSNull || cell["Question"] == nil{
            self.Question = ""
        }else{
            let qz = cell["Question"]
            
            self.Question = qz?.description
        }
        if cell["Option_A"] is NSNull || cell["Option_A"] == nil{
            self.Option_A = ""
        }else{
            let qz = cell["Option_A"]
            
            self.Option_A = qz?.description
        }
        if cell["Option_B"] is NSNull || cell["Option_B"] == nil{
            self.Option_B = ""
        }else{
            let qz = cell["Option_B"]
            
            self.Option_B = qz?.description
        }
        if cell["Option_C"] is NSNull || cell["Option_C"] == nil{
            self.Option_C = ""
        }else{
            
            let qz = cell["Option_C"]
            
            self.Option_C = qz?.description
        }
        if cell["Option_D"] is NSNull || cell["Option_D"] == nil{
            self.Option_D = ""
        }else{
           
            let qz = cell["Option_D"]
            
            self.Option_D = qz?.description
        }
        if cell["Subject_Title"] is NSNull || cell["Subject_Title"] == nil{
            self.Subject_Title = ""
        }else{
            let qz = cell["Subject_Title"]
            
            self.Subject_Title = qz?.description
        }
        if cell["Description"] is NSNull || cell["Description"] == nil{
            self.Description = ""
        }else{
            
            let qz = cell["Description"]
            
            self.Description = qz?.description
        }
        if cell["Test_ID"] is NSNull || cell["Test_ID"] == nil{
            self.Test_ID = "0"
        }else{
            let qz = cell["Test_ID"]
            
            self.Test_ID = qz?.description
        }
        if cell["ID"] is NSNull || cell["ID"] == nil{
             self.ID = 0
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        if cell["Subject_ID"] is NSNull || cell["Subject_ID"] == nil{
            self.Subject_ID = "0"
        }else{
            let qz = cell["Subject_ID"]
            
            self.Subject_ID = qz?.description
        }
        
        
        
        
        
        
        
        
        
        if cell["Que_Count"] is NSNull || cell["Que_Count"] == nil{
            self.Que_Count = "0"
        }else{
            let qz = cell["Que_Count"]
            
            self.Que_Count = qz?.description
        }
        
        if cell["SR_No"] is NSNull || cell["SR_No"] == nil{
            self.SR_No = "0"
        }else{
            let qz = cell["SR_No"]
            
            self.SR_No = qz?.description
        }
        if cell["QuizTiming"] is NSNull || cell["QuizTiming"] == nil{
            self.QuizTiming = ""
        }else{
            
            let qz = cell["QuizTiming"]
            
            self.QuizTiming = qz?.description
        }
        
    }
}


class QuizSubmitListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SubmitQuestionsViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Question_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                   
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        var QuestionLast = ""
                        if let data = dict["data"] as? AnyObject
                        {
                            let qz = data["Question"] as? AnyObject
                            
                            QuestionLast = (qz?.description)!
                            
                            if(QuestionLast != "" && QuestionLast != "<null>" && QuestionLast != nil ) {
                            var dataArray : [QuizSubmitModel] = []
                            
                            
                                if let celll = data as? [String:AnyObject]
                                {
                                    let object = QuizSubmitModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                            
                            
                            var arr : [String] = []
                            let dataStr = dataArray[0].QuizTiming
                            arr =  (dataStr?.components(separatedBy: ":"))!
                            var secondsData = Double()
                            for i in 0...arr.count - 1 {
                                if(i == 0){
                                    secondsData = Double(Int(arr[0])! * 60 * 60)
                                }else if(i == 1){
                                    secondsData = secondsData + Double(Int(arr[1])! * 60)
                                }else{
                                   secondsData = secondsData + Double(Int(arr[2])! )
                                }
                            }
                         
                            obj.lblTimeLeft.setCountDownTime(minutes: secondsData)
                            obj.lblTimeLeft.animationType = .Burn
                            obj.lblTimeLeft.start()
                          
                            
                             obj.totalQuesLabel.text = dataArray[0].Que_Count
                            
                            let intQue = Int(dataArray[0].SR_No!)! - 1
                            obj.attemptLabel.text = String(intQue)
                            obj.pendingLabel.text = String(Int(dataArray[0].Que_Count!)! - intQue)
                            success(dataArray as AnyObject)
                            }else{
                                
                                var arr : [String] = []
                                let dataNew = data["QuizTiming"] as? AnyObject
                                let dataStr = (dataNew?.description)!
                                arr =  (dataStr.components(separatedBy: ":"))
                                var secondsData = Double()
                                for i in 0...arr.count - 1 {
                                    if(i == 0){
                                        secondsData = Double(Int(arr[0])! * 60 * 60)
                                    }else if(i == 1){
                                        secondsData = secondsData + Double(Int(arr[1])! * 60)
                                    }else{
                                        secondsData = secondsData + Double(Int(arr[2])! )
                                    }
                                }
                                
                                obj.lblTimeLeft.setCountDownTime(minutes: secondsData)
                                obj.lblTimeLeft.animationType = .Burn
                                obj.lblTimeLeft.start()
                                
                                
                                
                                obj.pressFinalSubmitQuiz()
                            }
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                        obj.pressFinalSubmitQuiz()
                        print(response)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}





class QuizPreSubmitModel: NSObject {
    
    
    
    var Question: String?
    
    var Option_A : String?
    var Option_B : String?
    var Option_C : String?
    var Option_D : String?
    
    var ID  = Int()
    
    
    var Given_Option : String?
    var Correct_Answer : String?
    var Given_Answer : String?
    var Correct_Option : String?
    
    var Result : String?
    var Result_URL : String?
  
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Question"] is NSNull || cell["Question"] == nil{
            self.Question = ""
        }else{
            let qz = cell["Question"]
            
            self.Question = qz?.description
        }
        if cell["Option_A"] is NSNull || cell["Option_A"] == nil{
            self.Option_A = ""
        }else{
            let qz = cell["Option_A"]
            
            self.Option_A = qz?.description
        }
        if cell["Option_B"] is NSNull || cell["Option_B"] == nil{
            self.Option_B = ""
        }else{
            let qz = cell["Option_B"]
            
            self.Option_B = qz?.description
        }
        if cell["Option_C"] is NSNull || cell["Option_C"] == nil{
            self.Option_C = ""
        }else{
            
            let qz = cell["Option_C"]
            
            self.Option_C = qz?.description
        }
        if cell["Option_D"] is NSNull || cell["Option_D"] == nil{
            self.Option_D = ""
        }else{
            
            let qz = cell["Option_D"]
            
            self.Option_D = qz?.description
        }
        if cell["Given_Option"] is NSNull{
            self.Given_Option = ""
        }else{
            let qz = cell["Given_Option"]
            
            self.Given_Option = qz?.description
        }
        if cell["Correct_Answer"] is NSNull{
            self.Correct_Answer = ""
        }else{
            
            let qz = cell["Correct_Answer"]
            
            self.Correct_Answer = qz?.description
        }
        if cell["Given_Answer"] is NSNull{
            self.Given_Answer = "0"
        }else{
            let qz = cell["Given_Answer"]
            
            self.Given_Answer = qz?.description
        }
        if cell["ID"] is NSNull{
            self.ID = 0
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        if cell["Correct_Option"] is NSNull{
            self.Correct_Option = "0"
        }else{
            let qz = cell["Correct_Option"]
            
            self.Correct_Option = qz?.description
        }
        

        
        
        
        if cell["Result"] is NSNull{
            self.Result = "0"
        }else{
            let qz = cell["Result"]
            
            self.Result = qz?.description
        }
        
        if cell["Result_URL"] is NSNull{
            self.Result_URL = "0"
        }else{
            let qz = cell["Result_URL"]
            
            self.Result_URL = qz?.description
        }
       
        
    }
}


class QuizPreSubmitListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SubmitQuestionsViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Preview_questions_with_answers, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizPreSubmitModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizPreSubmitModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        obj.cellData.btnShowPrevious.isHidden = true
                        obj.cellData.lblNoData.isHidden = true
                        obj.cellData.lblNoDataHeight.constant = 0
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.stopLoading()
                        obj.cellData.btnShowPrevious.isHidden = false
                        print("DATA:fail")
                        obj.cellData.lblNoDataHeight.constant = 18
                        obj.cellData.lblNoData.isHidden = false
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                obj.cellData.btnShowPrevious.isHidden = false
                obj.cellData.lblNoData.isHidden = false
                print("DATA:error",errorStr)
                obj.cellData.lblNoDataHeight.constant = 18
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            //            obj.label.isHidden = false
            //            obj.refresh.endRefreshing()
            obj.cellData.btnShowPrevious.isHidden = false
            obj.cellData.lblNoData.isHidden = false
            obj.cellData.lblNoDataHeight.constant = 18
            obj.stopLoading()
        }
        
        
    }
    
    
}





