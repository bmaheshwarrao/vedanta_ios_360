//
//  UpcomingCompletedMissedTableViewself.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 26/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class UpcomingCompletedMissedTableViewCell: UITableViewCell {
    @IBOutlet weak var AuditLocation_TitleLbl: UILabel!
    @IBOutlet weak var AuditTypeLbl: UILabel!
    @IBOutlet weak var AuditDate: UILabel!
    @IBOutlet weak var AuditTitle: UILabel!
    @IBOutlet weak var AuditIdLbl: UILabel!
    @IBOutlet weak var containerView : UIViewX!
    @IBOutlet weak var AuditScoreLbl: UILabel!
    @IBOutlet weak var AuditEmailIdLbl: UILabel!
    @IBOutlet weak var AuditNameLbl: UILabel!
    @IBOutlet weak var AuditMobileLbl: UILabel!
    
    
    @IBOutlet weak var Auditor_CordinatorLbl: UILabel!
    @IBOutlet weak var viewExpand: UIView!
    @IBOutlet weak var heightExpandView: NSLayoutConstraint!
    @IBOutlet weak var bottomCell: NSLayoutConstraint!
    @IBOutlet weak var heightScore: NSLayoutConstraint!
    
    @IBOutlet weak var viewData: UIView!
    var screenStatus = UserDefaults.standard.string(forKey: "screenStatus")!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
//        self.frame.size.height = 194
//        btnExpand.setImage(UIImage(named: "arrowDown"), for: .normal)
//        heightExpandView.constant = 0
//        
//        viewExpand.isHidden = true
//        expand = true
    }
    var expand = Bool()
    @IBOutlet weak var btnExpand: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
