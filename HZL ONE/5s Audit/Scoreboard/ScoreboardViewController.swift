//
//  ScoreboardViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ScoreboardViewController: CommonVSClass {
    
    var scoreListDB:[ScoreListModel] = []

    var ScoListAPI = ScoreListAPI()
    
    
    var data: String?
    var lastObject: String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ScoreList()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "5s Score Card"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    @objc func ScoreList() {
        var para = [String:String]()
       // let parameter = []
        
//        para = parameter.filter { $0.value != ""}
//        print("para",para)
        self.ScoListAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.scoreListDB = [ScoreListModel]()
            self.scoreListDB = dict as! [ScoreListModel]
            print(self.scoreListDB)
            self.tableView.reloadData()
        }
        
    }
    
 
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "ScoreboardAuditListViewController") as! ScoreboardAuditListViewController
        
        
        ZIVC.Audit_Master_Id  = String(scoreListDB[(indexPath?.section)!].Audit_MasterID!)
        
        ZIVC.LocationID  = String()
        
        ZIVC.LocationLevel  = String()
        ZIVC.AuditTitle  = scoreListDB[(indexPath?.section)!].Audit_Title!
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ScoreboardViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreListDB.count
    }
    
    
}

extension ScoreboardViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScoreListTableViewCell
        
        
      
        cell.containerView.layer.cornerRadius = 8
        cell.scoreListDurationLbl.text = scoreListDB[indexPath.row].Audit_Type
        cell.scoreListLbl.text = scoreListDB[indexPath.row].Audit_Title
    
        
 
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        
        self.data = String(self.scoreListDB[indexPath.row].Audit_MasterID!)
        self.lastObject = String(self.scoreListDB[indexPath.row].Audit_MasterID!)
        
       
        return cell;
    }
}
