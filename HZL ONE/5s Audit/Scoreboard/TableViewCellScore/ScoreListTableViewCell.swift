//
//  ScoreListTableViewCell.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ScoreListTableViewCell: UITableViewCell {

    @IBOutlet weak var scoreListDurationLbl: UILabel!
    @IBOutlet weak var scoreListLbl: UILabel!
    @IBOutlet weak var containerView: UIViewX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
