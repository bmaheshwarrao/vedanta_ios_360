//
//  FivesTypeTableViewCell.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 27/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FivesTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var countStatusLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
      @IBOutlet weak var containerView : UIViewX!
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var heightScore: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewTick: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
