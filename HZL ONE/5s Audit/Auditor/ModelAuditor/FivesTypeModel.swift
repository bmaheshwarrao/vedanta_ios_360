//
//  FivesTypeModel.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 27/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability



class FivesTypeListModel: NSObject {
    
    
   
    
    var CL_5S_Type : String?
    var Attempt : String?
    var Unattempt : String?
    var Color_Code : String?
    
    var TotalQuestion : String?
    var Score : String?
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        if cell["Attempt"] is NSNull{
            self.Attempt = ""
        }else{
            let idd1 : Int = (cell["Attempt"] as? Int)!
            self.Attempt = String(idd1)
            
        }
        if cell["Unattempt"] is NSNull{
            self.Unattempt = ""
        }else{
            let idd1 : Int = (cell["Unattempt"] as? Int)!
            self.Unattempt = String(idd1)
            
        }
        if cell["TotalQuestion"] is NSNull{
            self.TotalQuestion = ""
        }else{
            let idd1 : Int = (cell["TotalQuestion"] as? Int)!
            self.TotalQuestion = String(idd1)
            
        }
        if cell["Score"] is NSNull{
            self.Score = ""
        }else{
            let idd1 : Int = (cell["Score"] as? Int)!
            self.Score = String(idd1)
            
        }
        if cell["CL_5S_Type"] is NSNull{
            self.CL_5S_Type = ""
        }else{
            self.CL_5S_Type = (cell["CL_5S_Type"] as? String)!
        }
        if cell["Color_Code"] is NSNull{
            self.Color_Code = ""
        }else{
            self.Color_Code = (cell["Color_Code"] as? String)!
        }
      
    }
}


class FivesTypeListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:FivesTypesViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Audit_AuditPointListStatus, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [FivesTypeListModel] = []
                            obj.totalQuestion = 0
                            obj.totalAttempted = 0
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = FivesTypeListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                    
                                    
                                }
                                
                            }
                            
                            
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
    
    
    func serviceCallingList(obj:AuditCheckListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Audit_AuditPointListStatus, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [FivesTypeListModel] = []
                         
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = FivesTypeListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                    
                                    
                                }
                                
                            }
                            
                            
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
    
    
    
    
}






