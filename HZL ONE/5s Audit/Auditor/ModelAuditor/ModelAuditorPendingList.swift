//
//  ModelAuditorPendingList.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

//
//  ModelAuditor.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


import Foundation
import Reachability



class AuditCheckListModel: NSObject {
    
    
    
    
    var Audit_ID : String?
    var CL_Point_ID : String?
    var CL_5S_Type : String?
    var CL_5S_Point : String?
    
    var Observation : String?
    var Score : String?
    var Report_Status : String?
    var flag : String?
    
    var Image : [ImageModel] = []
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        if cell["Audit_ID"] is NSNull{
            self.Audit_ID = ""
        }else{
            let idd1 : Int = (cell["Audit_ID"] as? Int)!
            self.Audit_ID = String(idd1)
            
        }
        
        if cell["CL_Point_ID"] is NSNull{
            self.CL_Point_ID = ""
        }else{
            let idd1 : Int = (cell["CL_Point_ID"] as? Int)!
            self.CL_Point_ID = String(idd1)
            
        }
        
        
        if cell["CL_5S_Type"] is NSNull{
            self.CL_5S_Type = ""
        }else{
            self.CL_5S_Type = (cell["CL_5S_Type"] as? String)!
        }
        if cell["CL_5S_Point"] is NSNull{
            self.CL_5S_Point = ""
        }else{
            self.CL_5S_Point = (cell["CL_5S_Point"] as? String)!
        }
        
        if cell["Observation"] is NSNull{
            self.Observation = ""
        }else{
            self.Observation = (cell["Observation"] as? String)!
        }
        if cell["Score"] is NSNull{
            self.Score = ""
        }else{
            let idd1 : Int = (cell["Score"] as? Int)!
            self.Score = String(idd1)
      
        }
        
        
        
        
        if cell["Report_Status"] is NSNull{
            self.Report_Status = ""
        }else{
            self.Report_Status = (cell["Report_Status"] as? String)!
        }
        if cell["flag"] is NSNull{
            self.flag = ""
        }else{
            self.flag = (cell["flag"] as? String)!
        }
   
        if cell["images"] == nil{
            
        }else{
       let dataCell =    cell["images"] as! NSArray
            if(dataCell.count > 0){
                Image = []
            for i in 0...dataCell.count - 1
            {
                
                if let cellImg = dataCell[i] as? [String:AnyObject]
                {
            let object = ImageModel()
            object.setDataInModel(cell: cellImg)
                    Image.append(object)
                }
            }
            }
        }
        
    }
}
class ImageModel: NSObject {
    var ImageURL : String?
     var ImageID : String?
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["ImageURL"] is NSNull{
            self.ImageURL = ""
        }else{
            self.ImageURL = (cell["ImageURL"] as? String)!
        }
        if cell["ImageID"] is NSNull{
            self.ImageID = ""
        }else{
            self.ImageID = (cell["ImageID"] as? String)!
        }
    }
}

class AuditCheckListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AuditCheckListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Audit_CheckList, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [AuditCheckListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = AuditCheckListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}






