//
//  ImageCollectionViewCell.swift
//  TestImage
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDelete: UILabel!
    @IBOutlet weak var imgCross: UIImageView!
    @IBOutlet weak var imgCollection: UIImageView!
    @IBOutlet weak var btnCrossImage: UIButton!
   
    @IBOutlet weak var btnNumber: UIButton!
}
