//
//  PendingObservationCoordinatorViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 19/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Foundation
import MessageUI
class PendingObservationCoordinatorViewController: CommonVSClass,MFMailComposeViewControllerDelegate {
    
    var PendingObservationDB:[PendingObListModel] = []
    
    var PendingListAPI = PendingObListAPI()
    var screen = String()
    var screenStatus = String()
    var data: String?
    var lastObject: String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        screen = UserDefaults.standard.string(forKey: "screen")!
        screenStatus = UserDefaults.standard.string(forKey: "screenStatus")!
        AuditList()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
        refresh.addTarget(self, action: #selector(AuditList), for: .valueChanged)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        self.tableView.addSubview(refresh)
        
        
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.AuditList()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        screen = UserDefaults.standard.string(forKey: "screen")!
        screenStatus = UserDefaults.standard.string(forKey: "screenStatus")!
          if(screen == "HOD"){
            if(screenStatus == "Pending"){
                self.title = screen + "> Pending Observation Audit"
            }else if(screenStatus == "Upcoming"){
                self.title = screen + "> Upcoming Audits"
            }else{
                self.title = screen + "> Completed Audits"
            }
            
            
            
            
          }else{
        if(screenStatus == "Pending"){
            self.title = "Pending Observation Audit"
        }else if(screenStatus == "Upcoming"){
            self.title = "Upcoming Audits"
        }else{
             self.title = "Completed Audits"
        }
        }
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    @objc func AuditList() {
        var urlStatus = String()
        var  parameter = [String:String]()
        if(screenStatus == "Pending"){
         urlStatus = URLConstants.Pending_Observations
             parameter = ["emp_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        }else if(screenStatus == "Upcoming"){
            urlStatus = URLConstants.Upcoming_Audits
             parameter = ["Emp_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        }else{
            urlStatus = URLConstants.Completed_Audits
             parameter = ["emp_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        }
        
        
        var para = [String:String]()
       // let parameter = ["emp_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.PendingListAPI.serviceCalling(obj: self, urlWithStatus: urlStatus, parameter: para) { (dict) in
            
            self.PendingObservationDB = [PendingObListModel]()
            self.PendingObservationDB = dict as! [PendingObListModel]
            print(self.PendingObservationDB)
            self.arrayOb = []
            for i in 0...self.PendingObservationDB.count - 1{
               self.arrayOb.append(false)
            }
            self.tableView.reloadData()
        }
        
    }
    var arrayOb : [Bool] = []
    
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "PendingObListViewController") as! PendingObListViewController
        
        
        ZIVC.auditId  = String(PendingObservationDB[(indexPath?.row)!].Audit_ID!)
        
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    @objc func tapCellPhone(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        var phone = String()
        if(screen == "HOD"){
            phone = PendingObservationDB[(indexPath?.row)!].AuditMobile_Number!
            
        }else{
            phone = PendingObservationDB[(indexPath?.row)!].Mobile_Number!
            
        }
        
        
        print(phone)
        
        let url1 = "tel://\(phone)"
        print(url1)
        if let url = URL(string: url1), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func btnExpandDataClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if(arrayOb[(indexPath?.row)!] == true){
        arrayOb[(indexPath?.row)!] = false
        }else{
            arrayOb[(indexPath?.row)!] = true
        }
        self.tableView.reloadRows(at: [indexPath!], with: .automatic)
    }
    @objc func tapCellEmail(_ sender: UITapGestureRecognizer) {
        var email = String()
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if(screen == "HOD"){
            email = PendingObservationDB[(indexPath?.row)!].AuditEmail_ID!
            
        }else{
            email = PendingObservationDB[(indexPath?.row)!].Email_ID!
            
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "dd MMM yyyy"
        let date = dateFormatter.date(from: self.PendingObservationDB[(indexPath?.row)!].Audit_DateTime!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        var dateStrStart = String()
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            dateStrStart = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            dateStrStart = date_TimeStr
            break;
        }
//        let date1 = dateFormatter.date(from: self.PendingObservationDB[(indexPath?.row)!].Last_Date!)
//
//        let dateFormatter21 = DateFormatter()
//        dateFormatter21.timeZone = NSTimeZone.system
//        dateFormatter21.dateFormat = "dd MMM yyyy"
//        var dateStrEnd = String()
//        switch date1 {
//        case nil:
//            let date_TimeStr = dateFormatter21.string(from: Date())
//            dateStrEnd = date_TimeStr
//            break;
//        default:
//            let date_TimeStr = dateFormatter21.string(from: date!)
//
//            dateStrEnd = date_TimeStr
//            break;
//        }
        let content =  "Audit Id : " + PendingObservationDB[(indexPath?.row)!].Audit_ID! + "\n Audit Type : " + PendingObservationDB[(indexPath?.row)!].Audit_Type! + "\n Audit Title : " + PendingObservationDB[(indexPath?.row)!].Audit_Title! + "\n Date : " + dateStrStart + "\n Location : " + PendingObservationDB[(indexPath?.row)!].Location_Title!
        
        let subject = PendingObservationDB[(indexPath?.row)!].Audit_Title!
        
        
        
        
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        sendEmail(sendAddress: email, subject: subject, content: content)
        
        
    }
    
    func sendEmail(sendAddress : String , subject : String , content : String) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([sendAddress])
        composeVC.setSubject(subject)
        composeVC.setMessageBody(content, isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        print(result)
        controller.dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PendingObservationCoordinatorViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PendingObservationDB.count
    }
    
    
}

extension PendingObservationCoordinatorViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PendingObCootdinatorTableViewCell
        
 
        
        cell.containerView.layer.cornerRadius = 8
        cell.AuditTypeLbl.text = PendingObservationDB[indexPath.row].Audit_Type!
        cell.AuditTitle.text = PendingObservationDB[indexPath.row].Audit_Title!
        cell.AuditIdLbl.text = "Audit Id #" + PendingObservationDB[indexPath.row].Audit_ID!
        cell.AuditLocation_TitleLbl.text = PendingObservationDB[indexPath.row].Location_Title!
        cell.AuditScoreLbl.text =   PendingObservationDB[indexPath.row].TotalScore!
        cell.TotalObservationLbl.text =   PendingObservationDB[indexPath.row].Open_Observation!
        cell.Auditor_CordinatorLbl.text = "Auditor Detail"
        if(screen == "HOD"){
              cell.AuditEmailIdLbl.text = PendingObservationDB[indexPath.row].AuditEmail_ID!
             cell.AuditNameLbl.text = PendingObservationDB[indexPath.row].AuditName!
             cell.AuditMobileLbl.text = PendingObservationDB[indexPath.row].AuditMobile_Number!
        }else{
            cell.AuditEmailIdLbl.text = PendingObservationDB[indexPath.row].Email_ID!
            cell.AuditNameLbl.text = PendingObservationDB[indexPath.row].Name!
            cell.AuditMobileLbl.text = PendingObservationDB[indexPath.row].Mobile_Number!
        }
       
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.PendingObservationDB[indexPath.section].Audit_DateTime!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        var dateStr = String()
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            dateStr = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            dateStr = date_TimeStr
            break;
        }
        
      
        
        cell.AuditDate.text = dateStr
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.viewData.isUserInteractionEnabled = true;
        cell.viewData.addGestureRecognizer(tapGesture)
        
        let tapGestureEmail = UITapGestureRecognizer(target: self, action: #selector(tapCellEmail(_:)))
        cell.AuditEmailIdLbl.isUserInteractionEnabled = true;
        cell.AuditEmailIdLbl.addGestureRecognizer(tapGestureEmail)
        let tapGesturePhone = UITapGestureRecognizer(target: self, action: #selector(tapCellPhone(_:)))
        cell.AuditMobileLbl.isUserInteractionEnabled = true;
        cell.AuditMobileLbl.addGestureRecognizer(tapGesturePhone)
        
        if(self.arrayOb[indexPath.row] == true){
           
                cell.frame.size.height = 274
           
            cell.btnExpand.setImage(UIImage(named: "arrowRight"), for: .normal)
            cell.heightExpandView.constant = 80
            cell.viewExpand.isHidden = false
        }else{
            
                cell.frame.size.height = 196
           
            cell.btnExpand.setImage(UIImage(named: "arrowDown"), for: .normal)
            cell.heightExpandView.constant = 0
            
            cell.viewExpand.isHidden = true
        }
        
        return cell;
    }
}
