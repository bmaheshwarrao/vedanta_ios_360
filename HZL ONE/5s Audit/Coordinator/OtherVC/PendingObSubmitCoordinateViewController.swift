//
//  PendingObSubmitCoordinateViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 19/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
import Fusuma
import Alamofire
class PendingObSubmitCoordinateViewController: CommonVSClass,FusumaDelegate {
 //   @IBOutlet var btns: [DLRadioButton]!
    var pointId = String()
    var auditId = String()
    @IBOutlet weak var heightCross: NSLayoutConstraint!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var imageUploadFile: UIImageView!
    @IBOutlet weak var viewPhotoUpload: UIView!
    @IBOutlet weak var textViewObservation: UITextView!
    @IBOutlet weak var radioOpen: DLRadioButton!
    @IBOutlet weak var radioClose: DLRadioButton!
   var screen = UserDefaults.standard.string(forKey: "screen")!
    var PlaceHolderText = "Type message here."
    @IBOutlet weak var viewCamera: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewObservation.layer.cornerRadius = 10.0
         textViewObservation.layer.borderWidth = 1.0
        textViewObservation.layer.borderColor = UIColor.black.cgColor
        textViewObservation.text = PlaceHolderText
         textViewObservation.textColor = .lightGray
        textViewObservation.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UploadImageTapped(_:)))
        viewPhotoUpload.isUserInteractionEnabled = true;
        viewPhotoUpload.addGestureRecognizer(tapGesture)
        viewCamera.constant = 40
        heightCross.constant = 0
        btnCross.isHidden = true;
        heightImage.constant = 0
        imageUploadFile.isHidden = true;
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        screen = UserDefaults.standard.string(forKey: "screen")!
         if(screen == "HOD"){
        self.title = "HOD > Submit Observation"
         }else{
            self.title = "Submit Observation Action"
        }
        
        
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var strAction = String()
   @IBAction func radioButtonClicked(_ sender: DLRadioButton){
    
    for button in sender.selectedButtons() {
        self.strAction = (button.titleLabel!.text!)
        print(String(format: "%@ is selected.\n", self.strAction));
        
    }
    }
   
    @IBAction func btnCrossClicked(_ sender: UIButton) {
        viewCamera.constant = 40
        heightCross.constant = 0
        btnCross.isHidden = true;
        heightImage.constant = 0
        imageUploadFile.isHidden = true;
        self.imagedata = nil
        viewPhotoUpload.isHidden = false
    }
    @IBAction func UploadImageTapped(_ sender: UITapGestureRecognizer) {
        let fusuma = FusumaViewController()
        
        fusuma.delegate = self
        fusuma.cropHeightRatio = 1.0
        fusuma.allowMultipleSelection = false
        //        fusuma.availableModes = [.video]
        fusumaSavesImage = true

        self.present(fusuma, animated: true, completion: nil)
    }
    var reachability = Reachability()!
      var imagedata: Data? = nil
    @IBAction func btnProcessClicked(_ sender: UIButton) {
        if reachability.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }else if(self.strAction == String()){
            
            self.view.makeToast("Please Select Status")
        }
        else if self.textViewObservation.text == "" || self.textViewObservation.text == PlaceHolderText{
            
            self.view.makeToast("Please Write Message")
        }else{
            
            var urlStr = String()
            
            self.startLoadingPK(view: self.view)
            
            if(screen == "HOD"){
                urlStr = URLConstants.Submit_Action_on_Observation_HOD
            }else{
              urlStr = URLConstants.Submit_Action_on_Observation
            }
          
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            
            let parameter =  ["Emp_ID": empId ,
                              "Audit_Id": auditId ,
                              "Point_Id":pointId,
                               "Comment":textViewObservation.text!,
                               "Status":self.strAction,
                               "Image":"imageName"
                
                
             
                ] as! [String:String]
            
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    var res : UIImage = UIImage()
                    let size = CGSize(width: 500, height: 500)
                    
                    res = self.imageResize(image: self.imageUploadFile.image!,sizeChange: size)
                    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:urlStr)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("5s Audit Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                    
                                    self.view.makeToast("Successfully Submitted.")
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        self.stopLoadingPK(view: self.view)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                   
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {

        switch source {

        case .camera:

            print("Image captured from Camera")

        case .library:

            print("Image selected from Camera Roll")

        default:

            print("Image selected")
        }
        viewCamera.constant = 0
        viewPhotoUpload.isHidden = true
        heightCross.constant = 30
        btnCross.isHidden = false;
        heightImage.constant = 120
        imageUploadFile.isHidden = false;
         self.imagedata = UIImageJPEGRepresentation(image, 1.0)!
       self.imageUploadFile.image = image
    }

    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {

        print("Number of selection images: \(images.count)")

        var count: Double = 0

        for image in images {

            //   DispatchQueue.main.asyncAfter(deadline: .now() + (3.0 * count)) {
            self.imageUploadFile.image = image
            print("w: \(image.size.width) - h: \(image.size.height)")
        }
        //count += 1
        //}
    }

    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {

        print("Image mediatype: \(metaData.mediaType)")
        print("Source image size: \(metaData.pixelWidth)x\(metaData.pixelHeight)")
        print("Creation date: \(String(describing: metaData.creationDate))")
        print("Modification date: \(String(describing: metaData.modificationDate))")
        print("Video duration: \(metaData.duration)")
        print("Is favourite: \(metaData.isFavourite)")
        print("Is hidden: \(metaData.isHidden)")
        print("Location: \(String(describing: metaData.location))")
    }

    func fusumaVideoCompleted(withFileURL fileURL: URL) {

        print("video completed and output to file: \(fileURL)")
        // self.fileUrlLabel.text = "file output to: \(fileURL.absoluteString)"
    }

    func fusumaDismissedWithImage(_ image: UIImage, source: FusumaMode) {

        switch source {

        case .camera:

            print("Called just after dismissed FusumaViewController using Camera")

        case .library:

            print("Called just after dismissed FusumaViewController using Camera Roll")

        default:

            print("Called just after dismissed FusumaViewController")
        }
    }

    func fusumaCameraRollUnauthorized() {

        print("Camera roll unauthorized")

        let alert = UIAlertController(title: "Access Requested",
                                      message: "Saving image needs to access your photo album",
                                      preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Settings", style: .default) { (action) -> Void in

            if let url = URL(string:UIApplicationOpenSettingsURLString) {

                UIApplication.shared.openURL(url)
            }
        })

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in

        })

        guard let vc = UIApplication.shared.delegate?.window??.rootViewController,
            let presented = vc.presentedViewController else {

                return
        }

        presented.present(alert, animated: true, completion: nil)
    }

    func fusumaClosed() {

        print("Called when the FusumaViewController disappeared")
    }

    func fusumaWillClosed() {

        print("Called when the close button is pressed")
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIImage {
  
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
}
extension PendingObSubmitCoordinateViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == PlaceHolderText)
        {
            textView.text = ""
            textView.textColor = .black
        }
        
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = PlaceHolderText
            textView.textColor = .lightGray
        }
        textViewObservation.endEditing(true)
        
    }
}
