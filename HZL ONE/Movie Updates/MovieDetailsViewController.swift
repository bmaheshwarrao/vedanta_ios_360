//
//  MovieDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class MovieDetailsViewController: CommonVSClass,UITableViewDelegate,UITableViewDataSource {
var movieDB = MovieModel()
    @IBOutlet weak var tableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        
     
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        

        self.title = "Movie Detail"
      
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
           return 1
        }else{
            return self.movieDB.movieTiming.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "details", for: indexPath) as! MovieDetailTableViewCell
        
        
        if(self.movieDB.Rating == "0"){
            cell.btnRate1.image = UIImage(named: "unStar")
            cell.btnRate2.image = UIImage(named: "unStar")
            cell.btnRate3.image = UIImage(named: "unStar")
            cell.btnRate4.image = UIImage(named: "unStar")
            cell.btnRate5.image = UIImage(named: "unStar")
        }
        else if(self.movieDB.Rating == "1"){
            cell.btnRate1.image = UIImage(named: "star")
            cell.btnRate2.image = UIImage(named: "unStar")
            cell.btnRate3.image = UIImage(named: "unStar")
            cell.btnRate4.image = UIImage(named: "unStar")
            cell.btnRate5.image = UIImage(named: "unStar")
        }else if(self.movieDB.Rating == "2"){
            cell.btnRate1.image = UIImage(named: "star")
            cell.btnRate2.image = UIImage(named: "star")
            cell.btnRate3.image = UIImage(named: "unStar")
            cell.btnRate4.image = UIImage(named: "unStar")
            cell.btnRate5.image = UIImage(named: "unStar")
        }else if(self.movieDB.Rating == "3"){
            cell.btnRate1.image = UIImage(named: "star")
            cell.btnRate2.image = UIImage(named: "star")
            cell.btnRate3.image = UIImage(named: "star")
            cell.btnRate4.image = UIImage(named: "unStar")
            cell.btnRate5.image = UIImage(named: "unStar")
        }else if(self.movieDB.Rating == "4"){
            cell.btnRate1.image = UIImage(named: "star")
            cell.btnRate2.image = UIImage(named: "star")
            cell.btnRate3.image = UIImage(named: "star")
            cell.btnRate4.image = UIImage(named: "star")
            cell.btnRate5.image = UIImage(named: "unStar")
        }else if(self.movieDB.Rating == "5"){
            cell.btnRate1.image = UIImage(named: "star")
            cell.btnRate2.image = UIImage(named: "star")
            cell.btnRate3.image = UIImage(named: "star")
            cell.btnRate4.image = UIImage(named: "star")
            cell.btnRate5.image = UIImage(named: "star")
        }
        
        
      
        
        cell.textViewDesc.text = self.movieDB.Description
        cell.textViewCast.text = self.movieDB.Cast
       
        let urlString = self.movieDB.Image
        let imageView = UIImageView()
        
        
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: urlShow!) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 400, height: 200)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageViewMovie.image = image
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                     let size = CGSize(width: 400, height: 200)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageViewMovie.image = UIImage(named: "placed")
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
             let size = CGSize(width: 400, height: 200)
           
            cell.imageViewMovie.image = UIImage(named: "placed")
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        cell.lblMovie.text = self.movieDB.Movie_Name
        cell.lblLanguage.text = self.movieDB.Language

        
        
        
        
        
        
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTalkies", for: indexPath) as! MovieTalkiesTableViewCell
            
            if(self.movieDB.movieTiming[indexPath.row].Talkies_Name == ""){
          
            }else{
                cell.textViewTalkies.text = "Talkies Name : " + self.movieDB.movieTiming[indexPath.row].Talkies_Name
                
            }
           // cell.lblDate.text = "Date : " + self.movieDB.movieTiming[indexPath.row].Movie_Date
            let fullTime: String = self.movieDB.movieTiming[indexPath.row].Timing
            var fullTimeArr = fullTime.components(separatedBy: ",")
            
            if(fullTimeArr.count > 0){
                var v1 = fullTimeArr.count / 3
                let v2 = fullTimeArr.count % 3
                if(v2 != 0){
                    v1 = v1 + 1
                }
                cell.heightCollectionCon.constant = CGFloat(v1 * 44) + 20
                cell.pageView(data: fullTimeArr)
            }
            
           
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
