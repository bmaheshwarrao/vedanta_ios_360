//
//  VehicleRequestModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class MyRequestVehicleDataModel: NSObject {
    
    
    var ID = Int()
    var Request_ID = String()
    var Req_Type = String()
    var Emp_No = String()
    var Name = String()
    var Mobile_No = String()
    var Purpose_of_Journey = String()
    var Date_of_Traval = String()
    var From_Place = String()
    var PickUp_Address = String()
    var Drop_Address = String()
    
    var No_Of_Persons = String()
    var Duration_From = String()
    var Duration_To = String()
    var Type_Of_Vehicle = String()
    var Approval_Authority = String()
    var ReqDate = String()
    var ApproveStatus = String()
    var ApproveBy = String()
    var ApproveDate = String()
    var AdminStatus = String()
    
    var AdminBy = String()
    var AdminDate = String()
    var RType = String()
    var LocationID = String()
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Request_ID"] is NSNull || str["Request_ID"] == nil{
            self.Request_ID = ""
        }else{
            
            let ros = str["Request_ID"]
            
            self.Request_ID = (ros?.description)!
            
            
        }
        if str["Req_Type"] is NSNull || str["Req_Type"] == nil{
            self.Req_Type =  ""
        }else{
            
            let fross = str["Req_Type"]
            
            self.Req_Type = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Emp_No"] is NSNull || str["Emp_No"] == nil{
            self.Emp_No = ""
        }else{
            let emp1 = str["Emp_No"]
            
            self.Emp_No = (emp1?.description)!
            // print(self.Employee_1)
        }
       
        if str["Name"] is NSNull || str["Name"] == nil{
            self.Name = ""
        }else{
            
            let empname = str["Name"]
            
            self.Name = (empname?.description)!
            
        }
        
        if str["Mobile_No"] is NSNull || str["Mobile_No"] == nil{
            self.Mobile_No = ""
        }else{
            let emp2 = str["Mobile_No"]
            
            self.Mobile_No = (emp2?.description)!
            
            
            
        }
        
        
        
        if str["Purpose_of_Journey"] is NSNull || str["Purpose_of_Journey"] == nil{
            self.Purpose_of_Journey = ""
        }else{
            let empname2 = str["Purpose_of_Journey"]
            
            self.Purpose_of_Journey = (empname2?.description)!
            
        }
        
        
        if str["Date_of_Traval"] is NSNull || str["Date_of_Traval"] == nil{
            self.Date_of_Traval = ""
        }else{
            let locid = str["Date_of_Traval"]
            
            self.Date_of_Traval = (locid?.description)!
            
        }
       
      
        if str["From_Place"] is NSNull || str["From_Place"] == nil{
            self.From_Place = ""
        }else{
            let locname = str["From_Place"]
            
            self.From_Place = (locname?.description)!
            
        }
        
        
        
        if str["PickUp_Address"] is NSNull || str["PickUp_Address"] == nil{
            self.PickUp_Address = ""
        }else{
            let locname = str["PickUp_Address"]
            
            self.PickUp_Address = (locname?.description)!
            
        }
        if str["Drop_Address"] is NSNull || str["Drop_Address"] == nil{
            self.Drop_Address = ""
        }else{
            let locname = str["Drop_Address"]
            
            self.Drop_Address = (locname?.description)!
            
        }
        if str["No_Of_Persons"] is NSNull || str["No_Of_Persons"] == nil{
            self.No_Of_Persons = ""
        }else{
            let locname = str["No_Of_Persons"]
            
            self.No_Of_Persons = (locname?.description)!
            
        }
       
        
        if str["Duration_From"] is NSNull || str["Duration_From"] == nil{
            self.Duration_From = ""
        }else{
            let locname = str["Duration_From"]
            
            self.Duration_From = (locname?.description)!
            
        }
        if str["Duration_To"] is NSNull || str["Duration_To"] == nil{
            self.Duration_To = ""
        }else{
            let locname = str["Duration_To"]
            
            self.Duration_To = (locname?.description)!
            
        }
        if str["Type_Of_Vehicle"] is NSNull || str["Type_Of_Vehicle"] == nil{
            self.Type_Of_Vehicle = ""
        }else{
            let locname = str["Type_Of_Vehicle"]
            
            self.Type_Of_Vehicle = (locname?.description)!
            
        }
        if str["Approval_Authority"] is NSNull || str["Approval_Authority"] == nil{
            self.Approval_Authority = ""
        }else{
            let locname = str["Approval_Authority"]
            
            self.Approval_Authority = (locname?.description)!
            
        }
       
        if str["ReqDate"] is NSNull || str["ReqDate"] == nil{
            self.ReqDate = ""
        }else{
            let locname = str["ReqDate"]
            
            self.ReqDate = (locname?.description)!
            
        }
        if str["ApproveStatus"] is NSNull || str["ApproveStatus"] == nil{
            self.ApproveStatus = ""
        }else{
            let locname = str["ApproveStatus"]
            
            self.ApproveStatus = (locname?.description)!
            
        }
        if str["ApproveBy"] is NSNull || str["ApproveBy"] == nil{
            self.ApproveBy = ""
        }else{
            let locname = str["ApproveBy"]
            
            self.ApproveBy = (locname?.description)!
            
        }
        if str["ApproveDate"] is NSNull || str["ApproveDate"] == nil{
            self.ApproveDate = ""
        }else{
            let locname = str["ApproveDate"]
            
            self.ApproveDate = (locname?.description)!
            
        }
        if str["AdminStatus"] is NSNull || str["AdminStatus"] == nil{
            self.AdminStatus = ""
        }else{
            let locname = str["AdminStatus"]
            
            self.AdminStatus = (locname?.description)!
            
        }
        
        if str["AdminBy"] is NSNull || str["AdminBy"] == nil{
            self.AdminBy = ""
        }else{
            let locname = str["AdminBy"]
            
            self.AdminBy = (locname?.description)!
            
        }
        if str["AdminDate"] is NSNull || str["AdminDate"] == nil{
            self.AdminDate = ""
        }else{
            let locname = str["AdminDate"]
            
            self.AdminDate = (locname?.description)!
            
        }
        if str["RType"] is NSNull || str["RType"] == nil{
            self.RType = ""
        }else{
            let locname = str["RType"]
            
            self.RType = (locname?.description)!
            
        }
        if str["LocationID"] is NSNull || str["LocationID"] == nil{
            self.LocationID = ""
        }else{
            let locname = str["LocationID"]
            
            self.LocationID = (locname?.description)!
            
        }
    }
    
}





class MyRequestVehicleLocalDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyRequestVehicleLocalViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequestVehicle, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "ok")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyRequestVehicleDataModel] = []
                            if(data.count > 0){
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyRequestVehicleDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            }else{
                                obj.label.isHidden = false
                                obj.tableView.isHidden = true
                                obj.noDataLabel(text: msg )
                                obj.refresh.endRefreshing()
                                obj.stopLoading()
                            }
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyRequestVehicleLocalDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyRequestVehicleLocalViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequestVehicle, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyRequestVehicleDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyRequestVehicleDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}









class MyRequestVehicleOutDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyRequestVehicleOutViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequestVehicle, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "ok")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyRequestVehicleDataModel] = []
                             if(data.count > 0){
                            for i in 0..<data.count
                            {
                               
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyRequestVehicleDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            } else {
                                obj.label.isHidden = false
                                obj.tableView.isHidden = true
                                obj.noDataLabel(text: msg )
                                obj.refresh.endRefreshing()
                                obj.stopLoading()
                            }
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyRequestVehicleOutDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyRequestVehicleOutViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequestVehicle, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyRequestVehicleDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyRequestVehicleDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
