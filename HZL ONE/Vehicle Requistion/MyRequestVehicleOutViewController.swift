//
//  MyRequestVehicleOutViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyRequestVehicleOutViewController:  CommonVSClass {
    var DataAPI = MyRequestVehicleOutDataAPI()
    var ReportedDB : [MyRequestVehicleDataModel] = []
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    var ReportedLoadMoreDB : [MyRequestVehicleDataModel] = []
    var DataLoadMoreAPI = MyRequestVehicleOutDataAPILoadMore()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.Reportdata()
        tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.estimatedRowHeight = 290
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refresh.addTarget(self, action: #selector(Reportdata), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.Reportdata()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
            }
            
        }
    }
    let reachability = Reachability()!
    var countID : Int = 0
    
    @objc func Reportdata() {
        
        
        
        
        
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey , "Type" : "2"]
        
        
        
        
        self.DataAPI.serviceCalling(obj: self, param: paramm ) { (dict) in
            
            self.ReportedDB = [MyRequestVehicleDataModel]()
            self.ReportedDB = dict as! [MyRequestVehicleDataModel]
            self.tableView.reloadData()
        }
        
    }
    var countData = 1
    @objc func callMyReportedLoadMore() {
        
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey , "Type" : "2","pn":String(countData)]
        self.DataLoadMoreAPI.serviceCalling(obj: self, param: paramm ) { (dict) in
            
            self.ReportedLoadMoreDB = [MyRequestVehicleDataModel]()
            self.ReportedLoadMoreDB = dict as! [MyRequestVehicleDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.ReportedDB.append(contentsOf: self.ReportedLoadMoreDB)
                self.tableView.reloadData()
                self.countData = self.countData + 1
                break;
            }
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension MyRequestVehicleOutViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return ReportedDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension MyRequestVehicleOutViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyRequestLocalOutTableViewCell
        
        
        
        
        
        let milisecondDate = self.ReportedDB[indexPath.section].Date_of_Traval
        let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
        let dateFormatterday = DateFormatter()
        dateFormatterday.dateFormat = "dd MMM yyyy"
        let valDate =  dateFormatterday.string(from: dateVarDate)
        cell.lblDate.text = valDate
        
        
        
        cell.lblNameandType.text = "Request Id #" + String(self.ReportedDB[indexPath.section].ID)
        cell.lblNameandType.textColor = UIColor.orange
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineSpacing = 0
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var startString : String = "You "
        if(empId != ReportedDB[indexPath.section].Emp_No){
            startString =  ReportedDB[indexPath.section].Name + "-" +   ReportedDB[indexPath.section].Emp_No + " "
        }
        let  mBuilder = startString  + "request for " + self.ReportedDB[indexPath.section].Req_Type + " to visit " +  self.ReportedDB[indexPath.section].Purpose_of_Journey + " with " + self.ReportedDB[indexPath.section].No_Of_Persons  +
            " person(s) on between dates "  +  self.ReportedDB[indexPath.section].Duration_From + " to " + self.ReportedDB[indexPath.section].Duration_To
        
        let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        
        
        //Submitted
        let SubmittedAttributedString = NSAttributedString(string:"request for ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        
        let range: NSRange = (agreeAttributedString.string as NSString).range(of: "request for ")
        if range.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
        }
        
        //for
        let forAttributedString = NSAttributedString(string:"to visit ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font:  UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        
        let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "to visit ")
        if forRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
        }
        
        let areaAttributedString = NSAttributedString(string:"with", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font:  UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        
        let areaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "with")
        if areaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: areaRange, with: areaAttributedString)
        }
        
        
        
        
        let subAreaAttributedString = NSAttributedString(string:"person(s) on between dates ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font:  UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "person(s) on between dates ")
        if subAreaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
        }
        
      
        let toAttributedString = NSAttributedString(string:"to", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font:  UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        
        let toRange: NSRange = (agreeAttributedString.string as NSString).range(of: "to")
        if toRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: toRange, with: toAttributedString)
        }
        
        agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
        
        
        
        cell.textViewDuration.attributedText = agreeAttributedString
        cell.textViewDuration.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        self.data = String(self.ReportedDB[indexPath.section].ID)
        self.lastObject = String(self.ReportedDB[indexPath.section].ID)
        //   cell.heightImage.constant = cell.heightRiskLevel.constant
        
        if ( self.data ==  self.lastObject && indexPath.section == self.ReportedDB.count - 1)
        {
            
            callMyReportedLoadMore()
            
        }
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        
        return cell
        
        
        
        
        
    }
}
