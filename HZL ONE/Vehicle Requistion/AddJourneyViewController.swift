//
//  AddJourneyViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var dataJourney : [journeyData] = []
class AddJourneyViewController: CommonVSClass, WWCalendarTimeSelectorProtocol ,UITextFieldDelegate{
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getTransportList()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
     
        self.title = "Add Journey"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
       
        
    }
    var TransportDB:[TransportDataModel] = []
    
    var TransportAPI = TransportDataAPI()
    @objc func getTransportList(){
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        self.TransportAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.TransportDB = [TransportDataModel]()
            self.TransportDB = dict as! [TransportDataModel]
            if(self.TransportDB.count > 0){
                for i in 0...self.TransportDB.count - 1 {
                    self.TransportListArray.append(self.TransportDB[i].Name!)
                    self.TransportIdArray.append(self.TransportDB[i].ID!)
                }
            }
            
            
            
            
        }
    }
    var TransportListArray : [String] = []
    var TransportIdArray : [String] = []
   var idTransport = String()
    @IBAction func btnTransportTypeClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.TransportListArray
            
            popup.sourceView = self.cellData.lblTransport
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblTransport.text = self.TransportListArray[row]
                self.cellData.lblTransport.textColor = UIColor.black
                self.idTransport = self.TransportIdArray[row]
                self.cellData.txtFrom.isEnabled = true
                self.cellData.txtTo.isEnabled = true
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    
     @IBAction func btnJourneyDateTypeClicked(_ sender: UIButton) {
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        selectdateVal = 1
        dateData = 0
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        
        present(selector, animated: true, completion: nil)
        
        
        
    }
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if(cellData.lblTransport.text == "" || cellData.lblTransport.text == "Select"){
            self.view.makeToast("Select Travel Type")
        }else if(cellData.txtFrom.text == ""){
            self.view.makeToast("Enter From Location")
        }else if(cellData.txtTo.text == ""){
            self.view.makeToast("Enter To Location")
        }else if(cellData.lblJourneyDate.text == ""){
            self.view.makeToast("Select Journey Date")
        }else{
            let obj = journeyData()
            obj.transportType = cellData.lblTransport.text!
            obj.transportTypeId = idTransport
            obj.from = cellData.txtFrom.text!
            obj.to = cellData.txtTo.text!
            obj.journey_date = cellData.lblJourneyDate.text!
            obj.journey_dateMilli = millisecJourneyDate
            dataJourney.append(obj)
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getMilliseconds(date1 : Date) -> String{
        //let strDate = date1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy  hh:mma"
        //let date = dateFormatter.date(from: strDate)

        let millieseconds = self.getDiffernce(toTime: date1)
        print(millieseconds)
        return String(millieseconds)
    }
    func getDiffernce(toTime:Date) -> Int{
        let elapsed = toTime.timeIntervalSince1970
        return Int(elapsed * 1000)
    }
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    var selectdateVal = 0
    var cellData : AddJourneyTableViewCell!
    var millisecJourneyDate = String()
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
      print(date.stringFromFormat("d' 'MMM' 'yyyy' 'h':'mma"))
        
        if(selectdateVal == 1){
            cellData.lblJourneyDate.text = date.stringFromFormat("dd' 'MMM' 'yyyy'")
            millisecJourneyDate = getMilliseconds(date1: date)
        }
        
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            // dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    var placeAPI = PlacesDataAPI()
    var placesDB:[PlacesDataModel] = []
    var FromArray : [String] = []
 
    @IBAction func textFieldFromDataChange(_ sender: UITextField) {
        if(cellData.lblTransport.text != "Select" && cellData.lblTransport.text != "" && sender.text != "") {
            let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            
            let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ,"type" : cellData.lblTransport.text! , "Search" : sender.text!]
            
            self.placeAPI.serviceCalling(obj: self,param : paramm) { (dict) in
                
                self.placesDB = [PlacesDataModel]()
                self.placesDB = dict as! [PlacesDataModel]
                if(self.placesDB.count > 0){
                    for i in 0...self.placesDB.count - 1 {
                        self.FromArray.append(self.placesDB[i].Display_Name!)
                        
                    }
                }
                
                DispatchQueue.main.async(execute: {() -> Void in
                    let popup = PopUpTableViewController()
                    
                    popup.itemsArray = self.FromArray
                    
                    popup.sourceView = self.cellData.txtFrom
                    popup.isScroll = true
                    popup.backgroundColor = UIColor.white
                    if popup.itemsArray.count > 5{
                        popup.popUpHeight = 200
                    }
                    else{
                        popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
                    }
                    popup.popUpWidth = UIScreen.main.bounds.size.width-60
                    popup.backgroundImage = nil
                    popup.itemTitleColor = UIColor.white
                    popup.itemSelectionColor = UIColor.lightGray
                    popup.arrowDirections = .any
                    popup.arrowColor = UIColor.white
                    popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                        
                        
                        
                        
                        
                        self.cellData.txtFrom.text = self.FromArray[row]
                        // self.cellData.lblTransport.textColor = UIColor.black
                        
                        popupVC.dismiss(animated: false, completion: nil)
                        
                    }
                    self.present(popup, animated: true, completion: {() -> Void in
                    })
                    
                    
                })
                
                
                
            }
        }
        
    }
        var ToArray : [String] = []
    @IBAction func textFieldToDataChange(_ sender: UITextField) {
        if(cellData.lblTransport.text != "Select" && cellData.lblTransport.text != "" && sender.text != "") {
            let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            
            let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ,"type" : cellData.lblTransport.text! , "Search" : sender.text!]
            
            self.placeAPI.serviceCalling(obj: self,param : paramm) { (dict) in
                
                self.placesDB = [PlacesDataModel]()
                self.placesDB = dict as! [PlacesDataModel]
                if(self.placesDB.count > 0){
                    for i in 0...self.placesDB.count - 1 {
                        self.ToArray.append(self.placesDB[i].Display_Name!)
                        
                    }
                }
                DispatchQueue.main.async(execute: {() -> Void in
                    let popup = PopUpTableViewController()
                    
                    popup.itemsArray = self.ToArray
                    
                    popup.sourceView = self.cellData.txtTo
                    popup.isScroll = true
                    popup.backgroundColor = UIColor.white
                    if popup.itemsArray.count > 5{
                        popup.popUpHeight = 200
                    }
                    else{
                        popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
                    }
                    popup.popUpWidth = UIScreen.main.bounds.size.width-60
                    popup.backgroundImage = nil
                    popup.itemTitleColor = UIColor.white
                    popup.itemSelectionColor = UIColor.lightGray
                    popup.arrowDirections = .any
                    popup.arrowColor = UIColor.white
                    popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                        
                        
                        
                        
                        
                        self.cellData.txtTo.text = self.ToArray[row]
                        // self.cellData.lblTransport.textColor = UIColor.black
                        
                        popupVC.dismiss(animated: false, completion: nil)
                        
                    }
                    self.present(popup, animated: true, completion: {() -> Void in
                    })
                    
                    
                })
                
                
            }
        }
    }
 


    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension AddJourneyViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 358
        
    }
}
extension AddJourneyViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddJourneyTableViewCell
        
        cellData = cell
        cell.lblTransport.text = "Select"
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
   cell.txtFrom.delegate = self
   cell.txtTo.delegate = self
        cell.txtFrom.isEnabled = false
        cell.txtTo.isEnabled = false
        
       
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.lblJourneyDate.text = date_TimeStr
       millisecJourneyDate = getMilliseconds(date1: Date())
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}


class journeyData :  NSObject {
    
    
    var transportType = String()
    var transportTypeId = String()
    var from = String()
    var to = String()
    var journey_date = String()
    var journey_dateMilli = String()
  
    
    
}
