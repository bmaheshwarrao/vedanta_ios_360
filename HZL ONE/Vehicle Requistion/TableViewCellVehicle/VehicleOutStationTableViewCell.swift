//
//  VehicleOutStationTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class VehicleOutStationTableViewCell: UITableViewCell {
    @IBOutlet weak var btnSelf: DLRadioButton!
    @IBOutlet weak var btnGuest: DLRadioButton!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var viewContact: UIView!
    
    
    @IBOutlet weak var txtFromStation: UITextField!
    @IBOutlet weak var viewFromStation: UIView!
    
    @IBOutlet weak var txtToStation: UITextField!
    @IBOutlet weak var viewToStation: UIView!
    
    
    @IBOutlet weak var textViewPickAddress: UITextView!
    @IBOutlet weak var viewPickAddress: UIView!
    
    @IBOutlet weak var textViewDropAddress: UITextView!
    @IBOutlet weak var viewDropAddress: UIView!
    
    @IBOutlet weak var btnStartDate: UIButton!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var viewStartDate: UIView!
    @IBOutlet weak var btnEndDate: UIButton!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var viewEndDate: UIView!
    
    @IBOutlet weak var txtPurpose: UITextField!
    @IBOutlet weak var viewPurpose: UIView!
    
    @IBOutlet weak var btnNoPerson: UIButton!
    @IBOutlet weak var lblNoPerson: UILabel!
    @IBOutlet weak var viewNoPerson: UIView!
    
    
    @IBOutlet weak var btnApprovalAuth: UIButton!
    @IBOutlet weak var lblApprovalAuth: UILabel!
    @IBOutlet weak var viewApprovalAuth: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        viewApprovalAuth.layer.borderWidth = 1.0
        viewApprovalAuth.layer.borderColor = UIColor.black.cgColor
        viewApprovalAuth.layer.cornerRadius = 10.0
        
        viewPickAddress.layer.borderWidth = 1.0
        viewPickAddress.layer.borderColor = UIColor.black.cgColor
        viewPickAddress.layer.cornerRadius = 10.0
        
        viewDropAddress.layer.borderWidth = 1.0
        viewDropAddress.layer.borderColor = UIColor.black.cgColor
        viewDropAddress.layer.cornerRadius = 10.0
        
        viewFromStation.layer.borderWidth = 1.0
        viewFromStation.layer.borderColor = UIColor.black.cgColor
        viewFromStation.layer.cornerRadius = 10.0
        
        viewToStation.layer.borderWidth = 1.0
        viewToStation.layer.borderColor = UIColor.black.cgColor
        viewToStation.layer.cornerRadius = 10.0
        
        viewEndDate.layer.borderWidth = 1.0
        viewEndDate.layer.borderColor = UIColor.black.cgColor
        viewEndDate.layer.cornerRadius = 10.0
        
        
        viewName.layer.borderWidth = 1.0
        viewName.layer.borderColor = UIColor.black.cgColor
        viewName.layer.cornerRadius = 10.0
        
        viewContact.layer.borderWidth = 1.0
        viewContact.layer.borderColor = UIColor.black.cgColor
        viewContact.layer.cornerRadius = 10.0
        
        viewPurpose.layer.borderWidth = 1.0
        viewPurpose.layer.borderColor = UIColor.black.cgColor
        viewPurpose.layer.cornerRadius = 10.0
        
        viewNoPerson.layer.borderWidth = 1.0
        viewNoPerson.layer.borderColor = UIColor.black.cgColor
        viewNoPerson.layer.cornerRadius = 10.0
        
        viewStartDate.layer.borderWidth = 1.0
        viewStartDate.layer.borderColor = UIColor.black.cgColor
        viewStartDate.layer.cornerRadius = 10.0
        txtContact.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
