//
//  MyRequestVehicleTourTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyRequestVehicleTourTableViewCell: UITableViewCell {

    
    @IBOutlet weak var textViewDuration: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNameandType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
