//
//  AddPassengerViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var dataPassenger : [passengerData] = []
class AddPassengerViewController: CommonVSClass {
@IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Add Passenger's"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    var pasengerArray = ["Self","Guest","Other Employee"]
    @IBAction func btnPassengerTypeClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.pasengerArray
            
            popup.sourceView = self.cellData.lblPassengerType
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblPassengerType.text = self.pasengerArray[row]
                self.cellData.lblPassengerType.textColor = UIColor.black
                
                
                if(self.pasengerArray[row] == "Self"){
                    
              var name = String()
                    let fname = UserDefaults.standard.string(forKey: "FirstName")
                    let Mname = UserDefaults.standard.string(forKey: "MiddleName")
                    let Lname = UserDefaults.standard.string(forKey: "Lastname")
                     let mobileData = UserDefaults.standard.string(forKey: "mobileData")!
                    let email = UserDefaults.standard.string(forKey: "Email")!
                    let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
                    
                    if(Mname == ""){
                        name = fname! + " " + Lname!
                    }else{
                         name = fname! + " " + Mname!  + " " + Lname!
                    }
                    self.cellData.txtName.text = name
                    self.cellData.txtContact.text = mobileData
                    
                    self.cellData.txtEmail.text = email
                    self.cellData.txtEmpId.text = empId
                    self.cellData.txtName.isEnabled = false
                    self.cellData.txtContact.isEnabled = false
                    self.cellData.txtEmail.isEnabled = false
                    self.cellData.txtEmpId.isEnabled = false
                    
                }else if(self.pasengerArray[row] == "Guest"){
                    self.cellData.txtName.text = ""
                    self.cellData.txtContact.text = ""
                    
                    self.cellData.txtEmail.text = ""
                    self.cellData.txtEmpId.text = ""
                    self.cellData.txtName.isEnabled = true
                    self.cellData.txtContact.isEnabled = true
                    self.cellData.txtEmail.isEnabled = true
                    self.cellData.txtEmpId.isEnabled = false
                }else{
                    self.cellData.txtName.text = ""
                    self.cellData.txtContact.text = ""
                    
                    self.cellData.txtEmail.text = ""
                    self.cellData.txtEmpId.text = ""
                    self.cellData.txtName.isEnabled = true
                    self.cellData.txtContact.isEnabled = true
                    self.cellData.txtEmail.isEnabled = true
                    self.cellData.txtEmpId.isEnabled = true
                }
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        var id = String()
         if(cellData.lblPassengerType.text == "Guest"){
            id = "data"
        }else{
             id = cellData.txtEmpId.text!
        }
        
        
        
        
        if(cellData.lblPassengerType.text == "" || cellData.lblPassengerType.text == "Select" ){
            self.view.makeToast("Select Passenger Type")
        }else if(id == ""){
            self.view.makeToast("Enter Employee Id")
        }else if(cellData.txtName.text == ""){
            self.view.makeToast("Enter Name")
        }else if(cellData.txtContact.text == ""){
            self.view.makeToast("Enter Contact")
        }else if(cellData.txtEmail.text == ""){
            self.view.makeToast("Enter Email")
        }else{
            let obj = passengerData()
            obj.passengerType = cellData.lblPassengerType.text!
           
            obj.name = cellData.txtName.text!
            obj.no = cellData.txtContact.text!
            obj.empId = cellData.txtEmpId.text!
            obj.email = cellData.txtEmail.text!
            dataPassenger.append(obj)
             self.navigationController?.popViewController(animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 var cellData : AddPassengerTableViewCell!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddPassengerViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 424
        
    }
}
extension AddPassengerViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddPassengerTableViewCell
        
        cellData = cell
        
        cell.lblPassengerType.text = "Select"
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}



class passengerData :  NSObject {
    
    
    var passengerType = String()
  
    var empId = String()
    var name = String()
    var no = String()
    var email = String()
    
    
}
