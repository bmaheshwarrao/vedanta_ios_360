//
//  VehicleTourViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SwiftyJSON
class VehicleTourViewController: CommonVSClass {
 @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Vehicle Tour"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
       tableView.reloadData()
        if(FilterDataFromServer.Authority_Name != "") {
            updateData()
        }
        
    }
    
    @objc func updateData(){
        if(FilterDataFromServer.Authority_Name != "") {
            cellData.lblApprovalAuth.text = FilterDataFromServer.Authority_Name
            cellData.lblApprovalAuth.textColor = UIColor.black
        } else {
            cellData.lblApprovalAuth.text = ""
            cellData.lblApprovalAuth.textColor = UIColor.lightGray
        }
        
        
        // tableView.reloadData()
    }
       var reachablty = Reachability()!
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if cellDataPurpose.txtPurpose.text == "" {
            
            self.view.makeToast("Enter Purpose of Journey")
        } else if dataJourney.count == 0 {
            
            self.view.makeToast("Add Journey Details")
        } else if dataPassenger.count == 0 {
            
            self.view.makeToast("Add Passenger Details")
        }else if cellData.lblApprovalAuth.text == "" || cellData.lblApprovalAuth.text == "Select" {
            
            self.view.makeToast("Select Approval Authority")
        }
        else{
            
            
            self.startLoadingPK(view: self.view)
            
           let IDData : String = UserDefaults.standard.string(forKey: "IDData")!
            
            
            var paramJourney : [[String:Any]] = []
            var ii = 0
            for value in dataJourney {
               
                let dateSet = "/Date(" + value.journey_dateMilli + ")/"
                    
                    
                
                
                
                let paramJourneyValue = ["Type": value.transportTypeId,
                                      "From_Place": value.from ,
                    "To_Place": value.to,
                    "Date": dateSet ,
                    "Start_Date": dateSet,
                    "ID": "0"
                    
                    ]
                    as [String:Any]
                
                 ii = ii + 1;
                 paramJourney.append(paramJourneyValue)
                
            }
          // let jsonparamJourney = JSON(paramJourney)
            var paramPassenger : [[String:Any]] = []
            var iiVal = 0
            for value in dataPassenger {
                
                let paramPasValue = ["Name": value.name,
                                         "SAP_TR_No": value.name ,
                                         "Pax_Type": "",
                                         "Emp_No": IDData ,
                                         "Mobile": value.no,
                                         "Email_Id": value.email ,
                                         "ID": "0",
                                         "Request_ID": "-1" ,
                    
                ]
                
            
                paramPassenger.append(paramPasValue)
                iiVal = iiVal + 1;
                
            }
            
            
           // let jsonparamPas = JSON(paramPassenger)
            
            
            let param = ["ID":"0",
                         "Request_ID":"" ,
                         "Emp_No": IDData,
                         "Purpose_of_Journey": cellDataPurpose.txtPurpose.text!,
                        
                         "SelectedApprover": FilterDataFromServer.Authority_Id
                
                
                
                
                
                
                ]
                as [String:String]
            
            
           // let jsonparam = JSON(param)
            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let parameter = ["UserID":empId,
                             "AuthKey":Profile_AuthKey ,
                             "Req":param ,
                             "ReqData":paramJourney,
                             "PassDetails":paramPassenger
                ] as [String:Any]
            
            
           
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.TourRequest, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                
                let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "ok" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = message
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(message)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
            
            
            
            
            
        }
    }
    @IBAction func btnPlanJourneyClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AddJourneyViewController") as! AddJourneyViewController
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    @IBAction func btnPassengerClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AddPassengerViewController") as! AddPassengerViewController
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    @IBAction func btnPlanJourneyCloseClicked(_ sender: UIButton) {
        
        dataJourney.remove(at: sender.tag)
        tableView.reloadData()
    }
    
    @IBAction func btnPassengerCloseClicked(_ sender: UIButton) {
        
        dataPassenger.remove(at: sender.tag)
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnApprovalAuthClicked(_ sender: UIButton) {
        
        FilterDataFromServer.Authority_Name = String()
        FilterDataFromServer.Authority_Id = String()
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ApprovalAuthorityViewController") as! ApprovalAuthorityViewController
        ZIVC.titleStr = "Select Approval Authority"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    var cellData : ApprovalAuthorityTableViewCell!
    var cellDataPurpose : PurposeofJourneyTableViewCell!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension VehicleTourViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
}
extension VehicleTourViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1){
            return dataJourney.count
        }else if(section == 3){
            return dataPassenger.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PurposeofJourneyTableViewCell
        
        cellDataPurpose = cell
            if(dataJourney.count > 0){
                cell.lblAddJourneyDetail.isHidden = true
            }else{
                cell.lblAddJourneyDetail.isHidden = false
            }
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        }else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellJourney", for: indexPath) as! PlanJourneyTableViewCell
            
            cell.lblTransportType.text = dataJourney[indexPath.row].transportType
            cell.lblJourneyDate.text = dataJourney[indexPath.row].journey_date
            cell.lblFrom.text = dataJourney[indexPath.row].from
            cell.lblTo.text = dataJourney[indexPath.row].to
            cell.btnClose.tag = indexPath.row
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }else if(indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellPas", for: indexPath) as! AddPassengerShowTableViewCell
            
            if(dataPassenger.count > 0){
                cell.lblAddPassengerDetail.isHidden = true
            }else{
                cell.lblAddPassengerDetail.isHidden = false
            }
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }else if(indexPath.section == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellPassenger", for: indexPath) as! PassengerTableViewCell
            
            cell.btnClose.tag = indexPath.row
            cell.lblPassengerType.text = dataPassenger[indexPath.row].passengerType
            cell.lblName.text = dataPassenger[indexPath.row].name
            cell.lblNo.text = dataPassenger[indexPath.row].no
            cell.lblEmail.text = dataPassenger[indexPath.row].email
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellAuth", for: indexPath) as! ApprovalAuthorityTableViewCell
            
            
            cellData = cell
             if(FilterDataFromServer.Authority_Name == "") {
            cell.lblApprovalAuth.text = "Select"
            }
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }
        
        
        
        
    }
}
