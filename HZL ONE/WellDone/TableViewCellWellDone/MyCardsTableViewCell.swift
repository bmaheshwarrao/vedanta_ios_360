//
//  MyCardsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyCardsTableViewCell: UITableViewCell {

   // @IBOutlet weak var heightImageCon: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIViewX!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var textViewContribution: UITextView!
    @IBOutlet weak var lblEmpName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
