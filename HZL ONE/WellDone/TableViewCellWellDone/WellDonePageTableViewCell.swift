//
//  WellDonePageTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class WellDonePageTableViewCell:  UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var view = UIView()
    var timer = Timer()
    var lastXAxis = CGFloat()
    var contentOffset = CGPoint()
    var i = CGFloat()
    var imageDB :[BannerImageIdea] = []
    
    var scrollingTimer = Timer()
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        
        getBannerImageData()
        
        // Initialization code
    }
    
    func pageFromMain()
    {
        self.timer.invalidate()
        
        
        self.timer = Timer()
        self.lastXAxis = CGFloat()
        self.contentOffset = CGPoint()
        self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
        
    }
    
    func saveimagePathData (imagePath:String, id:Int64,text_Message:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = BannerImageIdea(context: context)
        
        tasks.imagePath = imagePath
        tasks.id = id
        tasks.text_Message = text_Message
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
    
    @objc func getBannerImageData() {
        
        self.imageDB = [BannerImageIdea]()
        do {
            
            self.imageDB = try context.fetch(BannerImageIdea.fetchRequest())
            //self.refresh.endRefreshing()
            if(self.imageDB.count > 0) {
                //  BannerStruct.bannerPath = self.imageDB[0].text_Message!
            }
            self.imageCollectionView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
//    func deleteimagePathData()
//    {
//        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//        
//        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "BannerImageIdea")
//        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
//        
//        do {
//            try context.execute(batchDeleteRequest)
//            
//        } catch {
//            // Error Handling
//        }
//    }
    
    var reachablty = Reachability()!
    var dataArray: [BannerDataModel] = []
    func pageView(ApplicationName : String)
    {
        
        if(reachablty.connection != .none)
        {
            self.timer.invalidate()
            self.timer = Timer()
            self.lastXAxis = CGFloat()
            self.contentOffset = CGPoint()
            
            
            self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
            let param : [String: String] = ["Platform":URLConstants.platform,"ActionData":ApplicationName]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Application_Banner, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            self.dataArray = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = BannerDataModel()
                                    object.setDataInModel(str: cell)
                                    self.dataArray.append(object)
                                }
                                
                            }
                            
                            self.imageCollectionView.reloadData()
                            self.startTimer()
                            
                            
                            
                        }
                        
                    }
                    else
                    {
                        
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                
            }
            
            
        }
        else{
            
        }
        
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! ImageSlideCollectionViewCell
        
        cell.contentView.frame.size.width = cell.imageSlide.frame.size.width
        cell.contentView.frame.size.height = cell.imageSlide.frame.size.height
        
        cell.pageControl.numberOfPages = self.dataArray.count
        if let url = NSURL(string: dataArray[indexPath.row].Banner_URL) {
            cell.imageSlide.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "placed"))
        }
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.imageCollectionView.bounds.size.width, height: self.imageCollectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let myCell = cell as? ImageSlideCollectionViewCell {
            myCell.pageControl.currentPage = indexPath.item
        }
        
    }
    
    @objc func scrollToNextCell(){
        
        let cellSize = CGSize(width:self.contentView.frame.width, height:self.contentView.frame.height)
        
        
        self.contentOffset = self.imageCollectionView.contentOffset
        
        if(self.lastXAxis == self.contentOffset.x)
        {
            self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
            
        }
        
        self.imageCollectionView.scrollRectToVisible(CGRect(x:self.contentOffset.x + cellSize.width , y:self.contentOffset.y, width:cellSize.width , height:cellSize.height), animated: true)
        
        
        
        self.lastXAxis = self.contentOffset.x
        
        
    }
    
    func startTimer() {
        
        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(PageTableViewCell.scrollToNextCell), userInfo: nil, repeats: true);
        
        
    }
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

