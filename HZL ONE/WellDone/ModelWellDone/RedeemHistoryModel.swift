//
//  RedeemHistoryModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class RedeeemDataModel: NSObject {
    
    
    var ID = Int()
    var Outlet_ID = String()
    var Outlet_Name = String()
    var Outlet_Code = String()
    var Date = String()
    var Location_Id = String()
    var TnxId = String()
    var Card_ID = String()
    var Card_Amount = String()
    var Outlet_Remark = String()
    var Employee_Name = String()
    
 
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Outlet_ID"] is NSNull || str["Outlet_ID"] == nil{
            self.Outlet_ID = ""
        }else{
            
            let ros = str["Outlet_ID"]
            
            self.Outlet_ID = (ros?.description)!
            
            
        }
        if str["Outlet_Name"] is NSNull || str["Outlet_Name"] == nil{
            self.Outlet_Name =  ""
        }else{
            
            let fross = str["Outlet_Name"]
            
            self.Outlet_Name = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Outlet_Code"] is NSNull || str["Outlet_Code"] == nil{
            self.Outlet_Code = "0"
        }else{
            let emp1 = str["Outlet_Code"]
            
            self.Outlet_Code = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Date"] is NSNull || str["Date"] == nil{
            self.Date = ""
        }else{
            
            let empname = str["Date"]
            
            self.Date = (empname?.description)!
            
        }
        
        if str["Location_Id"] is NSNull || str["Location_Id"] == nil{
            self.Location_Id = ""
        }else{
            let emp2 = str["Location_Id"]
            
            self.Location_Id = (emp2?.description)!
            
            
            
        }
        
        
        
        if str["TnxId"] is NSNull || str["TnxId"] == nil{
            self.TnxId = ""
        }else{
            let empname2 = str["TnxId"]
            
            self.TnxId = (empname2?.description)!
            
        }
        
        
        if str["Card_ID"] is NSNull || str["Card_ID"] == nil{
            self.Card_ID = ""
        }else{
            let locid = str["Card_ID"]
            
            self.Card_ID = (locid?.description)!
            
        }
        
  
        if str["Card_Amount"] is NSNull || str["Card_Amount"] == nil{
            self.Card_Amount = ""
        }else{
            let locname = str["Card_Amount"]
            
            self.Card_Amount = (locname?.description)!
            
        }
        
        
        
        if str["Outlet_Remark"] is NSNull || str["Outlet_Remark"] == nil{
            self.Outlet_Remark = ""
        }else{
            let locname = str["Outlet_Remark"]
            
            self.Outlet_Remark = (locname?.description)!
            
        }
        if str["Employee_Name"] is NSNull || str["Employee_Name"] == nil{
            self.Employee_Name = ""
        }else{
            let locname = str["Employee_Name"]
            
            self.Employee_Name = (locname?.description)!
            
        }
        
        
    }
    
}





class RedeeemDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:ReedemHistoryViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Redeems, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [RedeeemDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = RedeeemDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class RedeeemDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:ReedemHistoryViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
           
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Redeems, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [RedeeemDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = RedeeemDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

