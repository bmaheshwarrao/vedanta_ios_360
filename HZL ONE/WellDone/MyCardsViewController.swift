//
//  MyCardsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyCardsViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{

    
    var MyCardDB:[MyCardDataModel] = []
    var MyCardAPI = MyCardDataAPI()
    
    var MyCardLoadMoreDB : [MyCardDataModel] = []
      var refreshControl = UIRefreshControl()
    
    
    
    var MyCardLoadMoreAPI = MyCardDataAPILoadMore()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        getMyCardData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl.addTarget(self, action: #selector(getMyCardData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = "My Card(s)"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMyCardData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.MyCardDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyCardsTableViewCell
        
     
        if(self.MyCardDB[indexPath.section].DateOfIssue != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MMM dd yyyy"
            let date : Date = dateFormatter.date(from: self.MyCardDB[indexPath.section].DateOfIssue)!
            dateFormatter.string(from: date)
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            let finalDate = dateFormatter2.string(from: date)
            cell.lblDate.text = finalDate
        }else{
            cell.lblDate.text = ""
        }
        
        cell.lblEmpName.text = self.MyCardDB[indexPath.section].Manager_Employee_Name
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        paragraph.lineSpacing = 2
        let  mBuilder = self.MyCardDB[indexPath.section].Employee_Name + " - " + self.MyCardDB[indexPath.section].Employee_ID + " (" + self.MyCardDB[indexPath.section].Location_Name + ") \n" + "is awarded for \n" +  self.MyCardDB[indexPath.section].Contribution
         let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.orange, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
         agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
        cell.textViewContribution.attributedText = agreeAttributedString
        self.data = String(self.MyCardDB[indexPath.section].ID)
        self.lastObject = String(self.MyCardDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.MyCardDB.count - 1)
        {
            
             self.getMyCardDataLoadMore( ID: String(Int(self.MyCardDB[indexPath.section].ID)))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.view.frame.width - 100
        
    }
    @objc func getMyCardData(){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        MyCardAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.MyCardDB = dict as! [MyCardDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getMyCardDataLoadMore(ID : String){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,"ID":ID]
        MyCardLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.MyCardLoadMoreDB =  [MyCardDataModel]()
            self.MyCardLoadMoreDB = dict as! [MyCardDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.MyCardDB.append(contentsOf: self.MyCardLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
