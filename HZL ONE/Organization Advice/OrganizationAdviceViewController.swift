//
//  OrganizationAdviceViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 19/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class OrganizationAdviceViewController:CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var adviceTipsDB:[AdviceDataModel] = []
    var adviceTipsAPI = AdviceDataAPI()
    
    var adviceTipsLoadMoreDB : [AdviceDataModel] = []
    
    var adviceStrNav = String()
    
    
    var adviceTipsLoadMoreAPI = AdviceDataAPILoadMore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getadviceData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getadviceData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = adviceStrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getadviceData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "OrganizationAdvice", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AdviceDetailsViewController") as! AdviceDetailsViewController
        ZIVC.File_title = self.adviceTipsDB[(indexPath?.section)!].Title
        ZIVC.File_Description = self.adviceTipsDB[(indexPath?.section)!].Description
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.adviceTipsDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrganizationAdviceTableViewCell
        
        
       
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
       
        
        
        cell.adviceTextView.text = self.adviceTipsDB[indexPath.section].Title
      
        self.data = String(self.adviceTipsDB[indexPath.section].ID)
        self.lastObject = String(self.adviceTipsDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.adviceTipsDB.count - 1)
        {
            
            self.getadviceDataLoadMore( ID: String(Int(self.adviceTipsDB[indexPath.section].ID)))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getadviceData(){
        var param = [String:String]()
        
        param =  [:]
        adviceTipsAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.adviceTipsDB = dict as! [AdviceDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getadviceDataLoadMore(ID : String){
        var param = [String:String]()
        
        param =  ["ID":ID]
        adviceTipsLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.adviceTipsLoadMoreDB =  [AdviceDataModel]()
            self.adviceTipsLoadMoreDB = dict as! [AdviceDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.adviceTipsDB.append(contentsOf: self.adviceTipsLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

