//
//  PopUpTableViewController.swift
//  VallSafety
//
//  Created by Bunga Maheshwar Rao on 08/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class PopUpTableViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    var firstRow: Bool? = false
    var sourceView: UIView?
    // menu items array
    var itemsArray = [Any]()
    // set background image
    var backgroundImage: UIImage?
    // set to clear if you set the backgroundImage
    var backgroundColor: UIColor?
    // item title color
    var itemTitleColor: UIColor?
    // item selection background
    var itemSelectionColor: UIColor?
    // popover arrow direction
    var arrowDirections = UIPopoverArrowDirection(rawValue: 0)
    // popover arrow color
    var arrowColor: UIColor?
    // item cell tapped block
    var popCellBlock: ((_ popupVC: PopUpTableViewController, _ popCell: UITableViewCell, _ row: Int, _ section: Int) -> Void)? = nil
    var tableView: UITableView?
    var popUpHeight:CGFloat?
    var popUpWidth:CGFloat?
    var isScroll : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UIScreen.main.bounds.size.width-120
        
        
        view.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: popUpWidth!, height: popUpHeight!)
        preferredContentSize = CGSize(width: popUpWidth!, height: popUpHeight!)
        popoverPresentationController?.sourceView = sourceView
        self.tableView = UITableView(frame: view.bounds)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.isScrollEnabled = isScroll!
        self.tableView?.separatorStyle = UITableViewCellSeparatorStyle.none
        view.addSubview(self.tableView!)
        
        if backgroundImage != nil && backgroundColor == nil {
            let tempImageView = UIImageView(image: backgroundImage)
            tempImageView.frame = (self.tableView?.frame)!
            self.tableView?.backgroundView = tempImageView
            self.tableView?.backgroundColor = UIColor.clear
        }else if backgroundImage != nil && backgroundColor != nil {
            let tempImageView = UIImageView(image: backgroundImage)
            tempImageView.frame = (self.tableView?.frame)!
            self.tableView?.backgroundView = tempImageView
            self.tableView?.backgroundColor = UIColor.clear
        }
        else if backgroundImage == nil && backgroundColor != nil {
            self.tableView?.backgroundColor = backgroundColor
        }
        else {
            self.tableView?.backgroundColor = UIColor.white
        }
        popoverPresentationController?.sourceRect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(150), height: CGFloat(30))
        popoverPresentationController?.permittedArrowDirections = arrowDirections
        popoverPresentationController?.backgroundColor = UIColor.white
        if arrowColor != nil {
            popoverPresentationController?.backgroundColor = arrowColor
        }
        self.tableView?.reloadData()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView?.reloadData()
    }
    //self == super.init(nibName: nil, bundle: nil)
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)   {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .popover
        popoverPresentationController?.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) ->
        UIModalPresentationStyle {
            return UIModalPresentationStyle.none
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "popupCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "popupCell")
        }
        cell?.textLabel?.numberOfLines = 0
        if firstRow == true{
            if indexPath.row == 0{
                cell?.textLabel?.textColor = UIColor.darkGray
            }else{
                cell?.textLabel?.textColor = UIColor.black
            }
        }else{
            cell?.textLabel?.textColor = UIColor.black
        }
        //tableView.separatorStyle = .singleLine
        cell?.textLabel?.text = "\(itemsArray[indexPath.row])"
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: UITableViewCell? = tableView.cellForRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
        if popCellBlock != nil {
            popCellBlock?( self, cell!, indexPath.row, indexPath.section)
        }
    }
    
    private func tableView(_ tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.textLabel?.textColor = UIColor.white
        if itemTitleColor != nil {
            cell.textLabel?.textColor = itemTitleColor
        }
        if backgroundImage == nil && backgroundColor == nil {
            cell.textLabel?.textColor = UIColor.black
        }
        cell.textLabel?.textAlignment = .center
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        let additionalSeparator = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(cell.frame.size.height), width: CGFloat(cell.frame.size.width), height: CGFloat(1)))
        additionalSeparator.backgroundColor = UIColor.lightGray
        cell.addSubview(additionalSeparator)
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.lightGray
        if itemSelectionColor != nil {
            bgColorView.backgroundColor = itemSelectionColor
            //[UIColor hex:@"283146"];
        }
        cell.selectedBackgroundView = bgColorView
        cell.textLabel?.alpha = 1.0
    }
    
    
    
    
}

