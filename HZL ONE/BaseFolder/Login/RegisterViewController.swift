//
//  RegisterViewController.swift
// RAS
//
//  Created by SARVANG INFOTCH on 04/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
struct MoveStruct {
    static var isMove = Bool()
    static var message = String()
}
class RegisterViewController: CommonVSClass {
var strRegister = String()
    @IBOutlet weak var txtMobile: MytextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBAction func btnRegisterClicked(_ sender: UIButton) {
         checkOTP()
     
    }
   
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnRegister.setTitle("Send OTP", for: .normal)
        txtMobile.clipsToBounds = true;
        txtMobile.layer.cornerRadius = 10;
txtMobile.layer.borderWidth = 1.0
        txtMobile.keyboardType = UIKeyboardType.numberPad
        txtMobile.layer.borderColor = UIColor.black.cgColor
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true) {
            MoveStruct.isMove = false
            self.navigationController?.popViewController(animated: false)
        }
        self.title = strRegister + " Mobile Number"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
       let reachability = Reachability()!
    func checkOTP() {
        if(self.reachability.connection != .none) {
            
            let EmpID : String = UserDefaults.standard.value(forKey: "EmployeeID") as! String
            let parameters = ["Employee_ID":EmpID,
                              "Mobile_No":self.txtMobile.text! , "AppName" : URLConstants.appName] as [String : Any]
            
              self.startLoadingPK(view: self.view)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.verifyOTP,parameters: parameters, successHandler: { (dict) in
                
              
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        
                        
                          self.stopLoadingPK(view: self.view)
                        if let data = dict["data"]
                        {
                            
                    
                           let OTP = data["OTP"] as! String
                            
                                
                                 UserDefaults.standard.set(OTP, forKey: "OTP")
                            if(self.strRegister == "Register") {
                                if(self.txtMobile.text != "") {
                                    UserDefaults.standard.set(self.txtMobile.text, forKey: "mobile")
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "CheckOTPViewController") as! CheckOTPViewController
                                    let navigationVC = UINavigationController(rootViewController: loginVC)
                                    navigationVC.navigationBar.barTintColor =  UIColor(red: 41/255, green: 88/255, blue: 144/255, alpha: 1.0)
                                    navigationVC.navigationBar.isTranslucent = false;
                                    loginVC.strRegister = self.strRegister
                                    self.present(navigationVC, animated: true, completion: nil)
                                }
                            } else {
                                UserDefaults.standard.set(self.txtMobile.text, forKey: "mobile")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "CheckOTPViewController") as! CheckOTPViewController
                                loginVC.strRegister = self.strRegister
                                self.navigationController?.pushViewController(loginVC, animated: true)
                            }
                                
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {  self.stopLoadingPK(view: self.view)
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                
                  self.stopLoadingPK(view: self.view)
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
              self.stopLoadingPK(view: self.view)
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            
            
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
