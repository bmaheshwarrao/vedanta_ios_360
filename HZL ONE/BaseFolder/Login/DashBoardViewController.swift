//
//  DashBoardViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 14/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation
class DashBoardViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource{
    
    
    
    @IBOutlet weak var homeTableView: UITableView!
    
    
    
    
    
    
    
    
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
        let leftToRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(leftTorightDidfire))
        
        leftToRight.direction = .right
        
        let rightToLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(rightToLeftDidfire))
        rightToLeft.direction = .left
        
        
        self.homeTableView.addGestureRecognizer(leftToRight)
        self.homeTableView.addGestureRecognizer(rightToLeft)
        self.view.addGestureRecognizer(leftToRight)
        self.view.addGestureRecognizer(rightToLeft)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(graphDataInsert), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "graphDataUpdate")), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(GetBusinessData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "mainLocationDataUpdate")), object: nil)
        //
        //
        //        graphDataInsert()
        
        
        
        
        GCMRegister()
        getBannerImageData()
        if reachability.connection == .none{
            self.view.makeToast("Internet is not available, please check your internet connection try again." )
        }
        
        
    }
    var reachability = Reachability()!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func profileDetailsData(){
        let authKey =  UserDefaults.standard.value(forKey: "Profile_AuthKey") as! String
        let parameters = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
                          "Profile_AuthKey":authKey,
                          "AppName":URLConstants.appName]
        let url = "http://sesasafety.dev.app6.in/API/" + "iProfile/GetProfile/"
        WebServices.sharedInstances.sendPostRequest(url: url, parameters: parameters, successHandler: { (response:[String : AnyObject]) in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                
                let respon = response["response"] as! [String:AnyObject]
                
                if self.reachablty.connection != .none{
                    
                    
                    if respon["status"] as! String == "success" {
                        
                        self.deleteProfileData()
                        
                        let dict = response["data"] as! [String:AnyObject]
                        var Employee_Name = String()
                        //                    var SBU_ID = Int()
                        //                    var SBU_Name = String()
                        //                    var Department = String()
                        //                    var Department_Name = String()
                        //                    var Designation = String()
                        //                    var Designation_Name = String()
                        var Gender = String()
                        var Email_ID_Official = String()
                        var Profile_Pic_Path = String()
                        var Mobile_Number = String()
                        
                        //                    if dict["S"] is NSNull{
                        //                        SBU_ID = 0
                        //                    }else{
                        //                        SBU_ID = (dict["S"] as? Int)!
                        //                    }
                        if dict["Employee_Name"] is NSNull{
                            Employee_Name = ""
                        }else{
                            Employee_Name = (dict["Employee_Name"] as? String)!
                        }
                        //                    if dict["SBU_Name"] is NSNull{
                        //                        SBU_Name = ""
                        //                    }else{
                        //                        SBU_Name = (dict["SBU_Name"] as? String)!
                        //                    }
                        //                    if dict["Designation"] is NSNull{
                        //                        Department = ""
                        //                    }else{
                        //                        Department = (dict["Designation"] as? String)!
                        //                    }
                        //                    if dict["Designation_Name"] is NSNull{
                        //                        Department_Name = ""
                        //                    }else{
                        //                        Department_Name = (dict["Designation_Name"] as? String)!
                        //                    }
                        //                    if dict["Designation"] is NSNull{
                        //                        Designation = ""
                        //                    }else{
                        //                        Designation = (dict["Designation"] as? String)!
                        //                    }
                        //                    if dict["Designation_Name"] is NSNull{
                        //                        Designation_Name = ""
                        //                    }else{
                        //                        Designation_Name = (dict["Designation_Name"] as? String)!
                        //                    }
                        
                        //                    var Unit_Name = String()
                        //                    if dict["Unit_Name"] is NSNull{
                        //                        Unit_Name = ""
                        //                    }else{
                        //                        Unit_Name =  (dict["Unit_Name"] as? String)!
                        //                    }
                        //                    var Unit = String()
                        //                    if dict["Unit"] is NSNull{
                        //                        Unit = ""
                        //                    }else{
                        //                        Unit =  (dict["Unit"] as? String)!
                        //                    }
                        //
                        //                    var Zone_Name = String()
                        //                    if dict["Zone_Name"] is NSNull{
                        //                        Zone_Name = ""
                        //                    }else{
                        //                        Zone_Name = (dict["Zone_Name"] as? String)!
                        //                    }
                        //                    var Zone = String()
                        //                    if dict["Zone"] is NSNull{
                        //                        Zone = ""
                        //                    }else{
                        //                        Zone = (dict["Zone"] as? String)!
                        //                    }
                        //
                        //                    var Area_Name = String()
                        //                    if dict["Area_Name"] is NSNull{
                        //                        Area_Name = ""
                        //                    }else{
                        //                        Area_Name = (dict["Area_Name"] as? String)!
                        //                    }
                        //                    var Area = String()
                        //                    if dict["Area"] is NSNull{
                        //                        Area = ""
                        //                    }else{
                        //                        Area =  (dict["Area"] as? String)!
                        //                    }
                        
                        if dict["Gender"] is NSNull{
                            Gender = ""
                        }else{
                            Gender = (dict["Gender"] as? String)!
                        }
                        if dict["Email_ID"] is NSNull{
                            Email_ID_Official = ""
                        }else{
                            Email_ID_Official = (dict["Email_ID"] as? String)!
                        }
                        //                    if dict["Profile_Pic_Path"] is NSNull{
                        //                        Profile_Pic_Path = ""
                        //                    }else{
                        //                        Profile_Pic_Path = (dict["Profile_Pic_Path"] as? String)!
                        //                    }
                        if dict["Mobile_Number"] is NSNull{
                            Mobile_Number = ""
                        }else{
                            Mobile_Number = (dict["Mobile_Number"] as? String)!
                        }
                        
                        
                        
                        
                        
                        self.saveProfileData(Employee_Name: Employee_Name ,  Gender: Gender , Email_ID_Official: Email_ID_Official as! String, Profile_Pic_Path: Profile_Pic_Path as! String, Mobile_Number: String(Mobile_Number))
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    }
                }
            }
            
        }) { (err) in
            print(err.description)
        }
        
    }
    
    
    
    
    //Core Data
    func saveProfileData (Employee_Name:String,Gender:String,Email_ID_Official:String,Profile_Pic_Path:String,Mobile_Number:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = Profile(context: context)
        
        tasks.employee_Name = Employee_Name
        //         tasks.sBU_ID = SBU_ID
        //        // tasks.sBU_Name = SBU_Name
        //         tasks.department = Department
        //         tasks.department_Name = Department_Name
        //  tasks.designation = Designation
        tasks.gender = Gender
        tasks.mobile_Number = Mobile_Number
        tasks.email_ID_Official = Email_ID_Official
        tasks.profile_Pic_Path = Profile_Pic_Path
        //tasks.designation_Name = Designation_Name
        
        //        tasks.area_id = Area
        //        tasks.area_name = Area_Name
        //
        //        tasks.zone_id = Zone
        //        tasks.zone_name = Zone_Name
        //        tasks.unit_id = Unit
        //        tasks.unit_name = Unit_Name
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
        
        
    }
    
    func deleteProfileData()
    {
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Profile")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    @IBAction func openSettingVC(_ sender: UIBarButtonItem) {
        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
        self.navigationController?.pushViewController(messageVC, animated: true)
    }
    @IBAction func openMessageVC(_ sender: Any) {
        
        //        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "Message") as! MessageViewController
        //        self.navigationController?.pushViewController(messageVC, animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = ProjectName
        self.tabBarController?.tabBar.isHidden = false
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    @objc func getBannerImageData() {
        
        var imageDB = [BannerImage]()
        do {
            
            imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(imageDB.count > 0) {
                //BannerStruct.bannerPath = imageDB[0].text_Message!
            }
            self.homeTableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: "HZL_ONE")
            tableView.isScrollEnabled = false
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttons") as! MainGraphTableViewCell
            
         
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        var navheight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        var tabHeight = self.tabBarController?.tabBar.frame.height
        
        switch navheight {
        case nil:
            navheight = 0
            break;
        default:
            break;
        }
        switch tabHeight {
        case nil:
            tabHeight = 0
            break;
        default:
            break;
        }
        
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height-navheight-tabHeight!
        
        if indexPath.section == 0{
            return screenHeight/2.42982
        }else{
            return screenHeight/1.709876
        }
        
    }
    
    //    @objc func graphDataInsert()
    //    {
    //        getGraphData()
    //
    //        piecountNosPer = [Int]()
    //        pieStatus = [String]()
    //        pieChartColorCode = [String]()
    //
    //        let path = NSIndexPath.init(row: 0, section: 1)
    //        let cell = homeTableView.cellForRow(at: path as IndexPath) as! MainGraphTableViewCell
    //
    //        cell.sustainabilityPieChart.animate(xAxisDuration: 2.0, yAxisDuration: 3.0)
    //        cell.sustainabilityPieChart.drawSlicesUnderHoleEnabled = true
    //        //sustainabilityPieChart.layer.cornerRadius = 4.0
    //        //cell.sustainabilityPieChart.layer.borderWidth = 2.0
    //        //cell.sustainabilityPieChart.layer.borderColor = UIColor.lightGray.cgColor
    //        cell.sustainabilityPieChart.chartDescription?.enabled = false
    //
    //        //sustainabilityPieChart.centerText = "Sustainability"
    //
    //
    //        for i in 0..<self.graphDB.count {
    //
    //            self.piecountNosPer.append(Int(self.graphDB[i].total_Count))
    //            self.pieStatus.append(self.graphDB[i].status! + " " + String(Int(self.graphDB[i].total_Count)))
    //            self.pieChartColorCode.append(self.graphDB[i].color_Code!)
    //
    //
    //        }
    //
    //        self.setChart1(dataPoints: self.pieStatus, values: self.piecountNosPer)
    //
    //
    //    }
    //
    //    func setChart1(dataPoints: [String], values: [Int]) {
    //
    //        var dataEntries1: [PieChartDataEntry] = []
    //        let path = NSIndexPath.init(row: 0, section: 1)
    //        let cell = homeTableView.cellForRow(at: path as IndexPath) as! MainGraphTableViewCell
    //
    //        for (index1,_) in dataPoints.enumerated()
    //        {
    //
    //
    //            let pieDataEntry1 = PieChartDataEntry(value: Double(values[index1]), label: dataPoints[index1],data:dataPoints[index1] as AnyObject?)
    //
    //            dataEntries1.append(pieDataEntry1)
    //
    //        }
    //
    //
    //        var colors2  = [UIColor]()
    //
    //        for index2 in self.pieChartColorCode
    //        {
    //
    //            let someColor = UIColor.colorwithHexString(index2, alpha: 8)
    //
    //            colors2.append(someColor!)
    //
    //        }
    //
    //
    //        let chartDataSet = PieChartDataSet(values: dataEntries1, label: "")
    //        let chartData = PieChartData()
    //
    //        chartData.addDataSet(chartDataSet)
    //
    //        chartDataSet.colors = colors2
    //        cell.sustainabilityPieChart.data = chartData
    //        cell.sustainabilityPieChart.drawEntryLabelsEnabled = false
    //        cell.sustainabilityPieChart.contentScaleFactor = 1.0
    //
    //
    //
    //    }
    //
    //    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight){
    //
    //        let DashbordHazardFromGraph = self.storyboard?.instantiateViewController(withIdentifier: "DashbordHazardFromGraph") as! DashbordHazardFromGraphViewController
    //
    //        if let value = entry.data as? String{
    //            DashbordHazardFromGraph.statusStr = value.byWords.first!
    //        }
    //
    //        FilterGraphStruct.hazard_Name_ID = String()
    //        FilterGraphStruct.business_NameID = String()
    //        FilterGraphStruct.area_ID = String()
    //        FilterGraphStruct.sub_area_ID = String()
    //        FilterGraphStruct.location_ID = String()
    //
    //        self.navigationController?.pushViewController(DashbordHazardFromGraph, animated: true)
    //
    //
    //    }
    
    
    
    
    //    func getGraphData() {
    //
    //        self.graphDB = [Graph]()
    //        do {
    //
    //            self.graphDB = try context.fetch(Graph.fetchRequest())
    //            self.homeTableView.reloadData()
    //
    //        } catch {
    //            print("Fetching Failed")
    //        }
    //    }
    
    @IBAction func MQuizClicked(_ sender: UIButton) {
        
        
     
        
       
      

        
    }
    
    @IBAction func HistoryClicked(_ sender: UIButton) {

    }
    
    @IBAction func SettingClicked(_ sender: UIButton) {
        
        let messageVC = self.storyboard?.instantiateViewController(withIdentifier: "settings") as! SettingsViewController
        self.navigationController?.pushViewController(messageVC, animated: true)
//        let InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxDisplayViewController") as! InboxDisplayViewController
//
//        self.navigationController?.pushViewController(InboxVC, animated: true)
    }
    
    @IBAction func NotificationClicked(_ sender: UIButton) {
        
//        let InboxVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxDisplayViewController") as! InboxDisplayViewController
//        
//        self.navigationController?.pushViewController(InboxVC, animated: true)
    }
    
    @objc func leftTorightDidfire()
    {
        switch UserDefaults.standard.bool(forKey: "isContractorLogin") || UserDefaults.standard.bool(forKey: "isWorkmanLogin"){
        case true:
            
            break;
        default:
            let tabBar1: UITabBar = (self.tabBarController?.tabBar)!
            
            let index: Int = (tabBar1.items)!.index(of: tabBar1.selectedItem!)!
            if(index > 0)
            {
                
                self.tabBarController?.selectedIndex = index - 1
                
            }
            else{
                return
            }
            break;
        }
        
    }
    
    @objc func rightToLeftDidfire()
    {
        
        switch UserDefaults.standard.bool(forKey: "isContractorLogin") || UserDefaults.standard.bool(forKey: "isWorkmanLogin"){
        case true:
            
            break;
        default:
            
            let tabBar1: UITabBar = (self.tabBarController?.tabBar)!
            
            let index: Int = (tabBar1.items)!.index(of: tabBar1.selectedItem!)!
            if(index < (tabBar1.items?.count)! - 1)
            {
                
                self.tabBarController?.selectedIndex = index + 1
                
            }
            else{
                return
            }
            
            
            break;
        }
        
        
        
    }
    
    var GCMToken = String()
    func GCMRegister(){
        
        if let object = UserDefaults.standard.value(forKey: "Token") {
            
            GCMToken = object as! String
        }
        
        let para = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
                    "GCM_Token":GCMToken,
                    "Platform":"iOS"]
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.Post_Gcm, parameters: para, successHandler: { (dict) in
            
            print("GCMRegister",dict)
            
        }) { (err) in
            print(err)
        }
        
    }
    
    
    
    //Core Data
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
