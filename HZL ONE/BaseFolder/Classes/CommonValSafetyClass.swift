//
//  CommonClass.swift

//

import UIKit
import PKHUD
import Reachability
import CoreData
import MBProgressHUD

class CommonVSClass: UIViewController {
    
    
    // Activity Indicator(start)
    
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let label: UITextView = UITextView()
    let active: UIActivityIndicatorView = UIActivityIndicatorView()
    var refresh = UIRefreshControl()
//    func startLoading(){
//
//
//        HUD.show(.progress)
//
//    }
     var AppName = String()                                                                                  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppName = "mQuiz"
        self.active.center = self.view.center
        self.active.activityIndicatorViewStyle = .gray
        self.active.hidesWhenStopped = true
        self.view.addSubview(self.active)
        
        
    }
    func startLoading(view:UIView){
        
        
        active.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2 - 100)
        active.activityIndicatorViewStyle = .gray
        active.startAnimating()
        self.active.isHidden = false
        active.isUserInteractionEnabled = false
        active.hidesWhenStopped = true
        view.addSubview(active)
        
        
    }
    func fontDataSemiBold() -> UIFont {
        return UIFont.systemFont(ofSize: 18.0, weight: .semibold)
    }
    
    func fontDataBold() -> UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .medium)
    }
    
    func stopLoading(view:UIView){
        
        
        active.stopAnimating()
        self.active.isHidden = true
        
    }
    func startLoading()
    {
      self.active.startAnimating()
    }
    func stopLoading()
    {
     self.active.stopAnimating()
        
 
    }
    var MBHud = MBProgressHUD(frame: CGRect(x: 0, y: -100, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + 100))
    var pkHud = PKHUD()
    func startLoadingPK(view : UIView){
       
//MBHud.show(animated: true)
//         view.addSubview(MBHud)
       HUD.show(.labeledRotatingImage(image: UIImage(named: "loading"), title: "Processing", subtitle: ""))
       
        
    }
    func stopLoadingPK(view : UIView){
        
       // MBHud.hide(animated: true)
      HUD.hide()
        
    }
    
    func startLoading(TB: UITableView){
        
        
        active.center = TB.center
        active.activityIndicatorViewStyle = .gray
        active.startAnimating()
        active.isUserInteractionEnabled = false
        active.hidesWhenStopped = true
        TB.addSubview(active)
        
    }
    
//    func stopLoading(){
//        HUD.hide()
//
//
//    }
    
    

    
    /// No Internet Image(Start)
    func noInternet()
    {
        let image: UIImageView = UIImageView()
        image.frame = CGRect.init(x: 20, y: 200, width: self.view.frame.width - 50, height: 250)
        image.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleLeftMargin, . flexibleRightMargin]
        
        image.center = self.view.center
        
        image.layer.cornerRadius = 10.0
        
        image.image = UIImage.init(named: "noReach")
        
        self.view.addSubview(image)
    }
    /// (end)
    
    
    
    /// No Data Image(Start)
    func noData()
    {
        let image: UIImageView = UIImageView()
        image.frame = CGRect.init(x: 20, y: 200, width: self.view.frame.width - 50, height: 250)
        image.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleLeftMargin, . flexibleRightMargin]
        
        image.center = self.view.center
        
        image.layer.cornerRadius = 10.0
        
        image.image = UIImage.init(named: "no_data")
        
        self.view.addSubview(image)
    }
    ///(end)
    
    
    /// No Data Label(Start)
    func noDataLabel(text: String)
    {
        
        
        label.frame = CGRect.init(x: 30, y: 20, width: self.view.frame.width - 20, height: 90)
        label.center = self.view.center
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = text
       label.isEditable = false
        label.isSelectable = false;
        label.isScrollEnabled = false
        label.textAlignment = .center
        label.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        label.backgroundColor = UIColor.clear
        self.view.addSubview(label)
        
    }
    ///(end)
    
    func noInternetLabel()
    {
        
        let label: UILabel = UILabel()
        label.frame = CGRect.init(x: 30, y: 20, width: self.view.frame.width - 20, height: 30)
        label.center = self.view.center
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = "No internet connection found. Check your internet connection or try again."
        label.numberOfLines = 2
        label.sizeToFit()
        label.textAlignment = .center
        label.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        
        self.view.addSubview(label)
        
    }
    
    func errorChecking(error: Error)
    {
        let errorStr = error.localizedDescription
        
        if error._code == NSURLErrorTimedOut {
            
            self.showSingleButtonWithMessage(title: "TimeOut!", message: errorStr, buttonName: "OK")
            
            // obj.stopLoading()
            
            
        }
        
        if("\(error._code)" == "-1003")
        {
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
        }
        else
        {
            
            self.showSingleButtonWithMessage(title: "Oops!", message: "something went wrong. Try Agin", buttonName: "OK")
            
        }
        
        
    }
    
    
    ///show alert for only display information
    func showSingleButtonAlertWithoutAction (title:String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    ///show alert with Single Button action
    func showSingleButtonAlertWithAction (title:String,buttonTitle:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func showSingleButtonAlertWithActionMessage (title:String,message : String,buttonTitle:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with right action button
    func showTwoButtonAlertWithRightAction (title:String,buttonTitleLeft:String,buttonTitleRight:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertActionStyle.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with left action button
    func showTwoButtonAlertWithLeftAction (title:String,buttonTitleLeft:String,buttonTitleRight:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertActionStyle.default, handler: { action in
            completionHandler()
        }))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with two action button
    func showTwoButtonAlertWithTwoAction(title:String,message : String,buttonTitleLeft:String,buttonTitleRight:String,completionHandlerLeft:@escaping () -> (),completionHandlerRight:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertActionStyle.default, handler: { action in
            completionHandlerLeft()
        }))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertActionStyle.default, handler: { action in
            completionHandlerRight()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func showSingleButtonWithMessage(title:String,message: String, buttonName: String)
    {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: buttonName, style: UIAlertActionStyle.default, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
//    func errorChecking(error: Error)
//    {
//        let errorStr = error.localizedDescription
//
//        if error._code == NSURLErrorTimedOut {
//
//            self.showSingleButtonWithMessage(title: "TimeOut!", message: errorStr, buttonName: "OK")
//
//
//
//        }
//
//        if("\(error._code)" == "-1003")
//        {
//            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
//
//        }
//        else
//        {
//
//            self.showSingleButtonWithMessage(title: "Oops!", message: "something went wrong. Try Agin", buttonName: "OK")
//
//        }
//
//
//    }
    
    func checkTextIsEmpty(text: String) -> Bool
    {
        
        if(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
        {
            return true
        }
        else
        {
            return false
        }
        
        
    }
    
    
    
    func noInternetShow()
    {
        self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
    }
    
    
    func alert (title:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            self.stopLoadingWithoutTouch()
            let alertController = UIAlertController(title: nil, message: title, preferredStyle: .alert)
            alertController.view.tintColor = UIColor.black
            self.present(alertController, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
                alertController.dismiss(animated: true, completion: {() -> Void in
                    
                    
                })
            })
            
        }
    }
    
    func showToast(message : String) {
        
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width-20, height: 35))
        //toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.blue
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func stopLoadingWithoutTouch()
    {
        self.active.stopAnimating()
        self.active.isUserInteractionEnabled = true
        //self.active.removeFromSuperview()
        
    }
//    var reachability = Reachability()!
//    func internetChecking(isAlert: Bool) -> Bool
//    {
//        if(self.reachability.connection != .none)
//        {
//            return true
//        }
//        else{
//
//            if(isAlert == true)
//            {
//                self.noInternetShow()
//            }
//            else
//            {
//                self.noDataLabel(text: "No internet connection avaliable. Please check your internet connection and try again!")
//            }
//
//
//            return false
//
//        }
//
//
//
//    }
    
    
    func downloadPDF(linkString:String,linkName:String,completionHandler:@escaping () -> ())
    {
        
        
        
            
            let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
            let destinationFileUrl = documentsUrl.appendingPathComponent(linkName)
            print(linkString)
        let linkStringData = linkString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            //Create URL to the source file you want to download
        let fileURL = URL(string:linkStringData!)
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            print(fileURL!)
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print(tempLocalUrl)
                        
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    
                    completionHandler()
                    
                    
                } else {
                    //print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as? Error);
                }
            }
            task.resume()
            
            
        
        
    }
    
    
    func checkPDFIsAvailable(linkName:String,  success:@escaping (String) -> Void,failure: @escaping () -> ())
    {
        
        
        print("linkName",linkName)
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent(linkName)?.path
        let fileManager = FileManager.default
        
        //        cell.pdfFilepath = filePath!
        
        let pdfFilepath = filePath!
        print(filePath!)
        if fileManager.fileExists(atPath: filePath!) {
            
            print("FILE AVAILABLE")
            
            
            success(pdfFilepath)
            
            
        } else {
            
            print("FILE NOT AVAILABLE")
            
            failure()
            
            
            
        }
        
    }
    
    
}
