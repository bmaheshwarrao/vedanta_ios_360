
//
//  MainGraphTableViewCell.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 18/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import MarqueeLabel
import UIKit
import Charts
import SDWebImage
import CoreData
class MainGraphTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
 let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    @IBOutlet weak var buttonCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonCollectionView.delegate = self
        buttonCollectionView.dataSource = self
    buttonCollectionView.isScrollEnabled = true
        // Initialization code
    }
    @objc func getBannerImageData(obj : HomeViewController) {
        
        dataHomePageDB = []
        do {
            
            dataHomePageDB = try context.fetch(HzlApplicationMarket.fetchRequest())
            if(dataHomePageDB.count > 0){
            self.buttonCollectionView.reloadData()
            obj.homeTableView.reloadData()
            obj.indexing = 1
            }
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    
    func deleteimagePathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "HzlApplicationMarket")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    
    var reachablty = Reachability()!
   
    func HomePageCalling(obj : HomeViewController)
    {
       
        
        if(reachablty.connection != .none)
        {
            
            let param = ["Platform":URLConstants.platform]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.AppMarket, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            self.deleteimagePathData()
                            dataHomePage = []
                            dataHomePageDB = []
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = HomePageDataModel()
                                    object.setDataInModel(str: cell)
                                    dataHomePage.append(object)
                                }
                                
                            }
                            self.getBannerImageData(obj: obj)
                        
                        }
                        
                       
                    }
                    else
                    {
                        self.getBannerImageData(obj: obj)
                     
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                
            }
            
            
        }
        else{
            self.getBannerImageData(obj: obj)
            
        }
        
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return dataHomePageDB.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttons", for: indexPath) as! buttonsCollectionViewCell
        
        let urlString = dataHomePageDB[indexPath.row].app_Icon!
        let imageView = UIImageView()
        
        
        
        
        
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: dataHomePageDB[indexPath.row].app_Icon!) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imgButton.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imgButton.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 100, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imgButton.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        cell.textViewBtnName.text = dataHomePageDB[indexPath.row].appName
       cell.btnAppName.tag = indexPath.row
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let wid = UIScreen.main.bounds.width / 4
        print(wid)
        return CGSize(width: wid, height: 100)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
}
