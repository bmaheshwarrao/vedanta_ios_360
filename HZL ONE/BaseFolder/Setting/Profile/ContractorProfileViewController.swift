//
//  ContractorProfileViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 21/03/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class ContractorProfileViewController: UIViewController {

    @IBOutlet weak var personalNoLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var POLabel: UILabel!
    @IBOutlet weak var contractorNameLabel: UILabel!
    @IBOutlet weak var fatherNameLabel: UILabel!
    @IBOutlet weak var plantLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var businessLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        
        
        
        
        
        
        self.nameLabel.text = UserDefaults.standard.value(forKey: "CName") as? String
        self.personalNoLabel.text = UserDefaults.standard.value(forKey: "EmployeeID") as? String
        self.POLabel.text = UserDefaults.standard.value(forKey: "PO") as? String
        self.contractorNameLabel.text = UserDefaults.standard.value(forKey: "Contractor") as? String
        self.fatherNameLabel.text = UserDefaults.standard.value(forKey: "Parent") as? String
        self.plantLabel.text = UserDefaults.standard.value(forKey: "Plant") as? String
        self.categoryLabel.text = UserDefaults.standard.value(forKey: "Category") as? String
        self.departmentLabel.text = UserDefaults.standard.value(forKey: "Department") as? String
        self.businessLabel.text = UserDefaults.standard.value(forKey: "BusinessType") as? String
        
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.title = "Profile"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }

}
