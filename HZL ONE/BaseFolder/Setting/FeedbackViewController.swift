
import UIKit
import Alamofire
import MobileCoreServices
import CoreData

class FeedbackViewController: CommonVSClass, UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
var StrNav = String()
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var suggesionTextView: UITextView!
    
    @IBOutlet weak var nameView: UIViewX!
    @IBOutlet weak var mobileNoView: UIViewX!
    
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var submitBtnBottomHeight: NSLayoutConstraint!
    
    @IBOutlet weak var suggestionImageView: UIImageView!
    
    var profileDB : [Profile] = []
    
    var imagedata: Data? = nil
    let pickerr = UIImagePickerController()
    let pickerImage = UIImage()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    @IBOutlet weak var nameHeight: NSLayoutConstraint!
    @IBOutlet weak var removeBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        pickerr.delegate = self
        
//        nameView.layer.cornerRadius = 8
//        mobileNoView.layer.cornerRadius = 8
//        suggesionTextView.layer.cornerRadius = 8
        submitButton.layer.cornerRadius = 8
        
        
        nameView.layer.cornerRadius = 8
        nameView.layer.borderWidth = 1
        nameView.layer.borderColor = UIColor.black.cgColor
        
        mobileNoView.layer.cornerRadius = 8
        mobileNoView.layer.borderWidth = 1
        mobileNoView.layer.borderColor = UIColor.black.cgColor
        
        suggesionTextView.layer.cornerRadius = 8
        suggesionTextView.layer.borderWidth = 1
        suggesionTextView.layer.borderColor = UIColor.black.cgColor
        
     
        
        suggesionTextView.textColor = .lightGray
        suggesionTextView.delegate = self
        
        var navheight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        switch navheight {
        case nil:
            navheight = 0
            break;
        default:
            break;
        }
        
        let screenSize = UIScreen.main.bounds
        
       // submitBtnBottomHeight.constant = screenSize.height - navheight - 526.5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getProfileDetailsData()
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = StrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Write suggestion here")
        {
            textView.text = ""
            textView.textColor = .black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = "Write suggestion here"
            textView.textColor = .lightGray
        }
        textView.resignFirstResponder()
    }
   
    
    func getProfileDetailsData() {
        
        switch UserDefaults.standard.bool(forKey: "isContractorLogin") {
        case true:
            nameLabel.text = UserDefaults.standard.value(forKey: "Employee_Name") as? String
            mobileNoLabel.text = ""
            break;
        default:
            self.profileDB = [Profile]()
            
            do {
                
                self.profileDB = try context.fetch(Profile.fetchRequest())
                if self.profileDB.count>0 {
                   
                    nameLabel.text = self.profileDB[0].employee_Name!
                    mobileNoLabel.text = self.profileDB[0].mobile_Number!
                    
                }
                
                
            } catch {
                print("Fetching Failed")
            }
            break;
        }
        
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
      
        submitSuggestionData()
    }
    var reachability = Reachability()!
    func submitSuggestionData(){
        
        if reachability.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if self.suggesionTextView.text == "" || self.suggesionTextView.text == "Write suggestion here"{
            
            self.view.makeToast("Please write suggestion")
        }else{
            
           self.startLoadingPK(view: self.view)
            var name = String()
            var mobileNo = String()
            
            
            switch UserDefaults.standard.bool(forKey: "isContractorLogin") {
            case true:
                name = UserDefaults.standard.value(forKey: "CName") as! String
                mobileNo = ""
                break;
            default:
                 name = self.profileDB[0].employee_Name!
                 mobileNo = self.profileDB[0].mobile_Number!
                 break;
            }
            
          
            
            let parameter =  ["Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID"),
                              "Mobile_No":mobileNo,
                              "Name":name,
                              "Message":suggesionTextView.text!] as! [String:String]
            
            
            
           
            
          
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Feedback_Post, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                let objectmsg = MessageCallServerModel()
                let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                self.stopLoadingPK(view: self.view)
                if respon["status"] as! String == "success" {
                    MoveStruct.isMove = true
                    MoveStruct.message = message
              self.navigationController?.popViewController(animated: true)
                   self.suggesionTextView.text = "Write suggestion here"
                    self.suggesionTextView.textColor = UIColor.lightGray
                }else{
                     self.stopLoadingPK(view: self.view)
                    self.view.makeToast(message)
                }
                
            }) { (err) in
                 self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
       
            
           
        }
    }
    
    @IBAction func suggestionImageTap(_ sender: Any) {
        selectImage()
    }
    
    func selectImage(){
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                
                self.camera()
                
            })
            let Library = UIAlertAction(title: "Choose From Library", style: .default, handler: { (alert) in
                
                self.gallery()
                
            })
            
            
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                
            })
            
            actionSheetController.addAction(camera)
            actionSheetController.addAction(Library)
            actionSheetController.addAction(cancel)
            self.present(actionSheetController, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType == (kUTTypeImage as String)
            {
                
                
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
                let resizedImage = image
                suggestionImageView.contentMode = .scaleAspectFill
                imageHeight.constant = 120
                suggestionImageView.image = resizedImage
                
                imagedata = UIImageJPEGRepresentation(resizedImage, 1.0)!
                
                switch suggestionImageView.image {
                case nil:
                    
                    imageHeight.constant = 0
                    imageWidth.constant = 0
                    nameHeight.constant = 0
                    removeBtn.isHidden = true
                    scrollView.reloadInputViews()
                    
                    
                default:
                    
                    imageHeight.constant = 120
                    imageWidth.constant = 120
                    nameHeight.constant = 8
                    removeBtn.isHidden = false
                    scrollView.reloadInputViews()
                    break;
                }
                
                
                dismiss(animated:true, completion: nil)
            }
            else
            {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }
    
   
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func camera()
    {
        pickerr.allowsEditing = false
        pickerr.sourceType = UIImagePickerControllerSourceType.camera
        pickerr.cameraCaptureMode = .photo
        pickerr.modalPresentationStyle = .fullScreen
        present(pickerr,animated: true,completion: nil)
        
    }
    
    func gallery()
    {
        pickerr.allowsEditing = false
        pickerr.sourceType = .photoLibrary
        present(pickerr, animated: true, completion: nil)
    }
    
    @IBAction func removAction(_ sender: Any) {
        
       
        imageHeight.constant = 0
        imageWidth.constant = 0
        nameHeight.constant = 0
        removeBtn.isHidden = true
        suggestionImageView.image = nil
        imagedata = nil
        scrollView.reloadInputViews()
        
        
    }
    

}
