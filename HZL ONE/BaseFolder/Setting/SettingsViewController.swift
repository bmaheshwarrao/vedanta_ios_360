//
//  SettingViewController.swift
// RAS
//
//  Created by Bunga Maheshwar Rao on 02/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import BRYXBanner
import AVFoundation
import CoreData
import AudioToolbox.AudioServices

//struct ProjectListToInsertData{
    
   // static var fromTo = String()
    
//}

class SettingsViewController: UIViewController,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    var player: AVAudioPlayer?
    
    var soundNameArray = [String]()
    var soundImageArray = [String]()
    var soundEnableArray = [String]()
    
    var vibrateNameArray = [String]()
    var vibrateImageArray = [String]()
    var profileDB : [Profile] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        self.soundNameArray = ["Notifications","Sound"]
        self.soundImageArray = ["notificEnable","soundEnable"]
        self.soundEnableArray = ["Enable","Device Default"]
        
        self.vibrateNameArray = ["Vibrate","Send me a test notification"]
        self.vibrateImageArray = ["vibrate","sendMe"]
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name(rawValue: "reloadTableView"), object: nil)
        
        
        
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.title = "Settings"
        
        getProfileDetailsData()
        self.tabBarController?.tabBar.isHidden = false
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = false
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
       
        if indexPath?.section == 0{
            
            switch UserDefaults.standard.bool(forKey: "isContractorLogin") {
            case true:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainVC = storyboard.instantiateViewController(withIdentifier: "ContractorProfile") as! ContractorProfileViewController
                self.navigationController?.pushViewController(mainVC, animated: true)
                break;
            default:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                NotificationCenter.default.post(name: NSNotification.Name.init("SettingsUpdate"), object: nil)
                let mainVC = storyboard.instantiateViewController(withIdentifier: "profileDetails") as! ProfileViewController
                self.navigationController?.pushViewController(mainVC, animated: true)
                
                break;
            }
            
            
        }
        else if indexPath?.section == 1{
            
            if indexPath?.row == 0{
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"{
                    
                    UserDefaults.standard.set("Disable", forKey: "notificationEnable")
                    UserDefaults.standard.set("notificDisable", forKey: "notificationEnableIm")
                    
                    
                    self.tableView.reloadData()
                    
                    
                }else{
                    UserDefaults.standard.set("Enable", forKey: "notificationEnable")
                    UserDefaults.standard.set("notificEnable", forKey: "notificationEnableIm")
                    self.tableView.reloadData()
                }
                
            }else if indexPath?.row == 1{
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
                        self.addChildViewController(popOverVC)
                        popOverVC.view.frame = self.view.frame
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                        
                    }
                    
                }else{
                    //
                }
            }
        }
            
        else if indexPath?.section == 2{
            
            if indexPath?.row == 0{
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"{
                    
                    if UserDefaults.standard.bool(forKey: "vibrateSwitch") == true{
                        UserDefaults.standard.set(false, forKey: "vibrateSwitch")
                    }else{
                        UserDefaults.standard.set(true, forKey: "vibrateSwitch")
                    }
                }else{
                    //
                }
            }else{
                
                if indexPath?.row == 2{
                    
                    switch UserDefaults.standard.bool(forKey: "isContractorLogin") {
                    case true:
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let FBVC = storyboard.instantiateViewController(withIdentifier: "Feedback") as! FeedbackViewController
                        self.navigationController?.pushViewController(FBVC, animated: true)
                        break;
                    default:
                        if self.profileDB.count>0{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let FBVC = storyboard.instantiateViewController(withIdentifier: "Feedback") as! FeedbackViewController
                            self.navigationController?.pushViewController(FBVC, animated: true)
                            
                        }
                        break;
                    }
                    
                    
                }else{
                    
                    if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
                    {
                        self.notificationBanner()
                    }else{
                        //
                    }
                }
            }
            
        }
        else{
            //
        }
        
        
        //
        
    }
    @objc func tapViewTapNotificationClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
     
        
        
        
        //
        
    }
    func notificationBanner(){
        
        let title = "HZL ONE"
        let subtitle = "This is what a notification from HZL ONE look like."
        
        if UserDefaults.standard.bool(forKey: "soundSwitch") == true{
            if UserDefaults.standard.value(forKey: "soundEnable") as! String == "Device Default"{
                let systemSoundID: SystemSoundID = 1315
                AudioServicesPlaySystemSound (systemSoundID)
            }else{
                playSound(soundName:UserDefaults.standard.value(forKey: "soundFileEnable") as! String)
            }
        }
        if UserDefaults.standard.bool(forKey: "vibrateSwitch") == true{
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
        
        
        let banner = Banner(title: title , subtitle: subtitle, image: UIImage(named: "app_logo"), backgroundColor: UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000))
        banner.dismissesOnTap = true
        banner.show(duration: 2.0)
        
        
    }
    
    func playSound (soundName:String) {
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    func getName()-> String{
        var name = String()
        
        let fname = UserDefaults.standard.string(forKey: "FirstName")
        let Mname = UserDefaults.standard.string(forKey: "MiddleName")
        let Lname = UserDefaults.standard.string(forKey: "Lastname")
        
        
        if(Mname == ""){
            name = fname! + " " + Lname!
        }else{
            name = fname! + " " + Mname!  + " " + Lname!
        }
        return name
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }
        else if section == 1{
            return 2
        }
        else if section == 2{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "profile") as! ProfileTableViewCell
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
        
            switch self.profileDB.count {
            case 1:
                
                if let url = NSURL(string: profileDB[indexPath.row].profile_Pic_Path!) {
                    cell.profilePic.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "profile"))
                }
                if(profileDB.count == 0){
                   
                    cell.nameLabel.text = getName()
                }else{
                    if(profileDB[indexPath.row].employee_Name! == ""){
                cell.nameLabel.text = profileDB[indexPath.row].employee_Name!
                    }else{
                       
                        cell.nameLabel.text = getName()
                    }
                }
                
                
                break;
            default:
                let loginType = UserDefaults.standard.value(forKey: "LoginType") as? String
                if(loginType == "E"){
                    cell.profilePic.image = UIImage (named: "profile")
                    cell.nameLabel.text = getName()
                }else{
                
                cell.profilePic.image = UIImage (named: "profile")
                cell.nameLabel.text = UserDefaults.standard.value(forKey: "CName") as? String
                }
            }
            
            cell.nameLabel.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
            cell.personalNoLabel.text = "View Profile"
            
            return cell
        }
        else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "settings") as! SettingsTableViewCell
            
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
            cell.nameLabel.text = self.soundNameArray[indexPath.row]
            
            if indexPath.row == 0{
                
                cell.settingsSwitch.isHidden = true
                cell.profilePic.image = UIImage.init(named:UserDefaults.standard.value(forKey: "notificationEnableIm") as! String)
                cell.changeLabel.text = UserDefaults.standard.value(forKey: "notificationEnable") as? String
                cell.viewLine.isHidden = false
                if cell.changeLabel.text == "Enable"{
                    
                    cell.changeLabel.textColor = UIColor(hexString: "2c3e50", alpha: 1.0)
                }else{
                    cell.changeLabel.textColor = UIColor.darkGray
                }
                
            }else if indexPath.row == 1{
                
                cell.settingsSwitch.isHidden = false
                cell.viewLine.isHidden = false
                cell.changeLabel.text = UserDefaults.standard.value(forKey: "soundEnable") as? String
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
                {
                    cell.settingsSwitch.isEnabled = true
                    cell.settingsSwitch.onTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
                    
                    if UserDefaults.standard.bool(forKey: "soundSwitch") == true{
                        
                        cell.settingsSwitch.isOn = true
                        cell.profilePic.image = UIImage.init(named: "soundEnable")
                        cell.changeLabel.textColor = UIColor(hexString: "2c3e50", alpha: 1.0)
                        
                    }else{
                        
                        cell.settingsSwitch.isOn = false
                        cell.profilePic.image = UIImage.init(named: "soundDisable")
                        cell.changeLabel.textColor = UIColor.darkGray
                    }
                    
                }else{
                    
                    cell.settingsSwitch.isEnabled = false
                    cell.settingsSwitch.onTintColor = UIColor.lightGray
                    cell.changeLabel.textColor = UIColor.darkGray
                }
                
                
            }
            
            return cell
            
        }else if indexPath.section == 2{
          
            let cell = tableView.dequeueReusableCell(withIdentifier: "vibrate") as! VibrateTableViewCell
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
            if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
            {
                cell.settingsSwitch.isEnabled = true
                cell.settingsSwitch.onTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
                if UserDefaults.standard.bool(forKey: "vibrateSwitch") == true{
                    cell.settingsSwitch.isOn = true
                }else{
                    cell.settingsSwitch.isOn = false
                }
            }else{
                cell.settingsSwitch.isEnabled = false
                cell.settingsSwitch.onTintColor = UIColor.lightGray
            }
            
            
            if indexPath.row == 1{
                cell.settingsSwitch.isHidden = true
            }else{
                cell.settingsSwitch.isHidden = false
            }
            switch indexPath.row {
            case 2:
                cell.settingsSwitch.isHidden = true
                break;
            default:
                break;
            }
            
            cell.nameLabel.text = self.vibrateNameArray[indexPath.row]
            cell.profilePic.image = UIImage.init(named: self.vibrateImageArray[indexPath.row])
            
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "powered") as! poweredByTableViewCell
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewCompanyTapClicked(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            
            switch UserDefaults.standard.bool(forKey: "isContractorLogin") {
            case true:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainVC = storyboard.instantiateViewController(withIdentifier: "ContractorProfile") as! ContractorProfileViewController
                self.navigationController?.pushViewController(mainVC, animated: true)
                break;
            default:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                NotificationCenter.default.post(name: NSNotification.Name.init("SettingsUpdate"), object: nil)
                let mainVC = storyboard.instantiateViewController(withIdentifier: "profileDetails") as! ProfileViewController
                self.navigationController?.pushViewController(mainVC, animated: true)
                
                break;
            }
            
            
        }
        else if indexPath.section == 1{
            
            if indexPath.row == 0{
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"{
                    
                    UserDefaults.standard.set("Disable", forKey: "notificationEnable")
                    UserDefaults.standard.set("notificDisable", forKey: "notificationEnableIm")
                    
                    
                    self.tableView.reloadData()
                    
                    
                }else{
                    UserDefaults.standard.set("Enable", forKey: "notificationEnable")
                    UserDefaults.standard.set("notificEnable", forKey: "notificationEnableIm")
                    self.tableView.reloadData()
                }
                
            }else if indexPath.row == 1{
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
                        self.addChildViewController(popOverVC)
                        popOverVC.view.frame = self.view.frame
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                        
                    }
                    
                }else{
                    //
                }
            }
        }
            
        else if indexPath.section == 2{
            
            if indexPath.row == 0{
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"{
                    
                    if UserDefaults.standard.bool(forKey: "vibrateSwitch") == true{
                        UserDefaults.standard.set(false, forKey: "vibrateSwitch")
                    }else{
                        UserDefaults.standard.set(true, forKey: "vibrateSwitch")
                    }
                }else{
                    //
                }
            }else{
                
                if indexPath.row == 2{
                    
                    switch UserDefaults.standard.bool(forKey: "isContractorLogin") {
                    case true:
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let FBVC = storyboard.instantiateViewController(withIdentifier: "Feedback") as! FeedbackViewController
                        self.navigationController?.pushViewController(FBVC, animated: true)
                        break;
                    default:
                        if self.profileDB.count>0{
                             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let FBVC = storyboard.instantiateViewController(withIdentifier: "Feedback") as! FeedbackViewController
                            self.navigationController?.pushViewController(FBVC, animated: true)
                            
                        }
                        break;
                    }
                    
                    
                }else{
                    
                    if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
                    {
                        self.notificationBanner()
                    }else{
                        //
                    }
                }
            }
            
        }
        else{
            //
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == 1){
     
        
            let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HzlMainCellHeaderTableViewCell
            
                header.lblHeader.text = "Notification Setting"
            
            header.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return header
        }else{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        
            view.backgroundColor = UIColor.white
            
            
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         if section == 1{
            return 35
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0{
            return 70
        }else if indexPath.section == 1{
            return 55
        }else if indexPath.section == 2{
            return 55
        }else{
            return 45
        }
        
    }
    
    
    
    
    @IBAction func switchAtion(_ sender: Any) {
        if let cell = (sender as AnyObject).superview??.superview as? SettingsTableViewCell {
            
            if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
            {
                cell.settingsSwitch.isEnabled = true
                if UserDefaults.standard.bool(forKey: "soundSwitch") == true{
                    UserDefaults.standard.set(false, forKey: "soundSwitch")
                }else{
                    UserDefaults.standard.set(true, forKey: "soundSwitch")
                }
                
                self.tableView.reloadData()
                
            }else{
                cell.settingsSwitch.isEnabled = false
            }
            
            
        }
        
    }
    
    @IBAction func vibrateSwitch(_ sender: Any) {
        
        
        if let cell = (sender as AnyObject).superview??.superview as? VibrateTableViewCell {
            
            if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
            {
                cell.settingsSwitch.isEnabled = true
                
                if UserDefaults.standard.bool(forKey: "vibrateSwitch") == true{
                    UserDefaults.standard.set(false, forKey: "vibrateSwitch")
                }else{
                    UserDefaults.standard.set(true, forKey: "vibrateSwitch")
                }
                
                self.tableView.reloadData()
            }else{
                cell.settingsSwitch.isEnabled = false
                self.tableView.reloadData()
            }
            
            
        }
        
    }
    
    @IBAction func btnCompanyLogoClecked(_ sender: UIButton) {
        
        
        linkHzl(urlData: "http://sarvang.com/", title: "Sarvang")
        
        
    }
    @IBAction func tapViewCompanyTapClicked(_ sender: UITapGestureRecognizer) {
        
        
        linkHzl(urlData: "http://sarvang.com/", title: "Sarvang")
        
        
    }
    func linkHzl(urlData : String , title : String){
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
        
        let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
        guard   let url = URL(string: urlData) else {
            return
        }
        progressWebViewController.disableZoom = true
        progressWebViewController.url = url
        progressWebViewController.bypassedSSLHosts = [url.host!]
        progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
        progressWebViewController.websiteTitleInNavigationBar = false
        progressWebViewController.navigationItem.title = title
        progressWebViewController.leftNavigaionBarItemTypes = [.reload]
        progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
        self.present(proNav, animated: true, completion: nil)
    }
    @objc func loadData(){
        self.tableView.reloadData()
    }
    
    func getProfileDetailsData() {
        
        self.profileDB = [Profile]()
        
        do {
            
            self.profileDB = try context.fetch(Profile.fetchRequest())
            
            self.tableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    
    @IBAction func logOutAction(_ sender: Any) {
        callExist()
    }
    
    func callExist(){
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure to Logout?", preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Logout", style: .default, handler: { (alert) in
                
                
                UserDefaults.standard.set(false, forKey: "isLogOut")

                UserDefaults.standard.removeObject(forKey: "Personnel_Number")
                UserDefaults.standard.set(false, forKey: "isLogin")
                UserDefaults.standard.set(false, forKey: "isContractorLogin")
                UserDefaults.standard.set(false, forKey: "isWorkmanLogin")
                
                UserDefaults.standard.set("Enable", forKey: "notificationEnable")
                UserDefaults.standard.set("Device Default", forKey: "soundEnable")
                
                UserDefaults.standard.set("notificEnable", forKey: "notificationEnableIm")
                UserDefaults.standard.set("soundEnable", forKey: "soundEnableIm")
                 UserDefaults.standard.set("", forKey: "UserCode")
                
                UserDefaults.standard.set(true, forKey: "soundSwitch")
                UserDefaults.standard.set(true, forKey: "vibrateSwitch")
                
                
                let systemSoundID: SystemSoundID = 1315
                UserDefaults.standard.set(systemSoundID, forKey: "soundFileEnable")
                self.GCMDelete()
                self.deleteAllLocalData()
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let mainVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.present(mainVC, animated: true, completion: nil)
                
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                
            })
            
            actionSheetController.addAction(camera)
            
            actionSheetController.addAction(cancel)
            self.present(actionSheetController, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    var GCMToken = String()
    func GCMDelete(){
        
        if let object = UserDefaults.standard.value(forKey: "Token") {
            
            GCMToken = object as! String
        }
       
        let para = [
            
            "GcmToken":GCMToken,
            "Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
             "Platform":"iOS"]
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.Logout, parameters: para, successHandler: { (dict) in
            
            print("GCMRegister",dict)
            
        }) { (err) in
            print(err)
        }
        
    }
    var localDataArray = [String]()
    func deleteAllLocalData()
    {
        
        
      
        localDataArray = [String]()
      localDataArray = ["BannerImage","Profile"]
        
        for i in 0..<localDataArray.count{
            
            let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: localDataArray[i])
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
            
            do {
                try context.execute(batchDeleteRequest)
                
            } catch {
                // Error Handling
            }
            
        }
    }
    
}
