//
//  PopUpViewController.swift
//  PopUp
//
//  Created by Andrew Seeley on 6/06/2016.
//  Copyright © 2016 Seemu. All rights reserved.
//

import UIKit
import AVFoundation

class PopUpViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var soundtableView: UITableView!
    
    @IBOutlet weak var soundContentView: UIView!
    
    var player: AVAudioPlayer?
    
    var ringTonesFileArray = [String]()
    var ringTonesNameArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.soundtableView.delegate = self
        self.soundtableView.dataSource = self
    
        self.ringTonesFileArray = ["Boing","Ding","Drop","Ta-Da","Plink","Wow","Here yuo go","Hi","Yoink","Knock brush","Woah!","None","Device Default"]
        
        self.ringTonesNameArray = ["Boing","Ding","Drop","Ta-Da","Plink","Wow","Here yuo go","Hi","Yoink","Knock brush","Woah!","None","Device Default"]
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.showAnimate()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return self.ringTonesFileArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sound")!
            
        cell.textLabel?.text = self.ringTonesNameArray[indexPath.row]
        if self.ringTonesNameArray[indexPath.row] ==  UserDefaults.standard.value(forKey: "soundEnable") as! String{
           cell.accessoryType = .checkmark
        }else{
           cell.accessoryType = .none
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if self.ringTonesNameArray[indexPath.row] == "Device Default"{
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            UserDefaults.standard.set(self.ringTonesNameArray[indexPath.row], forKey: "soundEnable")
            UserDefaults.standard.set(systemSoundID, forKey: "soundFileEnable")
            
        }else{
            
           playSound(soundName: self.ringTonesFileArray[indexPath.row])
           UserDefaults.standard.set(self.ringTonesNameArray[indexPath.row], forKey: "soundEnable")
           UserDefaults.standard.set(self.ringTonesFileArray[indexPath.row], forKey: "soundFileEnable")
           
        }
        soundtableView.reloadData()
       
    }
    

    func playSound (soundName:String) {
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    @IBAction func closePopUp(_ sender: AnyObject) {
        
        self.removeAnimate()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTableView"), object: nil)
    }
    
    @IBAction func okPopUp(_ sender: Any) {
        
       self.removeAnimate()
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTableView"), object: nil)
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }


}
