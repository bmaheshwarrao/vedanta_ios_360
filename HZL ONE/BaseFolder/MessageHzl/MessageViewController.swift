//
//  MessageViewController.swift
// RAS
//
//  Created by SARVANG INFOTCH on 18/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class MessageViewController: CommonVSClass,
UITableViewDelegate, UITableViewDataSource,UINavigationBarDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var MessagesDB:[MessagesDataModel] = []
    var messagesAPI = messagesNotiDataAPI()
    
    var MessagesLoadMoreDB : [MessagesDataModel] = []
    
    

    
    var messagesLoadMoreAPI = messagesNotiDataAPILoadMore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getMessageData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl.addTarget(self, action: #selector(getMessageData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        

        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMessageData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
    
        
 
            
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
//        print(MessagesDB[(indexPath?.section)!].Action_Type)
//        if(MessagesDB[(indexPath?.section)!].Action_Type == "HazardDetail") {
//
//            UserDefaults.standard.set(4, forKey: "hazard")
//
//            let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "HazardDetailsViewController") as! HazardDetailsViewController
//            ZIVC.hazardID = String(describing: MessagesDB[(indexPath?.section)!].Action_ID)
//            ZIVC.empId = ""
//            self.navigationController?.pushViewController(ZIVC, animated: true)
//
//        } else if(MessagesDB[(indexPath?.section)!].Action_Type == "NewHazardInArea") {
//             UserDefaults.standard.set(2, forKey: "hazard")
//            UserDefaults.standard.set("Pending", forKey: "Status")
//            let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
//
//
//            self.navigationController?.pushViewController(ZIVC, animated: true)
//        }  else if(MessagesDB[(indexPath?.section)!].Action_Type == "PendingToClose") {
//
//            let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "CloseViewController") as! CloseViewController
//            ZIVC.hazardID = String(describing: MessagesDB[(indexPath?.section)!].Action_ID)
//            ZIVC.imgdata = ""
//            ZIVC.notify = 1
//            ZIVC.status = String(describing: MessagesDB[(indexPath?.section)!].Action_Type)
//            self.navigationController?.pushViewController(ZIVC, animated: true)
//        }
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Notification"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
   
     
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.MessagesDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "message", for: indexPath) as! MessagesTableViewCell
        
   
        cell.messageslabel.text = self.MessagesDB[indexPath.section].Action
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.viewMessage.addGestureRecognizer(TapGesture)
        cell.viewMessage.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:S"
        let date : Date = dateFormatter.date(from: self.MessagesDB[indexPath.section].Date_Time)!
        dateFormatter.string(from: date)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        let finalDate = dateFormatter2.string(from: date)
        
        let dateFormatter3 = DateFormatter()
        dateFormatter3.timeZone = NSTimeZone.system
        dateFormatter3.dateFormat = "hh:mm a"
        let finalTime = dateFormatter3.string(from: date)
        
        cell.dateLabel.text = "\(finalDate)\(", ")\(finalTime)"
        self.data = String(self.MessagesDB[indexPath.section].ID)
        self.lastObject = String(self.MessagesDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.MessagesDB.count - 1)
        {
            
            self.getMessageDataLoadMore( ID: String(Int(self.MessagesDB[indexPath.section].ID)))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getMessageData(){
        var param = [String:String]()
//     let myTokenId =   UserDefaults.standard.string(forKey: "myTokenId")!
//    param =  ["UserID":UserDefaults.standard.string(forKey: "EmployeeID")! , "AppCode":URLConstants.appName , "Token_ID" : myTokenId]
          param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        messagesAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.MessagesDB = dict as! [MessagesDataModel]
            
            self.tableView.reloadData()
            
        }
    }
  
    @objc func getMessageDataLoadMore(ID : String){
     
        var param = [String:String]()
      
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,"ID":ID]
        messagesLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
        
             self.MessagesLoadMoreDB =  [MessagesDataModel]()
            self.MessagesLoadMoreDB = dict as! [MessagesDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.MessagesDB.append(contentsOf: self.MessagesLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    
    
}
