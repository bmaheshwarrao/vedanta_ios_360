//
//  MessagesTableViewCell.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 19/12/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit

class MessagesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var messageslabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
