//
//  PPCRMyComplaintViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import ViewPager_Swift
class PPCRMyComplaintViewController: CommonVSClass {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      

        let myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        myOptions.isTabHighlightAvailable = true
        // myOptions.is
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.yellow
        myOptions.tabViewBackgroundHighlightColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewTextDefaultColor = UIColor.yellow
        myOptions.tabViewTextHighlightColor = UIColor.yellow
        let viewPager = ViewPagerController()
        
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        viewPager.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(viewPager.view)
        
        viewPager.view.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0.0).isActive = true
        viewPager.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0).isActive = true
        viewPager.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0).isActive = true
        viewPager.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0).isActive = true
        viewPager.didMove(toParentViewController: self)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "My Request's"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav?.isTranslucent = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension PPCRMyComplaintViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 4
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Township",bundle : nil)
        if(position == 0) {
            
            let ppcrVC = storyBoard.instantiateViewController(withIdentifier: "PPCRTownshipViewController") as! PPCRTownshipViewController
            ppcrVC.ComplaintStatus = "Unattended"
            return  ppcrVC as CommonVSClass
        }else  if(position == 1) {
            
            let ppcrVC = storyBoard.instantiateViewController(withIdentifier: "PPCRTownshipViewController") as! PPCRTownshipViewController
            ppcrVC.ComplaintStatus = "Process"
            return  ppcrVC as CommonVSClass
        }else  if(position == 2) {
            
            let ppcrVC = storyBoard.instantiateViewController(withIdentifier: "PPCRTownshipViewController") as! PPCRTownshipViewController
            ppcrVC.ComplaintStatus = "Attended"
            return  ppcrVC as CommonVSClass
        }else {
            let ppcrVC = storyBoard.instantiateViewController(withIdentifier: "PPCRTownshipViewController") as! PPCRTownshipViewController
            ppcrVC.ComplaintStatus = "Reject"
            return  ppcrVC as CommonVSClass
        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "Pending" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "Process" , image: #imageLiteral(resourceName: "BackBlack")), ViewPagerTab(title: "Completed" , image: #imageLiteral(resourceName: "BackBlack")), ViewPagerTab(title: "Rejected" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension PPCRMyComplaintViewController : ViewPagerControllerDelegate {
    
}



