//
//  PPCRTownshipViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class PPCRTownshipViewController: CommonVSClass {
    
    //    var ID : [String] = ["16" ,"17"]
    //var Date : [String] = ["27-Sep-2018","27-Sep-2018"]
    
    var imagedata : [String] = []
    var hazarddisplay = Int()
    
    
    
    var DataAPI = PPCRTownshipDataAPI()
    var TownshipPPCRDB : [PPCRTownshipDataModel] = []
  
    
    
    @IBOutlet weak var tableViewTownship: UITableView!
    
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewTownship.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        tableViewTownship.delegate = self;
        tableViewTownship.dataSource = self;
        tableViewTownship.estimatedRowHeight = 290
        tableViewTownship.rowHeight = UITableViewAutomaticDimension
        
        
        refresh.addTarget(self, action: #selector(ReportdatabyStatus), for: .valueChanged)
        
        self.tableViewTownship.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        tableViewTownship.tableFooterView = UIView()
        
        

        
        // Do any additional setup after loading the view.
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.ReportdatabyStatus()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
            }
            
        }
    }
    func setUp() {
   
        
 
        ReportdatabyStatus()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUp()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func ImageViewTapped(_ sender : UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableViewTownship)
        let indexPath = self.tableViewTownship.indexPathForRow(at: buttonPosition)
        
        if TownshipPPCRDB[(indexPath?.section)!].ImagePath != "" {
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoomViewController
            ZIVC.zoomImageUrl = TownshipPPCRDB[(indexPath?.section)!].ImagePath
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }
    }
    
    @objc func tapCell(_ sender : UITapGestureRecognizer) {
        
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableViewTownship)
        let indexPath = self.tableViewTownship.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "Township", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ComplaintDetailsViewController") as! ComplaintDetailsViewController
     
        ZIVC.ComplaintDB = TownshipPPCRDB[(indexPath?.section)!]
    
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    var countID : Int = 0
    var ComplaintStatus = String()
    @objc func ReportdatabyStatus() {
        
        
        
        
      
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        
 
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
        
        
        dictWithoutNilValues = ["EmployeeID":empId,
                                
                                "ComplaintStatus" : ComplaintStatus]
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        
        print(parameter)
      
        
        
        self.DataAPI.serviceCalling(obj: self, param: parameter ) { (dict) in
            
            self.TownshipPPCRDB = [PPCRTownshipDataModel]()
            self.TownshipPPCRDB = dict as! [PPCRTownshipDataModel]
            self.tableViewTownship.reloadData()
        }
        
    }
   
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var imageData : Data? = nil
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension PPCRTownshipViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return TownshipPPCRDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension PPCRTownshipViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PPCRTableViewCell
        
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCell(_:)))
        cell.textViewRequest.addGestureRecognizer(TapGesture)
        cell.textViewRequest.isUserInteractionEnabled = true
        cell.lblId.text = "ID #"+String(describing: self.TownshipPPCRDB[indexPath.section].ID)
        
        
        
    
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.TownshipPPCRDB[indexPath.section].ComplaintDate)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.lblDate.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.lblDate.text = date_TimeStr
            break;
        }
        
      
        
       
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineSpacing = 0
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var startString : String = "You "
        if(empId != TownshipPPCRDB[indexPath.section].EmployeeID){
            startString =  TownshipPPCRDB[indexPath.section].Employee_Name + "-" +   String(TownshipPPCRDB[indexPath.section].EmployeeID) + " "
        }
        
        let  mBuilder = startString  + "register complaint for " + self.TownshipPPCRDB[indexPath.section].ComplaintSubject_Name + " under category " +  self.TownshipPPCRDB[indexPath.section].Category_Name +
            " with " + self.TownshipPPCRDB[indexPath.section].Remarks + " remark "
        
        let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        
        
        //Submitted
        let SubmittedAttributedString = NSAttributedString(string:"register complaint for ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        
        let range: NSRange = (agreeAttributedString.string as NSString).range(of: "register complaint for ")
        if range.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
        }
        
        //for
        let forAttributedString = NSAttributedString(string:"under category", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        
        let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "under category")
        if forRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
        }
        let subAreaAttributedString = NSAttributedString(string:"with", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "with")
        if subAreaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
        }
        
        
        let onAttributedString = NSAttributedString(string:"remark", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
        let onRange: NSRange = (agreeAttributedString.string as NSString).range(of: "remark")
        if onRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: onRange, with: onAttributedString)
        }
     
        cell.textViewRequest.attributedText = agreeAttributedString
        
       
     
        
        
        let imageViewDataCell = UIImageView()
        imageViewDataCell.image = UIImage(named : "placed")
        let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
        if(cell.imageFile != nil) {
            cell.imageFile.addGestureRecognizer(postImageTapGesture)
            cell.imageFile.isUserInteractionEnabled = true
            postImageTapGesture.numberOfTapsRequired = 1
        }
        
        
        
        
        
        
     
        print(self.TownshipPPCRDB[indexPath.section].ImagePath)
        // cell.imageFile.isHidden = true
        let imageView = UIImageView()
        let urlString = self.TownshipPPCRDB[indexPath.section].ImagePath
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.TownshipPPCRDB[indexPath.section].ImagePath) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    //  cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 100, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageFile.image = imageCell
            //cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        

        
        
   
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        
        return cell
        
        
        
        
        
        
    }
}



