//
//  LocationQuarterSubmitViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class LocationQuarterSubmitViewController: CommonVSClass {
    
    @IBOutlet weak var tableView: UITableView!
    var LocationId = String()
    var LocationName = String()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
   getQuarterList()
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        // Do any additional setup after loading the view.
    }
    var QuarterDB:[QuarterTownshipDataModel] = []
    var QuarterAPI = QuarterTownshipDataAPI()
    var QuarterDataListArray : [String] = []
    @objc func getQuarterList(){
        
        var paramm  : [String:String] = [:]
        paramm =  ["EmployeeID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        QuarterDataListArray = []
      
        self.QuarterAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.QuarterDB = [QuarterTownshipDataModel]()
            self.QuarterDB = dict as! [QuarterTownshipDataModel]
            if(self.QuarterDB.count > 0){
                for i in 0...self.QuarterDB.count - 1 {
                    self.QuarterDataListArray.append(self.QuarterDB[i].QuarterType)
                  
                }
            }
            
            
            
            
            
        }
    }
    var empLocDB:[EmpLocTownshipDataModel] = []
    var empLocAPI = EmpLocTownshipDataAPI()
    @objc func getLocationTownshipList(){
        
        var paramm  : [String:String] = [:]
        paramm =  ["EmployeeID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        empLocDB = []
        
        self.empLocAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.empLocDB = [EmpLocTownshipDataModel]()
            self.empLocDB = dict as! [EmpLocTownshipDataModel]
            if(self.empLocDB.count > 0){
                self.cellData.textViewLocation.text = self.empLocDB[0].Location_Name
                self.LocationId = self.empLocDB[0].Location
            }
            
            
            
            
            
        }
    }
    @IBAction func btnQuarterTypeClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.QuarterDataListArray
            
            popup.sourceView = self.cellData.textViewQuarter
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.textViewQuarter.text = self.QuarterDataListArray[row]
                self.cellData.textViewQuarter.textColor = UIColor.black
              
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
    let reachablty = Reachability()!
    @IBAction func btnQuarterRegisterClicked(_ sender: UIButton) {
        
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }else if (cellData.textViewQuarter.text == "" || cellData.textViewQuarter.text == "Select") {
            
            self.view.makeToast("Please Select Quarter")
        }
            
        else if (cellData.textViewReason.text == "") {
            
            self.view.makeToast("Please Write Reason")
        }
        else{
            
            
            
            self.startLoadingPK(view: self.view)
            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
          
            
            
            let parameter = [
                "EmployeeID":empId,
                
                "Created_BY":empId ,
                "Location_ID":LocationId ,
                 "Employee_Remark":cellData.textViewReason.text! ,
                "QuarterType": cellData.textViewQuarter.text!
                
                ] as [String:String]
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Insert_QuarterRequest, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = msg
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
        }
        
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Quarter Request"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Fusuma
    
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
    var indexing : Int = 1
    var cellData : LocationQuarterSubmitTableViewCell!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension LocationQuarterSubmitViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
}
extension LocationQuarterSubmitViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LocationQuarterSubmitTableViewCell
        cell.textViewLocation.text = LocationName
        cellData = cell
      // getLocationTownshipList()
        if(cell.textViewQuarter.text == ""){
            cell.textViewQuarter.text = "Select"
        }
        cell.textViewLocation.text = LocationName
       
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
