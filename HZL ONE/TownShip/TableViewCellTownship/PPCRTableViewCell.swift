//
//  PPCRTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PPCRTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewRequest: UITextView!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imageFile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
