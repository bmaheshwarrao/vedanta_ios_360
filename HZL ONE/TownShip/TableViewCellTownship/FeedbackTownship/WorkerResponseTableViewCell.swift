//
//  WorkerResponseTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class WorkerResponseTableViewCell: UITableViewCell {

    @IBOutlet weak var btnRate1: UIButton!
    @IBOutlet weak var btnRate2: UIButton!
    @IBOutlet weak var btnRate3: UIButton!
    @IBOutlet weak var btnRate4: UIButton!
    @IBOutlet weak var btnRate5: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnRate1Clicked(_ sender: UIButton) {
        btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate2.setImage(UIImage(named: "starDataGray"), for: .normal)
        btnRate3.setImage(UIImage(named: "starDataGray"), for: .normal)
        btnRate4.setImage(UIImage(named: "starDataGray"), for: .normal)
        btnRate5.setImage(UIImage(named: "starDataGray"), for: .normal)
        configureCell(cnt: 1)
    }
    @IBAction func btnRate2Clicked(_ sender: UIButton) {
        btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate3.setImage(UIImage(named: "starDataGray"), for: .normal)
        btnRate4.setImage(UIImage(named: "starDataGray"), for: .normal)
        btnRate5.setImage(UIImage(named: "starDataGray"), for: .normal)
        configureCell(cnt: 2)
    }
    @IBAction func btnRate3Clicked(_ sender: UIButton) {
        btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate3.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate4.setImage(UIImage(named: "starDataGray"), for: .normal)
        btnRate5.setImage(UIImage(named: "starDataGray"), for: .normal)
        configureCell(cnt: 3)
    }
    @IBAction func btnRate4Clicked(_ sender: UIButton) {
        btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate3.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate4.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate5.setImage(UIImage(named: "starDataGray"), for: .normal)
        configureCell(cnt: 4)
    }
    @IBAction func btnRate5Clicked(_ sender: UIButton) {
        btnRate1.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate2.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate3.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate4.setImage(UIImage(named: "starDataPink"), for: .normal)
        btnRate5.setImage(UIImage(named: "starDataPink"), for: .normal)
        configureCell(cnt: 5)
    }
    func configureCell(cnt : Int){
        FeedbackTownship.WorkerResponse = String(cnt)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
