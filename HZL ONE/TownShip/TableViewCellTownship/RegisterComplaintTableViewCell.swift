//
//  RegisterComplaintTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RegisterComplaintTableViewCell: UITableViewCell {
  
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var heightImageselected: NSLayoutConstraint!
    @IBOutlet weak var heightViewofClose: NSLayoutConstraint!
 
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblOptional: UILabel!

 
    @IBOutlet weak var textViewRemark: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textViewRemark.layer.borderWidth = 1.0
        textViewRemark.layer.borderColor = UIColor.black.cgColor
        textViewRemark.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
