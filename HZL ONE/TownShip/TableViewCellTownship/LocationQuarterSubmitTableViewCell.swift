//
//  LocationQuarterSubmitTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class LocationQuarterSubmitTableViewCell: UITableViewCell {

    @IBOutlet weak var viewQuarter: UIView!
    @IBOutlet weak var textViewQuarter: UITextView!
    @IBOutlet weak var textViewLocation: UITextView!
    @IBOutlet weak var textViewReason: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewQuarter.layer.borderWidth = 1.0
        viewQuarter.layer.borderColor = UIColor.black.cgColor
        viewQuarter.layer.cornerRadius = 10.0
        
        
        textViewReason.layer.borderWidth = 1.0
        textViewReason.layer.borderColor = UIColor.black.cgColor
        textViewReason.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
