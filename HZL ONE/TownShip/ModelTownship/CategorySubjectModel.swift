//
//  CategorySubjectModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class CategoryTownshipDataModel: NSObject {
    
    
    var categoryId = String()
    var category = String()
    
    
    
    

    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["categoryId"] is NSNull || str["categoryId"] == nil{
            self.categoryId = ""
        }else{
            
            let tit = str["categoryId"]
            
            self.categoryId = (tit?.description)!
            
            
        }
        if str["category"] is NSNull || str["category"] == nil{
            self.category =  ""
        }else{
            
            let desc = str["category"]
            
            self.category = (desc?.description)!

        }
        
    }
    
}





class CategoryTownshipDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:CategoryTownshipViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
         
               WebServices.sharedInstances.sendGetRequest(Url: URLConstants.Complaint_Category, successHandler: { (dict) in
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [CategoryTownshipDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = CategoryTownshipDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}


class SubjectTownshipDataModel: NSObject {
    
    
    var ID = String()
    var Subject = String()
    
    
    
    

    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Subject"] is NSNull || str["Subject"] == nil{
            self.Subject = ""
        }else{
            
            let tit = str["Subject"]
            
            self.Subject = (tit?.description)!
            
            
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID =  ""
        }else{
            
            let desc = str["ID"]
            
            self.ID = (desc?.description)!
            
        }
        
    }
    
}





class SubjectTownshipDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:SubjectTownshipViewController, Catid : String , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            let urlString = URLConstants.Complaint_Subject + "?CategoryID=" + Catid
            
            WebServices.sharedInstances.sendGetRequest(Url: urlString, successHandler: { (dict) in
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [SubjectTownshipDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = SubjectTownshipDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuarterTownshipDataModel: NSObject {
    
    
 
    var QuarterType = String()
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["QuarterType"] is NSNull || str["QuarterType"] == nil{
            self.QuarterType = ""
        }else{
            
            let tit = str["QuarterType"]
            
            self.QuarterType = (tit?.description)!
            
            
        }
       
        
    }
    
}





class QuarterTownshipDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:LocationQuarterSubmitViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
        
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.QuarterRequest_List, parameters: param , successHandler: { (dict) in
           
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                     
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [QuarterTownshipDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = QuarterTownshipDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                       
                        
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            }
            
            
        }
        else{
          
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}
