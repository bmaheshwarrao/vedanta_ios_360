//
//  PieChartZoneTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 27/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Charts
class PieChartZoneTableViewCell: UITableViewCell,ChartViewDelegate {
  @IBOutlet weak var pieChartView: PieChartView!
    
    @IBOutlet weak var lblZoneName: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var dataPointData : [String] = []
     var valuePointData : [Int] = []
    func cellConfigure(LineDB : [Zone_DB_Data]){
        valuePointData = []
        dataPointData = []
        if(LineDB.count > 0){
            for i in 0...LineDB.count - 1 {
                dataPointData.append(LineDB[i].zone_Name!)
                valuePointData.append(Int(LineDB[i].count_Data!)!)
            }
        }
        
        pieChartView.drawHoleEnabled = false
        
        pieChartView.drawEntryLabelsEnabled = false
        
        pieChartView.chartDescription?.text = ""
        pieChartView.delegate = self
        pieChartView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        // entry label styling
        pieChartView.entryLabelColor = .white
        pieChartView.entryLabelFont = .systemFont(ofSize: 15, weight: .medium)
     
        
       // pieChartView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        self.setChart1(dataPoints: self.dataPointData, values: self.valuePointData , colorCode :self.dataPointData)
    }
    func setChart1(dataPoints: [String], values: [Int] , colorCode : [String] ) {
        
        
        
        var dataEntries1: [PieChartDataEntry] = []
        
        
        
        for (index1,_) in dataPoints.enumerated()
        {
            
            
            let pieDataEntry1 = PieChartDataEntry(value: Double(values[index1]), label: dataPoints[index1],data:dataPoints[index1] as AnyObject?)
            
            dataEntries1.append(pieDataEntry1)
            
        }
        
        
        print("\(dataEntries1)")
        
        var colors2  = [UIColor]()
        
        for index2 in colorCode
        {
         
            colors2.append(.random())
            
        }
        
        print("colors\(colors2)")
        
        let chartDataSet = PieChartDataSet(values: dataEntries1, label: "")
        let chartData = PieChartData()
        
        
        
        chartData.addDataSet(chartDataSet)
        
        chartDataSet.colors = colors2
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        chartData.setValueFormatter(formatter)
        pieChartView.data = chartData
        
     
        chartDataSet.selectionShift = 5
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
