//
//  ZoneDashboardPieCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 15/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ZoneDashboardPieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblCountZone: UILabel!
   
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblZone: UILabel!
}
