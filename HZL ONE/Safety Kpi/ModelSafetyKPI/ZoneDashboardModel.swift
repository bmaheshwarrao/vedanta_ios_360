//
//  ZoneDashboardModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import CoreData



class ZoneDashboardModel: NSObject {
    
    
    var SL = String()
    var Zone_Name = String()
    var Count_Data = String()
    var Incident_Name = String()
    var Zone_id = String()
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject],Zoneid : String)
    {
        if str["SL"] is NSNull || str["SL"] == nil{
            self.SL = ""
        }else{
            
            let ros = str["SL"]
            
            self.SL = (ros?.description)!
            
            
        }
        if str["Zone_Name"] is NSNull || str["Zone_Name"] == nil{
            self.Zone_Name =  ""
        }else{
            
            let fross = str["Zone_Name"]
            
            self.Zone_Name = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
      
        if str["Count_Data"] is NSNull || str["Count_Data"] == nil{
            self.Count_Data = "0"
        }else{
            let emp1 = str["Count_Data"]
            
            self.Count_Data = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Incident_Name"] is NSNull || str["Incident_Name"] == nil{
            self.Incident_Name = ""
        }else{
            
            let empname = str["Incident_Name"]
            
            self.Incident_Name = (empname?.description)!
            
        }
        Zone_id = Zoneid
        saveZoneData(SL: Int64(SL)!, Zone_Name: Zone_Name, Count_Data: Count_Data, Incident_Name: Incident_Name,slStr : SL, Zone_id: Int64(Zone_id)!)
        
    }
    func saveZoneData (SL:Int64, Zone_Name:String,Count_Data:String,Incident_Name:String ,slStr : String,Zone_id:Int64){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = Zone_DB_Data(context: context)
        
        tasks.sl = SL
        tasks.zone_Name = Zone_Name
        tasks.count_Data = Count_Data
        tasks.incident_Name = Incident_Name
        tasks.unique_Sl = slStr
        tasks.zone_id = Zone_id
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
    
  
    
    
    
}



class ZoneSafetyKPIDataAPI
{
    func deleteZoneData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Zone_DB_Data")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    var reachablty = Reachability()!
    
    func serviceCalling(obj:LineChartZoneDashboardViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Zone_Dashboard_KPI, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                     
                        
                        self.deleteZoneData()
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ZoneDashboardModel] = []
                            var dataArrayId : [String] = []
                            for ii in 0..<data.count{
                               let obj = ZoneObject()
                                obj.id = ""
                                dataArrayId.append("")
                                
                            }
              
                            for i in 0..<data.count
                            {
                                var Zone_Name2 = String()
                                if let cell2 = data[i] as? [String:AnyObject]
                                {
                                if cell2["Zone_Name"] is NSNull || cell2["Zone_Name"] == nil{
                                    Zone_Name2 =  ""
                                }else{
                                    
                                    let fross = cell2["Zone_Name"]
                                    
                                    Zone_Name2 = (fross?.description)!
                                }
                                }
                                for ii in 0..<data.count{
                                    
                                    if let cell1 = data[ii] as? [String:AnyObject]
                                    {
                                        var Zone_Name1 = String()
                                        
                                        if cell1["Zone_Name"] is NSNull || cell1["Zone_Name"] == nil{
                                            Zone_Name1 =  ""
                                        }else{
                                            
                                            let fross = cell1["Zone_Name"]
                                            
                                            Zone_Name1 = (fross?.description)!
                                            
                                                
                                                if(Zone_Name1 == Zone_Name2){
                                                    if(dataArrayId[ii] == ""){
                                                        dataArrayId[ii] = String(i + 1)
                                                    }
                                                }
                                            
                                            
                                            
                                        }
                                    }
                                }
                                
                            }
           
                            for iz in 0..<data.count
                            {
                               
                                if let cell = data[iz] as? [String:AnyObject]
                                {
                                    let object = ZoneDashboardModel()
                                    object.setDataInModel(str: cell, Zoneid: dataArrayId[iz])
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
    func serviceCallingPieChart(obj:PieChartZoneViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Zone_Dashboard_KPI, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)

                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        
                        self.deleteZoneData()
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ZoneDashboardModel] = []
                            var dataArrayId : [String] = []
                            for ii in 0..<data.count{
                                let obj = ZoneObject()
                                obj.id = ""
                                dataArrayId.append("")
                                
                            }
                            
                            for i in 0..<data.count
                            {
                                var Zone_Name2 = String()
                                if let cell2 = data[i] as? [String:AnyObject]
                                {
                                    if cell2["Zone_Name"] is NSNull || cell2["Zone_Name"] == nil{
                                        Zone_Name2 =  ""
                                    }else{
                                        
                                        let fross = cell2["Zone_Name"]
                                        
                                        Zone_Name2 = (fross?.description)!
                                    }
                                }
                                for ii in 0..<data.count{
                                    
                                    if let cell1 = data[ii] as? [String:AnyObject]
                                    {
                                        var Zone_Name1 = String()
                                        
                                        if cell1["Zone_Name"] is NSNull || cell1["Zone_Name"] == nil{
                                            Zone_Name1 =  ""
                                        }else{
                                            
                                            let fross = cell1["Zone_Name"]
                                            
                                            Zone_Name1 = (fross?.description)!
                                            
                                            
                                            if(Zone_Name1 == Zone_Name2){
                                                if(dataArrayId[ii] == ""){
                                                    dataArrayId[ii] = String(i + 1)
                                                }
                                            }
                                            
                                            
                                            
                                        }
                                    }
                                }
                                
                            }
                            
                            for iz in 0..<data.count
                            {
                                
                                if let cell = data[iz] as? [String:AnyObject]
                                {
                                    let object = ZoneDashboardModel()
                                    object.setDataInModel(str: cell, Zoneid: dataArrayId[iz])
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ZoneObject: NSObject {
    var id : String = ""
}
