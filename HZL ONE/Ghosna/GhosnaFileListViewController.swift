//
//  GhosnaFileListViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 10/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class GhosnaFileListViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    var StrNav = String()
   
    var ghosnaListDB:[GhiosnaFileListModel] = []
    var ghosnaListAPI = GhosnaFileListModelDataAPI()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getghosnaListData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getghosnaListData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = StrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getghosnaListData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if(self.ghosnaListDB[(indexPath?.section)!].Ghoshna == "PDF"){
            let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
            let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HzlPdfReaderViewController") as! HzlPdfReaderViewController
            HZlPolicyTempData.ELCat_ID = self.ghosnaListDB[(indexPath?.section)!].ID
            
            HZlPolicyTempData.file_Url = self.ghosnaListDB[(indexPath?.section)!].File_Url
            HZlPolicyTempData.file_Name = "PDF"
            self.navigationController?.pushViewController(ELPVC, animated: true)
        }else if(self.ghosnaListDB[(indexPath?.section)!].Ghoshna == "Story"){
            let storyBoard = UIStoryboard(name: "Ghosna", bundle: nil)
            let ELPVC = storyBoard.instantiateViewController(withIdentifier: "StoryDetailsGhosnaViewController") as! StoryDetailsGhosnaViewController
        
            ELPVC.File_title = self.ghosnaListDB[(indexPath?.section)!].Title
            
            ELPVC.File_Url = self.ghosnaListDB[(indexPath?.section)!].File_Url
            ELPVC.File_Description = self.ghosnaListDB[(indexPath?.section)!].Story
            self.navigationController?.pushViewController(ELPVC, animated: true)
        }

        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ghosnaListDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GhosnaFileListTableViewCell
       
        if(self.ghosnaListDB[indexPath.section].Ghoshna == "PDF") {
            cell.imageFile.image = UIImage(named: "pdfFile")
        }else if(self.ghosnaListDB[indexPath.section].Ghoshna == "Story") {
//            if let url = NSURL(string: ghosnaListDB[indexPath.row].File_Url) {
//                cell.imageFile.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
//
//
//
//            }
            
            let urlString =  ghosnaListDB[indexPath.section].File_Url
           
            
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let url = NSURL(string: urlShow!) {
                
                SDWebImageManager.shared().downloadImage(with: url as URL!, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil) {
                        
                       
                        cell.imageFile.image = image
                        //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                        
                    }else{
                      
                        cell.imageFile.image = UIImage(named: "pdfFile")
                        // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    }
                })
            }else{
               
                cell.imageFile.image = UIImage(named: "pdfFile")
                //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
            }
        }else{
            cell.imageFile.isHidden = true
        }
  
        
        
        cell.textViewTitle.text = self.ghosnaListDB[indexPath.section].Title
        cell.textViewDesc.text = self.ghosnaListDB[indexPath.section].Scheduled_Date + " " + self.ghosnaListDB[indexPath.section].Scheduled_Time
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getghosnaListData(){
       
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
       
        ghosnaListAPI.serviceCalling(obj: self,  emp : empId ) { (dict) in
            
            self.ghosnaListDB = dict as! [GhiosnaFileListModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
