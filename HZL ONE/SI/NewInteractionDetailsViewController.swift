//
//  NewInteractionDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 31/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NewInteractionDetailsViewController:CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var InteractionCode  = String()
    var RequestorCode = String()
    var refreshControl = UIRefreshControl()
    
    var InteractionDB:[InteractionCategoryListReportedDataModel] = []
    var InteractionAPI = IntercationDetailsListApi()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getInteractionData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    @objc func getInteractionData(){
        self.tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "List Interaction"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getInteractionData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
  
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
                return  dataArrayInteraction.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
//        @IBOutlet weak var lblSituation: UILabel!
//        @IBOutlet weak var lblRisk: UILabel!
//        @IBOutlet weak var textViewCategory: UITextView!
//        @IBOutlet weak var lblCategoryName: UILabel!
        
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewInteractionDetailsTableViewCell
            
            cell.lblCategoryName.text = dataArrayInteraction[indexPath.row].areaName
        cell.lblRisk.text = dataArrayInteraction[indexPath.row].RiskPotential
          cell.lblSituation.text = dataArrayInteraction[indexPath.row].Situation
            cell.textViewCategory.text = dataArrayInteraction[indexPath.row].Category + " > " + dataArrayInteraction[indexPath.row].SubCategory + " > " + dataArrayInteraction[indexPath.row].SubSubCategory
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
 
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
