//
//  SiDataListInteractionModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation

class SIDataIntercationListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:NewInteractionSIViewController,filterStr : String, success:@escaping (AnyObject)-> Void)
    {
        
        
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString1 = String()
            var keyString2 = String()
            var param : [String:String] = [:]
            if(filterStr == "Category") {
                urlString = Base_Url_SI+"Category/GetCategoryByParent"
                keyString2 = "Category"
                keyString1 = "Id"
             
                param = ["ParentId":"0"]
            } else if(filterStr == "Sub Category"){
                
                
                urlString = Base_Url_SI+"Category/GetCategoryByParent"
                keyString2 = "Category"
                keyString1 = "Id"
                param = ["Parent":FilterDataFromServer.parent_Id,"ParentGroup":"SC"]
            } else if(filterStr == "Sub Sub Category"){
                
                
                urlString = Base_Url_SI+"Category/GetCategoryByParent"
                keyString2 = "Category"
                keyString1 = "Id"
                param = ["Parent":FilterDataFromServer.parent_Id,"ParentGroup":"SSC"]
            }else if(filterStr == "Area"){
                
                
                urlString = Base_Url_SI+"SubArea/Get"
                keyString2 = "AreaName"
                keyString1 = "Id"
                param = ["ParentId":String(FilterDataFromServer.area_Id)]
            }
            
            
            
            print(urlString)
            
            
            WebServices.sharedInstances.sendPostRequest(url: urlString, parameters: param, successHandler: { (dict) in
                
                if let response = dict["ReponseCode"]{
                    
                    //statusString  = dict["ResponseMessage"] as! String
                    
                    if(response as! Int == 200)
                    {
                        
                        
                        
                        
                        if let data = dict["ResponseValue"] as? [AnyObject]
                        {
                            
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DataListModel()
                                    
                                    if cell[keyString1] is NSNull{
                                        object.id = ""
                                    }else{
                                        
                                        let idd  = cell[keyString1]
                                        object.id = (idd?.description)!
                                    }
                                    
                                    if cell[keyString2] is NSNull{
                                        object.name = ""
                                    }else{
                                        let idd  = cell[keyString2]
                                        object.name = (idd?.description)!
                                        
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        statusString = "success"
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
        }
        else{
        }
    }
}

class SIDataGetIntercationListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:NewInteractionSIViewController,filterStr : String, success:@escaping (AnyObject)-> Void)
    {
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString1 = String()
            var keyString2 = String()
            var param : [String:String] = [:]
            if(filterStr == "Risk Potential") {
                urlString = Base_Url_SI+"RiskPotential/Get"
                keyString2 = "RiskPotential"
                keyString1 = "Id"
               
            } else if(filterStr == "Situation") {
                urlString = Base_Url_SI+"Situation/Get"
                keyString2 = "Situation"
                keyString1 = "Id"
            
            }
            
            
            
            print(urlString)
            
            
            
            WebServices.sharedInstances.sendGetRequest(Url: urlString, successHandler: { (dict) in
                if let response = dict["ReponseCode"]{
                    
                    //statusString  = dict["ResponseMessage"] as! String
                    
                    if(response as! Int == 200)
                    {
                        
                        
                        
                        if let data = dict["ResponseValue"] as? [AnyObject]
                        {
                            print(data)
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                
                                if let cell = data[i] as? [String : AnyObject]
                                {
                                    
                                    let object = DataListModel()
                                    
                                    if cell[keyString1] is NSNull{
                                        object.id = ""
                                    }else{
                                        
                                        
                                        
                                        let idd  = cell[keyString1]
                                        object.id = (idd?.description)!
                                    }
                                    
                                    if cell[keyString2] is NSNull{
                                        object.name = ""
                                    }else{
                                        let idd  = cell[keyString2]
                                        object.name = (idd?.description)!
                                        
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                            }
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        
                    }
                    else
                    {
                        statusString = "success"
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
            
            
            
            
        }
        else{
            
            
        }
        
        
    }
}
