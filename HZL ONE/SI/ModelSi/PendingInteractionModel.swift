//
//  PendingInteractionModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation



class InteractionRequestReportedDataModel: NSObject {
    
    
   
    var InteractionCode: String?
    var RequestorCode : String?
    var RequestorName : String?
    var ZoneId : String?
    var Zone : String?
    var UnitId : String?
    var Unit : String?
    var AreaId : String?
    var Area : String?
    
    var Status : String?
    var Accordion : String?
    var SIDate : String?
    var FromDate : String?
    var ToDate : String?
    var ModuleId : String?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["InteractionCode"] is NSNull || cell["InteractionCode"] == nil {
            self.InteractionCode = ""
        }else{
            let app = cell["InteractionCode"]
            self.InteractionCode = (app?.description)!
        }
        if cell["RequestorCode"] is NSNull || cell["RequestorCode"] == nil {
            self.RequestorCode = ""
        }else{
            let app = cell["RequestorCode"]
            self.RequestorCode = (app?.description)!
        }
        if cell["RequestorName"] is NSNull || cell["RequestorName"] == nil {
            self.RequestorName = ""
        }else{
            let app = cell["RequestorName"]
            self.RequestorName = (app?.description)!
        }
        
        
        if cell["ZoneId"] is NSNull || cell["ZoneId"] == nil {
            self.ZoneId = ""
        }else{
            let app = cell["ZoneId"]
            self.ZoneId = (app?.description)!
        }
        if cell["Zone"] is NSNull || cell["Zone"] == nil {
            self.Zone = ""
        }else{
            let app = cell["Zone"]
            self.Zone = (app?.description)!
        }
        if cell["UnitId"] is NSNull || cell["UnitId"] == nil {
            self.UnitId = ""
        }else{
            let app = cell["UnitId"]
            self.UnitId = (app?.description)!
        }
        
        if cell["Unit"] is NSNull || cell["Unit"] == nil {
            self.Unit = ""
        }else{
            let app = cell["Unit"]
            self.Unit = (app?.description)!
        }
        
        if cell["AreaId"] is NSNull || cell["AreaId"] == nil {
            self.AreaId = ""
        }else{
            let app = cell["AreaId"]
            self.AreaId = (app?.description)!
        }
        
        if cell["Area"] is NSNull || cell["Area"] == nil {
            self.Area = ""
        }else{
            let app = cell["Area"]
            self.Area = (app?.description)!
        }
        
        if cell["Status"] is NSNull || cell["Status"] == nil {
            self.Status = ""
        }else{
            let app = cell["Status"]
            self.Status = (app?.description)!
        }
        
        if cell["Accordion"] is NSNull || cell["Accordion"] == nil {
            self.Accordion = ""
        }else{
            let app = cell["Accordion"]
            self.Accordion = (app?.description)!
        }
        if cell["SIDate"] is NSNull || cell["SIDate"] == nil {
            self.SIDate = ""
        }else{
            let app = cell["SIDate"]
            self.SIDate = (app?.description)!
        }
        if cell["FromDate"] is NSNull || cell["FromDate"] == nil {
            self.FromDate = ""
        }else{
            let app = cell["FromDate"]
            self.FromDate = (app?.description)!
        }
        if cell["ToDate"] is NSNull || cell["ToDate"] == nil {
            self.ToDate = ""
        }else{
            let app = cell["ToDate"]
            self.ToDate = (app?.description)!
        }
        if cell["ModuleId"] is NSNull || cell["ModuleId"] == nil {
            self.ModuleId = ""
        }else{
            let app = cell["ModuleId"]
            self.ModuleId = (app?.description)!
        }
        
    }
}

class InteractionCategoryReportedDataModel: NSObject {
    
    var TotalNo : String?
    var Accordion : String?
    var FilteredInteractionsList : [InteractionRequestReportedDataModel]?
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["TotalNo"] is NSNull || cell["TotalNo"] == nil {
            self.TotalNo = ""
        }else{
            let app = cell["TotalNo"]
            self.TotalNo = (app?.description)!
        }
        if cell["Accordion"] is NSNull || cell["Accordion"] == nil {
            self.Accordion = ""
        }else{
            let app = cell["Accordion"]
            self.Accordion = (app?.description)!
        }
        
        if let data = cell["FilteredInteractionsList"] as? [AnyObject]
        {
            FilteredInteractionsList = []
            var dataArray : [InteractionRequestReportedDataModel] = []
            
            for i in 0..<data.count
            {
                
                if let celll = data[i] as? [String:AnyObject]
                {
                    let object = InteractionRequestReportedDataModel()
                    object.setDataInModel(cell: celll)
                    dataArray.append(object)
                }
                
            }
            FilteredInteractionsList = dataArray
          
            
            
            
            
            
        }
        
        
    }
}



class PendingIntercationListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:PendingInteractionViewController,  param: [String:String] ,success:@escaping (AnyObject)-> Void)
    {
        
         var param1 : [String:String] = [:]

        
        if(reachablty.connection != .none)
        {
            
         
              obj.startLoading()
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.PendingInteractionsList, parameters: param, successHandler: { (dict) in
                
                if let response = dict["ReponseCode"]{
                    
                    //statusString  = dict["ResponseMessage"] as! String
                    
                    if(response as! Int == 200)
                    {
                        
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        if let data = dict["ResponseValue"] as? [AnyObject]
                        {
                            
                            var dataArray : [InteractionCategoryReportedDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = InteractionCategoryReportedDataModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                    
                            
                            
                        }
                        
                        
                    }
                    else{
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
           
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                obj.stopLoading()
                //  obj.errorChecking(error: error)
                
            })
            
        }
        else{
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
    }
}
