//
//  ListDetailsInteractionTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ListDetailsInteractionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCorrectiveAction: UILabel!
    @IBOutlet weak var lblObservation: UILabel!
    @IBOutlet weak var lblSituation: UILabel!
    @IBOutlet weak var lblRiskPotential: UILabel!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblInteractionCodeDetails: UILabel!
    @IBOutlet weak var lblIntercationCode: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblRegisterDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
