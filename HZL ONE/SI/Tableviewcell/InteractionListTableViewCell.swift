//
//  InteractionListTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class InteractionListTableViewCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
  
    @IBOutlet weak var lblObservationDetails: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRequestId: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
