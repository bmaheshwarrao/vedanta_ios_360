//
//  InteractionDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class InteractionDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var textViewZone: UITextView!
    @IBOutlet weak var lblFinYear: UILabel!
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var lblRequestCode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
