//
//  NewInteractionMainViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NewInteractionMainViewController: CommonVSClass,UITableViewDataSource,UITableViewDelegate , WWCalendarTimeSelectorProtocol {
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        FilterDataFromServer.filterType = "Location"
        if(FilterDataFromServer.filterType == "Location") {
            callDataGetListData()
        }else{
            self.callDataListData()
        }
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        FilterDataFromServer.filterType = "Location"
       

        // Do any additional setup after loading the view.
    }
    @IBAction func btnPartnerClicked(_ sender: UIButton) {
        searchNameDB = []
        let storyboard1 = UIStoryboard(name: "SI", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "NewPartnerSearchViewController") as! NewPartnerSearchViewController
        
        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                if(FilterDataFromServer.filterType == "Location") {
                    callDataGetListData()
                }else{
                    self.callDataListData()
                }
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
                
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     @IBAction func btnClearPartnerClicked(_ sender: UIButton) {
        if(searchNameArrayDB.count > 0){
        searchNameArrayDB.remove(at: sender.tag)
            tableView.reloadData()
        }
    }
    var ResponseValue = String()
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if(cellData.lblZone.text == "Select Zone"){
            self.view.makeToast("Select Zone")
        }else if(cellData.lblUnit.text == "Select Unit"){
            self.view.makeToast("Select Unit")
        }else if(cellData.lblArea.text == "Select Area"){
            self.view.makeToast("Select Unit")
        }else if(dataArrayInteraction.count != 3){
            self.view.makeToast("Please Add Atleast 3 Interaction")
        }else if(searchNameArrayDB.count == 0){
            self.view.makeToast("Please Add Partner")
        }else{
            
            
            
            
            
            var paramPartner  : [[String:String]] = []
            var iiVal = 0
            for value in searchNameArrayDB {
                
              
                
                
                
                
                let paramPartnerValue = ["PartnerCode": value.PartnerCode,
                                      "PartnerName": value.PartnerName,
                                      "PartnerType": value.PartnerType,
                                      "PartnerTypeId": value.PartnerTypeId
                    
                    ]
                    as! [String:String]
                iiVal = iiVal + 1;
                
                paramPartner.append(paramPartnerValue)
                
                
                
                
            }
            var paramInteraction  : [[String:String]] = []
            
            for valueData in dataArrayInteraction {
                
                
                
                
                
                
                let paramInteractionValue = ["AreaLocation": valueData.areaName,
                                         "Category": valueData.Category,
                                         "CategoryId": valueData.CategoryId,
                                         "CorrectiveAction": valueData.textAction,
                                         "InteractionDetailCode": "",
                                         "NoOfObservation": valueData.NoofObservedPeople,
                                         "ObservationDetails": valueData.textObservation,
                                         "RiskPotential": valueData.RiskPotential,
                                         "RiskPotentialId": valueData.RiskPotentialId,
                                         "RiskSituation": valueData.Situation,
                                         "RiskSituationId": valueData.SituationId,
                                         "Status": valueData.status,
                                         "SubAreaId": valueData.areaId,
                                         "SubCategory": valueData.SubCategory,
                                         "SubCategoryId": valueData.SubCategoryId,
                                         "SubSubCategory":  valueData.SubSubCategory,
                                         "SubSubCategoryId": valueData.SubSubCategoryId
                    
                    ]
                    as! [String:String]
                
                
                paramInteraction.append(paramInteractionValue)
                
                
                
                
            }
            
            self.startLoadingPK(view: self.view)
            
            
           
            
            


            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let parameter = ["RequestorCode": empId ,
                         "RequestorName": "",
                         "SIDate": dateInteraction,
                         "SIEndTime": cellData.lblEndTime.text!,
                         "SISchedulerID": "",
                         "SIStartTime": cellData.lblStartTime.text!,
                         "SISubmitedDate": "",
                         "Shift": Shift,
                         "ShiftID": ShiftValue,
                         "Unit": FilterDataFromServer.unit_Name ,
                         "UnitId": String(FilterDataFromServer.unit_Id),
                         "UserTransactionCode": ResponseValue,
                         "Zone": FilterDataFromServer.location_name,
                         "ZoneId": FilterDataFromServer.location_id,
            "Action": "I-S",
            "Area": FilterDataFromServer.area_Name,
            "AreaId": FilterDataFromServer.area_Id,
            "FinancialYear": "20162017",
            "FinancialYearId": "",
            "InteractionDetailList" : paramInteraction ,
            "PartnerDetailList" : paramPartner ,
            "InteractionCode": ""
           
                ] as [String:Any]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetInteractionPost, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
               
                self.stopLoadingPK(view: self.view)
               
               if let response = response["ReponseCode"]{
                if(response as! Int == 200)
                {
                   // let message = response["ResponseMessage"] as! String
                    let message = "Interaction Added Successfully"
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = message
                    self.navigationController?.popViewController(animated: false)
                    
                }
                else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast("Please try again, something went wrong.")
                }
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast("Please try again, something went wrong.")
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
    }
    @IBAction func btnShowInteractionClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "SI", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "NewInteractionDetailsViewController") as! NewInteractionDetailsViewController
        
        self.navigationController?.pushViewController(submitVC, animated: true)
    }
    @IBAction func btnAddInteractionClicked(_ sender: UIButton) {
        if(cellData.lblZone.text == "Select Zone"){
            self.view.makeToast("Select Zone")
        }else if(cellData.lblUnit.text == "Select Unit"){
            self.view.makeToast("Select Unit")
        }else if(cellData.lblArea.text == "Select Area"){
            self.view.makeToast("Select Unit")
        }
        else if(cellData.lblStartTime.text == cellData.lblEndTime.text){
            self.showSingleButtonWithMessage(title: "", message: "Start Time and End Time are equal", buttonName: "OK")
        }else if(dataArrayInteraction.count == 3){
           self.view.makeToast("Cannot Add more than 3 Interaction")
        }else{
            
            
            let startDate = cellData.lblDate.text! + " " + cellData.lblStartTime.text!
            let ShiftADate = cellData.lblDate.text! + " 02:01 PM"
             let ShiftBDate = cellData.lblDate.text! + " 10:01 PM"
            let ShiftCDate = cellData.lblDate.text! + " 12:01 PM"
            let ShiftDDate = cellData.lblDate.text! + " 06:01 AM"
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "dd MMM yyyy hh:mm a"
            dateFormatter2.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd MMM yyyy hh:mm a"
            dateFormatter1.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
            let dateStart = dateFormatter1.date(from: startDate)
               let dateA = dateFormatter2.date(from: ShiftADate)
            let dateB = dateFormatter2.date(from: ShiftBDate)
            let dateC = dateFormatter2.date(from: ShiftCDate)
            let dateD = dateFormatter2.date(from: ShiftDDate)
            
            
            
           var compare = 0
            
            switch dateStart!.compare(dateD!) {
                
            case .orderedAscending:
                ShiftValue = "13"
                Shift = "C"
                compare = 1
            case .orderedSame:
                break;
            case .orderedDescending:
                break;
            }
            if(compare == 0){
            switch dateStart!.compare(dateC!) {
                
            case .orderedAscending:
                ShiftValue = "13"
                Shift = "C"
                compare = 1
            case .orderedSame:
                break;
            case .orderedDescending:
                
                break;
            }
            }
            if(compare == 0){
            switch dateStart!.compare(dateA!) {
                
            case .orderedAscending:
                ShiftValue = "11"
                Shift = "A"
                compare = 1
            case .orderedSame:
                break;
            case .orderedDescending:
                break;
            }
            }
            if(compare == 0){
            switch dateStart!.compare(dateB!) {
                
            case .orderedAscending:
                ShiftValue = "12"
                Shift = "B"
                compare = 1
            case .orderedSame:
                break;
            case .orderedDescending:
                break;
            }
            }
            print(Shift)
            print(ShiftValue)
            
        let storyBoard = UIStoryboard(name: "SI", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "NewInteractionSIViewController") as! NewInteractionSIViewController

        self.navigationController?.pushViewController(submitVC, animated: true)
        }
    }
    var ShiftValue = String()
    var Shift = String()
    func shiftCondition(){
        
    }
    var millisecStart = String()
    var millisecEnd = String()
    var millisecDate = String()
    func getMilliseconds(date1 : Date) -> Int{
        //let strDate = date1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy  hh:mma"
        //let date = dateFormatter.date(from: strDate)
        
        let millieseconds = self.getDiffernce(toTime: date1)
        
        return millieseconds
    }
    func getDiffernce(toTime:Date) -> Int{
        let elapsed = toTime.timeIntervalSince1970
        return Int(elapsed * 1000)
    }

    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
var selectdateVal = 0
    @IBAction func btnAddDateClicked(_ sender: UIButton) {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
       
        selectdateVal = 3
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        
        present(selector, animated: true, completion: nil)
    }
    @IBAction func btnAddStartTimeClicked(_ sender: UIButton) {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        
        selectdateVal = 1
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        present(selector, animated: true, completion: nil)
    }
    @IBAction func btnAddEndTimeClicked(_ sender: UIButton) {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        
        selectdateVal = 2
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        present(selector, animated: true, completion: nil)
    }
    
    
    var DataListModelDB:[DataListModel] = []
    
    var DataListCallApi = SIDataListApi()
    @objc func callDataListData(){
         self.startLoadingPK(view: self.tableView)
        self.DataListCallApi.serviceCalling(obj: self)
        { (dict) in
            self.DataListModelDB = dict as! [DataListModel]
            
            for i in 0...self.DataListModelDB.count - 1 {
                 if(FilterDataFromServer.filterType == "Unit"){
                    self.unitArray.append(self.DataListModelDB[i].name)
                    self.UnitIdArray.append(self.DataListModelDB[i].id)
                    
                 }else  if(FilterDataFromServer.filterType == "Area"){
                    self.areaArray.append(self.DataListModelDB[i].name)
                    self.areaIdArray.append(self.DataListModelDB[i].id)
                    
                }
            }
            self.stopLoadingPK(view: self.tableView)
            
        }
    }
    
    var DataListGetCallApi = SIDataGetListApi()
    @objc func callDataGetListData(){
        self.startLoadingPK(view: self.tableView)
        self.DataListGetCallApi.serviceCalling(obj: self)
        { (dict) in
            self.DataListModelDB = dict as! [DataListModel]
            for i in 0...self.DataListModelDB.count - 1 {
                if(FilterDataFromServer.filterType == "Location"){
                    self.ZoneArray.append(self.DataListModelDB[i].name)
                    self.ZoneIdArray.append(self.DataListModelDB[i].id)
                    
                }
            }
            self.stopLoadingPK(view: self.tableView)
            
        }
    }
    
    var ZoneArray : [String] = []
     var ZoneIdArray : [String] = []
    @IBAction func btnZoneClicked(_ sender: UIButton) {
        
       
     
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.ZoneArray
            
            popup.sourceView = self.cellData.lblZone
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                FilterDataFromServer.location_id = Int(self.ZoneIdArray[row])!
                FilterDataFromServer.location_name = self.ZoneArray[row]
                
                self.cellData.lblZone.text = self.ZoneArray[row]
                self.cellData.lblZone.textColor = UIColor.black
                FilterDataFromServer.filterType = "Unit"

                FilterDataFromServer.area_Id = Int()
                FilterDataFromServer.area_Name = String()
             self.updateData()
                if(FilterDataFromServer.filterType == "Location") {
                    self.callDataGetListData()
                }else{
                    self.callDataListData()
                }
                
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
        
    }
    var unitArray : [String] = []
    var UnitIdArray : [String] = []
    @IBAction func btnUnitClicked(_ sender: UIButton) {
        if(FilterDataFromServer.location_name != "") {
            DispatchQueue.main.async(execute: {() -> Void in
                let popup = PopUpTableViewController()
                
                popup.itemsArray = self.unitArray
                
                popup.sourceView = self.cellData.lblUnit
                popup.isScroll = true
                popup.backgroundColor = UIColor.white
                if popup.itemsArray.count > 5{
                    popup.popUpHeight = 200
                }
                else{
                    popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
                }
                popup.popUpWidth = UIScreen.main.bounds.size.width-60
                popup.backgroundImage = nil
                popup.itemTitleColor = UIColor.white
                popup.itemSelectionColor = UIColor.lightGray
                popup.arrowDirections = .any
                popup.arrowColor = UIColor.white
                popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                    
                    
                    
                    FilterDataFromServer.unit_Id = Int(self.UnitIdArray[row])!
                    FilterDataFromServer.unit_Name = self.unitArray[row]
                    
                    self.cellData.lblUnit.text = self.unitArray[row]
                    self.cellData.lblUnit.textColor = UIColor.black
                    FilterDataFromServer.filterType = "Area"
                    
                  
                    self.updateData()
                    
                    if(FilterDataFromServer.filterType == "Location") {
                        self.callDataGetListData()
                    }else{
                        self.callDataListData()
                    }
                    
                    popupVC.dismiss(animated: false, completion: nil)
                    
                }
                self.present(popup, animated: true, completion: {() -> Void in
                })
                
                
            })
        }
    }
    var areaArray : [String] = []
    var areaIdArray : [String] = []
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            
            
            DispatchQueue.main.async(execute: {() -> Void in
                let popup = PopUpTableViewController()
                
                popup.itemsArray = self.areaArray
                
                popup.sourceView = self.cellData.lblArea
                popup.isScroll = true
                popup.backgroundColor = UIColor.white
                if popup.itemsArray.count > 5{
                    popup.popUpHeight = 200
                }
                else{
                    popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
                }
                popup.popUpWidth = UIScreen.main.bounds.size.width-60
                popup.backgroundImage = nil
                popup.itemTitleColor = UIColor.white
                popup.itemSelectionColor = UIColor.lightGray
                popup.arrowDirections = .any
                popup.arrowColor = UIColor.white
                popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                    
                    
                    
                    FilterDataFromServer.area_Id = Int(self.areaIdArray[row])!
                    FilterDataFromServer.area_Name = self.areaArray[row]
                    
                    self.cellData.lblArea.text = self.areaArray[row]
                    self.cellData.lblArea.textColor = UIColor.black
                    FilterDataFromServer.filterType = "Area"
                    
                    
                    self.updateData()
                    
                    
                    
                    popupVC.dismiss(animated: false, completion: nil)
                    
                }
                self.present(popup, animated: true, completion: {() -> Void in
                })
                
                
            })
            
            
        }
    }
    @objc func updateData(){
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.location_name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != "" && FilterDataFromServer.location_name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
       
        
        if(FilterDataFromServer.location_name != "") {
            cellData.lblZone.text = FilterDataFromServer.location_name
            cellData.lblZone.textColor = UIColor.black
        } else {
            cellData.lblZone.text = "Select Zone"
            cellData.lblZone.textColor = UIColor.lightGray
        }
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "New Interaction"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        tableView.reloadData()
        
    }
    var dateInteraction = String()
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
        
        
        if(selectdateVal == 1){
            cellData.lblStartTime.text = date.stringFromFormat("hh':'mm a")
            startDate  = getMilliseconds(date1: date)
            millisecStart = String(startDate)
    
            DataDateStart = date
        }
        if(selectdateVal == 2){
            cellData.lblEndTime.text = date.stringFromFormat("hh':'mm a")
            endDate  = getMilliseconds(date1: date)
            millisecEnd = String(endDate)
            DataDateEnd = date
        }
        if(selectdateVal == 3){
            cellData.lblDate.text = date.stringFromFormat("dd' 'MMM' 'yyyy")
            dateInteraction = date.stringFromFormat("yyyy-MM-dd")
            let dates  = getMilliseconds(date1: date)
            millisecDate = String(dates)
            
        }
        print(String(startDate) + "-----" + String(endDate))
        
        switch DataDateStart.compare(DataDateEnd) {
            
        case .orderedAscending:
            print("less")
        case .orderedSame:
            print("same")
        case .orderedDescending:
            millisecEnd = millisecStart
            cellData.lblEndTime.text = cellData.lblStartTime.text
        }
        
        
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            // dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    var cellData : NewInteractionMainTableViewCell!
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.0
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
           return 1
        }else{
            if(searchNameArrayDB.count == 0){
                return 1
            }else{
           return searchNameArrayDB.count
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if(indexPath.section == 0) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewInteractionMainTableViewCell
        cellData = cell
     
        updateData()
        
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "hh:mm a"
        let dateFormatter1 = DateFormatter()
        dateFormatter1.timeZone = NSTimeZone.system
        dateFormatter1.dateFormat = "dd MMM yyyy"
          startDate  = getMilliseconds(date1: Date())
        let date_TimeStr = dateFormatter2.string(from: Date())
        if(cell.lblStartTime.text == ""){
        cell.lblStartTime.text = date_TimeStr
             millisecStart = String(startDate)
        }
        if(cell.lblEndTime.text == ""){
            cell.lblEndTime.text = date_TimeStr
            millisecEnd = String(startDate)
        }
        if(cell.lblDate.text == ""){
            let date_TimeDate = dateFormatter1.string(from: Date())
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "yyyy-MM-dd"
            let date_TimeDateIntercation = dateFormatter2.string(from: Date())
            dateInteraction = date_TimeDateIntercation
            cell.lblDate.text = date_TimeDate
            millisecDate = String(startDate)
        }
   
            if(searchNameArrayDB.count > 0){
                cell.lblPartnerCount.text = String(searchNameArrayDB.count)
                
            }else{
                cell.lblPartnerCount.text = "0"
            }
            if(dataArrayInteraction.count > 0){
                cell.lblInteractionCount.text =  String(dataArrayInteraction.count) + "/3"
                
            }else{
                cell.lblInteractionCount.text = "0/3"
            }
     
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! PartnerTableViewCell
            if(searchNameArrayDB.count > 0){
            cell.lblEmpName.text = "\(searchNameArrayDB[indexPath.row].PartnerName!) "
                  cell.lblEmpName.textAlignment = NSTextAlignment.left
            cell.clearBtn.tag = indexPath.row
                cell.clearBtn.isHidden = false
            }else{
                cell.lblEmpName.text = "No Partner Found"
                cell.lblEmpName.textAlignment = NSTextAlignment.center
                cell.clearBtn.isHidden = true
            }
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
}
