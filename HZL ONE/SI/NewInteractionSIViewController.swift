//
//  NewInteractionSIViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var dataArrayInteraction : [SINewInteractionObject] = []
class NewInteractionSIViewController: CommonVSClass,UITableViewDataSource,UITableViewDelegate , UIImagePickerControllerDelegate,UINavigationControllerDelegate {
 @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callDataListData(str: "Category")
        callDataListData(str: "Area")

        callDataGetListData(str: "Risk Potential")
         callDataGetListData(str: "Situation")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    var statusData = String()
    @IBAction func switcherValueChanged(_ sender: UISwitch) {
        if(sender.isOn == true){
            statusData = "Open"
        }else{
            statusData = "Closed"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "New Interaction"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        tableView.reloadData()
    }
    @objc func updateData(){
        if(FilterDataFromServer.sub_Area_Name != "" ) {
            cellData.lblLocation.text = FilterDataFromServer.sub_Area_Name
            cellData.lblLocation.textColor = UIColor.black
        } else {
            cellData.lblLocation.text = "Select Location"
            cellData.lblLocation.textColor = UIColor.lightGray
        }
        if(idCat != String()) {
           
            cellData.lblCategory.textColor = UIColor.black
        } else {
            cellData.lblCategory.text = "Select Category"
            cellData.lblCategory.textColor = UIColor.lightGray
        }
        
        
        if(idSubCat != String()) {
            
            cellData.lblSubCategory.textColor = UIColor.black
        } else {
            cellData.lblSubCategory.text = "Select Sub Category"
            cellData.lblSubCategory.textColor = UIColor.lightGray
        }
        
        if(idSubSubCat != String()) {
            
            cellData.lblSubSubCategory.textColor = UIColor.black
        } else {
            cellData.lblSubSubCategory.text = "Select Sub Sub-Category"
            cellData.lblSubSubCategory.textColor = UIColor.lightGray
        }
        if(riskId != String()) {
            
            cellData.lblRiskPotential.textColor = UIColor.black
        } else {
            cellData.lblRiskPotential.text = "Select Risk Potential"
            cellData.lblRiskPotential.textColor = UIColor.lightGray
        }
        if(situationId != String()) {
            
            cellData.lblSituation.textColor = UIColor.black
        } else {
            cellData.lblSituation.text = "Select Situation"
            cellData.lblSituation.textColor = UIColor.lightGray
        }
        
        
    }
   
  
    @IBAction func btnActionPlanClicked(_ sender: UIButton) {
        if(cellData.lblLocation.text == "Select Location")
        {
            self.view.makeToast("Select Location")
        }else if(idCat == String())
        {
            self.view.makeToast("Select Category")
        }else if(idSubCat == String())
        {
            self.view.makeToast("Select Sub Category")
        }else if(idSubSubCat == String())
        {
            self.view.makeToast("Select Sub Sub Category")
        }else if(riskId == String())
        {
            self.view.makeToast("Select Risk Potential")
        }else if(situationId == String())
        {
            self.view.makeToast("Select Situation")
        }else if(cellData.txtNoPeople.text == "")
        {
            self.view.makeToast("Write No of People")
        }else if(cellData.textViewOnservationn.text == "")
        {
            self.view.makeToast("Write Observation")
        }else if(cellData.textViewAction.text == "")
        {
            self.view.makeToast("Write Action")
        }else{
            let object = SINewInteractionObject()
            object.areaId = String(FilterDataFromServer.sub_Area_Id)
            object.areaName = FilterDataFromServer.sub_Area_Name
            object.CategoryId = idCat
            object.Category = cellData.lblCategory.text!
            object.SubCategoryId = idSubCat
            object.SubCategory = cellData.lblSubCategory.text!
            
            object.SubSubCategoryId = idSubSubCat
            object.SubSubCategory = cellData.lblSubSubCategory.text!
            object.RiskPotentialId = riskId
            object.RiskPotential = cellData.lblRiskPotential.text!
            
            object.SituationId = situationId
            object.Situation = cellData.lblSituation.text!
            object.NoofObservedPeople = cellData.txtNoPeople.text!
            object.textObservation = cellData.textViewOnservationn.text!
            object.textAction = cellData.textViewAction.text!
            object.status = statusData
            dataArrayInteraction.append(object)
            self.navigationController?.popViewController(animated: true)
        }
        
        
        
    }
    
    
    var subAreaArray : [String] = []
    var subAreaIdArray : [String] = []
    @IBAction func btnLocationClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.subAreaArray
            
            popup.sourceView = self.cellData.lblLocation
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                FilterDataFromServer.sub_Area_Id = Int(self.subAreaIdArray[row])!
                FilterDataFromServer.sub_Area_Name = self.subAreaArray[row]
                
                self.cellData.lblLocation.text = self.subAreaArray[row]
                self.cellData.lblLocation.textColor = UIColor.black
              
                
              
                self.updateData()
              
                
                
                
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
    var catArray : [String] = []
    var catIdArray : [String] = []
    var idCat = String()
    @IBAction func btnCategoryClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.catArray
            
            popup.sourceView = self.cellData.lblCategory
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                self.idCat = String(Int(self.catIdArray[row])!)
           
                
                self.cellData.lblCategory.text = self.catArray[row]
                self.cellData.lblCategory.textColor = UIColor.black
                self.idSubCat = ""
                self.cellData.lblSubCategory.text = ""
                self.cellData.lblSubCategory.textColor = UIColor.lightGray
                self.cellData.lblSubSubCategory.text = ""
                self.cellData.lblSubSubCategory.textColor = UIColor.lightGray
                FilterDataFromServer.filterType = "Sub Category"
          
                self.updateData()
                FilterDataFromServer.parent_Id = self.idCat
                self.callDataListData(str: "Sub Category")
                
                
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
    var subcatArray : [String] = []
    var subcatIdArray : [String] = []
    var idSubCat = String()
    @IBAction func btnSubCategoryClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.subcatArray
            
            popup.sourceView = self.cellData.lblSubCategory
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                self.idSubCat = String(Int(self.subcatIdArray[row])!)
                self.idSubSubCat = ""
                self.cellData.lblSubSubCategory.text = ""
                self.cellData.lblSubSubCategory.textColor = UIColor.lightGray
                self.cellData.lblSubCategory.text = self.subcatArray[row]
                self.cellData.lblSubCategory.textColor = UIColor.black
                FilterDataFromServer.filterType = "Sub Sub Category"
                
                self.updateData()
                FilterDataFromServer.parent_Id = self.idSubCat
                self.callDataListData(str: "Sub Sub Category")
                
                
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
    var subsubcatArray : [String] = []
    var subsubcatIdArray : [String] = []
    var idSubSubCat = String()
    @IBAction func btnSubSubCategoryClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.subsubcatArray
            
            popup.sourceView = self.cellData.lblSubSubCategory
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                self.idSubSubCat = String(Int(self.subsubcatIdArray[row])!)
                
                
                self.cellData.lblSubSubCategory.text = self.subsubcatArray[row]
                self.cellData.lblSubSubCategory.textColor = UIColor.black
              // FilterDataFromServer.parent_Id = self.idSubSubCat
                
                self.updateData()
                
           
                
                
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
     var riskArray : [String] = []
    var riskIdArray : [String] = []
    var riskId = String()
    @IBAction func btnRiskPotentialClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.riskArray
            
            popup.sourceView = self.cellData.lblRiskPotential
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
          
                 self.riskId = String(Int(self.riskIdArray[row])!)
                
                self.cellData.lblRiskPotential.text = self.riskArray[row]
                self.cellData.lblRiskPotential.textColor = UIColor.black
                
                
                self.updateData()
                
                
                
                
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
       var situationArray : [String] = []
     var situationIdArray : [String] = []
    var situationId = String()
    @IBAction func btnSituationClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.situationArray
            
            popup.sourceView = self.cellData.lblSituation
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.situationId = String(Int(self.situationIdArray[row])!)
                self.cellData.lblSituation.text = self.situationArray[row]
                self.cellData.lblSituation.textColor = UIColor.black
                
                
                self.updateData()
                
                
                
                
                
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
    }
   
    
    var DataListModelDB:[DataListModel] = []
    
    var DataListCallApi = SIDataIntercationListApi()
    @objc func callDataListData(str : String){
        
        if(str == "Category"){
            self.catArray = []
            self.catIdArray = []
            
        }else  if(str == "Sub Category"){
            self.subcatArray = []
            self.subcatIdArray = []
            
        }else  if(str == "Sub Sub Category"){
            self.subsubcatArray = []
            self.subsubcatIdArray = []
            
        }else  if(str == "Area"){
            self.subAreaArray = []
            self.subAreaIdArray = []
            
        }
        self.startLoadingPK(view: self.tableView)
        self.DataListCallApi.serviceCalling(obj: self , filterStr : str)
        { (dict) in
            self.DataListModelDB = dict as! [DataListModel]
            if(self.DataListModelDB.count > 0){
            for i in 0...self.DataListModelDB.count - 1 {
                if(str == "Category"){
                    self.catArray.append(self.DataListModelDB[i].name)
                    self.catIdArray.append(self.DataListModelDB[i].id)
                    
                }else  if(str == "Sub Category"){
                    self.subcatArray.append(self.DataListModelDB[i].name)
                    self.subcatIdArray.append(self.DataListModelDB[i].id)
                    
                }else  if(str == "Sub Sub Category"){
                    self.subsubcatArray.append(self.DataListModelDB[i].name)
                    self.subsubcatIdArray.append(self.DataListModelDB[i].id)
                    
                }else  if(str == "Area"){
                    self.subAreaArray.append(self.DataListModelDB[i].name)
                    self.subAreaIdArray.append(self.DataListModelDB[i].id)
                    
                }
            }
            }
            self.stopLoadingPK(view: self.tableView)
            
        }
    }
    
    var DataListGetCallApi = SIDataGetIntercationListApi()
    @objc func callDataGetListData(str : String){
        self.startLoadingPK(view: self.tableView)
        self.DataListGetCallApi.serviceCalling(obj: self, filterStr : str)
        { (dict) in
            self.DataListModelDB = dict as! [DataListModel]
            for i in 0...self.DataListModelDB.count - 1 {
                if(str == "Risk Potential"){
                    self.riskArray.append(self.DataListModelDB[i].name)
                    self.riskIdArray.append(self.DataListModelDB[i].id)
                   
                    
                }else if(str == "Situation"){
                    self.situationArray.append(self.DataListModelDB[i].name)
                    self.situationIdArray.append(self.DataListModelDB[i].id)
                   
                    
                }
            }
            self.stopLoadingPK(view: self.tableView)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnGalleryClicked(_ sender: UIButton) {
        self.openGallary()
    }
    @IBAction func btnImageButtonClicked(_ sender: UIButton) {
        
        
        
          self.openCamera()
//        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//
//        }))
//
//        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
//
//        }))
//
//        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
        
    }
    
        var imagedata: Data? = nil
    let imagePicker = UIImagePickerController()
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
        
    }
   
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cellData.imageViewPhoto.image = image
            imagedata = UIImageJPEGRepresentation(image, 1.0)!
            cellData.btnCamera.isHidden = true
            cellData.lblOptional.isHidden = true
            cellData.btnClose.isHidden = false
            cellData.imageViewPhoto.isHidden = false
            updateConst()
            
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // var reachability = Reachability()!
    
    @IBAction func btnCloseButtonClicked(_ sender: UIButton) {
        
        self.imagedata = nil
        self.cellData.btnClose.isHidden = true
        cellData.btnCamera.isHidden = false
        cellData.lblOptional.isHidden = false
        updateConst()
    }
    func updateConst(){
   
      
            
            if(self.cellData.btnClose.isHidden == true){
                self.cellData.heightViewofClose.constant = 0
                self.cellData.heightImageselected.constant = 0
                self.cellData.heightViewImage.constant = 40
            }else{
                self.cellData.heightViewofClose.constant = 20
                self.cellData.heightImageselected.constant = 100
                self.cellData.heightViewImage.constant = 120
            }
            
       
    }
    var indexing : Int = 1
    var cellData : NewInteractionSITableViewCell!
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.0
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewInteractionSITableViewCell
        cellData = cell
        cell.switcher.isOn = false
        updateData()
        if(statusData == String()){
            statusData = "Closed"
        }
        if(indexing == 1){
            cellData.btnClose.isHidden = true
            indexing = indexing + 1
        }
        updateConst()
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1050
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class SINewInteractionObject: NSObject {
    var status : String = ""
    var areaId : String = ""
    var areaName : String = ""
    var Category : String = ""
    var CategoryId : String = ""
    var SubCategory : String = ""
    var SubCategoryId : String = ""
    var SubSubCategory : String = ""
    var SubSubCategoryId : String = ""
    var RiskPotential : String = ""
    var RiskPotentialId : String = ""
    var Situation : String = ""
    var SituationId : String = ""
    var NoofObservedPeople : String = ""
    var textObservation : String = ""
    var textAction : String = ""
    //var image : Data = nil
}
