//
//  NameCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 30/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NameCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var textViewName: UITextView!
    
}
