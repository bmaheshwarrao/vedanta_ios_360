//
//  NewInteractionMainTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NewInteractionMainTableViewCell: UITableViewCell {
    @IBOutlet weak var viewZone: UIView!
    @IBOutlet weak var btnZone: UIButton!
    @IBOutlet weak var lblZone: UILabel!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var viewArea: UIView!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var lblArea: UILabel!
    
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var viewStartTime: UIView!
    @IBOutlet weak var btnStartTime: UIButton!
    @IBOutlet weak var lblStartTime: UILabel!
    
    @IBOutlet weak var viewEndTime: UIView!
    @IBOutlet weak var btnEndTime: UIButton!
    @IBOutlet weak var lblEndTime: UILabel!
    
    @IBOutlet weak var lblInteractionCount: UILabel!
    @IBOutlet weak var lblPartnerCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        viewZone.layer.borderWidth = 1.0
        viewZone.layer.borderColor = UIColor.black.cgColor
        viewZone.layer.cornerRadius = 10.0
        
        
        viewDate.layer.borderWidth = 1.0
        viewDate.layer.borderColor = UIColor.black.cgColor
        viewDate.layer.cornerRadius = 10.0
        
        viewStartTime.layer.borderWidth = 1.0
        viewStartTime.layer.borderColor = UIColor.black.cgColor
        viewStartTime.layer.cornerRadius = 10.0
        
        viewEndTime.layer.borderWidth = 1.0
        viewEndTime.layer.borderColor = UIColor.black.cgColor
        viewEndTime.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
