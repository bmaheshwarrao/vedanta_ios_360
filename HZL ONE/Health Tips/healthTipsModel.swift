//
//  healthTipsModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 02/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class HealthDataModel: NSObject {
    
    
    var ID = Int()
    var Title = String()
    var Description = String()
    var Category_ID = String()
    var Remarks = String()
    var Category = String()
    var Created_Date = String()
    
    
    
    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Title"] is NSNull || str["Title"] == nil{
            self.Title = ""
        }else{
            
            let tit = str["Title"]
            
            self.Title = (tit?.description)!
            
            
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description =  ""
        }else{
            
            let desc = str["Description"]
            
            self.Description = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Category_ID"] is NSNull || str["Category_ID"] == nil{
            self.Category_ID = "0"
        }else{
            let catId = str["Category_ID"]
            
            self.Category_ID = (catId?.description)!
            print(self.Category_ID)
        }
        if str["Remarks"] is NSNull || str["Remarks"] == nil{
            self.Remarks = ""
        }else{
            
            let rem = str["Remarks"]
            
            self.Remarks = (rem?.description)!
            
        }
        
        if str["Category"] is NSNull || str["Category"] == nil{
            self.Category = ""
        }else{
            let cat = str["Category"]
            
            self.Category = (cat?.description)!
            
            
            
        }
        if str["Created_Date"] is NSNull || str["Created_Date"] == nil{
            self.Created_Date = ""
        }else{
            let createDate = str["Created_Date"]
            
            self.Created_Date = (createDate?.description)!
            
            
            
            
            
            
            
        }
        
    }
    
}





class healthDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:HealthTipsViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Tips_Health_Tips, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [HealthDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = HealthDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class healthDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:HealthTipsViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Tips_Health_Tips, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [HealthDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = HealthDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

