//
//  ModelCSC.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 19/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class MemberModel: NSObject {
    
    
    
    var IS_App_Member: String?
 
    

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["IS_App_Member"] is NSNull || cell["IS_App_Member"] == nil {
            self.IS_App_Member = ""
        }else{
            let app = cell["IS_App_Member"]
            self.IS_App_Member = (app?.description)!
        }
       
        
        
    }
}


class MemberDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:CSCFirstViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Check_Member, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        
                        obj.textVieewMsg.isHidden = true
                       
                        obj.btnValidate.isHidden = true
                        
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [MemberModel] = []
                            if let celll = data as? [String:AnyObject]
                            {
                                let object = MemberModel()
                                object.setDataInModel(cell: celll)
                                dataArray.append(object)
                            }
                            
//                            for i in 0..<data.count
//                            {
//
                            
//
//                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        
                        
                        obj.textVieewMsg.isHidden = false
                        
                        obj.btnValidate.isHidden = false
                        obj.textVieewMsg.text = msg
                        obj.refresh.endRefreshing()
                      
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
           // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.textVieewMsg.isHidden = false
            obj.textVieewMsg.text = "No internet connection found. Check your internet connection and try again"
            obj.btnValidate.isHidden = false
            obj.label.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}
















class LoginVisitCscModel: NSObject {
    
    
    
    var Visit_Area_Name: String?
    var ID : String?
        var Zone_ID : String?
  
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Visit_Area_Name"] is NSNull || cell["Visit_Area_Name"] == nil {
            self.Visit_Area_Name = ""
        }else{
            let app = cell["Visit_Area_Name"]
            self.Visit_Area_Name = (app?.description)!
        }
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        if cell["Zone_ID"] is NSNull || cell["Zone_ID"] == nil {
            self.Zone_ID = ""
        }else{
            let app = cell["Zone_ID"]
            self.Zone_ID = (app?.description)!
        }
        
    }
}


class LoginVisitDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:CSCFirstViewController, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            let param : [String:String] = [:]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.User_Login_Visit_Location, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                   
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [LoginVisitCscModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = LoginVisitCscModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.textVieewMsg.isHidden = false
                        
                        obj.btnValidate.isHidden = false
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.textVieewMsg.isHidden = false
            obj.textVieewMsg.text = "No internet connection found. Check your internet connection and try again"
            obj.btnValidate.isHidden = false
            obj.label.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

