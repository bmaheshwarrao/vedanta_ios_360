//
//  GABTableViewCell.swift
//  HZL CSC
//
//  Created by Bunga Maheshwar Rao on 28/10/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class GABTableViewCell: UITableViewCell {

   
    
   @IBOutlet weak var btnGood: DLRadioButton!
    @IBOutlet weak var btnBad: DLRadioButton!
    @IBOutlet weak var btnAverage: DLRadioButton!
    @IBOutlet weak var lblNumber: UILabel!
  
    
    
    @IBOutlet weak var textViewQuestion: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
