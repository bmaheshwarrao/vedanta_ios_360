//
//  RateTableViewCell.swift
//  HZL CSC
//
//  Created by Bunga Maheshwar Rao on 28/10/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RateTableViewCell: UITableViewCell {

    @IBAction func btnRate0Clicked(_ sender: RoundShapeButton) {
        btnRate3.backgroundColor = UIColor.lightGray
        btnRate2.backgroundColor = UIColor.lightGray
        btnRate4.backgroundColor = UIColor.lightGray
        btnRate5.backgroundColor = UIColor.lightGray
        btnRate0.backgroundColor = UIColor.orange
        btnRate1.backgroundColor = UIColor.lightGray
        
        collectRate(strInt: 0)
    }
    @IBOutlet weak var btnRate0: RoundShapeButton!
    @IBOutlet weak var textViewQuestion: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var lblNumber: UILabel!
    @IBAction func btnRate5Clicked(_ sender: RoundShapeButton) {
        btnRate1.backgroundColor = UIColor.lightGray
        btnRate2.backgroundColor = UIColor.lightGray
        btnRate3.backgroundColor = UIColor.lightGray
        btnRate4.backgroundColor = UIColor.lightGray
        btnRate5.backgroundColor = UIColor.orange
        btnRate0.backgroundColor = UIColor.lightGray
        
        collectRate(strInt: 5)
    }
    @IBOutlet weak var btnRate5: RoundShapeButton!
    @IBAction func btnRate4Clicked(_ sender: RoundShapeButton) {
        btnRate1.backgroundColor = UIColor.lightGray
        btnRate2.backgroundColor = UIColor.lightGray
        btnRate3.backgroundColor = UIColor.lightGray
        btnRate5.backgroundColor = UIColor.lightGray
        btnRate4.backgroundColor = UIColor.orange
        btnRate0.backgroundColor = UIColor.lightGray
        
        collectRate(strInt: 4)
    }
    @IBOutlet weak var btnRate4: RoundShapeButton!
    @IBAction func btnRate3Clicked(_ sender: RoundShapeButton) {
        btnRate1.backgroundColor = UIColor.lightGray
        btnRate2.backgroundColor = UIColor.lightGray
        btnRate4.backgroundColor = UIColor.lightGray
        btnRate5.backgroundColor = UIColor.lightGray
        btnRate3.backgroundColor = UIColor.orange
        btnRate0.backgroundColor = UIColor.lightGray
        
        collectRate(strInt: 3)
    }
    @IBOutlet weak var btnRate3: RoundShapeButton!
    @IBOutlet weak var btnRate2: RoundShapeButton!
    @IBAction func btnRate2Clicked(_ sender: RoundShapeButton) {
        btnRate1.backgroundColor = UIColor.lightGray
        btnRate3.backgroundColor = UIColor.lightGray
        btnRate4.backgroundColor = UIColor.lightGray
        btnRate5.backgroundColor = UIColor.lightGray
        btnRate2.backgroundColor = UIColor.orange
        btnRate0.backgroundColor = UIColor.lightGray
        
        collectRate(strInt: 2)
    }
    @IBAction func btnRate1Clicked(_ sender: RoundShapeButton) {
        btnRate3.backgroundColor = UIColor.lightGray
        btnRate2.backgroundColor = UIColor.lightGray
        btnRate4.backgroundColor = UIColor.lightGray
        btnRate5.backgroundColor = UIColor.lightGray
        btnRate1.backgroundColor = UIColor.orange
        btnRate0.backgroundColor = UIColor.lightGray
        
        collectRate(strInt: 1)
    }
    func collectRate(strInt : Int) {
        
        let dataRate : String = String(strInt)
        //        print(dataRate)
        if(textViewQuestion.tag == 2){
            
            QuestionsData.Rate1 = dataRate
            
        }
        if(textViewQuestion.tag == 3){
            QuestionsData.Rate2 = dataRate
        }
        if(textViewQuestion.tag == 8){
            QuestionsData.Rate3 = dataRate
        }
        
        
    }
    @IBOutlet weak var btnRate1: RoundShapeButton!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
