//
//  TextFieldResTableViewCell.swift
//  HZL CSC
//
//  Created by Bunga Maheshwar Rao on 28/10/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class TextFieldResTableViewCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var textFieldAnswer: UITextField!
    @IBOutlet weak var textViewQuestions: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textFieldAnswer.delegate = self
        textFieldAnswer.keyboardType = UIKeyboardType.numberPad
    }
    @IBAction func textedit(_ sender: UITextField) {
        if(textViewQuestions.tag == 13) {
            QuestionsData.text6 = sender.text!
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblLine.backgroundColor = UIColor.orange
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        lblLine.backgroundColor = UIColor.black
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
