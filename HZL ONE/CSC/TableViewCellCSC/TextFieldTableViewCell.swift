//
//  TextFieldTableViewCell.swift
//  HZL CSC
//
//  Created by Bunga Maheshwar Rao on 28/10/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var textFieldAnswer: UITextField!
    @IBOutlet weak var textViewQuestions: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldAnswer.delegate = self;
        textFieldAnswer.keyboardType = UIKeyboardType.numberPad
        // Initialization code
    }
    @IBAction func textEdit(_ sender: UITextField) {
        if(textViewQuestions.tag == 1) {
            QuestionsData.text1 = sender.text!
        } else  if(textViewQuestions.tag == 4) {
            QuestionsData.text2 = sender.text!
        } else  if(textViewQuestions.tag == 5) {
            QuestionsData.text3 = sender.text!
        }else  if(textViewQuestions.tag == 6) {
            QuestionsData.text4 = sender.text!
        }else  if(textViewQuestions.tag == 7) {
            QuestionsData.text5 = sender.text!
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        lblLine.backgroundColor = UIColor.orange
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        lblLine.backgroundColor = UIColor.black
        
            
            
        
        
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
