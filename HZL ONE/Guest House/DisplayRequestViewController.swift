//
//  DisplayRequestViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class DisplayRequestViewController:CommonVSClass {
    
    @IBOutlet weak var tableDisplayHazAction: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
  
        
        //barSetup()
        tableDisplayHazAction.rowHeight = 70
        tableDisplayHazAction.delegate = self;
        tableDisplayHazAction.dataSource = self;
        refresh.addTarget(self, action: #selector(Reportdata), for: .valueChanged)
        
        self.tableDisplayHazAction.addSubview(refresh)
        
        tableDisplayHazAction.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "My Request"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        Reportdata()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }

    var arrayHazAction = ["Room Request","Food Request" ]
    var arrayHazActionCount : [Int] = [0,0]
    
    
 
  
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    let reachability = Reachability()!
    @objc func Reportdata() {
        if(self.reachability.connection != .none) {
            let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
               let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let parameters = ["UserID": pno ,"AuthKey": Profile_AuthKey  ]
            
            self.startLoading()
            print(parameters)
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequest,parameters: parameters, successHandler: { (dict) in
                
                //self.stopLoading()
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    print(msg)
                    if(statusString == "ok")
                    {
                        self.stopLoading()
                        self.arrayHazActionCount = []
                        
                        
                        if let data = dict["data"]
                        {
                            
                            self.refresh.endRefreshing()
                            self.arrayHazActionCount.append(Int(data["MyRoomRequest"] as! NSNumber))
                            self.arrayHazActionCount.append(Int(data["MyFoodRequest"] as! NSNumber))
                           
                            
                            
                            
                            self.tableDisplayHazAction.reloadData();
                            
                           
                            
                        }
                        
                        
                    }
                    else
                    {self.stopLoading()
                        self.refresh.endRefreshing()
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                self.refresh.endRefreshing()
                self.stopLoading()
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
            // self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            self.refresh.endRefreshing()
            self.stopLoading()
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension DisplayRequestViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
      
        if(indexPath.row == 0){
        
            let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        
            let ReportedVC = storyBoard.instantiateViewController(withIdentifier: "RoomRequestReportedViewController") as! RoomRequestReportedViewController
        
            self.navigationController?.pushViewController(ReportedVC, animated: true)
        }else{
            let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
            
            let ReportedVC = storyBoard.instantiateViewController(withIdentifier: "FoodRequestRoportedViewController") as! FoodRequestRoportedViewController
            
            self.navigationController?.pushViewController(ReportedVC, animated: true)
        }
        
        
        
    }
}
extension DisplayRequestViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrayHazActionCount.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DisplayHazardTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
       
            if(arrayHazAction[indexPath.row] == "Open") {
                cell.lblShowHazAction.textColor = UIColor.darkText
            } else {
                cell.lblShowHazAction.textColor = UIColor.darkGray
            }
            print(arrayHazActionCount[indexPath.row])
            cell.lblShowHazAction.text = arrayHazAction[indexPath.row]
            cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
            cell.lblAddData.text = String(arrayHazActionCount[indexPath.row])
            return cell
            
       
        
    }
}

