//
//  SelectRoomswithDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 04/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import EzPopup
    var dataNameArray : [nameDataArray] = []
var dataNameListArray : [nameListArray] = []
  var roomType = String()
class SelectRoomswithDetailsViewController: CommonVSClass , UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate ,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    var rowData  = Int()
    var requestModel = RequestFoodNewData()
    @IBOutlet weak var btnAddRoom: UIButton!
    
   
   
    
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.callRoomData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "callRoomDataAdd")), object: nil)
        getRoomTypeList()
        
        
       
     valCnt = 0
     tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Select Room"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var indexing = 0
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func btnRoomTypeClicked(_ sender: UIButton) {

        
    }
    
    @IBAction func btnAddRoomClicked(_ sender: UIButton) {
        
        
        if(cellData.txtNoofRooms.text == "" || cellData.txtNoofRooms.text == "0"){
               self.view.makeToast("Enter No of Rooms")
        }
        else if(dataNameArray.count == 0){
            self.view.makeToast("Enter Information")
        }else{
            if(Int(cellData.txtNoofRooms.text!)! >
                dataNameArray.count){
                self.view.makeToast("Enter Correct Information")
            }else{
            
                
                var paramRoom : [[String:Any]] = []
                var paramName : [[String:Any]] = []
                var iiVal = -1
                for valueName in dataNameArray {
                    
                    
                    let paramNameValue = ["ReqDataID": String(iiVal),
                                          "Aadhar" : valueName.Contact ,
                                          "Addresss" : valueName.Address ,
                                          "Name": valueName.name
                        
                        ]
                        as [String:Any]
                    
                    
                    
                    
                    
                    
                    paramName.append(paramNameValue)
                    iiVal = iiVal - 1;
                }
                    
                    let paramRoomValue = ["ID": "-1",
                                          "Person" : String(dataNameArray.count) ,
                                           "Detail" : cellData.txtNoofRooms.text! ,
                                          "RoomType": roomType
                        
                        ]
                        as [String:Any]
                    
                    paramRoom.append(paramRoomValue)
                    
                    
                    
                
                    
                
                
                
                self.startLoadingPK(view: self.view)
                
                
                let param = ["Aadhar": requestModel.Aadhar,
                             "ArrivalTime": requestModel.ArrivalTime,
                             "BillToCompany": requestModel.BillToCompany,
                             "BreakFast": requestModel.BreakFast,
                             "CheckInDate": requestModel.CheckInDate,
                             "CheckOutDate": requestModel.CheckOutDate,
                             "Contact":requestModel.Contact,
                             "Dinner": requestModel.Dinner,
                             "EmpID": requestModel.EmpID,
                             "FromDate": requestModel.FromDate,
                             "HouseID": requestModel.HouseID,
                             "ID": requestModel.ID,
                             "Lunch": requestModel.Lunch,
                             "Name": requestModel.CompanyName,
                             "Notes": requestModel.Notes,
                             "Remark": requestModel.Remark,
                             "ReqDate": requestModel.ReqDate,
                             "ReqType": requestModel.ReqType,
                             "SelectedApprover": requestModel.SelectedApprover,
                             "Purpose": requestModel.purpose,
                             "ToDate": requestModel.ToDate
                    
                    
                    
                    
                    
                    
                    ]
                    as [String:Any]
                
                
                
                
                
                let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
                let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
                let parameter = ["UserID":empId,
                                 "AuthKey":Profile_AuthKey ,
                                 "Req":param ,
                                 "listmember":paramName ,
                                 "ListRoom":paramRoom
                    ] as [String:Any]
                
                
                
                print("parameter",parameter)
                
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.RoomRequest, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                    
                    let respon = response["response"] as! [String:AnyObject]
                    self.stopLoadingPK(view: self.view)
                    let objectmsg = MessageCallServerModel()
                    let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                    if respon["status"] as! String == "ok" {
                        
                        self.stopLoadingPK(view: self.view)
                        
                        MoveStruct.isMove = true
                        MoveStruct.message = message
                        self.navigationController?.popViewController(animated: false)
                        
                        
                    }else{
                        self.stopLoadingPK(view: self.view)
                        self.view.makeToast(message)
                    }
                    
                }) { (err) in
                    self.stopLoadingPK(view: self.view)
                    print(err.description)
                }
                
                
            
            }
        }
        
    }
    @objc func callRoomData(){
        self.tableView.reloadData()
    }
 let customAlertVC = NewroomAlertPersonViewController.instantiate()
   @IBAction func btnAddPersonClicked(_ sender: UIButton) {
    
                guard let customAlertVC = customAlertVC else { return }
    
    customAlertVC.name = ""
    customAlertVC.contact = ""
    let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: self.view.frame.width - 50 , popupHeight : 400)
                
                popupVC.cornerRadius = 5
                present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnDeletePersonClicked(_ sender: UIButton) {
        dataNameArray.remove(at: sender.tag)
        self.tableView.reloadData()
    }
    @IBAction func btnDeletePersonSingleClicked(_ sender: UIButton) {
        dataNameArray.remove(at: sender.tag)
        self.tableView.reloadData()
    }
    
//    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
//        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.coll)
//        let indexPath = self.collectionView.indexPathForRow(at: buttonPosition)
//
//
//
//
//    }
    
    var RoomTypeDB:[RoomTypeDataModel] = []
    
    var RoomTypeAPI = RoomTypeDataAPI()
    var RoomTypeIdArray : [String] = []
    var RoomTypeListArray : [String] = []
    var RoomTypeColorArray : [Bool] = []
    @objc func getRoomTypeList(){
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        self.RoomTypeAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.RoomTypeDB = [RoomTypeDataModel]()
            self.RoomTypeDB = dict as! [RoomTypeDataModel]
            if(self.RoomTypeDB.count > 0){
                for i in 0...self.RoomTypeDB.count - 1 {
                    self.RoomTypeListArray.append(self.RoomTypeDB[i].Name)
                    self.RoomTypeIdArray.append(String(self.RoomTypeDB[i].ID))
                    self.RoomTypeColorArray.append(false)
                }
                self.tableView.reloadData()
               
            }
            
            
            
            
        }
    }
    
    
    var countText = Int()

    
    
  
    var cellData : NoofroomsTableViewCell!
    var cellDataCollect : RoomTypeDataTableViewCell!
    var valCnt = 0
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

            return 0.0
       
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 1
        }
        if(section == 2){
            if(dataNameArray.count > 0){
            return dataNameArray.count
            }else{
               return valCnt
            }
        }else{
        return valCnt
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellRoomTypeData", for: indexPath) as! RoomTypeDataTableViewCell
            cell.collectionViewRoomType.delegate = self
            cell.collectionViewRoomType.dataSource = self
            if(RoomTypeListArray.count > 0){
                var v1 = RoomTypeListArray.count / 2
                let v2 = RoomTypeListArray.count % 2
                if(v2 != 0){
                    v1 = v1 + 1
                }
                cell.collectionHeightCon.constant = CGFloat(v1 * 40) + 20
               
            }
       
            cell.collectionViewRoomType.reloadData()
            return cell
            
        } else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoofRooms", for: indexPath) as! NoofroomsTableViewCell
            
           cellData = cell
            
            cell.txtNoofRooms.text = "1"
            return cell
        }else{
            if(dataNameArray.count > 0){
            if(indexPath.row == dataNameArray.count - 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellPersonAdd", for: indexPath) as! RoomAddPersonTableViewCell
                cell.lblName.text = dataNameArray[indexPath.row].name
                cell.lblContact.text = dataNameArray[indexPath.row].Contact
                cell.textViewAddress.text = dataNameArray[indexPath.row].Address
                cell.lblSiNo.text = String(indexPath.row + 1)
                cell.btnDelete.tag = indexPath.row
                cell.btnAdd.tag = indexPath.row
                return cell
        
            }else{
               
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellPerson", for: indexPath) as! RoomPersonTableViewCell
                cell.lblName.text = dataNameArray[indexPath.row].name
                cell.lblContact.text = dataNameArray[indexPath.row].Contact
                cell.textViewAddress.text = dataNameArray[indexPath.row].Address
                cell.lblSiNo.text = String(indexPath.row + 1)
                cell.btnDelete.tag = indexPath.row
                return cell
            }
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellPersonNo", for: indexPath) as! UITableViewCell
               
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableViewAutomaticDimension
        
    }
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(self.RoomTypeListArray.count)
        return self.RoomTypeListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellRoomType", for: indexPath) as! RoomTypeGuestHouseCollectionViewCell

        cell.lblViewRoomtype.text = RoomTypeListArray[indexPath.row]
        cell.lblViewRoomtype.textColor = UIColor.orange
        if(self.RoomTypeColorArray[indexPath.row] == true){
            cell.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        }else{
            cell.backgroundColor = UIColor(hexString: "fafafa", alpha: 1.0)
        }
        

        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:165, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellRoomType", for: indexPath) as! RoomTypeGuestHouseCollectionViewCell
        roomType = RoomTypeIdArray[indexPath.row]
        dataNameArray = []
        self.RoomTypeColorArray = []
        if(self.RoomTypeListArray.count > 0){
            for i in 0...self.RoomTypeListArray.count - 1 {
                self.RoomTypeColorArray.append(false)
            }
        }
        self.RoomTypeColorArray[indexPath.row] = true
        
        valCnt = 1
                self.tableView.reloadData()
        
        
        
        if(self.requestModel.ReqType == "1"){
            var name = String()
            let mobileData = UserDefaults.standard.string(forKey: "mobileData")!
            let fname = UserDefaults.standard.string(forKey: "FirstName")
            let Mname = UserDefaults.standard.string(forKey: "MiddleName")
            let Lname = UserDefaults.standard.string(forKey: "Lastname")
            
            
            if(Mname == ""){
                name = fname! + " " + Lname!
            }else{
                name = fname! + " " + Mname!  + " " + Lname!
            }
            
            guard let customAlertVC = self.customAlertVC else { return }
            customAlertVC.name = name
            customAlertVC.contact = mobileData
            
            let popupVC1 = PopupViewController(contentController: customAlertVC, popupWidth: self.view.frame.width - 50 , popupHeight : 400)
            
            popupVC1.cornerRadius = 5
            self.present(popupVC1, animated: true, completion: nil)
        }
        
        //print(indexPath.item)
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class nameListArray :  NSObject {
    
    
    var nameArray = [nameDataArray]()
     var collapsed = Bool()
    var GuestType = String()
    var GuestTypeId = String()
    

}
class nameDataArray :  NSObject {
    
    
    var name = String()
    var Contact = String()
    var Address = String()
    
}



class GuestName :  NSObject {
    
    
    var Sino = String()
    var name = String()
    
    
    
}
