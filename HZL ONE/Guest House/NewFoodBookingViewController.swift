//
//  NewFoodBookingViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//


import UIKit
import DLRadioButton
import SwiftyJSON
class NewFoodBookingViewController: CommonVSClass , WWCalendarTimeSelectorProtocol {
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getGuestList()
        getApproverList()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
      var reachablty = Reachability()!
    @IBAction func btnFoodRequestClicked(_ sender: UIButton) {
        
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if cellData.lblGuest.text == ""  || cellData.lblGuest.text == "Select"{
            
            self.view.makeToast("Select Guest")
        } else if requestFor == "" {
            
            self.view.makeToast("Select Request For")
        } else if cellData.txtName.text == "" {
            
            self.view.makeToast("Enter Name")
        } else if cellData.txtContact.text == "" {
            
            self.view.makeToast("Enter Contact")
        }else if billing == "" {
            
            self.view.makeToast("Select Bill To")
        }else if(cellData.textViewPurpose.text == "" ){
            self.view.makeToast("Write Purpose")
        }else if dataListFood.count == 0 {
            
            self.view.makeToast("Please Add Food Details")
        }
        else{
            if(billing == "Company"){
             if cellData.lblLocationApp.text == "" || cellData.lblLocationApp.text == "Select" {
                
                self.view.makeToast("Select Location Approver")
             }else{
                saveData(approval: self.idApproval)
                }
            }else{
                let empIdVal : String = UserDefaults.standard.string(forKey: "EmployeeID")!
                saveData(approval : empIdVal)
            }
            
            

            
            
            
        }

    }
    
    func saveData(approval : String){
        let IDData : String = UserDefaults.standard.string(forKey: "IDData")!
        
        
        
        
        var paramFood  : [[String:Any]] = []
        var iiVal = 0
        for value in dataListFood {
            
            let dateSetFor = "/Date(" + value.forDateMilliSec + ")/"
            let dateSetTo = "/Date(" + value.toDateMilliSec + ")/"
            
            
            
            
            let paramFoodValue = ["ForDate": dateSetFor,
                                  "ToDt": dateSetTo ,
                                  "FType": value.foodType,
                                  "Persons": value.count ,
                                  "ID": "0"
                
                ]
                as [String:Any]
            iiVal = iiVal + 1;
            
            paramFood.append(paramFoodValue)
            
            
            
            
        }
       
        
        self.startLoadingPK(view: self.view)
        
        
        let param = ["ID": "0",
                     "EmpID": IDData,
                     
                     "Name": cellData.txtName.text!,
                     "Contact": cellData.txtContact.text!,
                     "HouseID": idGuest,
                     "Remark": "",
                     "ReqType": requestForId ,
                     "Purpose": cellData.textViewPurpose.text! ,
                     "SelectedApprover": approval
            
            
            
            
            
            
            ]
            as [String:String]
        
        
        
        
        
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let parameter = ["UserID":empId,
                         "AuthKey":Profile_AuthKey ,
                         "Req":param ,
                         "listFood":paramFood
            ] as [String:Any]
        
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.FoodRequest, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoadingPK(view: self.view)
            print(respon)
            let objectmsg = MessageCallServerModel()
            
            let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
            
            if respon["status"] as! String == "ok" {
               
                self.stopLoadingPK(view: self.view)
                
                MoveStruct.isMove = true
                MoveStruct.message = message
                self.navigationController?.popViewController(animated: false)
                
                
            }else{
                self.stopLoadingPK(view: self.view)
                self.view.makeToast(message)
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            print(err.description)
        }
        
        
        
    }
    
    @IBAction func btnDeleteClicked(_ sender: UIButton) {
        
        dataListFood.remove(at: sender.tag)
        tableView.reloadData()
    }
   
    
    var approverDB:[ApproverListModel] = []
    
    var approverAPI = ApproverListFoodDataAPI()
    @objc func getApproverList(){
       
            let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
            
            self.approverAPI.serviceCalling(obj: self , param : paramm) { (dict) in
                
                self.approverDB = [ApproverListModel]()
                self.approverDB = dict as! [ApproverListModel]
                if(self.approverDB.count > 0){
                    for i in 0...self.approverDB.count - 1 {
                        self.ApproverListArray.append(self.approverDB[i].Full_Name!)
                        self.ApproverIdArray.append(self.approverDB[i].ID!)
                    }
                }
                
                
                
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "New Food Booking"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        tableView.reloadData()
        
    }
    
    
    
    var guestDB:[GuestHouseModel] = []
    
    var guestAPI = GuestHouseFoodDataAPI()
    @objc func getGuestList(){
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        self.guestAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.guestDB = [GuestHouseModel]()
            self.guestDB = dict as! [GuestHouseModel]
            if(self.guestDB.count > 0){
                for i in 0...self.guestDB.count - 1 {
                    self.GuestListArray.append(self.guestDB[i].Name!)
                    self.GuestIdArray.append(self.guestDB[i].ID!)
                }
            }
            
            
            
            
        }
    }
    var idGuest = String()
    var GuestListArray :[String] = []
    var GuestIdArray :[String] = []
    @IBAction func btnGuestHouseClicked(_ sender: UIButton) {
//        if(guestDB.count == 0){
//            getGuestList()
//        }
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.GuestListArray
            
            popup.sourceView = self.cellData.lblGuest
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblGuest.text = self.GuestListArray[row]
                self.cellData.lblGuest.textColor = UIColor.black
                self.idGuest = self.GuestIdArray[row]
               
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    
    var ApproverListArray :[String] = []
    var ApproverIdArray :[String] = []
    var requestFor = String()
    var requestForId = String()
    @IBAction func btnReuestForClicked(_ sender: DLRadioButton) {
        for button in sender.selectedButtons() {
            
            requestFor =   button.titleLabel!.text!
        }
     requestForData()
    }
    
    
    func requestForData(){
        if(requestFor == "Self"){
            var name = String()
            let mobileData = UserDefaults.standard.string(forKey: "mobileData")!
            let fname = UserDefaults.standard.string(forKey: "FirstName")
            let Mname = UserDefaults.standard.string(forKey: "MiddleName")
            let Lname = UserDefaults.standard.string(forKey: "Lastname")
            requestForId = "1"
            
            if(Mname == ""){
                name = fname! + " " + Lname!
            }else{
                name = fname! + " " + Mname!  + " " + Lname!
            }
            cellData.txtName.text = name
            
            cellData.txtContact.text = mobileData
            cellData.txtName.isEnabled = false
            cellData.txtContact.isEnabled = false
        }else{
            requestForId = "2"
            cellData.txtName.text = ""
            cellData.txtContact.text = ""
            cellData.txtName.isEnabled = true
            cellData.txtContact.isEnabled = true
        }
    }
    
    
    
    
    var billing = String()
    @IBAction func btnBillingClicked(_ sender: DLRadioButton) {
       
        for button in sender.selectedButtons() {
            
            billing =   button.titleLabel!.text!
        }
        billingData()
    }
    func billingData(){
        if(billing == "Company"){
            cellData.imageHeightCon.constant = 20
            cellData.lblHeightCon.constant = 21
            cellData.viewHeightCon.constant = 70
            cellData.locationHeightCon.constant = 40
        }else{
            cellData.imageHeightCon.constant = 0
            cellData.lblHeightCon.constant = 0
            cellData.viewHeightCon.constant = 0
            cellData.locationHeightCon.constant = 0
        }
         cellData.lblLocationApp.text = "Select"
        idApproval = String()
        billingWork = true
        tableView.reloadData()
    }
    var breakFast = String()
    var lunch = String()
    var dinner = String()
    @IBAction func btnFoodDetailClicked(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "FoodDetailsViewController") as! FoodDetailsViewController
        
        self.navigationController?.pushViewController(submitVC, animated: true)
    }
  
    
    var idApproval = String()
    @IBAction func btnLocationApprovalClicked(_ sender: UIButton) {
//        if(self.approverDB.count == 0){
//            getApproverList()
//        }
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.ApproverListArray
            
            popup.sourceView = self.cellData.lblLocationApp
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblLocationApp.text = self.ApproverListArray[row]
                self.cellData.lblLocationApp.textColor = UIColor.black
                self.idApproval =  self.ApproverIdArray[row]
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    var billingWork : Bool = false
    var cellData : NewFoodBookingTableViewCell!
    
  
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension NewFoodBookingViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
     
            return UITableViewAutomaticDimension
        
        
    }
}
extension NewFoodBookingViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1){
            return  dataListFood.count
        }else{
        return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewFoodBookingTableViewCell
        
        cellData = cell
            if(dataListFood.count > 0){
                cellData.lblNoData.isHidden = true
            }else{
                cellData.lblNoData.isHidden = false
            }
           // cell
            if(cell.lblGuest.text == ""){
        cell.lblGuest.text = "Select"
                cell.lblGuest.textColor = UIColor.lightGray
            }
            if(cell.lblLocationApp.text == ""){
            cell.lblLocationApp.text = "Select"
                   cell.lblLocationApp.textColor = UIColor.lightGray
            }
            if(requestFor == String()){
            cell.btnSelf.isSelected = true
            requestFor = "Self"
                requestForData()
            }
            if(billing == String()){
            cell.btnCompany.isSelected = true
            billing = "Company"
                cell.imageHeightCon.constant = 20
                cell.lblHeightCon.constant = 21
                cell.viewHeightCon.constant = 70
                cell.locationHeightCon.constant = 40
                billingWork = true
            }
            if( billingWork == true){
                cell.frame.size.height = cell.frame.size.height - 70
                billingWork = false
            }else{
                cell.frame.size.height = cell.frame.size.height
                 billingWork = false
            }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        } else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellFood", for: indexPath) as! FoodDetailsDataTableViewCell
            
    cell.lblFoodType.text = dataListFood[indexPath.row].foodType
       cell.lblTo.text = dataListFood[indexPath.row].toDate
            cell.lblFor.text = dataListFood[indexPath.row].forDate
            cell.btnClose.tag = indexPath.row
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! ButtonFoodTableViewCell
            
 
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }
        
        
        
    }
}




class FoodDetailingModel: NSObject {
    
    var foodType = String()
    var count = String()
    var forDate = String()
    var toDate = String()
    var forDateMilliSec = String()
    var toDateMilliSec = String()
    
}
