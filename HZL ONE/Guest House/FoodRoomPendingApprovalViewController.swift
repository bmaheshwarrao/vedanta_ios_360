//
//  FoodRoomPendingApprovalViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 19/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import EzPopup
class FoodRoomPendingApprovalViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var FoodRoomDB:[PendingApprovalGuestHouseDataModel] = []
    var FoodRoomAPI = FoodRoomPendingApprovalDataAPI()
    
    var FoodLoadMoreDB : [PendingApprovalGuestHouseDataModel] = []
    
    
    
    
    var FoodRoomLoadMoreAPI = FoodRoomPendingApprovalDataAPILoadMore()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFoodData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getFoodData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.approveData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "ApproveGuestHouse")), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.rejectData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "RejectGuestHouse")), object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Food Request"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getFoodData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @objc func approveData(){
       
        var strUrl = String()
        if(typeData == "1"){
            strUrl = URLConstants.RoomApproverApprove
        }else if(typeData == "2"){
            strUrl = URLConstants.RoomAdminApprove
        }else if(typeData == "3"){
            strUrl = URLConstants.FoodApproverApprove
        }else if(typeData == "4"){
            strUrl = URLConstants.FoodAdminApprove
        }
        DataSave(urlString: strUrl, strStatus: "2")
    }
    @objc func rejectData(){
        var strUrl = String()
        if(typeData == "1"){
            strUrl = URLConstants.RoomApproverApprove
        }else if(typeData == "2"){
            strUrl = URLConstants.RoomAdminApprove
        }else if(typeData == "3"){
            strUrl = URLConstants.FoodApproverApprove
        }else if(typeData == "4"){
            strUrl = URLConstants.FoodAdminApprove
        }
        DataSave(urlString: strUrl, strStatus: "3")
    }
    @objc func DataSave(urlString : String , strStatus : String){
        //let IDData : String = UserDefaults.standard.string(forKey: "IDData")!
        
        
       
        
        
        self.startLoadingPK(view: self.view)
        
  
    

        
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let parameter = ["UserID":empId,
                         "AuthKey":Profile_AuthKey ,
                         "ReqID":ReqId ,
                         "Status":strStatus
            ] as [String:Any]
        
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: urlString, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoadingPK(view: self.view)
            let objectmsg = MessageCallServerModel()
            
            let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
            
            
            
            if respon["status"] as! String == "ok" {
                
                self.stopLoadingPK(view: self.view)
                
                self.ReqId = String()
               self.view.makeToast(message)
                self.getFoodData()
                
            }else{
                self.stopLoadingPK(view: self.view)
                self.view.makeToast(message)
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            print(err.description)
        }
        
        
    }
    var ReqId = String()
  //  let ActionTakenAlertVC = RACPopupViewController.instantiate()
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        ReqId =  String(self.FoodRoomDB[(indexPath?.row)!].ID)
        print(self.FoodRoomDB[(indexPath?.row)!].HouseName)
//        guard let customAlertVC = ActionTakenAlertVC else { return }
        let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        let customAlertVC = storyBoard.instantiateViewController(withIdentifier: "RACPopupViewController") as! RACPopupViewController
        customAlertVC.pendingApprovalDB = self.FoodRoomDB[(indexPath?.row)!]
        customAlertVC.typeString = typeData
        let popupVC = PopupViewController(contentController: customAlertVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: self.view.frame.height - 100)
       
        popupVC.cornerRadius = 5
        popupVC.canTapOutsideToDismiss = true
        present(popupVC, animated: true, completion: nil)
        
     
        //
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func getFoodData(){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        param = ["UserID": pno ,"AuthKey": Profile_AuthKey , "Type" : typeData ]
        print(param)
        FoodRoomAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.FoodRoomDB = dict as! [PendingApprovalGuestHouseDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    var countData = 1
    @objc func getFoodDataLoadMore(ID : String){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        param = ["UserID": pno ,"AuthKey": Profile_AuthKey , "Type" : typeData ,"pn":String(countData) ]
        
        FoodRoomLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.FoodLoadMoreDB =  [PendingApprovalGuestHouseDataModel]()
            self.FoodLoadMoreDB = dict as! [PendingApprovalGuestHouseDataModel]
            
            switch dict.count {
            case 0:
                //self.countData = self.countData + 1
                break;
            default:
                self.FoodRoomDB.append(contentsOf: self.FoodLoadMoreDB)
                self.tableView.reloadData()
                self.countData = self.countData + 1
                break;
            }
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.FoodRoomDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(typeData == "3" || typeData == "4"){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFood", for: indexPath) as! FoodrequestreportedTableViewCell
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        
        
        cell.containerView.cornerRadius = 10;
        
        cell.textViewGuestName.text = self.FoodRoomDB[indexPath.row].HouseName
        
        cell.lblName.text = self.FoodRoomDB[indexPath.row].Name
        cell.lblContact.text = self.FoodRoomDB[indexPath.row].Contact
        
        
        let milisecondDate = self.FoodRoomDB[indexPath.row].ReqDate
        let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
        let dateFormatterday = DateFormatter()
        dateFormatterday.dateFormat = "dd"
        let valDate =  dateFormatterday.string(from: dateVarDate)
        cell.lblDate.text = valDate
        let dateFormattermon = DateFormatter()
        dateFormattermon.dateFormat = "MMM"
        let valDateMon =  dateFormattermon.string(from: dateVarDate)
        cell.lblMonth.text = valDateMon
        
        let dateFormatterWeekDay = DateFormatter()
        dateFormatterWeekDay.dateFormat = "EEEE"
        let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
        cell.lblDay.text = valDateWeekDay
        
        
        
        
        
        
        
        
        
        self.data = String(self.FoodRoomDB[indexPath.row].ID)
        self.lastObject = String(self.FoodRoomDB[indexPath.row].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.FoodRoomDB.count - 1)
        {
            
            self.getFoodDataLoadMore( ID: String(Int(self.FoodRoomDB[indexPath.row].ID)))
            
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellRoom", for: indexPath) as! RoomRequestReportedTableViewCell
            
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
            cell.textViewGuestHouse.text = self.FoodRoomDB[indexPath.row].HouseName
            
//            cell.lblNameNo.text = self.RoomDB[indexPath.row].Name
//            cell.lblContact.text = self.RoomDB[indexPath.row].Contact
            cell.containerView.layer.cornerRadius = 10
            
            let milisecondDate = self.FoodRoomDB[indexPath.row].ReqDate
            let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
            let dateFormatterday = DateFormatter()
            dateFormatterday.dateFormat = "dd"
            let valDate =  dateFormatterday.string(from: dateVarDate)
            cell.lblDateTop.text = valDate
            let dateFormattermon = DateFormatter()
            dateFormattermon.dateFormat = "MMM"
            let valDateMon =  dateFormattermon.string(from: dateVarDate)
            cell.lblMonthTop.text = valDateMon
            
            let dateFormatterWeekDay = DateFormatter()
            dateFormatterWeekDay.dateFormat = "EEEE"
            let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
            cell.lblDay.text = valDateWeekDay
            
            
            
            
            
            let milisecondCheckIn = self.FoodRoomDB[indexPath.row].CheckInDate
            let dateVarCheckIn = Date.init(timeIntervalSince1970: TimeInterval(milisecondCheckIn)!/1000)
            let dateFormatterdayCheckIn = DateFormatter()
            dateFormatterdayCheckIn.dateFormat = "dd"
            let valDateCheckIn =  dateFormatterdayCheckIn.string(from: dateVarCheckIn)
            cell.lblDateFor.text = valDateCheckIn
            let dateFormattermonCheckIn = DateFormatter()
            dateFormattermonCheckIn.dateFormat = "MMM"
            let valDateMonCheckIn =  dateFormattermonCheckIn.string(from: dateVarCheckIn)
            cell.lblMonthFor.text = valDateMonCheckIn
            
            
            
            
            
            
            
            
            
            
            
            let milisecondCheckOut = self.FoodRoomDB[indexPath.row].CheckOutDate
            let dateVarCheckOut = Date.init(timeIntervalSince1970: TimeInterval(milisecondCheckOut)!/1000)
            let dateFormatterdayCheckOut = DateFormatter()
            dateFormatterdayCheckOut.dateFormat = "dd"
            let valDateCheckOut =  dateFormatterdayCheckOut.string(from: dateVarCheckOut)
            cell.lblDateTo.text = valDateCheckOut
            let dateFormattermonCheckOut = DateFormatter()
            dateFormattermonCheckOut.dateFormat = "MMM"
            let valDateMonCheckOut =  dateFormattermonCheckOut.string(from: dateVarCheckOut)
            cell.lblMonthTo.text = valDateMonCheckOut
            
            
            
            
            
            
            self.data = String(self.FoodRoomDB[indexPath.row].ID)
            self.lastObject = String(self.FoodRoomDB[indexPath.row].ID)
            
            if ( self.data ==  self.lastObject && indexPath.section == self.FoodRoomDB.count - 1)
            {
                
                self.getFoodDataLoadMore( ID: String(Int(self.FoodRoomDB[indexPath.row].ID)))
                
            }
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
