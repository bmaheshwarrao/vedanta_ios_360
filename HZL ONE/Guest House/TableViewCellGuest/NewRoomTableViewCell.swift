//
//  NewRoomTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 22/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class NewRoomTableViewCell: UITableViewCell {

    @IBOutlet weak var btnGuest: DLRadioButton!
    @IBOutlet weak var btnSelf: DLRadioButton!
    @IBOutlet weak var btnGuestView: UIButton!
    @IBOutlet weak var lblGuest: UILabel!
    @IBOutlet weak var viewGuest: UIView!
    
    
    @IBOutlet weak var viewCompanyName: UIView!
    @IBOutlet weak var txtCompanyName: UITextField!
   
    
    @IBOutlet weak var companyViewHeightCon: NSLayoutConstraint!
    @IBOutlet weak var companyLblheightCon: NSLayoutConstraint!
    @IBOutlet weak var viewComapnyHeightCon: NSLayoutConstraint!
    @IBOutlet weak var btnDinner: DLRadioButton!
    @IBOutlet weak var btnLunch: DLRadioButton!
    @IBOutlet weak var btnBreakFast: DLRadioButton!
    @IBOutlet weak var btnBillable: DLRadioButton!
    
    
    @IBOutlet weak var viewLocationApp: UIView!
    
    @IBOutlet weak var btnLocationApp: UIButton!
    @IBOutlet weak var lblLocationApp: UILabel!
    @IBOutlet weak var viewCheckin: UIView!
    
    @IBOutlet weak var btnCheckIn: UIButton!
    @IBOutlet weak var lblCheckIn: UILabel!
    
    @IBOutlet weak var textViewPurpose: UITextView!
    @IBOutlet weak var viewCheckOut: UIView!
    
    @IBOutlet weak var btnCheckOut: UIButton!
    @IBOutlet weak var lblCheckOut: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewGuest.layer.borderWidth = 1.0
        viewGuest.layer.borderColor = UIColor.black.cgColor
        viewGuest.layer.cornerRadius = 10.0
        
        viewCompanyName.layer.borderWidth = 1.0
        viewCompanyName.layer.borderColor = UIColor.black.cgColor
        viewCompanyName.layer.cornerRadius = 10.0
        
        textViewPurpose.layer.borderWidth = 1.0
        textViewPurpose.layer.borderColor = UIColor.black.cgColor
        textViewPurpose.layer.cornerRadius = 10.0
        
        viewCheckin.layer.borderWidth = 1.0
        viewCheckin.layer.borderColor = UIColor.black.cgColor
        viewCheckin.layer.cornerRadius = 10.0
        
        viewCheckOut.layer.borderWidth = 1.0
        viewCheckOut.layer.borderColor = UIColor.black.cgColor
        viewCheckOut.layer.cornerRadius = 10.0
        
        viewLocationApp.layer.borderWidth = 1.0
        viewLocationApp.layer.borderColor = UIColor.black.cgColor
        viewLocationApp.layer.cornerRadius = 10.0
        self.btnBillable.isMultipleSelectionEnabled = true;
        
        self.btnBreakFast.isMultipleSelectionEnabled = true;
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
