//
//  RoomRequestReportedTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RoomRequestReportedTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var textViewMobile: UITextView!
    @IBOutlet weak var textViewDesc: UITextView!
//    @IBOutlet weak var lblNameNo: UILabel!
//    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var textViewGuestHouse: UITextView!
    
    @IBOutlet weak var textViewName: UITextView!
    
    @IBOutlet weak var lblDateTop: UILabel!
    @IBOutlet weak var lblMonthTop: UILabel!
     @IBOutlet weak var lblDay: UILabel!
    
    
    @IBOutlet weak var textViewMobileHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDateFor: UILabel!
    @IBOutlet weak var lblMonthFor: UILabel!
    
    @IBOutlet weak var lblDateTo: UILabel!
    @IBOutlet weak var lblMonthTo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
