//
//  RoomRequestDetailsFoodRequestTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RoomRequestDetailsFoodRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBreakfast: UILabel!
    @IBOutlet weak var lbllunch: UILabel!
    @IBOutlet weak var lblDinner: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
