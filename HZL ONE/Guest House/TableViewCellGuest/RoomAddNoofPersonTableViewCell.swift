//
//  RoomAddNoofPersonTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 04/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RoomAddNoofPersonTableViewCell: UITableViewCell {

    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var txtPersonNo: UITextField!
    @IBOutlet weak var viewNoofPerson: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtPersonNo.keyboardType = UIKeyboardType.numberPad
        viewNoofPerson.layer.borderWidth = 1.0
        viewNoofPerson.layer.borderColor = UIColor.black.cgColor
        viewNoofPerson.layer.cornerRadius = 10.0
        txtPersonNo.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
