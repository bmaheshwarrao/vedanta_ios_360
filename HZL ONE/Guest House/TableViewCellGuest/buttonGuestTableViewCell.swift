//
//  buttonGuestTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 04/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
import CoreData
var dataRoomType : [RoomTypeDataModel] = []
var selectedInt = -1
class buttonGuestTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var heightCollect: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
//        buttonCollectionView.isScrollEnabled = true
        // Initialization code
    }
    var reachablty = Reachability()!
    func ButtonGuestCallingReload(obj : SelectRoomswithDetailsViewController)
    {
        dataNameArray = []
        var calInt = 0
        if(dataRoomType.count % 3 == 0){
            let div = (dataRoomType.count / 3 )
            calInt = div
        }else  if(dataRoomType.count % 3 != 0){
            let div = (dataRoomType.count / 3 ) + 1
            calInt = div
        }
        self.heightCollect.constant = CGFloat(calInt * 70)
        self.collectionView.reloadData()
        obj.indexing = 1
        
    }
    func ButtonGuestCalling(obj : SelectRoomswithDetailsViewController)
    {
        
        
        if(reachablty.connection != .none)
        {
            
            let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.RoomType, parameters: paramm , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            dataRoomType = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = RoomTypeDataModel()
                                    object.setDataInModel(str: cell)
                                    dataRoomType.append(object)
                                }
                                
                            }
                            var calInt = 0
                            if(dataRoomType.count % 3 == 0){
                                let div = (dataRoomType.count / 3 )
                                calInt = div
                            }else  if(dataRoomType.count % 3 != 0){
                                let div = (dataRoomType.count / 3 ) + 1
                                calInt = div
                            }
                            self.heightCollect.constant = CGFloat(calInt * 70)
                            self.collectionView.reloadData()
                            obj.tableView.reloadData()
                            obj.indexing = 1
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                
            }
            
            
        }
        else{
            
        }
        
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataRoomType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellButton", for: indexPath) as! buttonGuestCollectionViewCell
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        paragraph.lineSpacing = 0
      cell.btnRoomType.tag = indexPath.row
          let titleStr = NSMutableAttributedString(string: dataRoomType[indexPath.row].Name, attributes: [NSAttributedStringKey.foregroundColor: UIColor.orange, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
         titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: titleStr.length))
        cell.textViewRoomType.attributedText = titleStr
        if(selectedInt == indexPath.row){
            cell.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        }else{
            cell.backgroundColor = UIColor.white
        }
        return cell
        
       
  
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let wid = (UIScreen.main.bounds.width - 30) / 3
        print(wid)
        return CGSize(width: wid, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
        
    }
}

class RoomTypeDataModel: NSObject {
    
    
    var ID = Int()
    var Name = String()
    var ForVIP = String()
    var ForParty = String()
    
    var isSelected = Bool()
    
    
    

    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Name"] is NSNull || str["Name"] == nil{
            self.Name = ""
        }else{
            
            let tit = str["Name"]
            
            self.Name = (tit?.description)!
            
            
        }
        if str["ForVIP"] is NSNull || str["ForVIP"] == nil{
            self.ForVIP =  ""
        }else{
            
            let desc = str["ForVIP"]
            
            self.ForVIP = (desc?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["ForParty"] is NSNull || str["ForParty"] == nil{
            self.ForParty = ""
        }else{
            let catId = str["ForParty"]
            
            self.ForParty = (catId?.description)!
            
        }
        
        self.isSelected = false
        
    }
    
}

