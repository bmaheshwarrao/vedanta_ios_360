//
//  RoomRequestDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RoomRequestDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonthTop: UILabel!
    @IBOutlet weak var lblDateTop: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    
    
    @IBOutlet weak var lblMemberCount: UILabel!
    @IBOutlet weak var lblMonthFor: UILabel!
    @IBOutlet weak var lblDateFor: UILabel!
    @IBOutlet weak var lblWeekDayFor: UILabel!
    @IBOutlet weak var lblTimeFor: UILabel!
    @IBOutlet weak var lblMonthTo: UILabel!
    @IBOutlet weak var lblDateTo: UILabel!
    @IBOutlet weak var lblWeekDayTo: UILabel!
    @IBOutlet weak var lblTimeTo: UILabel!
    
    
    @IBOutlet weak var textViewGuestName: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
