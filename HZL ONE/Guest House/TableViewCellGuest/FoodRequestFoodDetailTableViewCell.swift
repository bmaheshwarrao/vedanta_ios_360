//
//  FoodRequestFoodDetailTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FoodRequestFoodDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var lblMember: UILabel!
    @IBOutlet weak var lblMonthFrom: UILabel!
    @IBOutlet weak var lblDateFrom: UILabel!
    @IBOutlet weak var lblFoodType: UILabel!
    
    @IBOutlet weak var lblMonthTo: UILabel!
    @IBOutlet weak var lblDateTo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
