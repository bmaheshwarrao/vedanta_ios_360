//
//  RoomRequestDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import EzPopup
class RoomRequestDetailsViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var RoomRequestDB = RoomRequestReportedDataModel()
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    var statusData = String()
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnApprove: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(statusData == "2"){
            btnCancel.isHidden = true
            btnReject.isHidden = false
            btnApprove.isHidden = false
            stackView.isHidden = false
        }else{
            if RoomRequestDB.Cancelled ==  true {
                btnCancel.isHidden = true
                btnCancel.isEnabled = false
            }else{
                btnCancel.setTitle("Cancel", for: .normal)
                btnCancel.isEnabled = true
            }
           
            btnReject.isHidden = true
            btnApprove.isHidden = true
            stackView.isHidden = true
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getDetails), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
         NotificationCenter.default.addObserver(self, selector: #selector(self.ProcessDataRoom), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "RoomGuestHouse")), object: nil)
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnMembersClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "MembersListViewController") as! MembersListViewController
        ZIVC.RoomRequestDB  = self.RoomRequestDB
        let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: self.view.frame.height - 100)
        popupVC.cornerRadius = 5
        popupVC.canTapOutsideToDismiss = true
        present(popupVC, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Booking Details"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getDetails()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    
    var typeData = String()
    var myAppovalRejectType = String()
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        //        typeData = "CancelFood"
        //        alertshow()
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to Cancel Request?", preferredStyle: .actionSheet)
        
        let Alertcancel = UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            
            self.startLoadingPK(view: self.view)
            
            
            
            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let parameter = ["UserID":empId,
                             "AuthKey":Profile_AuthKey ,
                             "ReqID":String(self.RoomRequestDB.ID)
                ] as [String:Any]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.RoomCancel, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                
                let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "ok" {
                    
                    self.stopLoadingPK(view: self.view)
                    MoveStruct.isMove = true
                    MoveStruct.message = message
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(message)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
            
            
        })
        
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (alert) in
            
        })
        
        actionSheetController.addAction(Alertcancel)
        
        actionSheetController.addAction(cancel)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    @IBAction func btnApproveClicked(_ sender: UIButton) {
        myAppovalRejectType = "2"
        
        alertshow()
    }
    @IBAction func btnRejectClicked(_ sender: UIButton) {
        myAppovalRejectType = "3"
        
        alertshow()
    }
    @objc func alertshow(){
        let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        let customAlertVC = storyBoard.instantiateViewController(withIdentifier: "RACPopupViewController") as! RACPopupViewController
        
        customAlertVC.typeString = "Room"
        let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: self.view.frame.width - 50, popupHeight: 250)
        
        popupVC.cornerRadius = 5
        popupVC.canTapOutsideToDismiss = true
        present(popupVC, animated: true, completion: nil)
    }
      var typeDataVal = String()
    @objc func ProcessDataRoom(){
        
        var strUrl = String()
        if(typeDataVal == "1"){
            strUrl = URLConstants.RoomApproverApprove
        }else if(typeDataVal == "2"){
            strUrl = URLConstants.RoomAdminApprove
        }else if(typeDataVal == "3"){
            strUrl = URLConstants.FoodApproverApprove
        }else if(typeDataVal == "4"){
            strUrl = URLConstants.FoodAdminApprove
        }
        DataSave(urlString: strUrl, strStatus: myAppovalRejectType)
    }
    @objc func DataSave(urlString : String , strStatus : String){
        //let IDData : String = UserDefaults.standard.string(forKey: "IDData")!
        
        
        
        
        
        self.startLoadingPK(view: self.view)
        
        
        
        
        
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let parameter = ["UserID":empId,
                         "AuthKey":Profile_AuthKey ,
                         "ReqID":String(RoomRequestDB.ID) ,
                         "Remark" : textMsgAction ,
                         "Status":strStatus
            ] as [String:Any]
        
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: urlString, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoadingPK(view: self.view)
            let objectmsg = MessageCallServerModel()
            
            let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
            if respon["status"] as! String == "ok" {
                
                self.stopLoadingPK(view: self.view)
                MoveStruct.isMove = true
                MoveStruct.message = message
                
                self.navigationController?.popViewController(animated: true)
                
                
            }else{
                self.stopLoadingPK(view: self.view)
                self.view.makeToast(message)
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            print(err.description)
        }
        
        
    }
    @objc func getDetails(){
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var statusArray : [String] = []
    var statusArrayColor : [UIColor] = []
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! RequestHeaderTableViewCell
        if(section == 1){
            header.lblHeader.text = "Request Status"
        }else if(section == 2){
            header.lblHeader.text = "Room Detail"
        }else if(section == 3){
            header.lblHeader.text = "Bill Detail"
        }else{
            header.lblHeader.text = ""
        }
        return header
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(section == 0  || section == 4){
            
            return 0.0
        }else{
            return 35.0
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
            return 1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RoomRequestDetailsTableViewCell
            
            cell.lblName.text = RoomRequestDB.Name
            
            cell.lblNo.text = RoomRequestDB.Contact
            
            cell.textViewGuestName.text = RoomRequestDB.HouseName
            
            
            
            
            
            
            let milisecondDate = RoomRequestDB.ReqDate
            let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
            let dateFormatterday = DateFormatter()
            dateFormatterday.dateFormat = "dd"
            let valDate =  dateFormatterday.string(from: dateVarDate)
            cell.lblDateTop.text = valDate
            let dateFormattermon = DateFormatter()
            dateFormattermon.dateFormat = "MMM"
            let valDateMon =  dateFormattermon.string(from: dateVarDate)
            cell.lblMonthTop.text = valDateMon
            
            let dateFormatterWeekDay = DateFormatter()
            dateFormatterWeekDay.dateFormat = "EEEE"
            let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
            cell.lblDay.text = valDateWeekDay
            
            
      

            
            let milisecondCheckIn = RoomRequestDB.CheckInDate
            let dateVarCheckIn = Date.init(timeIntervalSince1970: TimeInterval(milisecondCheckIn)!/1000)
            let dateFormatterdayCheckIn = DateFormatter()
            dateFormatterdayCheckIn.dateFormat = "dd"
            let valDateCheckIn =  dateFormatterdayCheckIn.string(from: dateVarCheckIn)
            cell.lblDateFor.text = valDateCheckIn
            let dateFormattermonCheckIn = DateFormatter()
            dateFormattermonCheckIn.dateFormat = "MMM"
            let valDateMonCheckIn =  dateFormattermonCheckIn.string(from: dateVarCheckIn)
            cell.lblMonthFor.text = valDateMonCheckIn
            
            let dateFormatterWeekDayCheckIn = DateFormatter()
            dateFormatterWeekDayCheckIn.dateFormat = "EEEE"
            let valDateWeekDayCheckIn =  dateFormatterWeekDayCheckIn.string(from: dateVarCheckIn)
            cell.lblWeekDayFor.text = valDateWeekDayCheckIn
            let dateFormatterWeekDayTimeFor = DateFormatter()
            dateFormatterWeekDayTimeFor.dateFormat = "hh:mm a"
            let valDateWeekDayTimeFor =  dateFormatterWeekDayTimeFor.string(from: dateVarCheckIn)
            cell.lblTimeFor.text = valDateWeekDayTimeFor
           
          //  cell.lblTimeFor.text =  RoomRequestDB.ArrivalTime
            
            
            
            
            
            
            let milisecondCheckOut = RoomRequestDB.CheckOutDate
            let dateVarCheckOut = Date.init(timeIntervalSince1970: TimeInterval(milisecondCheckOut)!/1000)
            let dateFormatterdayCheckOut = DateFormatter()
            dateFormatterdayCheckOut.dateFormat = "dd"
            let valDateCheckOut =  dateFormatterdayCheckOut.string(from: dateVarCheckOut)
            cell.lblDateTo.text = valDateCheckOut
            let dateFormattermonCheckOut = DateFormatter()
            dateFormattermonCheckOut.dateFormat = "MMM"
            let valDateMonCheckOut =  dateFormattermonCheckOut.string(from: dateVarCheckOut)
            cell.lblMonthTo.text = valDateMonCheckOut
            
            let dateFormatterWeekDayCheckOut = DateFormatter()
            dateFormatterWeekDayCheckOut.dateFormat = "EEEE"
            let valDateWeekDayCheckOut =  dateFormatterWeekDayCheckOut.string(from: dateVarCheckOut)
            cell.lblWeekDayTo.text = valDateWeekDayCheckOut
            
            
            let dateFormatterWeekDayTimeTo = DateFormatter()
            dateFormatterWeekDayTimeTo.dateFormat = "hh:mm a"
            let valDateWeekDayTimeTo =  dateFormatterWeekDayTimeTo.string(from: dateVarCheckOut)
            cell.lblTimeTo.text = valDateWeekDayTimeTo
           
          cell.lblMemberCount.text = String(RoomRequestDB.listmember.count)
            
            
            
            
            
            return cell
        }else if(indexPath.section == 1 ){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellStatus", for: indexPath) as! CheckStatusFoodRoomTableViewCell
            var statusValue = String()
            var statusColor = UIColor()
            if RoomRequestDB.Cancelled ==  true {
                statusValue = "Cancelled"
                statusColor = UIColor.colorwithHexString("D22222", alpha: 1.0)
                statusArray.append(statusValue)
                statusArrayColor.append(statusColor)
            }else{
                if(RoomRequestDB.ApproveStatus == "1"){
                    statusValue = "Pending from Approver"
                    statusColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
                    statusArray.append(statusValue)
                    statusArrayColor.append(statusColor)
                }
                if(RoomRequestDB.ApproveStatus == "2"){
                    statusValue = "Accepted from Approver"
                    statusColor = UIColor.colorwithHexString("75BF44", alpha: 1.0)
                    statusArray.append(statusValue)
                    statusArrayColor.append(statusColor)
                }
                if(RoomRequestDB.ApproveStatus == "3"){
                    statusValue = "Rejected from Approver"
                    statusColor = UIColor.colorwithHexString("D22222", alpha: 1.0)
                    statusArray.append(statusValue)
                    statusArrayColor.append(statusColor)
                }
                if(RoomRequestDB.AdminStatus == "1"){
                    statusValue = "Pending from Admin"
                    statusColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
                    statusArray.append(statusValue)
                    statusArrayColor.append(statusColor)
                }
                if(RoomRequestDB.AdminStatus == "2"){
                    statusValue = "Accepted from Admin"
                    statusColor = UIColor.colorwithHexString("75BF44", alpha: 1.0)
                    statusArray.append(statusValue)
                    statusArrayColor.append(statusColor)
                }
                if(RoomRequestDB.AdminStatus == "3"){
                    statusValue = "Rejected from Admin"
                    statusColor = UIColor.colorwithHexString("D22222", alpha: 1.0)
                    statusArray.append(statusValue)
                    statusArrayColor.append(statusColor)
                }
            }
            
            
            let fullTimeArr = statusArray
            
            if(fullTimeArr.count > 0){
                var v1 = fullTimeArr.count / 2
                let v2 = fullTimeArr.count % 2
                if(v2 != 0){
                    v1 = v1 + 1
                }
                cell.heightCollectionCon.constant = CGFloat(v1 * 44) + 20
                cell.pageView(data: fullTimeArr ,color : statusArrayColor)
            }
            return cell
        } else if(indexPath.section == 2 ){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellFood", for: indexPath) as! RoomRequestDetailsFoodRequestTableViewCell
        
            if(RoomRequestDB.Lunch == false){
                cell.lbllunch.text = "No"
            }else{
                cell.lbllunch.text = "Yes"
            }
            
            if(RoomRequestDB.Dinner == false){
                cell.lblDinner.text = "No"
            }else{
                cell.lblDinner.text = "Yes"
            }
            
            if(RoomRequestDB.BreakFast == false){
                cell.lblBreakfast.text = "No"
            }else{
                cell.lblBreakfast.text = "Yes"
            }
            return cell
        }else  if(indexPath.section == 3 ){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellBill", for: indexPath) as! FoodRequestBillingTableViewCell
            if(RoomRequestDB.BillToCompany == true){
                cell.lblBilling.text = "Yes"
            }else{
                cell.lblBilling.text = "No"
            }
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellApprove", for: indexPath) as! RemarkGuestTableViewCell
            
        
            
            if(RoomRequestDB.ApproveRemark == ""){
                cell.textViewRemark.text = "----"
            }else{
                cell.textViewRemark.text = RoomRequestDB.ApproveRemark
            }
            if(RoomRequestDB.AdminRemark == ""){
                cell.textViewAdminRemark.text = "----"
            }else{
                cell.textViewAdminRemark.text = RoomRequestDB.AdminRemark
            }
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
