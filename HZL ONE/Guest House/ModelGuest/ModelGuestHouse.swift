//
//  ModelGuestHouse.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 22/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation

class GuestHouseModel: NSObject {
    
    
    
    var ID: String?
    var LocationName: String?
    var FullName: String?
    var ListUsers: String?
    var Name: String?
    var LocationID: String?
    var Address: String?
    var Contacts: String?
    var Lati: String?
    var Longi: String?
    var LocationApproverRequired: String?
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["LocationName"] is NSNull || cell["LocationName"] == nil {
            self.LocationName = ""
        }else{
            let app = cell["LocationName"]
            self.LocationName = (app?.description)!
        }
        
        if cell["FullName"] is NSNull || cell["FullName"] == nil {
            self.FullName = ""
        }else{
            let app = cell["FullName"]
            self.FullName = (app?.description)!
        }
        
        if cell["ListUsers"] is NSNull || cell["ListUsers"] == nil {
            self.ListUsers = ""
        }else{
            let app = cell["ListUsers"]
            self.ListUsers = (app?.description)!
        }
        
        if cell["Name"] is NSNull || cell["Name"] == nil {
            self.Name = ""
        }else{
            let app = cell["Name"]
            self.Name = (app?.description)!
        }
        
        if cell["LocationID"] is NSNull || cell["LocationID"] == nil {
            self.LocationID = ""
        }else{
            let app = cell["LocationID"]
            self.LocationID = (app?.description)!
        }
        
        if cell["Address"] is NSNull || cell["Address"] == nil {
            self.Address = ""
        }else{
            let app = cell["Address"]
            self.Address = (app?.description)!
        }
        
        
        if cell["Contacts"] is NSNull || cell["Contacts"] == nil {
            self.Contacts = ""
        }else{
            let app = cell["Contacts"]
            self.Contacts = (app?.description)!
        }
        if cell["Lati"] is NSNull || cell["Lati"] == nil {
            self.Lati = ""
        }else{
            let app = cell["Lati"]
            self.Lati = (app?.description)!
        }
        if cell["Longi"] is NSNull || cell["Longi"] == nil {
            self.Longi = ""
        }else{
            let app = cell["Longi"]
            self.Longi = (app?.description)!
        }
        if cell["LocationApproverRequired"] is NSNull || cell["LocationApproverRequired"] == nil {
            self.LocationApproverRequired = ""
        }else{
            let app = cell["LocationApproverRequired"]
            self.LocationApproverRequired = (app?.description)!
        }
        
        
        
    }
}


class GuestHouseDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:NewRoomViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetHouseList, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [GuestHouseModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = GuestHouseModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                       obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        
                        
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.refresh.endRefreshing()
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
}






class ApproverListModel: NSObject {
    
    
    
    var ID: String?
    var Employee_ID: String?
   
    var First_Name: String?
    var Middle_Name: String?
    var Last_Name: String?
     var Full_Name: String?

    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
        }
        
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil {
            self.Employee_ID = ""
        }else{
            let app = cell["Employee_ID"]
            self.Employee_ID = (app?.description)!
        }
        
      
        
        if cell["First_Name"] is NSNull || cell["First_Name"] == nil {
            self.First_Name = ""
        }else{
            let app = cell["First_Name"]
            self.First_Name = (app?.description)!
        }
        
        if cell["Middle_Name"] is NSNull || cell["Middle_Name"] == nil {
            self.Middle_Name = ""
        }else{
            let app = cell["Middle_Name"]
            self.Middle_Name = (app?.description)!
        }
        
        if cell["Last_Name"] is NSNull || cell["Last_Name"] == nil {
            self.Last_Name = ""
        }else{
            let app = cell["Last_Name"]
            self.Last_Name = (app?.description)!
        }
        
        if(self.Middle_Name != ""){
            self.Full_Name = self.First_Name! + " " + self.Middle_Name! + " " + self.Last_Name!
        }else{
           self.Full_Name = self.First_Name! + " " + self.Last_Name!
        }
        
    }
}


class ApproverListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:NewRoomViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetApproverList, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "ok")
                    {
                        
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ApproverListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ApproverListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        
                        
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.refresh.endRefreshing()
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
}
