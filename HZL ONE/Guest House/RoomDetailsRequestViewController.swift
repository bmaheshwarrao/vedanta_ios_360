//
//  RoomDetailsRequestViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 04/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class RoomDetailsRequestViewController: CommonVSClass ,UITableViewDataSource,UITableViewDelegate{
var requestModel = RequestFoodNewData()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(dataNameListArray.count == 0){
            selectedInt = -1
            let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
            let submitVC = storyBoard.instantiateViewController(withIdentifier: "SelectRoomswithDetailsViewController") as! SelectRoomswithDetailsViewController
            submitVC.rowData = 0
            self.navigationController?.pushViewController(submitVC, animated: true)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Room Request"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        tableView.reloadData()
    }
    @IBAction func btnrequestBookingClicked(_ sender: UIButton) {
        
        if(dataNameListArray.count > 0){
            
           
            
            

            
            var paramRoom : [[String:Any]] = []
            var paramName : [[String:Any]] = []
            var iiVal = -1
            for value in dataNameListArray {
                
                
                
                
               
                for valueName in value.nameArray {
                 
                    
                    
                    let paramNameValue = ["ReqDataID": String(iiVal),
                                 "Name": valueName.name
                    
                        ]
                        as [String:Any]
                    
                    
                    
                    
                    
                    
                    paramName.append(paramNameValue)
                }
                
                
                let paramRoomValue = ["ID": String(iiVal),
                                      "RoomType": value.GuestType
                    
                    ]
                    as [String:Any]
               
                paramRoom.append(paramRoomValue)
                
                
                
                iiVal = iiVal - 1;
                
            }
            
            
            self.startLoadingPK(view: self.view)
            
            
            let param = ["Aadhar": requestModel.Aadhar,
                         "ArrivalTime": requestModel.ArrivalTime,
                         "BillToCompany": requestModel.BillToCompany,
                         "BreakFast": requestModel.BreakFast,
                         "CheckInDate": requestModel.CheckInDate,
                         "CheckOutDate": requestModel.CheckOutDate,
                         "Contact":requestModel.Contact,
                         "Dinner": requestModel.Dinner,
                         "EmpID": requestModel.EmpID,
                         "FromDate": requestModel.FromDate,
                         "HouseID": requestModel.HouseID,
                         "ID": requestModel.ID,
                         "Lunch": requestModel.Lunch,
                         "Name": requestModel.Name,
                         "Notes": requestModel.Notes,
                         "Remark": requestModel.Remark,
                         "ReqDate": requestModel.ReqDate,
                         "ReqType": requestModel.ReqType,
                         "SelectedApprover": requestModel.SelectedApprover,
                         "ToDate": requestModel.ToDate
                
                
                
                
                
                
                ]
                as [String:Any]
            
         let jsonParam = JSON(param)
         
            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let parameter = ["UserID":empId,
                             "AuthKey":Profile_AuthKey ,
                             "Req":param ,
                             "listmember":paramName ,
                             "ListRoom":paramRoom
                ] as [String:Any]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.RoomRequest, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                
                let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "ok" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = message
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(message)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
            
            
            
        }
        
    }
    
    @IBAction func btnDeleteNameClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        dataNameListArray[(indexPath?.section)!].nameArray.remove(at: (indexPath?.row)!)
        tableView.reloadData()
        
        
    }
    @IBAction func btnDeleteSingleClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        dataNameListArray.remove(at: (indexPath?.section)!)
        tableView.reloadData()
    }
    
    @IBAction func btnDeleteDoubleClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        dataNameListArray.remove(at: sender.tag)
        tableView.reloadData()
        
    }
    @IBAction func btnAddDoubleClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "SelectRoomswithDetailsViewController") as! SelectRoomswithDetailsViewController
        selectedInt = -1
    
        self.navigationController?.pushViewController(submitVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! RoomDetailsHeaderTableViewCell
        header.lblRoomType.text = dataRoomType[section].Name
        header.lblGuestType.text = String(dataNameListArray[section].nameArray.count) + " Guest"
        
        header.setCollapsed(dataNameListArray[section].collapsed)
        header.delegate = self
        header.section = section
        return header
        
    }
  
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        return 50.0
        
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(section == dataNameListArray.count - 1){
        
         let footer = tableView.dequeueReusableCell(withIdentifier: "cellAddDelete") as! ButtonAddDeleteTableViewCell
            footer.btnDelete.tag = section
        return footer
        }else{
        
       let footer = tableView.dequeueReusableCell(withIdentifier: "cellDelete") as! buttonDeleteGUESTTableViewCell
            
       footer.btnDelete.tag = section
        return footer
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        
        return 58.0
        
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataNameListArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  dataNameListArray[section].nameArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellName", for: indexPath) as! RoomDetailsNameTableViewCell
            
            cell.lblName.text = String(indexPath.row + 1) + ") " + dataNameListArray[indexPath.section].nameArray[indexPath.row].name
        
            
            
            return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RoomDetailsRequestViewController: CollapsibleRoomDetailsHeaderTableViewCellDelegate {
    
    func toggleSection(_ header: RoomDetailsHeaderTableViewCell, section: Int) {
        let collapsed = !dataNameListArray[section].collapsed
        
        // Toggle collapse
        dataNameListArray[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
