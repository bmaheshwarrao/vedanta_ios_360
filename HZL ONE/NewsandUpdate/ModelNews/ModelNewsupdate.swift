//
//  ModelNewsupdate.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 01/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class NewsDataModel: NSObject {
    
    
    var ID = Int()
    var Title = String()
    var Description = String()
    var Category_ID = String()
    var Remarks = String()
    var Category = String()
    var File_Url = String()
    
    

    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Title"] is NSNull || str["Title"] == nil{
            
        }else{
            self.Title = (str["Title"] as? String)!
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            
        }else{
            self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Category_ID"] is NSNull || str["Category_ID"] == nil{
            self.Category_ID = "0"
        }else{
            let catId = (str["Category_ID"] as AnyObject)
            print(catId.stringValue)
            self.Category_ID = catId.stringValue
        }
        if str["Remarks"] is NSNull || str["Remarks"] == nil{
            self.Remarks = ""
        }else{
            self.Remarks = (str["Remarks"] as? String)!
        }
        
        if str["Category"] is NSNull || str["Category"] == nil{
            self.Category = ""
        }else{
            self.Category = (str["Category"] as? String)!
        }
        if str["File_Url"] is NSNull || str["File_Url"] == nil{
            self.File_Url = ""
        }else{
            let url = (str["File_Url"] as? String)!
           // print(url.stringValue)
            self.File_Url = url
            
            
            
            
        }
        
    }
    
}

class messagesDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:NewsandupdateViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Tips_News_And_Update, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [NewsDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = NewsDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class messagesDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:NewsandupdateViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Tips_News_And_Update, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.label.isHidden = true
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [NewsDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = NewsDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refreshControl.endRefreshing()
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
             obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

