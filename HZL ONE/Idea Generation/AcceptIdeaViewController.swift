//
//  AcceptIdeaViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Alamofire
class AcceptIdeaViewController: CommonVSClass,UIImagePickerControllerDelegate,UINavigationControllerDelegate ,UITextViewDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageData.isHidden = true
        self.crossImage.isHidden = true;
        self.imageHeight.constant = 0
        textViewAcept.clipsToBounds = true;
        textViewAcept.layer.cornerRadius = 10;
        textViewAcept.layer.borderWidth = 1.0
        textViewAcept.layer.borderColor = UIColor.black.cgColor
        textViewAcept.delegate = self;
        textViewAcept.text = PLACEHOLDER_TEXT
        // Do any additional setup after loading the view.
    }
    var Idea_Id = String()
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    var reachability = Reachability()!
    var imagedata: Data? = nil
    @IBAction func acceptBtnClicked(_ sender: UIButton) {
        
        if reachability.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if self.textViewAcept.text == "" || self.textViewAcept.text == PLACEHOLDER_TEXT{
            
            self.view.makeToast("Please Write ")
        }else{
            
            
            
            self.startLoadingPK(view: self.view)
            
            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            let parameter = [
                "Platform":"iOS" ,
                "Message":textViewAcept.text,
                "Idea_ID" : Idea_Id ,
                "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String:String]
            
            print(parameter)
            
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Accepct_Idea)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                
                                if(statusString == "success")
                                {
                                    
                                    
                                    
                                   
                                        self.stopLoadingPK(view: self.view)
                                        MoveStruct.isMove = true
                                        MoveStruct.message = msg
                                        self.navigationController?.popViewController(animated: false)
                                    
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }
        
        
    }
    @IBAction func btnCrosCameraClicked(_ sender: UIButton) {
        
        self.imageData.isHidden = true
        self.crossImage.isHidden = true;
        self.imageHeight.constant = 0
        self.imageData.image = nil
        self.lblCamera.isHidden = false
        self.btnCamera.isHidden = false
    }
    
    @IBAction func btnCameraClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var textViewAcept: UITextView!
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async {
                self.imageData.isHidden = false
                self.crossImage.isHidden = false;
                self.imageHeight.constant = 120
                self.lblCamera.isHidden = true
                self.btnCamera.isHidden = true
                
                self.imageData.image = image
                self.imagedata = UIImageJPEGRepresentation(image, 1.0)!
                
                switch self.imageData.image {
                case nil:
                    
                    self.imageData.isHidden = true
                    self.crossImage.isHidden = true;
                    self.imageHeight.constant = 0
                    
                    
                    
                    
                default:
                    
                    
                    self.imageData.isHidden = false
                    self.crossImage.isHidden = false;
                    self.imageHeight.constant = 120
                    break;
                }
            }
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var crossImage: UIButton!
    @IBOutlet weak var imageData: UIImageView!
    @IBOutlet weak var lblCamera: UILabel!
    @IBOutlet weak var btnCamera: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Accept Idea"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //  callMyReportedHazardData()
        
    }
    
    
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String)
    {
        // make it look (initially) like a placeholder
        aTextview.textColor = UIColor.lightGray
        aTextview.text = placeholderText
    }
    
    var PLACEHOLDER_TEXT = "Write here..."
    func applyNonPlaceholderStyle(aTextview: UITextView)
    {
        // make it look like normal text instead of a placeholder
        aTextview.textColor = UIColor.darkText
        aTextview.alpha = 1.0
    }
    func textViewShouldBeginEditing(aTextView: UITextView) -> Bool
    {
        if aTextView == textViewAcept && aTextView.text == PLACEHOLDER_TEXT
        {
            // move cursor to start
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    func moveCursorToStart(aTextView: UITextView)
    {
        DispatchQueue.main.async {
            aTextView.selectedRange = NSMakeRange(0, 0);
        }
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if newLength > 0 // have text, so don't show the placeholder
        {
            // check if the only text is the placeholder and remove it if needed
            // unless they've hit the delete button with the placeholder displayed
            if textView == textViewAcept && textView.text == PLACEHOLDER_TEXT
            {
                if text.utf16.count == 0 // they hit the back button
                {
                    return false // ignore it
                }
                applyNonPlaceholderStyle(aTextview: textView)
                textView.text = ""
            }
            return true
        }
        else  // no text, so show the placeholder
        {
            applyPlaceholderStyle(aTextview: textView, placeholderText: PLACEHOLDER_TEXT)
            moveCursorToStart(aTextView: textView)
            return false
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
