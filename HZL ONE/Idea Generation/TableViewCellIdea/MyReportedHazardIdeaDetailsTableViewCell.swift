//
//  MyReportedHazardIdeaDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyReportedHazardIdeaDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewInDetails: UIImageView!
    
    @IBOutlet weak var noAssignedLabel: UILabel!
    @IBOutlet weak var showAssignedBtn: UIButton!
    @IBOutlet weak var benefitTextView: UITextView!
    @IBOutlet weak var benefitLabelTextView: UITextView!
    //  @IBOutlet weak var deptLabel: UILabel!
    //  @IBOutlet weak var themeLabel: UILabel!
    // @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var ownerTextView: UITextView!
    
    @IBOutlet weak var showButtonHeight: NSLayoutConstraint!
   
    
    // @IBOutlet weak var myButtonConst: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblNoHeight: NSLayoutConstraint!
    @IBOutlet weak var assignedToLabel: UILabel!
    
    @IBOutlet weak var statusColorView: UIViewX!
    
    @IBOutlet weak var lblDateIdea: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var statusOfHazard: UIView!
    @IBOutlet weak var statusOfHazard1: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
