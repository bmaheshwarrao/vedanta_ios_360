//
//  DatahazardListModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability

class ReportedInboxStatus: NSObject {
    
    
    
    
    var Image_URL: String?
    
    var Benefit : String?
    var Theme_ID : String?
    var ThemeName : String?
    var Dept_ID : String?
    var Department_Name : String?
    var ID  = Int()
    
    var User_Type : String?
    var Status_DateTime : String?
    var Description : String?
    var Idea_Status : String?
    
    var Employee_ID : String?
    var Employee_Name : String?
    var Create_Date_Time : String?
    
    
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["Benefit"] is NSNull || cell["Benefit"] == nil{
            self.Benefit = ""
        }else{
          
            let emp1 = cell["Benefit"]
            
            self.Benefit = (emp1?.description)!
            
            
        }
        if cell["Status_DateTime"] is NSNull || cell["Status_DateTime"] == nil{
            self.Status_DateTime = ""
        }else{
           
            
            let emp1 = cell["Status_DateTime"]
            
            self.Status_DateTime = (emp1?.description)!
        }
        if cell["Create_Date_Time"] is NSNull || cell["Create_Date_Time"] == nil{
            self.Create_Date_Time = ""
        }else{
          
            let emp1 = cell["Create_Date_Time"]
            
            self.Create_Date_Time = (emp1?.description)!
        }
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil{
            self.Employee_Name = ""
        }else{
            let emp1 = cell["Employee_Name"]
            
            self.Employee_Name = (emp1?.description)!
        }
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil{
            self.Employee_ID = ""
        }else{
            let emp1 = cell["Employee_ID"]
            
            self.Employee_ID = (emp1?.description)!
            
        }
        if cell["Theme_ID"] is NSNull || cell["Theme_ID"] == nil{
            
        }else{
            let emp1 = cell["Theme_ID"]
            
            self.Theme_ID = (emp1?.description)!
        }
        if cell["Dept_ID"] is NSNull || cell["Dept_ID"] == nil{
            
        }else{
            let emp1 = cell["Dept_ID"]
            
            self.Dept_ID = (emp1?.description)!
        }
        
        if cell["ThemeName"] is NSNull || cell["ThemeName"] == nil{
            self.ThemeName = ""
        }else{
            let emp1 = cell["ThemeName"]
            
            self.ThemeName = (emp1?.description)!
        }
        if cell["Department_Name"] is NSNull || cell["Department_Name"] == nil{
            self.Department_Name = ""
        }else{
            let emp1 = cell["Department_Name"]
            
            self.Department_Name = (emp1?.description)!
        }
        
        
        if cell["User_Type"] is NSNull || cell["User_Type"] == nil{
            
        }else{
            let emp1 = cell["User_Type"]
            
            self.User_Type = (emp1?.description)!
        }
        
        if cell["Description"] is NSNull || cell["Description"] == nil{
            self.Description = ""
        }else{
            let emp1 = cell["Description"]
            
            self.Description = (emp1?.description)!
        }
        if cell["ID"] is NSNull || cell["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        
        if cell["Idea_Status"] is NSNull || cell["Idea_Status"] == nil{
            
        }else{
            let emp1 = cell["Idea_Status"]
            
            self.Idea_Status = (emp1?.description)!
        }
        
        
        if cell["Image_URL"] is NSNull || cell["Image_URL"] == nil{
            self.Image_URL = ""
        }else{
            let emp1 = cell["Image_URL"]
            
            self.Image_URL = (emp1?.description)!
        }
        
        
        
        
        
        
        
    }
}


class ReportedInboxForStatus
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:InboxReportedViewController,parameter:[String:String],status:String,statusData:String  , url : String , success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewReported)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableViewReported.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        obj.stopLoading(view: obj.tableViewReported)
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ReportedInboxStatus] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ReportedInboxStatus()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewReported)
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableViewReported.isHidden = true
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewReported)
                        //  obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                obj.label.isHidden = true
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                //  obj.stopLoading()
            })
            
            
        }
        else{
            obj.label.isHidden = false
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewReported.isHidden = true
            obj.refresh.endRefreshing()
            // obj.stopLoading()
        }
        
        
    }
    
    
}


class ReportedInboxForStatusLoadMore
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:InboxReportedViewController,parameter:[String:String] , url : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        //obj.tableView.isHidden = false
                        //obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ReportedInboxStatus] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ReportedInboxStatus()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No "+status+" hazard found" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


